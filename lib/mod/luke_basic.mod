
# These recognize all the basic shapes.  

# It also does geometric primitive manipulations with the ModifyMode, etc.

rec.core.recognizers.LooseLineRecognizer
rec.core.recognizers.LooseCircleRecognizer

rec.core.recognizers.SelectionLoopRecognizer

# this does fast move recognition.
#rec.core.recognizers.MoveRecognizer

rec.core.recognizers.ModifyModeRecognizer false
rec.core.recognizers.ClickRecognizer
rec.core.recognizers.SelectionRecognizer

rec.core.recognizers.ClickConsumer

