# recognize polygons and all that

# Putting the quadrilateral recognizer before the triangle recognizer
#      allows for undoing triangles and then making them quadrilaterals.


#rec.core.recognizers.HexagonRecognizer
#rec.core.recognizers.PentagonRecognizer

Color polygon gray

rec.core.recognizers.QuadFromPolyRecognizer
#rec.core.recognizers.QuadrilateralRecognizer
#rec.core.recognizers.TriangleRecognizer

#rec.core.recognizers.EquilateralTriangleRecognizer
rec.core.recognizers.RectangleRecognizer
rec.core.recognizers.SquareRecognizer

# this is now included in basic.mod so that we can switch between 
#   basic.mod and toolkitrecognizers.mod 
#rec.core.recognizers.PolygonRecognizer

# reasoners
rec.core.reasoners.PolygonReasoner
rec.core.reasoners.SquareReasoner
rec.core.reasoners.CircleReasoner
