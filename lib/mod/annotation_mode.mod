# Thses are the modules and recognizers that are active when we are in
# annotation mode.

MajorEditMode Annotation

Grammar rec.annotate.recognizers.EnterAnnotationMode
rec.annotate.recognizers.LeaveAnnotationModeRecognizer

##########################
# Selections/Deletion
##########################
rec.core.recognizers.ClickRecognizer
rec.core.recognizers.ButtonClickRecognizer
rec.core.recognizers.SelectionRecognizer
rec.core.recognizers.SelectionLoopRecognizer

#reasoners
rec.core.reasoners.ButtonClickReasoner

Module selection_mode

##########################
# Basic types of stuff
##########################
Module commonvoice

rec.core.recognizers.LineRecognizer
rec.core.recognizers.SquiggleArrowRecognizer
rec.core.reasoners.SquiggleReasoner

##########################
# Annotations
##########################

## The top level grammar and recognizers
Grammar rec.annotate.recognizers.TopLevel
Grammar rec.annotate.recognizers.Explain
rec.annotate.recognizers.SentenceRecognizer
rec.annotate.recognizers.QuestionRecognizer

rec.annotate.recognizers.MovementGestureRecognizer
rec.annotate.recognizers.PointingRecognizer

rec.annotate.reasoners.AnnotationReasoner
rec.annotate.reasoners.QuestionReasoner

# Load up the rules:
Module annotation_rules.mod
