
rec.house.DragModifyModeRecognizer false

# so doors are drawn correctly.
rec.house.RegionShuffler

rec.house.DoorRecognizer

# make sure all wall stuff is recognized before any region
#Part:rec.house.Wall
#Part:rec.house.Region

# keeps the wall and region data structures up-to-date
rec.house.DataFixer

# connect broken wall chunkies!
rec.house.WallConnector

# moves walls when regions move
rec.house.WallMover

# if 2 walls are same, remove the new one
# if they overlap then cut out the middle bit.
rec.house.WallMerger

# if endpoint of some line lies on new line, cut new line
# also cut old if new cuts old.
rec.house.WallTBreaker

# if 2 lines cross, break one of them
rec.house.WallBreaker

rec.house.RegionCutter

rec.house.RegionRecognizer
rec.house.FullRegionRecognizer

rec.house.RegionReducer

# orient lines horiz and vertically
#rec.house.GraphPaper

rec.house.AutoNamer

# turns all lines into walls automatically
# rec.house.WallRecognizer

# allow for clicks in the interior of the region to select the region
rec.core.recognizers.SelectPolyRecognizer

rec.core.recognizers.PolygonStrokeRecognizer

