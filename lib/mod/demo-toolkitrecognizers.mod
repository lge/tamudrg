# These recognize all the basic shapes.  

rec.core.recognizers.ToolkitLineRecognizer

# polgon sub-segments, break into individ. lines
# rec.core.recognizers.ToolkitFacitedLineBreakerRecognizer
rec.core.recognizers.ToolkitPolyLineRecognizer
rec.core.recognizers.ToolkitPolygonRecognizer
rec.core.recognizers.ToolkitComplexRecognizer

#rec.core.recognizers.LooseCircleRecognizer
rec.core.recognizers.ToolkitCircleRecognizer
#rec.core.recognizers.ToolkitArcRecognizer
#rec.core.recognizers.ToolkitSpiralRecognizer

# we don't know what to do with ovals at the moment so lets not recognize them...
# rec.core.recognizers.ArcRecognizer
# rec.core.recognizers.OvalRecognizer

# to get the small circles (for pin joint recog.)
#rec.core.recognizers.VeryLooseCircleRecognizer

# the reasoner
rec.core.reasoners.SquiggleReasoner
rec.core.reasoners.TemporalReasoner
