# to label edges 0 or 1.
rec.fsm.WritingRecognizer

# describe edge if it sees edge in click
# rec.fsm.EdgeDescriber

# Finite State Machine Recognizers

rec.fsm.AcceptRecognizer
rec.fsm.EdgeSelfRecognizer

rec.fsm.MoveEdgeRecognizer
rec.fsm.MoveNodeRecognizer 

# rec.fsm.CircleArranger

# only rec nodes if there is a nearby line which will be turned into an edge
rec.fsm.SmartNodeRecognizer

rec.fsm.SmartEdgeRecognizer
rec.fsm.FSMSelectionLoopRecognizer
# rec.core.SelectionLoopRecognizer

rec.fsm.NodeNamer
rec.fsm.EdgeNamer

rec.fsm.EdgeAligner
rec.fsm.NodeAligner

rec.fsm.EdgeBreaker

rec.fsm.NodeRemover
rec.fsm.EdgeRemover

#for group selection stuff
rec.house.DragModifyModeRecognizer


rec.fsm.DirectedEdgeRecombiner
rec.fsm.EdgeDupReducer

# this seperates overlapping edges
rec.fsm.EdgeDupRecognizer

# if two arrowheads appear on an edge, this fixes it.
rec.fsm.DoubleArrowFixer

# rec.fsm.Eraser








