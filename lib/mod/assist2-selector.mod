
# this does fast move recognition (dragging of objects)
# rec.core.recognizers.MoveRecognizer

#rec.core.recognizers.ModifyModeRecognizer
# allows for dynamic drag
#rec.house.DragModifyModeRecognizer false
rec.core.recognizers.ButtonClickRecognizer
rec.core.recognizers.ClickRecognizer

rec.core.recognizers.SelectionRecognizer

#rec.core.recognizers.ClickConsumer

rec.core.recognizers.SelectionLoopRecognizer

#reasoners
rec.core.reasoners.ButtonClickReasoner
rec.core.reasoners.SelectionReasoner
#rec.core.reasoners.MoveReasoner