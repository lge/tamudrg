
# These recognize all the basic shapes.  

# It also does geometric primitive manipulations with the ModifyMode, etc.

rec.core.recognizers.LooseLineRecognizer
#rec.core.PolygonStrokeRecognizer
rec.core.recognizers.LooseCircleRecognizer

rec.core.recognizers.PolygonRecognizer

# polgon sub-segments, break into individ. lines
Recognizer rec.core.recognizers.FacitedLineBreaker

# rec.core.recognizers.ArcRecognizer
rec.core.recognizers.OvalRecognizer

# to get the small circles (for pin joint recog.)
#rec.core.recognizers.VeryLooseCircleRecognizer

# the reasoner
rec.core.reasoners.SquiggleReasoner
rec.core.reasoners.TemporalReasoner
