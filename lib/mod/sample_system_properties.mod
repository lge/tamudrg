# A list of system/machine dependent properties

SystemProperty Width "800"
SystemProperty Height "600"

SystemProperty stroke-width "3"

SystemProperty WM-window-title "Working Model 2D"
SystemProperty WM-exe "C:\Program Files\Working Model 2D\PROGRAM\WM.exe"

# on fast machines make this number smaller to slow down the animation
# this value works well on about at 500mHz machine.
# SystemProperty WM-animation-step "0.05"

# SystemProperty speech "on"
