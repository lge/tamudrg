# This is the top level module for annotations.  It has recognizers to
# start and leave annotation mode as well as recognizers that are
# active during standard editing modes.

#Module annotation_selection.mod
Module annotation_mode.mod

Grammar rec.annotate.recognizers.EnterAnnotationMode
rec.annotate.recognizers.EnterAnnotationModeRecognizer
