# These recognizers are only active when the system is in "Selection"
# mode (i.e. while there is something selected).

# active in "Selection" mode
EditMode Annotation
EditMode Selection

Grammar rec.annotate.recognizers.PartNamer

rec.annotate.recognizers.NameUtteranceRecognizer
rec.annotate.recognizers.GetNameUtteranceRecognizer
