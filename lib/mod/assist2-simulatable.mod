# recognize stuff that can be simulated

# Colors
Color sim black
Color sim_body blue
Color sim_constraint purple
Color sim_rod black

# Recognizers
rec.simulate.recognizers.SimSpringRecognizer
rec.simulate.recognizers.SimCircleRecognizer
rec.simulate.recognizers.SimPolyFromPolyRecognizer
rec.simulate.recognizers.SimForceRecognizer
rec.simulate.recognizers.SimForce2Recognizer
rec.core.recognizers.ArrowRecognizer
rec.simulate.recognizers.SimAnchorRecognizer
rec.simulate.recognizers.SimMotorRecognizer
rec.simulate.recognizers.SimGearRecognizer
rec.simulate.recognizers.SimSpringDamperRecognizer
rec.simulate.recognizers.PinJointRecognizer
rec.simulate.recognizers.DamperRecognizer
rec.simulate.recognizers.SimPulleyRecognizer
rec.simulate.recognizers.SimRodRecognizer
rec.simulate.recognizers.SimRopeRecognizer
rec.simulate.recognizers.SimRotationalDamperRecognizer
rec.simulate.recognizers.SimRotationalSpringRecognizer
rec.simulate.recognizers.SimTorqueRecognizer

# the reasoners
rec.simulate.reasoners.SimulateReasoner
rec.simulate.reasoners.RodReasoner
rec.simulate.reasoners.PinJointReasoner
rec.simulate.reasoners.SpringReasoner
rec.simulate.reasoners.SimPulleyReasoner
rec.simulate.reasoners.SimMotorReasoner
rec.simulate.reasoners.SimGearReasoner
rec.simulate.reasoners.SimGearTrainReasoner
rec.simulate.reasoners.SimSpringDamperReasoner
rec.simulate.reasoners.SimQuadReasoner
rec.simulate.reasoners.SimTorqueReasoner
rec.simulate.reasoners.SimRotationalDamperReasoner

# Speech grammars and recognizers and reasoners

Grammar rec.simulate.recognizers.Run
rec.simulate.recognizers.RunUtteranceRecognizer

