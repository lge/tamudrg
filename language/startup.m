%STARTUP  Startup file
%   The file 
%   is executed when MATLAB starts up, if it exists 
%   anywhere on the path.  

%   Copyright 1984-2000 The MathWorks, Inc. 
%   $Revision: 1.1 $  $Date: 2006/11/22 22:55:28 $

% This path should state where the matlab directory is in the drg directory
% This file should be put in the matlab work directory
addpath(genpath('C:\usr\eclipse\drg\language\matlab'))