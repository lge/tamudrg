(define abstract-shape Text
  (description
    "An abstract shape for English text.
  )
  (required
    (Point upper_left)
    (Point upper_right)
    (Point base_left)
    (Point base_right)
    (Point lower_left)
    (Point lower_right)
  )
  (aliases
    (Line border_base (new Line (base_left base_right)))
    (Line border_top (new Line (top_left top_right)))
    (Line border_left (new Line (base_left top_left)))
    (Line border_right (new Line (base_right top_right)))
    (Line border_lower (new Line (lower_left lower_right))) 
    (double rotation border_base.angle)
  )
)

(define abstract-shape UpperCaseLetter
  (is-a
    Text
  )
  (aliases
    (Point lower_left base_left)
    (Point lower_right base_right)
  )
)

(define abstract-shape LowerCaseLetter
  (is-a
    Text
  )
)

(define shape UpperCaseA
  (description 
    "The upper case letter A."
  )
  (is-a
    UpperCaseLetter
  )
  (components 
    (Line left)
    (Line right)
    (Line bottom)
  )
  (constraints
    (meet left.p2 right.p1)
    (meet left.midpoint bottom.p1)
    (meet right.midpoint bottom.p2)
    (angle left right 45)
  )
  (aliases
    (Point base_left left.pl)
    (Point base_right right.p2)
    (Line base left.p1 right.p2)
    (Point upper_left (new Point (left.p2 base.angle (/ (- base.length) 2))))
    (Point upper_right (new Point (left.p2 base.angle (/ base.length 2))))
  )
)

(define shape UpperCaseB
  (description
    "The upper case letter B."
  )
  (is-a
    UpperCaseLetter
  )
  (components
    (Line left)
    (Arc top)
    (Arc bottom)
  )
  (constraints
    (equal top.degrees 270)
    (equal bottom.degrees 270)
    (meet left.p1 bottom.p1)
    (meet left.midpoint bottom.p2)
    (meet left.midpoint top.p1)
    (meet left.p2 top.p2)
    (equal top.radius bottom.radius)
    (parallel midpoints left)
    (ccangle bottom_right left 90)
  )
  (aliases
    (Point upper_left left.p2)
    (Point lower_left left.p1)
    (Line midpoints top.center bottom.center)
    (Line bottom_half left.p1 left.midpoint)
    (Line bottom_right bottom_half.midpoint bottom.center)
    (Point upper_right (new Point (upper_left bottom_right.angle (* top.radius 2))))
    (Point lower_right (new Point (lower_left bottom_right.angle (* top.radius 2))))
  )
)

(define shape UpperCaseC
  (description
    "The upper case letter C."
  )
  (is-a
    UpperCaseLetter
  )
  (components
    (Arc a)
  )
  (constraints
    (equal a.degrees 270)
    (ccangle up left 90)
  )
  (aliases
    (Line up a.p2 a.p1)
    (Line left up.midpoint a.center)
    (Point upper_middle (new Point (a.center up.angle a.radius)))
    (Point upper_left (new Point (upper_middle left.angle a.radius)))
    (Point lower_left (new Point (upper_left (- up.angle) (* 2 a.radius))))
    (Point upper_right (new Point (up.midpoint up.angle a.radius)))
    (Point lower_right (new Point (up.midpoint (- up.angle) a.radius)))
  )
)

(define shape UpperCaseD
  (description
    "The upper case letter D."
  )
  (is-a
    UpperCaseLetter
  )
  (Components
    (Arc a)
    (Line left)
  )
  (constraints
    (equal a.degrees 270)
    (meet a.p1 left.p1)
    (meet a.p2 left.p1)
    (ccangle go_right left 90)
  )
  (aliases
    (Line go_right (new Line (left.midpoint a.center)))
    (Point upper_left left.p2) 
    (Point lower_left left.p1) 
    (Point upper_middle (new Point (a.center left.angle a.radius)))
    (Point upper_right (new Point (upper_middle go_right.angle a.radius)))
    (Point lower_right (new Point (upper_right (- left.angle) (* 2 a.radius)))) 
  )
)

(define shape UpperCaseE
  (description
    "The upper case letter E."
  )
  (is-a
    UpperCaseLetter
  )
  (components
    (Line left)
    (Line top)
    (Line middle)
    (Line bottom)
  )
  (constraints
    (meet left.p1 bottom.p1)
    (meet left.midpoint middle.p1)
    (meet left.p2 top.p1)
    (parallel top bottom)
    (parallel bottom middle)
    (parallel top middle)
    (perpendicular top left)
    (perpendicular middle left)
    (perpendicular bottom left)
    (meet rightside.midpoint middle.p2)
    (not (intersect rightside left))
    (equal top.length middle.length)
    (equal middle.length bottom.length)
    (equal left.length (* middle 2))
    (ccangle bottom left 90)
  )
  (aliases
    (Line rightside (new Line (top.p2 bottom.p2)))
    (Point upper_left top.p1)
    (Point upper_right top.p2)
    (Point lower_left bottom.p1)
    (Point lower_right bottom.p2)
  )
)

(define shape UpperCaseF
  (description
    "The upper case letter F."
  )
  (is-a
    UpperCaseLetter
  )
  (components
    (Line left)
    (Line top)
    (Line middle)
  )
  (constraints
    (equal top.length middle.length)
    (equal (* 2 top.length) left.length)
    (meet left.p1 top.p1)
    (meet left.midpoint middle.p1)
    (not (intersect right left))
    (ccangle top left 90)
    (perpendicular middle left)
    (parallel middle top)
  )
  (aliases
    (Line right (new Line (top.p2 middle.p2)))
    (Point upper_left top.p1)
    (Point upper_right top.p2)
    (Point lower_left left.p2)
    (Point lower_right (new Point (left.p2 top.angle top.length)))
  )
)


(define shape UpperCaseG
  (description
    "The upper case letter G."
  )
  (is-a
    UpperCaseLetter
  )
  (components
    (UpperCaseC c)
    (Line l)
  )
  (constraints
    (meet c.a.p2 l.midpoint)
    (perpendicular c.up l)
    (equal l.length c.a.radius)
  )
  (aliases
    (Point upper_left c.upper_left)
    (Point upper_right c.upper_right)
    (Point lower_left c.lower_left)
    (Point lower_right c.lower_right)
  )
)

(define shape UpperCaseH
  (description
    "The upper case letter H."
  )
  (is-a
    UpperCaseLetter
  )
  (components
    (Line left)
    (Line right)
    (Line middle)
  )
  (constraints
    (parallel left right)
    (equal left.length right.length)
    (equal (* 2 middle.length) right.length)
    (perpendicular left middle)
    (meet middle.p1 left.midpoint)
    (meet middle.p2 right.midpoint)
    (ccangle middle left 90)
    (ccangle middle right 90)
  )
  (aliases
    (Point upper_left left.p2)
    (Point upper_right right.p2)
    (Point lower_left left.p1)
    (Point lower_right right.p1)
  )
)

(define shape UpperCaseI
  (description
    "The upper case letter I."
  )
  (is-a
    UpperCaseLetter
  )
)

(define shape UpperCaseI3
  (description
    "The upper case letter I with 3 lines."
  )
  (is-a
    UpperCaseI
  )
  (components
    (Line middle)
    (Line top)
    (Line bottom)
  )
  (constraints
    (meet middle.p1 top.midpoint)
    (meet middle.p2 bottom.midpoint)
    (perpendicular middle top)
    (parallel top bottom)
    (not (intersect left middle))
    (not (intersect right middle))
    (parallel left middle)
    (parallel left right)
  )
  (aliases
    (Line left (new Line (top.p1 bottom.p1)))
    (Line right (new Line (top.p2 bottom.p2)))
    (Point upper_left top.p1)
    (Point upper_right top.p2)
    (Point lower_left bottom.p1)
    (Point lower_right bottom.p2)
  )
)

(define shape UpperCaseI1
  (description
    "The upper case letter I with 1 line."
  )
  (is-a
    UpperCaseI
  )
  (components
    (Line middle)
  )
  (aliases
    (Point upper_left middle.p1)
    (Point upper_right middle.p1)
    (Point lower_left middle.p2)
    (Point lower_right middle.p2)
  )
)


(define shape UpperCaseJPlain
  (description
    "The upper case letter J, invertible with no top."
  )
  (components
    (Arc a)
    (Line middle)
  )
  (constraints
    (equal a.degrees 180)
    (equal (* 3 a.radius) middle.length)
    (meet a.p2 middle.p1)
    (perpendicular middle cross)
    (> long.length middle.length)
  )
  (aliases
    (Line cross (new Line (a.p1 a.p2)))
    (Line long (new Line (a.midpoint middle.p2)))
    (Point middle_bottom (new Point (j.middle.p1 (- middle.angle) j.a.radius))
    (Line middle_long (new Line (j.middle.p2 middle_bottom))
  )
)

(define shape UpperCaseJPlainWithTop
  (description
    "The upper case letter J, invertible with top."
  )
  (components
    (UpperCaseJPlain j)
    (Line top)
  )
  (constraints
    (meet j.middle.p2 top.midpoint)
    (perpendicular j.middle top)
    (equal top.length (* 2 j.arc.radius))
  )
  (aliases
    (Line middle j.middle)
    (Arc a j.a)
    (Line cross j.cross)
    (Line middle_long j.middle_long)
  )
)


(define shape UpperCaseJInverseWithoutTop
  (description
    "The upper case letter J inverted without a top."
  )
  (is-a 
    UpperCaseJInverse
  )
  (components
    (UpperCaseJPlain j)
  )
  (constraints
    (ccangle j.cross j.middle 90)
  )
  (aliases
    (Point upper_left j.middle_long.p1)
    (Point lower_left j.middle_long.p2)
    (Point upper_right (new Point (j.middle_long.p1 (- j.cross.angle) (* 2 j.a.radius))))
    (Point lower_right (new Point (j.middle_long.p2 (- j.cross.angle) (* 2 j.a.radius))))
  )
  (aliases
    (Line middle j.middle)
    (Arc a j.a)
    (Line cross j.cross)
    (Line middle_long j.middle_long)
  )
)

(define shape UpperCaseJForwardWithoutTop
  (description
    "The upper case letter J without a top."
  )
  (is-a
    UpperCaseJ
  )
  (components
    (UpperCaseJPlain j)
  )
  (constraints
    (ccangle j.middle j.cross 90)
  )
  (aliases
    (Point upper_right j.middle.p2)
    (Point lower_right (new Point (j.middle.p1 (- middle.angle) j.a.radius))
    (Point upper_left (new Point (j.middle_long.p1 (- j.cross.angle) (* 2 j.a.radius))))
    (Point lower_left (new Point (j.middle_long.p2 (- j.cross.angle) (* 2 j.a.radius))))
    (Line middle j.middle)
    (Arc a j.a)
    (Line cross j.cross)
    (Line middle_long j.middle_long)
  )
)

(define shape UpperCaseJInvertedWithTop
  (description
    "The upper case letter J inverted with a top line."
  )
  (is-a
    UpperCaseJInverse
  )
  (components
    (UpperCaseJPlainWithTop jt)
  )
  (constraints
    (ccangle jt.j.cross jt.j.middle 90)
  )
  (aliases
    (Line middle j.middle)
    (Arc a j.a)
    (Line cross j.cross)
    (Line middle_long j.middle_long)
    (Point upper_right (new Point (middle_long.p1 (- cross.angle) (* 2 a.radius))))
    (Point lower_right (new Point (middle_long.p2 (- cross.angle) (* 2 a.radius))))
    (Point upper_left jt.top.p1)
    (Point lower_left (new Point (jt.top.p1 middle_long.angle middle_long.length)))
  )
)

(define shape UpperCaseJForwardWithTop
  (description
    "The upper case letter J with a top line."
  )
  (is-a 
    UpperCaseJ
  )
  (components
    (UpperCaseJPlainWithTop j)
  )
  (constraints
    (ccangle jt.j.middle jt.j.cross 90)
  )
  (aliases
    (Line middle j.middle)
    (Arc a j.a)
    (Line cross j.cross)
    (Line middle_long j.middle_long)
    (Point upper_left (new Point (middle_long.p1 (- cross.angle) (* 2 a.radius))))
    (Point lower_left (new Point (middle_long.p2 (- cross.angle) (* 2 a.radius))))
    (Point upper_right jt.top.p1)
    (Point lower_right (new Point (jt.top.p1 middle_long.angle middle_long.length)))
  )
)

(define shape UpperCaseJ
  (description
    "The upper case letter J."
  )
  (is-a
    UpperCaseLetter
  )
)

(define shape UpperCaseJInverse
  (description
    "The upper case letter J inverted."
  )
  (is-a
    UpperCaseLetter
  )
  (required
    (Point upper_left)
    (Point upper_right)
    (Point lower_left)
    (Point lower_right)
    (Line middle_long)
    (Arc a)
    (Line cross)
  )
)


(define shape UpperCaseK
  (description
    "The upper case letter K."
  )
  (is-a
    UpperCaseLetter
  )
  (components
    (Line left)
    (Line up)
    (Line down)
  )
  (constraints
    (perpendicular up down)
    (meet up.p1 left.midpoint)
    (meet down.p1 left.midpoint)
    (angle up left 45)
    (angle down left 45)
    (ccangle top left 90)
  )
  (aliases
    (Point upper_left left.p1)
    (Point lower_left left.p2)
    (Point upper_right up.p2)
    (Point lower_right down.p2)
    (Point top (new Point (left.p1 up.p2)))
  )
)

(define shape UpperCaseL
  (description
    "The upper case letter L."
  )
  (is-a
    UpperCaseLetter
  )
  (components
    (Line left)
    (Line bottom)
  )
  (constraints
    (perpendicular left bottom)
    (meet left.p1 bottom.p1)
    (< left.length bottom.length)
    (ccangle bottom left 90)
  )
  (aliases
    (Point upper_left left.p2)
    (Point upper_right (new Point (left.p2 bottom.angle bottom.length)))
    (Point lower_left left.p1)
    (Point lower_right bottom.p2)
  )
)

(define shape UpperCaseM
  (description
    "The upper case letter M."
  )
  (is-a
    UpperCaseLetter
  )
  (components
    (Line left)
    (Line right)
    (Line down)
    (Line up)
  )
  (constraints
    (parallel left right)
    (equal left.length right.length)
    (equal down.length up.length)
    (equal (* 2 down.length) left.length)
    (meet left.p1 down.p1)
    (meet down.p1 up.p2)
    (meet up.p1 right.p1)
    (angle left down 45)
    (angle right up 45)
    (perpendicular down up)
    (ccangle left top 90)
  )
  (aliases
    (Point upper_left left.p1)
    (Point upper_right right.p1)
    (Point lower_left left.p2)
    (Point lower_right right.p2)
    (Line bottom (new Line left.p2 right.p2))
    (Line top (new Line left.p1 right.p1))
  )
)

(define shape UpperCaseN
  (description
    "The upper case letter N."
  )
  (is-a
    UpperCaseLetter
  )
  (components
    (Line left)
    (Line middle)
    (Line right)
  )
  (constraints
    (parallel left right)
    (meet left.p2 middle.p1)
    (meet middle.p2 right.p2)
    (angle left middle 45)
    (angle middle right 45)
    (ccangle bottom left 90)
    (parallel top bottom)
    (perpendicular top left)
  )
  (aliases
    (Line top (new Line (left.p2 right.p1)))
    (Line bottom (new Line (left.p1 right.p2)))
    (Point upper_left top.p1)
    (Point upper_right top.p2)
    (Point lower_left bottom.p1)
    (Point lower_right bottom.p2)
  )
)

(define shape UpperCaseO
  (description
    "The upper case letter O."
  )
  (is-a
    UpperCaseLetter
  )
  (components
    (Circle c)
  )
  (constraints
  )
  (aliases
    (Point upper_left undefined)
    (Point upper_right undefined)
    (Point lower_left undefined)
    (Point lower_right undefined)
  )
)

(define shape UpperCaseP
  (description
    "The upper case letter P."
  )
  (is-a
    UpperCaseLetter
  )
  (components
    (Arc a)
    (Line l)
  )
  (constraints
    (equal a.degrees 270)
    (meet a.p1 l.p2)
    (meet a.p2 l.midpoint)
    (ccangle middlearc l 90)
  )
  (aliases
    (Line top (new Line (l.midpoint l.p2)))
    (Line middlearc (new Line (top.midpoint a.center)))
    (Point upper_left l.p2)
    (Point lower_left l.p1)
    (Point upper_right (new Point (l.p2 middlearc.angle (+ middlearc.length arc.radius))))
    (Point lower_right (new Point (l.p1 middlearc.angle (+ middlearc.length arc.radius))))
  )
)

(define shape UpperCaseQ
  (description
    "The upper case letter Q."
  )
  (is-a
    UpperCaseLetter
  )
  (components
    (Circle c)
    (Line l)
  )
  (constraints 
    (intersect l.midpoint c)
    (equal l.length c.radius)
    (collinear l.p1 l.p2 c.center)
    (not (contains c l.p2))
  )
  (aliases
    (Point lower_right l.p2)
    (Point lower_left undefined)
    (Point upper_right undefined)
    (Point upper_left undefined)
  )
)

(define shape UpperCaseR
  (description
    "The upper case letter R."
  )
  (is-a
    UpperCaseLetter
  )
  (components
    (Line left)
    (Line diag)
    (Arc a)
  )
  (constraints

  )
)

(define shape UpperCaseS
  (description
    "The upper case letter S."
  )
  (is-a
    UpperCaseLetter
  )
  (components
    (Arc top)
    (Arc bottom)
  )
)

(define shape UpperCaseT
  (description
    "The upper case letter T."
  )
  (is-a
    UpperCaseLetter
  )
  (components
    (Line top)
    (Line middle)
  )
)

(define shape UpperCaseU
  (description
    "The upper case letter U."
  )
  (is-a
    UpperCaseLetter
  )
  (components
    (Line left)
    (Line right)
    (Arc a)
  )
)

(define shape UpperCaseV
  (description
    "The upper case letter V."
  )
  (is-a
    UpperCaseLetter
  )
  (components
    (Line left)
    (Line right)
  )
  (constraints
    
  )
  (aliases
    
  )
)

(define shape UpperCaseW
  (description
    "The upper case letter W."
  )
  (is-a
    UpperCaseLetter
  )
)

(define shape UpperCaseWStraight
  (description
    "The upper case letter W."
  )
  (is-a
    UpperCaseLetter
  )
  (components
    (UpperCaseM m)
  )
  (constraints
  )
  (aliases
    (Point upper_left m.lower_left)
    (Point lower_left m.upper_left)
    (Point upper_right m.lower_right)
    (Pont lower_right m.upper_right)
  )
)

(define shape UpperCaseWCurve
  (description
    "The upper case letter W."
  )
  (is-a
    UpperCaseW
  )
  (components
    (UpperCaseJInverse left)
    (UpperCaseJ right)
  )
  (constraints
    (collinear left.cross.p1 left.cross.p2 right.cross.p1)
    (collinear left.cross.p1 left.cross.p2 right.cross.p2)
    (meet left.a.p2 right.a.p2)
    (parallel left.middle_long right.middle_long)
    (equal left.middle_long.length right.middle_long.length)
    (perpendicular top left.middle_long)
  )
  (aliases
    (Line top (new Line (left.middle_long.p1 right.middle_long.p1)))
  )
)

(define shape UpperCaseX
  (description
    "The upper case letter X."
  )
  (is-a
    UpperCaseLetter
  )
  (components 
    (Line lr)
    (Line rl)
  )
  (constraints
    (meet lr.midpoint rl.midpoint)
    (perpendicular lr rl)
    (equal lr.length rl.length)
  )
  (aliases
    (Point upper_left lr.p1)
    (Point upper_right rl.p1)
    (Point lower_left rl.p2)
    (Point lower_right lr.p2)
  )
)

(define shape UpperCaseY
  (description
    "The upper case letter Y."
  )
  (is-a
    UpperCaseLetter
  )
  (components
    (Line left)
    (Line right)
    (Line middle)
  )
  (constraints
    (meet left.p2 middle.p1)
    (meet right.p2 middle.p1)
    (equal left.length middle.length)
    (equal right.length left.length)
    (not (intersect top middle))
    (perpendicular left right)
    (perpendicular top middle)
    (ccangle middle right 135) 
  )
  (aliases
    (Line top (new Line (left.p1 right.p1)))
    (Point upper_left top.p1)
    (Point upper_right top.p2)
    (Point lower_left (new Point (middle.p2 (- top.angle) (/ top.length 2))))
    (Point lower_right (new Point (middle.p2 (top.angle) (/ top.length 2))))  
  )
)

(define shape UpperCaseZ
  (description
    "The upper case letter Z."
  )
  (is-a
    UpperCaseLetter
  )
  (components
    (Line top)
    (Line middle)
    (Line bottom)
  )
  (constraints
    (angle top middle 45)
    (angle bottom middle 45)
    (parallel top bottom)
    (equal top.length bottom.length)
    (meet top.p2 middle.p1)
    (meet middle.p2 bottom.p1)
    (parallel left right)
    (equal left.length right.length)
    (meet cross.midpoint middle.midpoint)
    (equal cross.length middle.length)
    (ccangle bottom left 90)
    (perpendicular top left)
  )
  (aliases
    (Line left (new Line (top.p1 bottom.p1)))
    (Line right (new Line (top.p2 bottom.p2)))
    (Line cross (new Line (top.p1 bottom.p2)))    
    (Point upper_left top.p1)
    (Point upper_right top.p2)
    (Point lower_left bottom.p1)
    (Point lower_right bottom.p2)
  )
)