function error = intersect(line1, line2)

if isLineIntersecting(line1, line2);
    error = 0;
else
    error = getLineDistance(line1, line2);
end