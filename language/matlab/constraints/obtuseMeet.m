function error = obtuseMeet(line1, line2)

distance11 = getLineLength([line1(1,:); line2(1,:)]); 
distance12 = getLineLength([line1(1,:); line2(2,:)]);
distance21 = getLineLength([line1(2,:); line2(1,:)]);
distance22 = getLineLength([line1(2,:); line2(2,:)]);

distance = getLineEndpointDistance(line1, line2); 
%distanceLong = getLongEndpointDistance(line1, line2);
maxlen = getLineLength(line1) + getLineLength(line2);
minlen = sqrt(getLineLength(line1)^2 + getLineLength(line2)^2);

if distance == distance11
    otherdist = distance22;
elseif distance == distance12
    otherdist = distance21;
elseif distance == distance21
    otherdist = distance12;
else
    otherdist = distance11;
end

if otherdist < minlen * 1.1
    error = minlen*1.1 - otherdist;
elseif otherdist > maxlen * .9
    error = otherdist - maxlen*.9;
else
    error = 0;
end

error = error + distance;