function error = slanted(line1, line2)

angle1 = getLineAngle(line1);
angle2 = getLineAngle(line2);

dif = angle1 - angle2;
mpi = mod(dif, pi/2);

if mpi > .38 && mpi < 1.18
    error = 0;
else
    error = mod(dif, .78);
end

error = error + notConnected(line1, line2);
