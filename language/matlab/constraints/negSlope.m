function error = negSlope(line1)

angle = getLineAngle(line1);

if angle > -1.3 && angle < -.26 
    error = 0;
else  
   error = abs(angle - .76)*10;
end