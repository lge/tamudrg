function error = posSlope(line1)

angle = getLineAngle(line1);

if angle > .26 && angle < 1.3
    error = 0;
else
    l1x1 = line1(1,1);
    l1y1 = line1(1,2);
    l1x2 = line1(2,1);
    l1y2 = line1(2,2);   
    error = abs((l1x2 - l1x1) - (l1y2 - l1y1));
end