function error = overlapLeftOf(line1, line2)

point1 = getLineMidpoint(line1);
point2 = getLineMidpoint(line2);

leftCenter = point1(1);
rightCenter = point2(1);
leftMax = getLineMaxX(line1);
rightMin = getLineMinX(line2);

if leftMax < rightMin
    error = rightMin - leftMax;
elseif rightCenter < leftCenter
    error = leftCenter - rightCenter;
else
    error = 0;
end