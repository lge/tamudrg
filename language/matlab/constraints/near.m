function error = near(line1, line2)

distance = getLineDistance(line1, line2);
len1 = getLineLength(line1);
len2 = getLineLength(line2);
mindistance = max(10, min(len1/4,len2/4));
maxdistance = max(40, min(len1, len2));

if distance < mindistance
    error = mindistance - distance;
elseif distance > maxdistance;
    error = distance - maxdistance;
else
    error = 0;
end
