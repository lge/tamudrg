function error = perpendicular(line1, line2)
%m1 = -1/m2
%dy1/dx1 = -1/(dy2/dx2)
%dy1/dx1 = -dx2/dy2
%dy1*dy2 = -dx2*dx1;
[dx1, dy1] = getDirectionVector(line1);
[dx2, dy2] = getDirectionVector(line2);
error = (dx2*dx1 + dy2*dy1)^2;