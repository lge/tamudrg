function error = notConnected(line1, line2)

alloweddistance = min(getLineLength(line1) * .15, getLineLength(line2) * .15);
error = 0;
distance = getLineLength([line1(1,:); line2(1,:)]); 
if distance < alloweddistance 
    error = error + alloweddistance - distance;
end
distance = getLineLength([line1(1,:); line2(2,:)]);
if distance < alloweddistance 
    error = error + alloweddistance - distance;
end
distance = getLineLength([line1(2,:); line2(1,:)]);
if distance < alloweddistance 
    error = error + alloweddistance - distance;
end
distance = getLineLength([line1(2,:); line2(2,:)]);
if distance < alloweddistance 
    error = error + alloweddistance - distance;
end