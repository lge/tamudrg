function error = acuteMeet(line1, line2)

distance11 = getLineLength([line1(1,:); line2(1,:)]); 
distance12 = getLineLength([line1(1,:); line2(2,:)]);
distance21 = getLineLength([line1(2,:); line2(1,:)]);
distance22 = getLineLength([line1(2,:); line2(2,:)]);

distance = getLineEndpointDistance(line1, line2); 
%distanceLong = getLongEndpointDistance(line1, line2);
maxlen = max(getLineLength(line1),getLineLength(line2));

if distance == distance11
    otherdist = distance22;
elseif distance == distance12
    otherdist = distance21;
elseif distance == distance21
    otherdist = distance12;
else
    otherdist = distance11;
end

if otherdist < maxlen * .1
    error = 20 - otherdist;
elseif otherdist > maxlen + 1;
    error = otherdist - maxlen - 1;
else
    error = 0;
end

error = error + distance;