function error = above(line1, line2)

l1y1 = line1(1,2);
l1y2 = line1(2,2);
l2y1 = line2(1,2);
l2y2 = line2(2,2);

error = 0;

if l2y1 > l1y1
    error = error + l2y1 - l1y1;
end
if l2y1 > l1y2
    error = error + l2y1 - l1y2;
end
if l2y2 > l1y1
    error = error + l2y2 - l1y1;
end
if l2y2 > l1y2
    error = error + l2y2 - l1y2;
end
