function error = far(line1, line2)

distance = getLineDistance(line1, line2);
mindistance = max(40, min(getLineLength(line1), getLineLength(line2)));

if distance > mindistance
    error = 0;
else 
    error = mindistance - distance;
end