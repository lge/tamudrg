function error = larger(line1, line2)

len1 = getLineLength(line1);
len2 = getLineLength(line2);

if len1 >  2 * len2
    error = 0;
else
    error = 2*getLineLength(line2) - getLineLength(line1);
end