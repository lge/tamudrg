function error = leftOf(line1, line2)

line1Max = getLineMaxX(line1); 
line2Min = getLineMinX(line2); 

if line1Max < line2Min
    error = 0;
else
    error = line1Max - line2Min;
end