function error = overlapAbove(line1, line2)

point1 = getLineMidpoint(line1);
point2 = getLineMidpoint(line2); 

topCenter = point1(2);
bottomCenter =point2(2);
topMax = getLineMaxY(line1);
bottomMin = getLineMinY(line2);

if topMax < bottomMin
    error = bottomMin - topMax;
elseif topCenter < bottomCenter
    error = bottomCenter - topCenter;
else 
    error = 0;
end