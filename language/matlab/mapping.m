%mapping
global m_horizontal m_vertical m_posSlope m_negSlope m_near m_far 
global m_intersects m_bisects_cc m_bisects_c1 m_bisects_c2 m_bisects_1c m_bisects_2c 
global m_connected_11 m_connected_12 m_connected_21 m_connected_22 
global m_bisects_Lc m_bisects_cL m_meets_L1 m_meets_L2 m_meets_1L m_meets_2L
global m_sameX m_leftOf m_rightOf  m_overlapLeftOf m_overlapRightOf
global m_sameY m_above m_below m_overlapAbove m_overlapBelow
global m_parallel m_perpendicular m_slanted m_acuteMeet m_obtuseMeet
global m_equalArea m_larger m_smaller m_equalAngle m_mapping

m_horizontal = 1;
m_vertical =2;
m_posSlope =3;
m_negSlope =4;

m_intersects =7;
m_bisects_cc =8;
m_bisects_c1 =9;
m_bisects_c2 =10;
m_bisects_1c =11;
m_bisects_2c =12;
m_connected_11 =13;
m_connected_12 =14;
m_connected_21 =15;
m_connected_22 =16;
m_bisects_Lc =17;
m_bisects_cL =18;
m_meets_L1 =19;
m_meets_L2 =20;
m_meets_1L =21;
m_meets_2L = 22;
m_near =5;
m_far =6;

m_parallel =33;
m_perpendicular =34;
m_slanted =35;
m_acuteMeet =36;
m_obtuseMeet =37;

m_sameX =23;
m_leftOf =24;
m_rightOf =25;
m_overlapLeftOf =26;
m_overlapRightOf = 27;
m_sameY =28;
m_above =29;
m_below =30;
m_overlapAbove =31;
m_overlapBelow =32;


m_equalArea =38;
m_larger =39;
m_smaller =40;
m_equalAngle =41;

m_mapping{m_horizontal}='horizontal';
m_mapping{m_vertical}='vertical';          
m_mapping{m_posSlope}='posSlope';
m_mapping{m_negSlope}='negSlope';
m_mapping{m_near}='near';
m_mapping{m_far}='far';
m_mapping{m_intersects}='intersects';
m_mapping{m_bisects_cc}='bisects_cc';
m_mapping{m_bisects_c1}='bisects_c1';
m_mapping{m_bisects_c2}='bisects_c2';
m_mapping{m_bisects_1c}='bisects_1c';
m_mapping{m_bisects_2c}='bisects_2c';
m_mapping{m_connected_11}='connected_11';
m_mapping{m_connected_12}='connected_12';
m_mapping{m_connected_21}='connected_21';
m_mapping{m_connected_22}='connected_22';
m_mapping{m_bisects_Lc}='bisects_Lc';
m_mapping{m_bisects_cL}='bisects_cL';
m_mapping{m_meets_L1}='meets_L1';
m_mapping{m_meets_L2}='meets_L2';
m_mapping{m_meets_1L}='meets_1L';
m_mapping{m_meets_2L}='meets_2L';
m_mapping{m_sameX}='sameX';
m_mapping{m_leftOf}='leftOf';
m_mapping{m_rightOf}='rightOf';
m_mapping{m_overlapLeftOf}='overlapLeftOf';
m_mapping{m_overlapRightOf}='overlapRightOf';
m_mapping{m_sameY}='sameY';
m_mapping{m_above}='above';
m_mapping{m_below}='below';
m_mapping{m_overlapAbove}='overlapAbove';
m_mapping{m_overlapBelow}='overlapBelow';
m_mapping{m_parallel}='parallel';
m_mapping{m_perpendicular}='perpendicular';
m_mapping{m_slanted}='slanted';
m_mapping{m_acuteMeet}='acuteMeet';
m_mapping{m_obtuseMeet}='obtuseMeet';
m_mapping{m_equalArea}='equalArea';
m_mapping{m_larger}='larger';
m_mapping{m_smaller}='smaller';
m_mapping{m_equalAngle}='equalAngle';
