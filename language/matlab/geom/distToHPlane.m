function theDistance = distToHPlane(hplanePts,z)
%function theDistance = distToHplanePts(hplanePts,z,L,U)
%this function assumes the hyperplane is given by hplanePts
%you want to find the distance from point z to this hyperplane

%form hplane equation a'x=k
a = computeNormal(hplanePts);

k = a'*hplanePts(1,:)'; %use any point on hplane to get rhs k
                                
if norm(a) == 0
    theDistance = Inf;
else    
    theDistance = abs((dot(a,z)-k)/norm(a));
end
