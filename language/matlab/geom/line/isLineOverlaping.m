function bool = isLineOverlaping(line1, line2)

if isInfLineOverlaping(line1, line2) == 0
    bool = 0;
elseif isLineBoundingBoxOverlaping(line1, line2)
    bool = 1;
else
    bool = 0;
end