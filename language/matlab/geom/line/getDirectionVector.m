function [x,y] = getDirectionVector(line)

x =(line(2,1) - line(1,1))/getLineLength(line);
y =(line(2,2) - line(1,2))/getLineLength(line);