function x = getLineMaxX(line)

x = max(line(:,1));