function error = orientation(angle)
%error(1) = horizontal error
%error(2) = posSlope error
%error(3) = vertical error
%error(4) = negSlope error
while angle > pi/2
  angle = angle - pi;
end
while angle < - pi/2
  angle = angle + pi;
end
error(1) = abs(angle) * 4;
error(2) = min(abs(angle - pi/4), abs(angle + 3*pi/4));
error(3) = min(abs(angle - pi/2), abs(angle + pi/2)) * 4;
error(4) = min(abs(angle + pi/4), abs(angle - 3*pi/4));


maxerror = max(error);
error = error/maxerror;