function distance = getLineEndpointDistance(line1, line2)


distance = getLineLength([line1(1,:); line2(1,:)]); 
distance = max(distance, getLineLength([line1(1,:); line2(2,:)]));
distance = max(distance, getLineLength([line1(2,:); line2(1,:)]));
distance = max(distance, getLineLength([line1(2,:); line2(2,:)]));