function error = relativeLength(len1, len2)
%error(1) = shorter error
%error(2) = equallength error
%error(3) = longer error


threshold = min(len1, len2);

error(1) = max(0, len1 - len2 + threshold);
error(2) = abs(len1 - len2);
error(3) = max(0, len2 - len1 + threshold);







maxerror = max(error);
error = error/maxerror;