function y = getLineMinY(line)

y = min(line(:,2));