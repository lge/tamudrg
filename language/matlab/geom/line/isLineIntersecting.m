function bool = isLineIntersecting(line1, line2)

if isLineOverlaping(line1, line2)
    bool = 1;
else
    point = getInfLineIntersectingPoint(line1, line2);
    if isPointOnLine(point, line1) && isPointOnLine(point, line2)
        bool = 1;
    else
        bool = 0;
    end
end 