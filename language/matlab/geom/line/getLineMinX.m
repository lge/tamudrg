function x = getLineMinX(line)

x = min(line(:,1));