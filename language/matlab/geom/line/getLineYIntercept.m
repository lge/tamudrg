function b = getLineYIntercept(line)
% returns the yIntercept b
% y = mx + b
% b = y - mx
% if line is vertical, returns NaN

x1 = line(1,1);
y1 = line(1,2);

if isLineVertical(line)
    b = NaN;
else
    b = y1 - getLineSlope(line) * x1;
end





