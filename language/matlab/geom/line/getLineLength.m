function l = getLineLength(line)

x1 = line(1,1);
y1 = line(1,2);
x2 = line(2,1);
y2 = line(2,2);

l = norm([x2-x1, y2-y1]);
