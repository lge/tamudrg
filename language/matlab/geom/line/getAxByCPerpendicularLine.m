function array = getAxByCPerpendicularLine(line, point)

line = ABCLineToSegment(line);

if isLineHorizontal(line)
    % makeVertical
    y_val = 0;
    x_val = 1;
    c_val = point(1);
elseif isLineVertical(line)
    % makeHorizontal
    y_val = 1;
    x_val = 0;
    c_val = point(2);
else
    x_val = 1;
    y_val = getLineSlope(line);
    c_val = y_val * point(2) + point(1);
end

array = [x_val, y_val, c_val];
