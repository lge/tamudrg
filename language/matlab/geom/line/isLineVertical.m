function bool = isLineVertical(line)
%computes if the line is a vertical line.
%returns 1 if vertical
%returns 0 if not vertical

x1 = line(1,1);
x2 = line(2,1);

if x1 == x2 
    bool = 1;
else
    bool = 0;
end


    