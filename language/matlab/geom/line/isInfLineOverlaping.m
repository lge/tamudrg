function bool = isInfLineOverlaping(line1, line2)

point = getInfLineIntersectingPoint(line1, line2);
if isnan(point(1,1)) %&& isnan(point(1,2))
    bool = 1;
else
    bool = 0;
end
