function m = getLineSlope(line)
%computes the slope of a line.

x1 = line(1,1);
y1 = line(1,2);
x2 = line(2,1);
y2 = line(2,2);

m = (y2-y1)/(x2-x1);