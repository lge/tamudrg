function y = getLineMaxY(line)

y = max(line(:,2));