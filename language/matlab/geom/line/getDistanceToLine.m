function theDistance = getDistanceToLine(point, line)

if isPointOnLine(point, line)
    theDistance = 0;
else
  if  distToHPlane(line, point) < .001
    dist(1) = getLineLength([line(1,:); point]);
    dist(2) = getLineLength([line(2,:); point]);
    theDistance = min(dist);
  else
    if length(line) == 3
      array1 = line;
    else
      array1 = getLineAxByC(line);
    end
    perpline = getPerpendicularLine(line, point);
    array2 = getLineAxByC(perpline);
    
    A = [array1(1), array1(2); array2(1), array2(2)];
    b = [array1(3); array2(3)];
    
    intersectsPoint = linsolve(A, b)';
    if isPointOnLine(intersectsPoint, line)
       theDistance = getLineLength([intersectsPoint; point]);
    else 
        dist(1) = getLineLength([line(1,:); point]);
        dist(2) = getLineLength([line(2,:); point]);
        theDistance = min(dist);
    end
  end
end