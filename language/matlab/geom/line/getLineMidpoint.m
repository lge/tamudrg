function  point = getLineMidpoint(line)

x = (line(1,1) + line(2,1))/2;
y = (line(1,2) + line(2,2))/2;
point = [x,y];