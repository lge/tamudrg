function error = topology(line1, len1, mid1, line2, len2, mid2)
%error(1) = far error
%error(2) = near error
%error(3) = intersects error
%error(4) = bisects_cc error
%error(5) = bisects_c1 error
%error(6) = bisects_c2 error
%error(7) = bisects_1c error
%error(8) = bisects_2c error
%error(9) = connected_11 error
%error(10) = connected_12 error
%error(11) = connected_21 error
%error(12) = connected_22 error
%error(13) = bisects_Lc error
%error(14) = bisects_cL error
%error(15) = bisects_L1 error
%error(16) = bisects_L2 error
%error(17) = bisects_1L error
%error(18) = bisects_2L error

l1p1 = line1(1,:);
l1p2 = line1(2,:);
l2p1 = line2(1,:);
l2p2 = line2(2,:);

distance = getLineDistance(line1, line2);
fardistance = max([40, len1, len2]);
farnearcuttoff = min(len1, len2);
neardistance = min(20, min(len1/4, len2/4));
coincidentdistance = min(len1, len2)/8;
ipoint = getInfLineIntersectingPoint(line1, line2);


line114 = getLineMidpoint([mid1; l1p1]);
line134 = getLineMidpoint([mid1; l1p2]);
line214 = getLineMidpoint([mid2; l2p1]);
line234 = getLineMidpoint([mid2; l2p2]);

error(1) = max(0, farnearcuttoff - distance);
error(2) = abs(farnearcuttoff/4 - distance);
error(3) = min([getLineLength([line114; line214]), getLineLength([line114; line234]), getLineLength([line134; line214]), getLineLength([line134; line234])]); 
error(4) = getLineLength([mid1; mid2]) + getLineLength([mid1; ipoint]) + getLineLength([mid2;ipoint]) * 10000;
error(5) = getLineLength([mid1; l2p1]) + getLineLength([mid1; ipoint]) + getLineLength([l2p1;ipoint]);
error(6) = getLineLength([mid1; l2p2]) + getLineLength([mid1; ipoint]) + getLineLength([l2p2;ipoint]);
error(7) = getLineLength([mid2; l1p1]) + getLineLength([mid2; ipoint]) + getLineLength([l1p1;ipoint]);
error(8) = getLineLength([mid2; l1p2]) + getLineLength([mid2; ipoint]) + getLineLength([l1p2;ipoint]);
error(9) = getLineLength([line1(1,:); l2p1]) + getLineLength([l1p1; ipoint]) + getLineLength([l2p1;ipoint]);
error(10) = getLineLength([line1(1,:); l2p2])+ getLineLength([l1p1; ipoint]) + getLineLength([l2p2;ipoint]);
error(11) = getLineLength([line1(2,:); l2p1]) + getLineLength([l1p2; ipoint]) + getLineLength([l2p1;ipoint]);
error(12) = getLineLength([line1(2,:); l2p2]) + getLineLength([l1p2; ipoint]) + getLineLength([l2p2;ipoint]);
error(13) = max([getDistanceToLine(mid2, line1), coincidentdistance]) + getLineLength([mid2; ipoint]);
error(14) = max([getDistanceToLine(mid1, line2), coincidentdistance]) + getLineLength([mid1; ipoint]); 
error(15) = max([getDistanceToLine(l2p1, line1), coincidentdistance]) + getLineLength([l2p1; ipoint]); 
error(16) = max([getDistanceToLine(l2p2, line1), coincidentdistance]) + getLineLength([l2p2; ipoint]); 
error(17) = max([getDistanceToLine(l1p1, line2), coincidentdistance]) + getLineLength([l1p1; ipoint]); 
error(18) = max([getDistanceToLine(l1p2, line2), coincidentdistance]) + getLineLength([l1p2; ipoint]); 


maxerror = max(error);
error = error/maxerror;