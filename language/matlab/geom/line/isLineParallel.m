function bool = isLineParallel(line1, line2)

if isLineVertical(line1) && isLineVertical(line2)
    bool = 1;
elseif isLineVertical(line1) || isLineVertical(line2)
    bool = 0;
elseif getLineSlope(line1) == getLineSlope(line2)
    bool = 1;
else
    bool = 0;
end
