function bool = isLinePerpendicular(line1, line2)

if isLineVertical(line1) && isLineHorizontal(line2)
   bool = 1;
elseif isLineHorizontal(line1) && isLineVertical(line2)
    bool = 1;
elseif ifLineVertical(line1) || isLineVertical(line2) || isLineHorizontal(line1) || isLineHorizontal(line2)
    bool = 0;
elseif getLineSlope(line1) == 1/getLineSlope(line2)
    bool = 1;
else
    bool = 0;
end