function theDistance = getLineDistance(line1, line2)

if isLineIntersecting(line1, line2)
    theDistance = 0;
else
    dist(1) = getDistanceToLine(line2(1,:), line1);
    dist(2) = getDistanceToLine(line2(2,:), line1);
    dist(3) = getDistanceToLine(line1(1,:), line2);
    dist(4) = getDistanceToLine(line1(2,:), line2);
    theDistance = min(dist);
end

    
    
    