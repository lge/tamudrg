function error = relativeLocation(dim, line1, center1, line2, center2)
%if dim is 1, we are checking relative x value
%if dim is 2, we are checking relative y value
%error(1) = leftOf error or below error
%error(2) = overlapLeftOf error or overlapBelow error
%error(3) = sameX or sameY error
%error(4) = overlapRightOf or overlapAbove error
%error(5) = rightOf or above error

max1 = max(line1(1,dim), line1(2,dim));
max2 = max(line2(1,dim), line2(2,dim));
min1 = min(line1(1,dim), line1(2,dim));
min2 = min(line2(1,dim), line2(2,dim));
minval = min(min1, min2);
maxval = max(max1, max2);
totalspan = maxval - minval;
error(1) = max(0, max1 - min2 + 2);
error(2) = abs(center2(dim) - max1) + abs(center1(dim) - min2);
error(3) = abs(center1(dim) - center2(dim)) * 4;
error(4) = abs(center1(dim) - max2) + abs(center2(dim) - min1);
error(5) = max(0, max2 - min1 + 2);

%is intersection point more perceptually important?




maxerror = max(error);
error = error/maxerror;

