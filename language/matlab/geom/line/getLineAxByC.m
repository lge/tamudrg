function array = getLineAxByC(line)
% puts the formula into the form [a b c], where ax + by = c
% y = mx + b => - mx + y = b ... 

x1 = line(1,1);
y1 = line(1,2);
x2 = line(2,1);
y2 = line(2,2);

if abs(x2-x1) < .001
    y_val = 0;
    x_val = 1;
    c_val = line(1,1);
elseif abs(y2-y1) < .001
    y_val = 1;
    x_val = 0;
    c_val = line(1,2);
else
    y_val = 1;
    x_val = -getLineSlope(line);
    c_val = getLineYIntercept(line);
end

array = [x_val, y_val, c_val];

