function point = getInfLineIntersectingPoint(line1, line2)

if length(line1) == 3
  array1 = line1;
else
  array1 = getLineAxByC(line1);
end
if length(line2) == 3
  array2 = line2;
else
  array2 = getLineAxByC(line2);
end

A = [array1(1), array1(2); array2(1), array2(2)];
b = [array1(3); array2(3)];

[X, R] = linsolve(A, b);
point = X';
