function bool = isLineBoundingBoxOverlaping(line1, line2)

if getLineMinX(line1) > getLineMaxX(line2)
    bool = 0;
elseif getLineMaxX(line1) < getLineMinX(line2)
    bool = 0;
elseif getLineMinY(line1) > getLineMaxY(line2)
    bool = 0;
elseif getLineMaxY(line1) < getLineMinY(line2)
    bool = 0;
else
    bool = 1;
end