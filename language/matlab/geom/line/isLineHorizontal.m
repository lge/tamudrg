function bool = isLineHorizontal(line)

y1 = line(1,2);
y2 = line(2,2);

if y1 == y2
    bool = 1;
else
    bool = 0;
end