function dif = getLineDifAngle(line1, line2)

angle1 = getLineAngle(line1);
angle2 = getLineAngle(line2);

dif = angle1 - angle2;