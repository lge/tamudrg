function bool = isPointOnLine(point, line)

x = point(1);
y = point(2);

x1 = line(1,1);
y1 = line(1,2);
x2 = line(2,1);
y2 = line(2,2);

if x > x1 && x > x2
    bool = 0;
elseif x < x1 && x < x2
    bool = 0;
elseif y > y1 && y > y2
    bool = 0;
elseif y < y1 && y < y2
    bool = 0;
elseif distToHPlane(line, point) < .001
    bool = 1;
else  
    bool = 0;
end

    