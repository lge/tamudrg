function perpline = getPerpendicularLine(line, point)

if length(line) == 3
  line = ABCLineToSegment(line);
end
angle = getLineAngle(line);
len = getLineLength(line);
newangle = angle + pi/2;

p1 = point;
p2 = [p1(1) + cos(newangle) * len, p1(2)  + sin(newangle) * len];
perpline = [p1;p2];
