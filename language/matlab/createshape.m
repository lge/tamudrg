function [x, fval, exitflag, output] = ...
  createshape (x0, strConstraints, attempts, upperb)
%x and x0 are a vector of double values representing each of the points
%of the lines (e.g., for a single lined shape this x = [x1 y1 x2 y2]. 
%Multiple lines are simply repeated.
%x0 contains the initial value

%constraints = translateConstraints(strConstraints);
if isnumeric(strConstraints(1,1))
  constraints = strConstraints;
else
 constraints = translateConstraints(strConstraints);
end

options = optimset('LargeScale','off','Display','off', 'MaxFunEvals', ...
  500, 'MaxIter', 500); %, 'Display', 'Iter');

for i = 1 : length(x0)
    lb(i) = 0;  
    ub(i) = upperb;
end

lineMatrix = makeLineMatrix(x0);

minfval = 10000;
for i=1:attempts
   if i==1
       xstart=x0;
   else
       %upperb should be 500 - else created invalid first shape
       xstart=upperb*rand(1,length(x0));
   end
   [x,fval, exitflag, output] = ...
     fminsearch(@(x)objectiveFunction(x,lineMatrix,constraints, 0), ...
     xstart,options);
   if exitflag > 0 && fval < .01       
       break;
   end
   if fval < minfval
       minfval = fval;
       minx = x;
       minexit = exitflag;
       minoutput = output;
   end
   [x,fval, exitflag, output, lambda, grad, hessian ] = ...
     fmincon(@(x)objectiveFunction(x,lineMatrix,constraints, 0), ...
     xstart, [], [], [], [], lb, ub, ...
     @(x)nonlinearinequalities(x,lineMatrix, constraints),options);
   if exitflag > 0 && fval < .01
       break      
   end
   if fval < minfval
       minfval = fval;
       minx = x;
       minexit = exitflag;
       minoutput = output;
   end 
   fval = minfval;
   x = minx;
   exitflag = minexit;
   output = minoutput;
end
%plotLine(x);