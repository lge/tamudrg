function error = relativeAngle(len1, ang1, len2, ang2, topologyerror)
%error(1) = parallel error
%error(2) = perpendicular error
%error(3) = slanted error
%error(4) = acuteMeet error
%error(5) = obtuseMeet error

%error(9) = connected_11 error
%error(10) = connected_12 error
%error(11) = connected_21 error
%error(12) = connected_22 error
connect11error = topologyerror(9);
connect12error = topologyerror(10);
connect21error = topologyerror(11);
connect22error = topologyerror(12);


[connecterror, connection] = min([connect11error, connect12error, connect21error, connect22error]);

if connection == 1
  angledif = ang2-ang1;
elseif connection == 2
  angledif = ang2-(2*pi) - ang1;
elseif connection == 3
  angledif = ang2 - (ang1 - 2*pi);
else
  angledif = ang2 - ang1;
end

coincidentdistance = min(len1, len2)/8;

angerror = orientation(angledif);
%[angerror,choice] = min(differror);



error(1) = angerror(1) + coincidentdistance;
error(2) = angerror(3) + coincidentdistance;
error(3) = min([angerror(2), angerror(4)]) + coincidentdistance;
error(4) = angerror(2) + connecterror;
error(5) = angerror(4) + connecterror;

maxerror = max(error);
error = error/maxerror;


%NEED TO GET INTERSECTION POINT - THEN FIND DISTANCE FROM ENDPOINT. 
