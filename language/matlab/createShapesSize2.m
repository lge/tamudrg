function createShapesSize2()
addpath(genpath('.'))
mapping;
global m_horizontal m_vertical m_posSlope m_negSlope m_near m_far 
global m_intersects m_bisects_cc m_bisects_c1 m_bisects_c2 m_bisects_1c m_bisects_2c 
global m_connected_11 m_connected_12 m_connected_21 m_connected_22 
global m_bisects_Lc m_bisects_cL m_meets_L1 m_meets_L2 m_meets_1L m_meets_2L
global m_sameX m_leftOf m_rightOf  m_overlapLeftOf m_overlapRightOf
global m_sameY m_above m_below m_overlapAbove m_overlapBelow
global m_parallel m_perpendicular m_slanted m_acuteMeet m_obtuseMeet
global m_equalArea m_larger m_smaller m_equalAngle m_mapping

m_orientation = [m_horizontal m_vertical m_posSlope m_negSlope];
m_topology = [m_far, m_near, m_intersects, m_bisects_cc, m_bisects_c1, m_bisects_c2, m_bisects_1c, m_bisects_2c, m_connected_11, m_connected_12, m_connected_21, m_connected_22, m_bisects_Lc, m_bisects_cL, m_meets_L1, m_meets_L2, m_meets_1L, m_meets_2L];
m_xlocation = [m_sameX, m_leftOf, m_rightOf,  m_overlapLeftOf, m_overlapRightOf];
m_ylocation = [m_sameY, m_above, m_below, m_overlapAbove, m_overlapBelow];
m_angle = [m_parallel, m_perpendicular, m_slanted, m_acuteMeet, m_obtuseMeet];
m_size = [m_equalArea, m_larger, m_smaller];

ffound = fopen('line2-found.txt', 'a');
flost = fopen('line2-lost.txt', 'a');
frecent = fopen('line2-recent.txt','r');
if frecent > 0
  last = fread(frecent)
  fclose(frecent);
end
firsttime = 0;
a = 1;
while a <= length(m_orientation)
  b = 1;
  while b <= length(m_orientation)
    c = 1;
    while c <= length(m_topology)
      d = 1;
      while d <= length(m_xlocation)
        e = 1;
        while e <= length(m_ylocation)
          f = 1;
          while f <= length(m_angle)
            g = 1;
            while g <= length(m_size)
              if firsttime == 0 
                  firsttime = 1;
                if frecent > 0
                  a = last(1);
                  b = last(2);
                  c = last(3);
                  d = last(4);
                  e = last(5);
                  f = last(6);
                  g = last(7);
                end
              end
              constraint(1,:) = [m_orientation(a), 1, 0];
              constraint(2,:) = [m_orientation(b), 2, 0];
              constraint(3,:) = [m_topology(c), 1, 2];
              constraint(4,:) = [m_xlocation(d), 1,2];
              constraint(5,:) = [m_ylocation(e), 1,2];
              constraint(6,:) = [m_angle(f), 1,2];
              constraint(7,:) = [m_size(g), 1,2];
              x0 = [250,250, 290, 290, 150, 150, 190, 190];
              [x, fval, exitflag, output] = createshape(x0, constraint, 1, 500);
              if fval < .05 
                s = ['Shape{Line[P1(' int2str(x(1)) ':' int2str(x(2)) ') P2(' int2str(x(3)) ':' int2str(x(4)) ')] Line[P1(' int2str(x(5)) ':' int2str(x(6)) ') P2(' int2str(x(7)) ':' int2str(x(8)) ')]}']
                fprintf(ffound, '%s\n', s);
              else
                no = constraint(:,1)'
                fprintf(flost, '%d, %d, %d, %d, %d, %d, %d;\n', no(1),no(2),no(3),no(4),no(5),no(6),no(7));
                last = [a b c d e f g];
                frecent = fopen('line2-recent.txt', 'w');
                fwrite(frecent, last);
                fclose(frecent);
              end              
              g = g + 1;
            end
            f = f + 1;
          end
          e = e + 1;
        end
        d = d + 1;
      end
      c = c + 1;
    end
    b = b + 1;
  end
  a = a + 1;
end
    

fclose(flost);
fclose(ffound);