function lineMatrix = makeLineMatrix(x0)

lineMatrix = 0;
count = 1;
for i = 1 : length(x0) / 4
  lineMatrix(i, 1) = count;
  count = count+ 1;
  lineMatrix(i, 2) = count;
  count = count + 1;
  lineMatrix(i, 3) = count;
  count = count + 1;
  lineMatrix(i, 4) = count;
  count = count + 1;
end