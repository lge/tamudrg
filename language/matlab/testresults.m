function testresults()

summaryfile = strcat('data/L2_summary.txt');

subdirname = 'data/L2/';

fsummary = fopen(summaryfile, 'w');
filenameshape = strcat('data/L2/L2_1_1_1_1_1_1_1__SHAPE.txt');
filenameerror = strcat('data/L2/L2_1_1_1_1_1_1_1__ERROR.txt');


%files = dir(strcat(subdirname, '*SHAPE.txt'));
%length(files)
for a = 1:4
  for b = 1:4
    for c = 1:5
      for d = 1:5
        for e = 1:3
          for f = 1:18
            for g = 1:5
              constraints = [a,b,c,d,e,f,g];
%for j = 1 : length(files)
%  if length(findstr('SHAPE.txt', files(j).name)) > 0 && ~files(j).isdir  
%    constraints = cell2mat(textscan(files(j).name, 'L2_%d_%d_%d_%d_%d_%d_%d__SHAPE.txt'));
    middle = strcat(num2str(constraints(1)), '_', num2str(constraints(2)), '_', num2str(constraints(3)), '_', num2str(constraints(4)), '_', num2str(constraints(5)), '_', num2str(constraints(6)), '_', num2str(constraints(7)),'_');
    filenameshape = strcat('data/L2/L2_', middle, '_SHAPE.txt');
    filenameerror = strcat('data/L2/L2_', middle, '_ERROR.txt');
    
    fshape = fopen(filenameshape, 'r');
    ferror = fopen(filenameerror, 'r');
    if fshape == -1
      continue;
    end
    if ferror == -1
      continue;
    end
    val = cell2mat(textscan(fshape, 'Shape{Line[P1(%d:%d) P2(%d:%d)] Line[P1(%d:%d) P2(%d:%d)]}'));
    error = cell2mat(textscan(ferror, '%f'));
%    s = sprintf('%d %d %d %d %d %d %d %d %f %d %d %d %d %d %d %d\n', val, error, constraints);
    fprintf(fsummary, '%d %d %d %d %d %d %d %d %f %d %d %d %d %d %d %d\n', val, error, constraints);
    fclose(fshape);
    fclose(ferror);
            end
          end
        end
      end
    end
  end
end
fclose(fsummary);
      
% line1 = [x(1),x(2);x(3),x(4)];
% line2 = [x(5),x(6);x(7),x(8)];
% mid1 = getLineMidpoint(line1);
% angle1 = getLineAngle(line1);
% len1 = getLineLength(line1);
% 
% mid2 = getLineMidpoint(line2);
% angle2 = getLineAngle(line2);
% len2 = getLineLength(line2);

% 
% [oe1, orient1] = min(orientation(angle1));
% [oe2, orient2] = min(orientation(angle2));
% 
% xLocationError = relativeLocation(1, line1, mid1, line2, mid2);
% [xerror, xchoice] = min(xLocationError)
%     
%     yLocationError = relativeLocation(2, line1, mid1, line2, mid2);
%     [yerror, ychoice] = min(yLocationError)
%     
%     lengthError = relativeLength(len1, len2);
%     [lenerror, lenchoice] = min(lengthError)
%     
%     topologyError = topology(line1, len1, mid1, line2, len2, mid2);
%     [toperror, topchoice] = min(topologyError)
%     
%     relativeAngleError = relativeAngle(len1, angle1, len2, angle2, topologyError);
%     [angleerror, anglechoice] = min(relativeAngleError)
% 
%     totalerror = angleerror + toperror + lenerror + yerror + xerror + oe2 + oe1
% 
%     label = strcat('L2_', num2str(orient1), '_', num2str(orient2), ...
%     '_', num2str(xchoice), '_', num2str(ychoice), '_', num2str(lenchoice), '_',  ...
%     num2str(topchoice), '_', num2str(anglechoice), '_')
%   
% plot(line1(:,1), line1(:,2), '-r', line2(:,1), line2(:,2));
