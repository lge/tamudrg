function num = computeError(x, lineMatrix, constraints)

num = 0; %ob(x);

[a,b] = size(lineMatrix);
for i = 1 : a
  lines(i,1) = x(lineMatrix(i,1));
  lines(i,2) = x(lineMatrix(i,2));
  lines(i,3) = x(lineMatrix(i,3));
  lines(i,4) = x(lineMatrix(i,4));    
%  ceq(i) = getLineLength(lineToPoints(line(i, :))) - x(linelengths(i));
end

[a,b] = size(lines);

%count = i;
[a,b] = size(constraints);

for i = 1 : a    
    row = constraints(i,:);
    if row{2} > 0
        line1 = lineToPoints(lines(row{2},:));
        l1x1 = line1(1,1);
        l1y1 = line1(1,2);
        l1x2 = line1(2,1);
        l1y2 = line1(2,2);        
    end
    if row{3} > 0
        line2 = lineToPoints(lines(row{3},:));
        l2x1 = line2(1,1);
        l2y1 = line2(1,2);
        l2x2 = line2(2,1);
        l2y2 = line2(2,2);  
    end
%    if row{4} > 0
%        line3 = lineToPoints(lines(row{3}, :));
%    end
%    if row{5} > 0
%        line4 = lineToPoints(lines(row{4}, :));
%    end
    if strcmp(row(1), 'horizontal')
        num = num + abs(l1y1 - l1y2);
    elseif strcmp(row(1), 'vertical')
        num = num + abs(l1x1 - l1x2);
    elseif strcmp(row(1), 'posSlope')
        num = num + posSlope(line1);
    elseif strcmp(row(1), 'negSlope')
        num = num + negSlope(line1);
    elseif strcmp(row(1), 'near')     
        num = num + near(line1, line2); 
    elseif strcmp(row(1), 'far')
        num = num  + far(line1, line2);
    elseif strcmp(row(1), 'intersects') || strcmp(row(1), 'bisects_cc')
       num = num + getLineLength([getLineMidpoint(line1); getLineMidpoint(line2)]); 
    elseif strcmp(row(1), 'bisects_c1')
      num = num + getLineLength([getLineMidpoint(line1); line2(1,:)]); 
    elseif strcmp(row(1), 'bisects_c2')
      num = num + getLineLength([getLineMidpoint(line1); line2(2,:)]); 
    elseif strcmp(row(1), 'bisects_1c')
      num = num + getLineLength([getLineMidpoint(line2); line1(1,:)]); 
    elseif strcmp(row(1), 'bisects_2c')
      num = num + getLineLength([getLineMidpoint(line2); line1(2,:)]); 
    elseif strcmp(row(1), 'connected_11')
      num = num + getLineLength([line1(1,:); line2(1,:)]); 
    elseif strcmp(row(1), 'connected_12')
       num = num + getLineLength([line1(1,:); line2(2,:)]); 
    elseif strcmp(row(1), 'connected_21')
       num = num + getLineLength([line1(2,:); line2(1,:)]); 
    elseif strcmp(row(1), 'connected_22')
        num = num + getLineLength([line1(2,:); line2(2,:)]); 
    elseif strcmp(row(1), 'bisects_Lc')        
        num = num + getDistanceToLine(getLineMidpoint(line2), line1); 
    elseif strcmp(row(1), 'bisects_cL')
%        num = num + getDistanceToLine(getLineMidpoint(line1), line2); 
    elseif strcmp(row(1), 'meets_L1')
        num = num + getDistanceToLine(line2(1,:), line1); 
    elseif strcmp(row(1), 'meets_L2')
        num = num + getDistanceToLine(line2(2,:), line1); 
    elseif strcmp(row(1), 'meets_1L')
        num = num + getDistanceToLine(line1(1,:), line2);
    elseif strcmp(row(1), 'meets_2L')
        num = num + getDistanceToLine(line1(2,:), line2);
    elseif strcmp(row{1}, 'sameX')
        num = num + abs(l1x1 + l1x2 - l2x1 - l2x2);
    elseif strcmp(row(1), 'sameY')
        num = num + abs(l1y1 + l1y2 - l2y1 - l2y2); 
    elseif strcmp(row(1), 'leftOf')
        num = num + leftOf(line1, line2);
    elseif strcmp(row(1), 'rightOf')
        num = num + leftOf(line2, line2); 
    elseif strcmp(row(1), 'above')
        num = num + above(line1, line2);
    elseif strcmp(row(1), 'below')
        num = num + above(line2, line1);
    elseif strcmp(row(1), 'overlapLeftOf')
        num = num + overlapLeftOf(line1, line2);
    elseif strcmp(row(1), 'overlapRightOf')
        num = num + overlapLeftOf(line2, line1);      
    elseif strcmp(row(1), 'overlapAbove')
        num = num + overlapAbove(line1, line2);          
    elseif strcmp(row(1), 'overlapBelow')
        num = num + overlapAbove(line2, line1);             
    elseif strcmp(row(1), 'equalAngle')
        num = num + abs((getLineAngle(line1) - getLineAngle(line2)) - (getLineAngle(line3) - getLineAngle(line4)));
    elseif strcmp(row(1), 'parallel')
        [v1x, v1y] = getDirectionVector(line1);
        [v2x, v2y] = getDirectionVector(line2);
        num = num + abs(v1x - v2x) + abs(v1y - v2y); 
    elseif strcmp(row(1), 'perpendicular')
        [v1x, v1y] = getDirectionVector(line1);
        [v2x, v2y] = getDirectionVector(line2);
        num = num + abs(v2x*v1x + v2y*v1y);
    elseif strcmp(row(1), 'acuteMeet')
        num = num + acuteMeet(line1, line2);
    elseif strcmp(row(1), 'obtuseMeet')
        num = num + obtuseMeet(line1, line2);
    elseif strcmp(row(1), 'slanted')
        num = num + slanted(line1, line2);
    elseif strcmp(row(1), 'equalLength') || strcmp(row(1), 'equalArea')
        num = num + abs(getLineLength(line1) - getLineLength(line2));
    elseif strcmp(row(1), 'longer') || strcmp(row(1), 'larger')
        num = num + larger(line1, line2);
    elseif strcmp(row(1), 'smaller') || strcmp(row(1), 'shorter')
        num = num + larger(line2, line1);
    else
        s = ['error can not find ' row(1)]
    end
end

num;
