function plotLine(x)

axes;
axis([-10 510 -10 510]);

for i = 1 : length(x)/4
  x1 = x(i*4 - 3);
  y1 = x(i*4 - 2);
  x2 = x(i*4 - 1);
  y2 = x(i*4);
  line([x1;x2], [y1;y2], 'LineWidth', 4, 'Color', [1,(.3*(i-1)),(.3*(i-1))]);    
end