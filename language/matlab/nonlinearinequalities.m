function [c,ceq] = nonlinearinequalities(x, lineMatrix, constraints)

c = 0;
ceq = 0;

[a,b] = size(lineMatrix);
ccount = 1;
ceqcount = 1;

for i = 1 : a
  line(i,1) = x(lineMatrix(i,1));
  line(i,2) = x(lineMatrix(i,2));
  line(i,3) = x(lineMatrix(i,3));
  line(i,4) = x(lineMatrix(i,4));   
  c(ccount) = 30 - getLineLength(lineToPoints(line(i, :))); 
  ccount = ccount + 1;
end



for i = 1 : a
    for j = i + 1 : a
        if i == j 
            continue;
        end;
        line1 = lineToPoints(line(i, :));
        line2 = lineToPoints(line(j, :));
        ceq(ceqcount) = isLineOverlaping(line1, line2);
        ceqcount = ceqcount +1;
    end
end

