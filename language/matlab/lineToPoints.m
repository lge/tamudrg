function points = lineToPoints(line)

points(1,1) = line(1);
points(1,2) = line(2);
points(2,1) = line(3);
points(2,2) = line(4);
