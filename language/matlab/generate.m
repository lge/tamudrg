function generate()


summaryfile = 'C:\data\L2_sum.txt';
fsummary = fopen(summaryfile, 'w');

addpath(genpath('.'));
%set(0, 'DefaultLineLineWidth', 1);
%'start'
%orientationString = {'horizontal', 'posSlope', 'vertical', 'negSlope'};
%xlocationString = {'leftOf', 'overlapLeftOf', 'sameX', 'overlapRightOf', 'rightOf'};
%ylocationString = {'below', 'overlapBelow', 'sameY', 'overlapAbove', 'above'};
%lengthString = {'shorter', 'equalLength', 'longer'};
%topologyString = {'far', 'near', 'intersects', 'bisects_cc', 'bisects_c1', 'bisects_c2', 'bisects_1c', 'bisects_2c', 'connected_11', 'connected_12', 'connected_21', 'connected_22', 'bisects_Lc', 'bisects_cL', 'bisects_L1', 'bisects_L2', 'bisects_1L', 'bisects_2L'};
%angleString = {'parallel', 'perpendicular', 'slanted', 'acuteMeet', 'obtuseMeet'};

%hold off;
%line1 = [0, 0 ; 200, 0];
%plot(line1(:,1), line1(:,2), '-k');
%hold on;
%for c = 0 : .1: 2*pi
%  line2 = [-cos(c)*50, -50*sin(c); cos(c)*50, 50*sin(c)];  
%   to = topology(line1, getLineLength(line1), getLineMidpoint(line1), line2,getLineLength(line2), getLineMidpoint(line2));
%   relativeAngleError = relativeAngle(getLineLength(line1), getLineAngle(line1), getLineLength(line2),  getLineAngle(line2), to);
%   [error, choice] = min(relativeAngleError);
%   if choice == 1
%     plot(line2(:,1), line2(:,2), '-r');
%   elseif choice == 2
%     plot(line2(:,1), line2(:,2), '-g');
%   elseif choice == 3
%     plot(line2(:,1), line2(:,2), '-b');
%   elseif choice == 4
%     plot(line2(:,1), line2(:,2), '-c');
%   else 
%     plot(line2(:,1), line2(:,2), '-m');    
%   end
%   
%   line2 = [180, 0;  + 180 + cos(c)*50, 50*sin(c)];  
%   to = topology(line1, getLineLength(line1), getLineMidpoint(line1), line2,getLineLength(line2), getLineMidpoint(line2));
%   relativeAngleError = relativeAngle(getLineLength(line1), getLineAngle(line1), getLineLength(line2),  getLineAngle(line2), to);
%   [error, choice] = min(relativeAngleError);
%   if choice == 1
%     plot(line2(:,1), line2(:,2), '-r');
%   elseif choice == 2
%     plot(line2(:,1), line2(:,2), '-g');
%   elseif choice == 3
%     plot(line2(:,1), line2(:,2), '-b');
%   elseif choice == 4
%     plot(line2(:,1), line2(:,2), '-c');
%   else 
%     plot(line2(:,1), line2(:,2), '-m');
%   end
% end
% 
% plot(line1(:,1), line1(:,2), '-k');
% pause(1);
% 
% newplot;
% hold off;
% for c = -50 : 5 : 250
%   l2 = [100,c;200,c];
%   plot(l2(:,1), l2(:,2), '-k');
%   hold on;
% for a = 0:100
%   
%   l1 = [a*3, 20; a*3, 200]; 
%   to = topology(l1, getLineLength(l1), getLineMidpoint(l1), l2,getLineLength(l2), getLineMidpoint(l2));
%   [error,choice] = min(to);
%   if choice == 1
%     plot(l1(:,1), l1(:,2), '-r');
%   elseif choice == 2
%     plot(l1(:,1), l1(:,2), '-g');
%   elseif choice == 3
%     plot(l1(:,1), l1(:,2), '-b');
%   elseif choice == 4
%     plot(l1(:,1), l1(:,2), ':r');
%   elseif choice == 5
%     plot(l1(:,1), l1(:,2), ':g');
%   elseif choice == 6
%     plot(l1(:,1), l1(:,2), ':b');
%   elseif choice == 7
%     plot(l1(:,1), l1(:,2), ':c');
%   elseif choice == 8
%     plot(l1(:,1), l1(:,2), ':m');
%   elseif choice == 9
%     plot(l1(:,1), l1(:,2), '*r');
%   elseif choice == 10
%     plot(l1(:,1), l1(:,2), '*g');
%   elseif choice == 11
%     plot(l1(:,1), l1(:,2), '*b');
%   elseif choice == 12
%     plot(l1(:,1), l1(:,2), '*c');
%   elseif choice == 13
%     plot(l1(:,1), l1(:,2), '--r');
%   elseif choice == 14
%     plot(l1(:,1), l1(:,2), '--g');
%   elseif choice == 15
%     plot(l1(:,1), l1(:,2), '--b');
%   elseif choice == 16
%     plot(l1(:,1), l1(:,2), '--c');
%   elseif choice == 17
%     plot(l1(:,1), l1(:,2), '--m');
%   elseif choice == 18
%     plot(l1(:,1), l1(:,2), '--y');
%   end
% end
% hold off;
% end
% 'done'
% 
% hold on;
%   l2 = [10,5.5;20,5.5];
%   plot(l2(:,1), l2(:,2), '-k');
% for a = 0:20
%   l1 = [15 - a, a; 15 + a, a]; 
%   lengthError = relativeLength(getLineLength(l1), getLineLength(l2));
%   [error,choice] = min(lengthError);
%   if choice == 1
%     plot(l1(:,1), l1(:,2), '-r');
%   elseif choice == 2
%     plot(l1(:,1), l1(:,2), '-g');
%   elseif choice == 3
%     plot(l1(:,1), l1(:,2), '-b');
%   end
% end
% 'done'
% pause(1);
% hold off;
% 
% 
% hold on;
%   l2 = [10,5.5;20,5.5];
%   plot(l2(:,1), l2(:,2), '-k');
% for a = 0:20
%   l1 = [15 - a, a; 15 + a, a]; 
%   lengthError = relativeLength(getLineLength(l1), getLineLength(l2));
%   [error,choice] = min(lengthError);
%   if choice == 1
%     plot(l1(:,1), l1(:,2), '-r');
%   elseif choice == 2
%     plot(l1(:,1), l1(:,2), '-g');
%   elseif choice == 3
%     plot(l1(:,1), l1(:,2), '-b');
%   end
% end
% 'done'
% pause(1);
% hold off;
% 
% hold off;
% hold on;
%   l2 = [10,3;15,3];
%   plot(l2(:,1), l2(:,2), '-k');
% for a = 0:20
%   l1 = [a,0; a+5, 5]; 
%   xlocationError = relativeLocation(1, l1, getLineMidpoint(l1), l2, getLineMidpoint(l2));
%   [error,choice] = min(xlocationError);
%   if choice == 1
%     plot(l1(:,1), l1(:,2), '-r');
%   elseif choice == 2
%     plot(l1(:,1), l1(:,2), '-g');
%   elseif choice == 3
%     plot(l1(:,1), l1(:,2), '-b');
%   elseif choice == 4
%     plot(l1(:,1), l1(:,2), '-m');
%   else
%     plot(l1(:,1), l1(:,2), '-c');
%   end
% end
% 'done'
% pause(5);
% hold off;
% hold on;
% for i = - pi : .1 : pi
%   orientationError = orientation(i);
%   [error,choice] = min(orientationError);
%   if choice == 1
%     plot([0,cos(i)],[0,sin(i)], '-r');
%   elseif choice == 2
%     plot([0,cos(i)],[0,sin(i)], '-g');
%   elseif choice == 3
%     plot([0,cos(i)],[0,sin(i)], '-b');
%   else
%     plot([0,cos(i)],[0,sin(i)], '-m');
%   end
% end

points = ones (49,2);
count = 0;
for a = 1:7
  for b = 1:7
    count = count + 1;
    points(count,:) = [a,b];    
  end
end
count
%pause(1)


count = 0;
lines = ones(1176,4);
for a = 1:length(points)
  for b = a + 1:length(points)
    count = count +1;        
    lines(count, :) = [points(a,1),points(a,2),points(b,1),points(b,2)];
  end
end
count

linlen = ones(length(lines),1);
midpoint = ones(length(lines),2);
angle = ones(length(lines),1);
orientationError = ones(length(lines),4);
bestorientation = ones(length(lines),2);
for a = 1:length(lines)  
  line1 = [lines(a,1), lines(a,2); lines(a,3), lines(a,4)];
  linlen(a) = getLineLength(line1);
  midpoint(a,:) = getLineMidpoint(line1);
  angle(a) = atan2(lines(a,4) - lines(a,2), lines(a,3)- lines(a,1));
  orientationError(a,:) = orientation(angle(a));
  [error, choice] = min(orientationError(a,:));
  bestorientation(a,:) = [error, choice];
end
lines(500,:)
linlen(500);
midpoint(500,:);
angle(500);

varray = ones(1,8);
%equalLength = ones(length(lines), length(lines));
errorarray = ones(4,4,5,5,3,18) * 20;%f %d %d %d %d %d %d %d
valarray = zeros(4,4,5,5,3,18,8);
for l1 = 1:length(lines)  
  line1 = [lines(l1,1), lines(l1,2); lines(l1,3), lines(l1,4)];
  mid1 = midpoint(l1,:);
  %angle1 = angle(l1);
  len1 = linlen(l1);
  orient1 = bestorientation(l1,:);
  % choice = orient1(2);
  %orientationString{choice};
  for l2 = 1:length(lines)
    if mod(l2,100) == 0
      [l1, l2]
    end
    %  angle2 = angle(b);
    line2 = [lines(l2,1), lines(l2,2); lines(l2,3), lines(l2,4)];
    mid2 = midpoint(l2,:);
    len2 = linlen(l2);
    orient2 = bestorientation(l2,:);
    %orientationString{orient2(2)};
    
    xLocationError = relativeLocation(1, line1, mid1, line2, mid2);
    [xerror, xchoice] = min(xLocationError);     
    
    yLocationError = relativeLocation(2, line1, mid1, line2, mid2);
    [yerror, ychoice] = min(yLocationError);
    
    lengthError = relativeLength(len1, len2);
    [lenerror, lenchoice] = min(lengthError);
    
    topologyError = topology(line1, len1, mid1, line2, len2, mid2);
    [toperror, topchoice] = min(topologyError);

    array = [lines(l1,:) lines(l2,:)];
    %relativeAngleError = relativeAngle(len1, angle1, len2, angle2, topologyError);
    %[angleerror, anglechoice] = min(relativeAngleError);
    a = orient1(2);
    b = orient2(2);
    c = xchoice;
    d = ychoice;
    e = lenchoice;
    f = topchoice;
   % const = [a b c d e f]
    totalerror = toperror + lenerror + yerror + xerror + orient2(1) + orient1(1);
    if totalerror < errorarray(a,b,c,d,e,f)
      errorarray(a,b,c,d,e,f) = totalerror;
      valarray(a,b,c,d,e,f,:) = array;
    end
  %   varray(1:8) = valarray(a,b,c,d,e,f,:);
  %   sprintf('%d %d %d %d %d %d %d %d %f %d %d %d %d %d %d\n',  varray, errorarray(a,b,c,d,e,f), a,b,c,d,e,f);
%  label = strcat('L2', num2str(orient1(2)), '_', num2str(orient2(2)), ...
%    '_', num2str(xchoice), '_', num2str(ychoice), '_', num2str(lenchoice), '_',  ...
%    num2str(topchoice));
  
%    filenameline = strcat('data/L2/', label, '_SHAPE.txt');
%    filenameerror = strcat('data/L2/', label, '_ERROR.txt');

%    try
%      olderror = eval([label, ';']);
%    catch
%      olderror = min(400);
%    end
%    if olderror > totalerror
%      evalstr = [label, ' = ', num2str(totalerror), ';'];
%      eval(evalstr);    
%      ferror = fopen(filenameerror, 'wt');
%      fprintf(ferror, '%f\n', totalerror);
%      fshape = fopen(filenameline, 'wt');
%      x = [line1(1,1), line1(1,2), line1(2,1), line1(2,2), line2(1,1), line2(1,2), line2(2,1), line2(2,2)];
%      s = ['Shape{Line[P1(' int2str(x(1)) ':' int2str(x(2)) ') P2(' int2str(x(3)) ':' int2str(x(4)) ')] Line[P1(' int2str(x(5)) ':' int2str(x(6)) ') P2(' int2str(x(7)) ':' int2str(x(8)) ')]}'];
%      fprintf(fshape, '%s\n', s);
%      fclose(fshape);
%      fclose(ferror);
  end
    %replace error and line field  
end

%summaryfile = 'C:\data\L2_sum.txt';
%fsummary = fopen(summaryfile, '-w');
for a = 1:4
  for b = 1:4
    for c = 1:5
      for d = 1:5
        for e = 1:3
          for f = 1:18
            if errorarray(a,b,c,d,e,f) < 2              
              varray(1:8) = valarray(a,b,c,d,e,f,:);
              fprintf(fsummary, '%d %d %d %d %d %d %d %d %f %d %d %d %d %d %d\n',  varray, errorarray(a,b,c,d,e,f), a,b,c,d,e,f);
            end
          end
        end
      end
    end
  end
end
fclose(fsummary);