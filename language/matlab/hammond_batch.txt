#PBS -S /bin/bash -N matlab1 -l walltime=48:00:00,cputime=48:00:00,mem=1900mb,nc
pus=1
#PBS -j oe

cd $TMPDIR

# copy matlab input file (contains matlab commands) to work  directory
cp -r $PBS_O_WORKDIR/matlabcode ./matlabcode

cd matlabcode

# load matlab module & run matlab
module load matlab                  # Needed only on cosmos
matlab -nodisplay -nodesktop -nojvm < run.m > output.txt

# save files
cp line2-lost.txt $PBS_O_WORKDIR/matlabcode/line2-lost.txt
cp line2-recent.txt $PBS_O_WORKDIR/matlabcode/line2-recent.txt
cp line2-found.txt $PBS_O_WORKDIR/matlabcode/line2-found.txt
cp output.txt $PBS_O_WORKDIR/matlabcode/output.txt

cd ..

# clean up
rm -fr *
