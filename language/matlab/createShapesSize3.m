function createShapesSize3()
addpath(genpath('.'))
mapping;
global m_horizontal m_vertical m_posSlope m_negSlope m_near m_far 
global m_intersects m_bisects_cc m_bisects_c1 m_bisects_c2 m_bisects_1c m_bisects_2c 
global m_connected_11 m_connected_12 m_connected_21 m_connected_22 
global m_bisects_Lc m_bisects_cL m_meets_L1 m_meets_L2 m_meets_1L m_meets_2L
global m_sameX m_leftOf m_rightOf  m_overlapLeftOf m_overlapRightOf
global m_sameY m_above m_below m_overlapAbove m_overlapBelow
global m_parallel m_perpendicular m_slanted m_acuteMeet m_obtuseMeet
global m_equalArea m_larger m_smaller m_equalAngle m_mapping

m_orientation = [m_horizontal m_vertical m_posSlope m_negSlope];
m_topology = [m_far, m_near, m_intersects, m_bisects_cc, m_bisects_c1, m_bisects_c2, m_bisects_1c, m_bisects_2c, m_connected_11, m_connected_12, m_connected_21, m_connected_22, m_bisects_Lc, m_bisects_cL, m_meets_L1, m_meets_L2, m_meets_1L, m_meets_2L];
m_xlocation = [m_sameX, m_leftOf, m_rightOf,  m_overlapLeftOf, m_overlapRightOf];
m_ylocation = [m_sameY, m_above, m_below, m_overlapAbove, m_overlapBelow];
m_angle = [m_parallel, m_perpendicular, m_slanted, m_acuteMeet, m_obtuseMeet];
m_size = [m_equalArea, m_larger, m_smaller];

ffound = fopen('line3-found.txt', 'a');
flost = fopen('line3-lost.txt', 'a');
frecent = fopen('line3-recent.txt','r');
if frecent > 0
  last = fread(frecent);
  fclose(frecent);
end
firsttime = 0;

a = 1;
while a <= length(m_orientation)
  b = 1;
  while b <= length(m_orientation)
    c = 1;   
    while c <= length(m_orientation)
      d = 1;
      while d <= length(m_topology)
        e = 1;
        while e <= length(m_topology)
          f = 1;
          while f <= length(m_topology)
            g = 1;
            while g <= length(m_xlocation)
              h = 1;
              while h <= length(m_xlocation)
                i = 1;
                while i <= length(m_xlocation)
                  j = 1;
                  while j <= length(m_ylocation)
                    k = 1;
                    while k <= length(m_ylocation)
                      l = 1;
                      while l <= length(m_ylocation)
                        m = 1;
                        while m <= length(m_angle)
                          n = 1;
                          while n <= length(m_angle)
                            o = 1;
                            while o <= length(m_angle)
                              p = 1;
                              while p <= length(m_size)
                                q = 1;
                                while q <= length(m_size)
                                  r = 1;
                                  while r <= length(m_size)
                                    if firsttime == 0 
                                      firsttime = 1;
                                      if frecent > 0
                                        a = last(1);
                                        b = last(2);
                                        c = last(3);
                                        d = last(4);
                                        e = last(5);
                                        f = last(6);
                                        g = last(7);
                                        h = last(8);
                                        i = last(9);
                                        j = last(10); 
                                        k = last(11);
                                        l = last(12);
                                        m = last(13);
                                        n = last(14);
                                        o = last(15);
                                        p = last(16);
                                        q = last(17);
                                        r = last(18);
                                      end
                                    end
                                    constraint(1,:) = [m_orientation(a), 1, 0];
                                    constraint(2,:) = [m_orientation(b), 2, 0];
                                    constraint(3,:) = [m_orientation(c), 3, 0];
                                    constraint(4,:) = [m_topology(d), 1, 2];
                                    constraint(5,:) = [m_topology(e), 1, 3];
                                    constraint(6,:) = [m_topology(f), 2, 3];
                                    constraint(7,:) = [m_xlocation(g), 1,2];
                                    constraint(8,:) = [m_xlocation(h), 1,3];
                                    constraint(9,:) = [m_xlocation(i), 2,3];              
                                    constraint(10,:) = [m_ylocation(j), 1,2];
                                    constraint(11,:) = [m_ylocation(k), 1,3];
                                    constraint(12,:) = [m_ylocation(l), 2,3];
                                    constraint(13,:) = [m_angle(m), 1,2];
                                    constraint(14,:) = [m_angle(n), 1,3];
                                    constraint(15,:) = [m_angle(o), 2,3];
                                    constraint(16,:) = [m_size(p), 1,2];
                                    constraint(17,:) = [m_size(q), 1,3];
                                    constraint(18,:) = [m_size(r), 2,3];
                                    x0 = [250,250, 290, 290, 150, 150, 190, 190, 50,50, 300,300];
                                    [x, fval, exitflag, output] = createshape(x0, constraint, 1, 500);
                                    if fval < .05 
                                      s = ['Shape{Line[P1(' int2str(x(1)) ':' int2str(x(2)) ') P2(' int2str(x(3)) ':' int2str(x(4)) ')] Line[P1(' int2str(x(5)) ':' int2str(x(6)) ') P2(' int2str(x(7)) ':' int2str(x(8)) ')]}']
                                      fprintf(ffound, '%s\n', s);
                                    else
                                      no = constraint(:,1)'
                                      fprintf(flost, '%d, %d, %d, %d, %d, %d, %d, %d, %d, %d, %d, %d, %d, %d, %d, %d, %d, %d;\n', no(1),no(2),no(3),no(4),no(5),no(6),no(7),no(8),no(9),no(10),no(11),no(12),no(13),no(14),no(15),no(16),no(17),no(18));
                                      last = [a b c d e f g h i j k l m n o p q r];
                                      frecent = fopen('line3-recent.txt', 'w');
                                      fwrite(frecent, last);
                                      fclose(frecent);
                                    end    
                                    r = r + 1;
                                  end
                                  q = q + 1;
                                end
                                p = p + 1;
                              end
                              o = o + 1;
                            end
                            n = n + 1;
                          end
                          m = m + 1;
                        end
                        l = l + 1;
                      end
                      k = k + 1;
                    end
                    j = j + 1;
                  end
                  i = i + l;
                end
                h = h + 1;
              end
              g = g + 1;
            end
            f = f + 1;
          end
          e = e + 1;
        end
        d = d + 1;
      end
      c = c + 1;
    end
    b = b + 1;
  end
  a = a + 1;
end
    

fclose(flost);
fclose(ffound);