function num = objectiveFunction(x, lineMatrix, constraints, debug)

global m_horizontal m_vertical m_posSlope m_negSlope m_near m_far ...
  m_intersects m_bisects_cc m_bisects_c1 m_bisects_c2 ...
  m_bisects_1c m_bisects_2c m_connected_11 m_connected_12 ...  
  m_connected_21 m_connected_22 m_bisects_Lc m_bisects_cL ...
  m_meets_L1 m_meets_L2  m_meets_1L m_meets_2L m_sameX ... 
  m_leftOf m_rightOf  m_overlapLeftOf m_overlapRightOf ...
  m_sameY m_above m_below m_overlapAbove m_overlapBelow ...
  m_parallel m_perpendicular m_slanted m_acuteMeet m_obtuseMeet ...
  m_equalArea m_larger m_smaller m_equalAngle m_mapping

num = 0; 

[a,b] = size(lineMatrix);
for i = 1 : a
  lines(i,1) = x(lineMatrix(i,1));
  lines(i,2) = x(lineMatrix(i,2));
  lines(i,3) = x(lineMatrix(i,3));
  lines(i,4) = x(lineMatrix(i,4));    
  line1 = lineToPoints(lines(i,:));
  if getLineLength(line1) < 30
      num = num + 30 - getLineLength(line1);
  end
  for j = 1:4
  if lines(i, j) < 0
      num = num - lines(i,j);
  end
  if lines(i,j) > 500
      num = num + lines(i, j) - 500; 
  end
  end
end

[a,b] = size(constraints);
for i = 1 : a    
    row = constraints(i,:);
    constraint = row(1);
    if row(2) > 0
        line1 = lineToPoints(lines(row(2),:));
        l1x1 = line1(1,1);
        l1y1 = line1(1,2);
        l1x2 = line1(2,1);
        l1y2 = line1(2,2);        
    end
    if row(3) > 0
        line2 = lineToPoints(lines(row(3),:));
        l2x1 = line2(1,1);
        l2y1 = line2(1,2);
        l2x2 = line2(2,1);
        l2y2 = line2(2,2);  
    end
    if row{4} > 0
      line3 = lineToPoints(lines(row{3}, :));
    end
    if row{5} > 0
        line4 = lineToPoints(lines(row{4}, :));
    end

    if constraint == m_horizontal
          num = num + abs(l1y1 - l1y2);
    elseif constraint == m_vertical
        num = num + abs(l1x1 - l1x2);
    elseif constraint == m_posSlope
        num = num + posSlope(line1);
    elseif constraint == m_negSlope
        num = num + negSlope(line1);
             
    elseif constraint == m_equalArea
        num = num + abs(getLineLength(line1) - getLineLength(line2));
    elseif constraint == m_larger
        num = num + larger(line1, line2);
    elseif constraint == m_smaller
        num = num + larger(line2, line1);
        
    elseif constraint == m_sameX
        num = num + abs(l1x1 + l1x2 - l2x1 - l2x2);
    elseif constraint == m_sameY
        num = num + abs(l1y1 + l1y2 - l2y1 - l2y2);
    elseif constraint == m_leftOf
        num = num + leftOf(line1, line2);
    elseif constraint == m_rightOf
        num = num + leftOf(line2, line2); 
    elseif constraint == m_above
        num = num + above(line1, line2);
    elseif constraint == m_below
        num = num + above(line2, line1);
    elseif constraint == m_overlapLeftOf
        num = num + overlapLeftOf(line1, line2);
    elseif constraint == m_overlapRightOf
        num = num + overlapLeftOf(line2, line1);      
    elseif constraint == m_overlapAbove
        num = num + overlapAbove(line1, line2);          
    elseif constraint == m_overlapBelow
        num = num + overlapAbove(line2, line1);     

   elseif constraint == m_parallel
        [v1x, v1y] = getDirectionVector(line1);
        [v2x, v2y] = getDirectionVector(line2);
        num = num + abs(v1x - v2x) + abs(v1y - v2y); 
    elseif constraint == m_perpendicular
        num = num + perpendicular(line1, line2);
    elseif constraint == m_acuteMeet
        num = num + acuteMeet(line1, line2);
    elseif constraint == m_obtuseMeet
        num = num + obtuseMeet(line1, line2);
    elseif constraint == m_slanted
      num = num + slanted(line1, line2);
        
    elseif constraint == m_near   
        num = num + near(line1, line2); 
    elseif constraint == m_far
        num = num  + far(line1, line2);
    elseif constraint == m_intersects || constraint == m_bisects_cc
       num = num + getLineLength([getLineMidpoint(line1); ...
         getLineMidpoint(line2)]); 
       
    elseif constraint == m_bisects_c1
      num = num + getLineLength([getLineMidpoint(line1); line2(1,:)]); 
    elseif constraint == m_bisects_c2
      num = num + getLineLength([getLineMidpoint(line1); line2(2,:)]); 
    elseif constraint == m_bisects_1c
      num = num + getLineLength([getLineMidpoint(line2); line1(1,:)]); 
    elseif constraint == m_bisects_2c
      num = num + getLineLength([getLineMidpoint(line2); line1(2,:)]); 

    elseif constraint == m_connected_11
      num = num + 10*getLineLength([line1(1,:); line2(1,:)]); 
    elseif constraint == m_connected_12
       num = num + 10*getLineLength([line1(1,:); line2(2,:)]); 
    elseif constraint == m_connected_21
       num = num + 10*getLineLength([line1(2,:); line2(1,:)]); 
    elseif constraint == m_connected_22
        num = num + 10*getLineLength([line1(2,:); line2(2,:)]);         
    elseif constraint == m_bisects_Lc        
        num = num + getDistanceToLine(getLineMidpoint(line2), line1); 
    elseif constraint == m_bisects_cL
        num = num + getDistanceToLine(getLineMidpoint(line1), line2); 
    elseif constraint == m_meets_L1
        num = num + getDistanceToLine(line2(1,:), line1); 
    elseif constraint == m_meets_L2
        num = num + getDistanceToLine(line2(2,:), line1); 
    elseif constraint == m_meets_1L
        num = num + getDistanceToLine(line1(1,:), line2);
    elseif constraint == m_meets_2L
        num = num + getDistanceToLine(line1(2,:), line2);
        
    elseif constraint == m_equalAngle
        num = num + abs((getLineAngle(line1) - getLineAngle(line2)) - ...
          (getLineAngle(line3) - getLineAngle(line4)));
        
    else
        s = ['error can not find ' constraint]
    end
    if debug
      m_mapping(row(1))
      num
    end
end

num;
