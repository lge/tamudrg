<?xml version="1.0" encoding="ISO-8859-1"?>

<xsl:stylesheet version="1.0" 
xmlns:xsl="http://www.w3.org/1999/XSL/Transform"> 

  <xsl:template match="/">
  <html>
  <head>
  <title>
  LADDER Shape Collection: <xsl:value-of select="ShapeCollection/@name" />
  </title>
  <meta name="author" content="Tracy Hammond"></meta>   
  <meta name="keywords" content="sketch, sketch recognition, ladder, predefined, displays, primitives, sketching, language, sketch language"></meta>
  </head>
  <body>

    <h2>LADDER Shape Collection: 
      <xsl:value-of select="ShapeCollection/@name" /> </h2>

    <h3>Author: Tracy Hammond, hammond @ csail.mit.edu</h3>

    <xsl:for-each select="ShapeCollection/Shape">

  
    <br></br><hr></hr><br></br>

      (<b>Shape</b> &#xa0; <xsl:value-of select="@name" />

      <xsl:if test="Comment">
        <br></br> &#xa0; &#xa0;  
        (Comment <xsl:value-of select="Comment" /> )
      </xsl:if> 

      <xsl:if test="Components">
        <br></br> &#xa0; &#xa0;  
        (Components

        <xsl:for-each select="Components/Component">
          <br></br> &#xa0; &#xa0; &#xa0; &#xa0; (
            <a>
            <xsl:attribute name="href">
            http://www.ai.mit.edu/~hammond/research/language/predefined/shapes.html#<xsl:value-of select="@type" />
            </xsl:attribute>
            <xsl:value-of select="@type" /> 
            </a> &#xa0;
            <xsl:value-of select="." />)
        </xsl:for-each>
        <br></br> &#xa0; &#xa0;  )
        </xsl:if>

        <xsl:if test="Constraints">
        <br></br> &#xa0; &#xa0;  
        (Constraints 
        <xsl:for-each select="Constraints/Constraint">
           <br></br> &#xa0; &#xa0; &#xa0; &#xa0; (
            <a>
            <xsl:attribute name="href">
            http://www.ai.mit.edu/~hammond/research/language/predefined/constraints.html#<xsl:value-of select="@name" />
            </xsl:attribute>
            <xsl:value-of select="@name" /> 
            </a> 
            <xsl:for-each select="Component">
               &#xa0; <xsl:value-of select="." />
            </xsl:for-each>)
        </xsl:for-each>
        <br></br> &#xa0; &#xa0;  )
        </xsl:if>

        <xsl:if test="Aliases">
        <br></br> &#xa0; &#xa0;  
        (Aliases
        <xsl:for-each select="Aliases/Alias">
           <br></br> &#xa0; &#xa0; &#xa0; &#xa0; (
            <a>
            <xsl:attribute name="href">
            http://www.ai.mit.edu/~hammond/research/language/predefined/shapes.html#<xsl:value-of select="@type" />
            </xsl:attribute>
            <xsl:value-of select="@type" /> 
            </a> 
            &#xa0; <xsl:value-of select="@name" /> 
            &#xa0; <xsl:value-of select="." />
            )
        </xsl:for-each>
        <br></br> &#xa0; &#xa0;  )
        </xsl:if>
   
    <br></br>) <br></br>

      <xsl:for-each select="Images/Image">
        <br></br>
        <a>
        <xsl:attribute name="href">
          images/<xsl:value-of select="@src" />
        </xsl:attribute>
        <img>
        <xsl:attribute name="src">
         images/<xsl:value-of select="@src" />
        </xsl:attribute>
        <xsl:attribute name="alt">
          Image of  <xsl:value-of select="." /> 
        </xsl:attribute>
        <xsl:attribute name="border">0</xsl:attribute>
        <br></br><xsl:value-of select="." /> 
        </img>
        </a>
      </xsl:for-each>

    <br></br>

    </xsl:for-each>


  </body>
  </html>
  </xsl:template>
</xsl:stylesheet>
