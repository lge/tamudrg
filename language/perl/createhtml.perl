#!/usr/local/bin/perl5
#$Id: createhtml.perl,v 1.1 2003/11/09 20:11:50 hammond Exp $
#Author: Tracy Hammond
#Copyright: MIT
#Created: 2003-07-17 16:30:22
#Automatically generates the html files describing the language

require "shapes.lib";
require "constraints.lib";
require "display.lib";
require "editing.lib";
require "examples/processDir.lib";
require "examples/processDD.lib";
require "examples/paren.lib";
require "examples/convertXML.lib";

use Cwd;

print "Automatically generating HTML files\n";

$predefinedDir = "../predefined/";
#$htmldir = "/home/ai3/hammond/public_html/research/language/predefined/";
$htmldir = "../../../public_html/research/language/predefined/";
@$dirs[0] = $predefinedDir;
@$dirs[1] = $htmldir;
processShapes($dirs);
processConstraints($dirs); 
processDisplay($dirs);  
processEditing($dirs);

print "doing second part";

chdir "../../../";
#print "pwd = " . cwd() . "\n";

$htmldir = "public_html/research/language/examples/";
$exampledir = "drg/language/domain_descriptions/";
@$dirs[0] = $exampledir;
@$dirs[1] = $htmldir;

processDir($dirs);
