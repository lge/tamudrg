#!/bin/sh

DIST_DIR_NAME="bb"
JAR_NAME="bb-system.jar"
CODE_DIRS="edu"
DOC_DIRS=$CODE_DIRS
JARS="*.jar"
MAIN_CLASS="edu.mit.sketch.system.SketchSystem"
BIN_FILES="win32/*.dll win32/*.exe"
OTHER_FILE_TYPES="pkg ddl"
######################################################################
# End Configuration vars
######################################################################

DIST_DIR=$DRG_DIR/dist/$DIST_DIR_NAME
rm -fr $DIST_DIR

MANIFEST=${TMPDIR:-/tmp}/make-dist.sh.$$
trap "rm $MANIFEST* 2>/dev/null" 0

mkdir -p $DIST_DIR/lib $DIST_DIR/src $DIST_DIR/doc $DIST_DIR/resources

# make the manifest
pushd $DRG_DIR/lib
echo "Manifest-Version: 1.0" > $MANIFEST
echo "Main-Class: $MAIN_CLASS" >> $MANIFEST
echo "Class-Path: ../resources " $JARS >> $MANIFEST
popd

# make the JAR file
pushd $DRG_DIR/code/classes;
jar cfm $DIST_DIR/lib/$JAR_NAME $MANIFEST $CODE_DIRS
popd

# make the docs
pushd $DRG_DIR/code/src
javadoc -J-mx64m -J-ms64m \
  -d $DIST_DIR/doc \
  -version -author -use \
  -link http://java.sun.com/products/jdk/1.4/docs/api \
 `find $DOC_DIRS -name \*.java -print | sed -e 's|/[^/]*$||g' | sort | uniq | sed -e 's|/|.|g'` | grep "javadoc:"
popd 

# copy the libraries
pushd $DRG_DIR/lib
cp --parents $JARS $BIN_FILES $DIST_DIR/lib
popd

# copy the sources
pushd $DRG_DIR/code/src
find $CODE_DIRS -name \*.java | \
    xargs cp --parents --target-directory $DIST_DIR/src
popd

# make the scripts
BATCH="$DIST_DIR/$DIST_DIR_NAME.bat"
echo "set PATH=lib;lib/win32;%PATH%" > $BATCH
echo "java -classpath resources;lib/$JAR_NAME $MAIN_CLASS %*" >> $BATCH
     
SCRIPT="$DIST_DIR/$DIST_DIR_NAME.sh"
echo "#!/bin/sh" > $SCRIPT
echo "PATH=lib/linux:\$PATH" >> $SCRIPT
echo "java -classpath resources:lib/$JAR_NAME $MAIN_CLASS \$@" >> $SCRIPT
chmod +x $SCRIPT

# copy any extra files iterspersed with the sources
pushd $DRG_DIR/code/src
for ext in $OTHER_FILE_TYPES ; do
    find . -name "*.$ext" | xargs cp --parents --target-directory $DIST_DIR/resources ;
done
popd

pushd $DRG_DIR/dist
tar czf $DIST_DIR_NAME.tgz $DIST_DIR_NAME
popd

echo "done making distribution in: $DIST_DIR"

