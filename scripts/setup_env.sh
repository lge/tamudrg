#!/bin/sh

# TODO: write script to build classpath by listing .jar's in library directory

# This is just a suggested set of variables you should edit this file
# and include it in your .bashrc file (with this line:
# ". ~/drg/scripts/setup_env.sh").  

# ********************** DO NOT EDIT THIS FILE. ****************************
# Override any of these settings by creating ~/.drg_setup.sh
# ********************** IF YOU DO EDIT THIS FILE: ****************************
# If you do edit it this file and check it in,
# make sure you do not screw up other peoples' settings.

if [ "$OSTYPE" = "linux" -o "$OSTYPE" = "linux-gnu" -o "$OSTYPE" = "Linux" ]; then

    if [ -z "${DRG_DIR}" ] ; then
        export DRG_DIR=${HOME}/drg
    fi
    
    if [ -z "${JAVA_HOME}" ] ; then
        export JAVA_HOME=/afs/csail/groupa/drg/software/linux/jdk/current
    fi
    
    if [ -z "${ANT_HOME}" ] ; then
        export ANT_HOME=$DRG_DIR/ant
    fi

    # if DRG_DONT_CHANGE_PATH is set then do not modify the path or classpath
    if [ -z "$DRG_DONT_CHANGE_PATH" ] ; then
	DRG_PATH=${ANT_HOME}/bin:${DRG_DIR}/scripts:${DRG_DIR}/lib:${DRG_DIR}/lib/javacc2.1/bin
	DRG_PATH=${DRG_DIR}/lib/linux:${DRG_PATH}
	export PATH=${DRG_PATH}:${PATH}:${JAVA_HOME}/bin
	DRG_JARS=`find ${DRG_DIR}/lib/*.jar -name '*.jar' -printf "%p:"`
	export CLASSPATH=${DRG_DIR}/code/classes:${DRG_DIR}/code/src:${DRG_JARS}${CLASSPATH}
    fi

elif [ "$OSTYPE" = "cygwin" ]; then
    if [ -z "${DRG_DIR}" ] ; then
        export DRG_DIR=c:/drg
    fi
    
    if [ -z "${JAVA_HOME}" ] ; then
        echo "You must set JAVA_HOME in your .bashrc file with a line similar to: export JAVA_HOME=\"c:/Program Files/Java/jdk1.5.0_02\""
    fi
    
    if [ -z "${ANT_HOME}" ] ; then
        export ANT_HOME=$DRG_DIR/ant
    fi

    # if DRG_DONT_CHANGE_PATH is set then do not modify the path or classpath
    if [ -z "$DRG_DONT_CHANGE_PATH" ] ; then
	DRG_UDIR=`cygpath -u ${DRG_DIR}`
        ANT_UDIR=`cygpath -u ${ANT_HOME}`

	export PATH=${ANT_UDIR}/bin:${DRG_UDIR}/scripts:${DRG_UDIR}/lib:${DRG_UDIR}/lib/javacc2.1/bin:${PATH}
	export PATH=${DRG_UDIR}/lib/win32:${PATH}

	# when using cygwin java CLASSPATH still needs to be ';' separated
	DRG_JARS=`find ${DRG_DIR}/lib/*.jar -name '*.jar' -printf "%p;"`
	export CLASSPATH="${DRG_DIR}/code/classes;${DRG_DIR}/code/src;${DRG_DIR}/lib;${DRG_JARS}${CLASSPATH}"
    fi
else
    echo "You must use Linux or Cygwin to use the drg setup"
fi


