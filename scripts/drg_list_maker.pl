#!/usr/bin/perl

# This creates:
# "files" that is used by make to obtain the list of current agents
# "rmic.bat" that runs the very speedy dos rmic program over all the agents

$package = shift;

@javaFiles =  <*.java>;
@grammarFiles = <*.gram>;

$filelist = ".dependencies";

open($filelist,">$filelist");

print $filelist "TFILES = ";
foreach (@javaFiles) {
    if ( !( $_ =~ /EHA\.java/ ) )
    {
	print $filelist " \\\n\t$_";
    }
}

print $filelist "\n\n";
print $filelist "GFILES = ";
foreach (@grammarFiles) {
  print $filelist " \\\n\t$_";
}

close $filelist;
