;; $Id: dot-emacs.el,v 1.5 2004/11/11 23:51:24 moltmans Exp $
;; dot-emacs.el -- 
;; Author: Michael Oltmans <moltmans@ai.mit.edu> 
;; Created: <Thu May 31 15:43:17 2001> 
;; Time-stamp: <2004-11-11 17:22:23 moltmans> 

;; This is a .emacs file that you can use to do some basic settings
;; for dealing with java in emacs.  I have made it try to conform to
;; our coding standards where possible (i.e. 2 spaces for tabs).

;; This file will only work on fracas/mystic.  If you are running under
;; windows or on a different machine you need to install and load a
;; bunch of libraries.  Let me know if you need help.

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Java and JDE Stuff
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(defun my-java-mode-hook ()
  ;; Make the tab size 2
  (setq c-basic-offset 2)

  ;; this make sure that braces on their own lines are NOT indented
  ;; e.g.
  ;; if ( foo &&
  ;;      bar )
  ;; {
  ;; And NOT:
  ;; if ( foo &&
  ;;      bar )
  ;;   {
  (c-set-offset 'substatement-open 0)
  
  ;; Uncomment this if you want a newline after each brace
  ; (c-toggle-auto-state 1)
  ;; When in combo with the above line it won't do a newline after
  ;; commas or semicolons just braces
  ; (setq c-hanging-semi&comma-criteria (list (lambda () 'stop)))

  ;; RET does newline and indent instead of just newline
  (define-key java-mode-map "\C-m" 'newline-and-indent)
  ;; Make tab do smart indenting instead of a plain old tab
  (define-key java-mode-map [tab] 'c-indent-command)

  ;; If you use tags you may like these bindings to help with variable
  ;; name completions.
  ;(define-key java-mode-map [C-tab] 'complete-tag)
  ;; C-i is the same as tab so bind it too.
  ;(define-key java-mode-map "\C-i" 'complete-tag)
)
  
;; This activates all the above settings when you open a java file
(add-hook 'java-mode-hook 'my-java-mode-hook)

;; This causes the compile window to scroll as it goes instead of just
;; staying with the top page of output
(setq compilation-scroll-output t)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; Some convenient key bindings for compiles.

(defun my-ant-targeted (target)
  "build the given target"
  (interactive)
  (jde-ant-build (jde-ant-interactive-get-buildfile)
                 target
                 (concat "-Duser.dir=" 
                         (file-truename default-directory))))

(global-set-key [f6]   (lambda () (interactive)
                         (my-ant-targeted "subbuild")))
(global-set-key [f7]   (lambda () (interactive)
                         (my-ant-targeted "subrebuild")))
(global-set-key [(shift f6)] (lambda () (interactive)
                               (my-ant-targeted "build")))
(global-set-key [(shift f7)] (lambda () (interactive)
                               (my-ant-targeted "rebuild")))
(global-set-key [f5]   'jde-ant-build)
(global-set-key [f8]   'previous-error)
(global-set-key [f9]   'next-error)



;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Very clever buffer switching package that lets you find buffers by
;; typing substrings.  There are docs via `C-h iswitchb'
(iswitchb-default-keybindings)

;; since Exceed can't really switch frames correctly stick to the
;; current window
(setq iswitchb-default-method 'samewindow)
(setq iswitchb-regexp t)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; makes tab completion ignore case.
(setq completion-ignore-case t)

(custom-set-variables
 ;; Lots of colors are always good.  :-)
 '(global-font-lock-mode t nil (font-lock))

 ;; make the mini buffer fit all the text
 '(resize-minibuffer-mode t nil (rsz-mini))
 '(resize-minibuffer-window-exactly t)

 ;; show matching parens and braces
 '(show-paren-style (quote parenthesis))
 '(show-paren-mode t nil (paren))
)
