#! /bin/csh

setenv DRG_JARS `find ${DRG_DIR}/lib/*.jar -name '*.jar' -printf "%p:"`
setenv CLASSPATH ${DRG_DIR}/code/classes:${DRG_DIR}/code/src:${DRG_DIR}/lib:${DRG_JARS}

