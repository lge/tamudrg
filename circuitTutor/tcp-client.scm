(define port-num 12345)

(define (connect pnum)
  (open-tcp-stream-socket "localhost" pnum))

(define env (the-environment))

(define (repl io-port)
  (if (not (null? io-port))
      (let* ((input (read-line io-port))
	     (output (eval (string->expression input) env)))
	;(display "input ")
	;(pp input)
	(display "output ")
	(pp output)
	(write-line output io-port)
	(flush-output io-port)
	(if (not (eq? input 'EXIT))
	    (repl io-port)
	    (begin (pp 'donedonedone) (close-port io-port)))
	)))
		   
	

(define (string->expression str)
  (let ((result
	 (with-input-from-string str
	   (lambda ()
	     (let lp ((x (read)))
	       (if (eof-object? x)
		   '()
		   (cons x (lp (read)))))))))
    (if (= (length result) 1)
	(car result)
	result)))

(define io (connect port-num))

(repl io)









