// (c) MIT 2003.  All rights reserved.

////////////////////////////////////////////////////////////////////////////////
//                                                                            //
// AUTHOR:      Tevfik Metin Sezgin                                           //
//              Massachusetts Institute of Technology                         //
//              Department of Electrical Engineering and Computer Science     //
//              Artificial Intelligence Laboratory                            //
//                                                                            //
// E-MAIL:        mtsezgin@ai.mit.edu, mtsezgin@mit.edu                       //
//                                                                            //
// COPYRIGHT:   Tevfik Metin Sezgin                                           //
//              All rights reserved. This code can not be copied, modified,   //
//              or distributed in whole or partially without the written      //
//              permission of the author. Also see the COPYRIGHT file.        //
//                                                                            //
////////////////////////////////////////////////////////////////////////////////
package edu.mit.sketch.geom;

/** 
  * 
  * See the end of the file for the log of changes.
  * 
  * $Author: calvarad $
  * $Date: 2003/06/26 19:57:14 $   
  * $Revision: 1.6 $
  * $Headers$
  * $Id: GeometricObject.java,v 1.6 2003/06/26 19:57:14 calvarad Exp $     
  * $Name:  $   
  * $Locker:  $
  * $Source: /projects/drg/CVSROOT/drg/code/src/edu/mit/sketch/geom/GeometricObject.java,v $
  *  
  **/


import edu.mit.sketch.grammar.Terminal;

import java.awt.Graphics;

/**
  *
  * All the objects who want to be Geometric must implement this
  * interface.
  *
  **/

public interface GeometricObject
extends          Terminal,
                 Translatable
{
    /**
    *
    * Available GeometricObject types.
    *
    **/
    public static final int RECTANGLE = 0;
    public static final int SQUARE    = 1;
    public static final int ELLIPSE   = 2;
    public static final int CIRCLE    = 3;
    public static final int POLYGON   = 4;
    public static final int LINE      = 5;
    public static final int POINT     = 6;

    /**
    *
    * This method should return a polygon that fully contains the 
    * current object. The polygon is implicity closed and the last 
    * point doesn't necessarily have to be the same as the first 
    * (zeroth) point.
    *
    **/
    public abstract Polygon 
    getPolygonalBounds();

    /**
    *
    * This method should return true if the input object touches
    * this geometric object.
    *
    **/
    public abstract boolean
    touches( GeometricObject object );

    /**
    *
    * This method should return the spatial relation of the input
    * parameter with respect to this object. see the SpatialRelation
    * class for a detailed list of possible spatial relations.
    * Another version of this method should be implemented for 
    * handling spatial relations where a rotated coordinate
    * system is to be used.
    *
    **/
    public abstract int
    spatialRelation( GeometricObject object );

    /**
    *
    * This method should return the Rectangular bounds of the object.
    *
    **/
    public abstract Rectangle
    getRectangularBounds();
    
    /**
    *
    * Returns false if the argument is not completely inside 
    * this object. Return true O/W.
    *
    **/
    public abstract boolean
    containsGeometricObject( GeometricObject object );
    
    /**
    *
    * Returns false if the objects in the input array are
    * completely inside this object. Return true O/W.
    *
    **/
    public abstract boolean
    containsGeometricObjects( GeometricObject objects[] );
    
    /**
    *
    * This method is used to set the original data points that
    * forms this GeometricObject
    *
    **/
    public abstract void
    setDataPoints( Polygon points );
    
    /**
    *
    * This method is used to get the original data points that
    * forms this GeometricObject
    *
    **/
    public abstract Polygon
    getDataPoints();

  /**
   *  This method is used to get the original data points that
   * form the geometric object in their vertex form.
   */
  public abstract Vertex[] getOriginalVertices();

  public abstract void setOriginalVertices( Vertex[] pts );
  
  /**
   * This method copies the geometric object (includeing the
   * underlying data and returns it.
   **/
    public abstract GeometricObject
    copy() ;
  
}


/** 
  * 
  * $Log: GeometricObject.java,v $
  * Revision 1.6  2003/06/26 19:57:14  calvarad
  * Lots of bug fixes
  *
  * Revision 1.5  2003/03/06 01:08:49  moltmans
  * Added copyright to all the files.
  *
  * Revision 1.4  2002/08/14 18:35:56  moltmans
  * Added a copy method to Geometric primitives so that we can make deep
  * copies of objects.  The copy method is careful to not reuse any data.
  *
  * Revision 1.3  2001/12/07 20:05:02  moltmans
  * General cosmetic changes and typo fixes to various files.
  *
  * Revision 1.2  2001/11/23 03:23:30  mtsezgin
  * Major reorganization.
  *
  * Revision 1.1.1.1  2001/03/29 16:25:00  moltmans
  * Initial directories for DRG
  *
  * Revision 1.19  2000/09/06 22:40:36  mtsezgin
  * Combinations of curves and polygons are successfully approximated
  * by straight lines and Bezier curves as appropriate. System works
  * quite reliably.
  *
  * Revision 1.13  2000/06/08 03:15:54  mtsezgin
  *
  * Translation added to the interface.
  *
  * Revision 1.12  2000/06/03 01:52:32  mtsezgin
  * *** empty log message ***
  *
  * Revision 1.11  2000/05/03 23:26:45  mtsezgin
  * *** empty log message ***
  *
  * Revision 1.10  2000/04/28 04:45:03  mtsezgin
  *
  * Now each GeometricObject keeps the mouse input that was previously
  * discarded. User can switch between seeing the recognized mode and
  * the raw mode. setDataPoints( Polygon points ) and getDataPoints()
  * are added to GeometricObject, and all the implementors are modified
  * accordingly.
  *
  * Revision 1.9  2000/04/25 22:13:55  mtsezgin
  *
  * The getBounds changed to getRectangularBounds which returns a Rectangle.
  *
  * Revision 1.8  2000/04/13 06:24:07  mtsezgin
  *
  * The current version of the program recognized Crosses, and Shades.
  * Implementors of Terminal and their descendants were modified to
  * implement the changes in GeometricObject.
  *
  * Revision 1.7  2000/04/11 00:41:46  mtsezgin
  *
  * Now the whole package succesfully parses a motor.
  *
  * Revision 1.6  2000/04/07 04:28:54  mtsezgin
  *
  * Added Rotatable interface. Rectangle and Line are Rotatable for now, but
  * Rectangle should be modified to have an angle field. Also other rotatable
  * classes should also implement Rotatable interface.
  *
  * Revision 1.5  2000/04/06 21:33:42  mtsezgin
  *
  * Spatial relation represents 9 basic relations for now. It should be extended.
  *
  * GeometricObject is extended. Implementors extended with empty stubs.
  *
  * Revision 1.4  2000/04/06 19:16:23  mtsezgin
  *
  * Modified all the classes to use my Point class which extends java.awt.Point
  * instead of directly using java.awt.Point
  *
  * Revision 1.3  2000/04/01 22:51:21  mtsezgin
  *
  * Started implementation. Also from now on using jdk 1.2
  *
  * Revision 1.2  2000/04/01 20:34:02  mtsezgin
  *
  * Renamed Oval.java to Ellipse.java
  *
  * Revision 1.1.1.1  2000/04/01 03:07:07  mtsezgin
  * Imported sources
  *
  * Revision 1.3  2000/03/31 22:41:03  mtsezgin
  *
  * Started Log tracking.
  *
  *  
  **/
