// (c) MIT 2003.  All rights reserved.

////////////////////////////////////////////////////////////////////////////////
//                                                                            //
// AUTHOR:      Tevfik Metin Sezgin                                           //
//              Massachusetts Institute of Technology                         //
//              Department of Electrical Engineering and Computer Science     //
//              Artificial Intelligence Laboratory                            //
//                                                                            //
// E-MAIL:        mtsezgin@ai.mit.edu, mtsezgin@mit.edu                       //
//                                                                            //
// COPYRIGHT:   Tevfik Metin Sezgin                                           //
//              All rights reserved. This code can not be copied, modified,   //
//              or distributed in whole or partially without the written      //
//              permission of the author. Also see the COPYRIGHT file.        //
//                                                                            //
////////////////////////////////////////////////////////////////////////////////
package edu.mit.sketch.geom;


/** 
  * 
  * See the end of the file for the log of changes.
  * 
  * $Author: moltmans $
  * $Date: 2003/03/06 01:08:49 $   
  * $Revision: 1.2 $
  * $Headers$
  * $Id: Range.java,v 1.2 2003/03/06 01:08:49 moltmans Exp $     
  * $Name:  $   
  * $Locker:  $
  * $Source: /projects/drg/CVSROOT/drg/code/src/edu/mit/sketch/geom/Range.java,v $
  *  
  **/

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;

import java.io.Serializable;

import java.util.Comparator;
import java.util.Vector;


/**
  *
  * This class represents a Point described by a 
  * java.awt.geom.Line2D.Double
  *
  **/
public
class      Range
implements Comparator
{
    /**
    *
    * Lower bound of the range
    *
    **/
    public int min;
    

    /**
    *
    * Upper bound of the range
    *
    **/
    public int max;


    /**
    *
    * The constructor.
    *
    **/    
  public Range()
    {
    }


    /**
    *
    * The constructor.
    *
    **/    
  public Range( int min, int max )
    {
        this.min = min;
        this.max = max;
    }
    
    
    /**
    *
    * Override toString
    *
    **/    
    public String
    toString()
    {
        return "Range [" + min + ", " + max + "] ";
    }

    
    /**
    *
    * Returns an array containing the elements of the Vector.
    *
    **/    
    public static Range[]
    vectorToArray( Vector ranges )
    {
        Range result[] = new Range[ranges.size() ];
        for ( int i=0; i< result.length; i++ ) {
            result[i] = (Range)( ranges.elementAt( i ) );
        }
        
           return result;
    }
    

    /**
    *
    * Implement comparator
    *
    * Warning: The ranges should be non-overlapping for proper
    * sorting.
    *
    **/    
    public int 
    compare( Object o1, Object o2 )
    {
        if ( ((Range)o1).max < ((Range)o2).min ) {
            return -1;
        } 
        
        if ( ((Range)o1).max > ((Range)o2).min ) {
            return -1;
        }
        
        if ( ( ((Range)o1).max == ((Range)o2).min ) &&
             ( ((Range)o2).max == ((Range)o1).min ) ) {
            return 0;
        }
        
        return -1;        
    }
    

    /**
    *
    * Implement comparator
    *
    * Warning: The ranges should be non-overlapping for proper
    * sorting.
    *
    **/    
    public boolean
    equals( Object o1 )
    {
        return ( ((Range)o1).max == max ) && ( ((Range)o1).min == min );
    }
    

    /**
    *
    * Append the ranges with the input range
    *
    **/    
    public static Range[]
    appendRanges( Range ranges[], Range range )
    {
        Range result[] = new Range[ranges.length+1];
        
        int offset = 0;
        for ( int i=0; i<ranges.length; i++ ) {
            if ( ( range.max <= ranges[i].min ) && ( offset == 0 ) ) {
                offset = 1;
                result[i] = range;
            }
            result[i+offset] = ranges[i];
        }
        
        if ( offset == 0 ) {
            result[result.length-1] = range;
        }
        
        return result;
    }
    
    
    /**
    *
    * For obtaining a complete copy of an array of Ranges objects.
    *
    **/    
    public static Range[]
    cloneRanges( Range ranges[] )
    {
        Range copy[] = new Range[ranges.length];
        for ( int i=0; i< ranges.length; i++ ) {
            copy[i] = new Range( ranges[i].min, ranges[i].max );
        }
        
        return copy;
    }
}
/** 
  * 
  * $Log: Range.java,v $
  * Revision 1.2  2003/03/06 01:08:49  moltmans
  * Added copyright to all the files.
  *
  * Revision 1.1.1.1  2001/03/29 16:25:00  moltmans
  * Initial directories for DRG
  *
  * Revision 1.18  2000/09/06 22:40:51  mtsezgin
  * Combinations of curves and polygons are successfully approximated
  * by straight lines and Bezier curves as appropriate. System works
  * quite reliably.
  *
  * Revision 1.12  2000/06/08 03:14:30  mtsezgin
  *
  * Made the class Serializable for supporting saving and loading
  * designs. Both the object attributes, and the original data points
  * are stored and restored.
  *
  * Revision 1.11  2000/06/03 01:52:34  mtsezgin
  * *** empty log message ***
  *
  * Revision 1.10  2000/05/03 23:26:46  mtsezgin
  * *** empty log message ***
  *
  * Revision 1.9  2000/04/28 04:45:04  mtsezgin
  *
  * Now each GeometricObject keeps the mouse input that was previously
  * discarded. User can switch between seeing the recognized mode and
  * the raw mode. setDataPoints( Polygon points ) and getDataPoints()
  * are added to GeometricObject, and all the implementors are modified
  * accordingly.
  *
  * Revision 1.8  2000/04/25 22:18:57  mtsezgin
  *
  * The getBounds changed to getRectangularBounds which returns a Rectangle.
  *
  * Revision 1.7  2000/04/17 07:02:30  mtsezgin
  *
  * Finally made the Rectangle really rotatable.
  *
  * Revision 1.6  2000/04/13 06:24:08  mtsezgin
  *
  * The current version of the program recognized Crosses, and Shades.
  * Implementors of Terminal and their descendants were modified to
  * implement the changes in GeometricObject.
  *
  * Revision 1.5  2000/04/11 01:41:46  mtsezgin
  * *** empty log message ***
  *
  * Revision 1.4  2000/04/11 00:41:47  mtsezgin
  *
  * Now the whole package succesfully parses a motor.
  *
  * Revision 1.3  2000/04/07 04:28:54  mtsezgin
  *
  * Added Rotatable interface. Rectangle and Line are Rotatable for now, but
  * Rectangle should be modified to have an angle field. Also other rotatable
  * classes should also implement Rotatable interface.
  *
  *
  **/
