// (c) MIT 2003.  All rights reserved.

////////////////////////////////////////////////////////////////////////////////
//                                                                            //
// AUTHOR:      Tevfik Metin Sezgin                                           //
//              Massachusetts Institute of Technology                         //
//              Department of Electrical Engineering and Computer Science     //
//              Artificial Intelligence Laboratory                            //
//                                                                            //
// E-MAIL:        mtsezgin@ai.mit.edu, mtsezgin@mit.edu                       //
//                                                                            //
// COPYRIGHT:   Tevfik Metin Sezgin                                           //
//              All rights reserved. This code can not be copied, modified,   //
//              or distributed in whole or partially without the written      //
//              permission of the author. Also see the COPYRIGHT file.        //
//                                                                            //
////////////////////////////////////////////////////////////////////////////////
package edu.mit.sketch.geom;


/** 
  * 
  * See the end of the file for the log of changes.
  * 
  * $Author: moltmans $
  * $Date: 2003/03/06 01:08:49 $   
  * $Revision: 1.5 $
  * $Headers$
  * $Id: GeneralPathProcessor.java,v 1.5 2003/03/06 01:08:49 moltmans Exp $     
  * $Name:  $   
  * $Locker:  $
  * $Source: /projects/drg/CVSROOT/drg/code/src/edu/mit/sketch/geom/GeneralPathProcessor.java,v $
  *  
  **/

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Shape;

import java.awt.geom.AffineTransform;
import java.awt.geom.CubicCurve2D;
import java.awt.geom.FlatteningPathIterator;
import java.awt.geom.Line2D;
import java.awt.geom.PathIterator;
import java.awt.geom.QuadCurve2D;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;

import java.util.ArrayList;

import edu.mit.sketch.toolkit.StrokeData;
import edu.mit.sketch.util.AWTUtil;
import edu.mit.sketch.ui.Tablet;

import edu.mit.sketch.util.Util;

/**
  *
  * Class for converting a general path object to 
  * a symbolic representation
  *
  **/
public
class GeneralPathProcessor
{
     /**
    *
    * Converts a general path object to a symbolic representation
    *
    **/    
    public static final int MAX_FEATURES = 36*36*36; 


     /**
    *
    * Converts a general path object to a symbolic representation
    *
    **/    
    public static final int ALPHABET_SIZE = 36;


   /**
    *
    * Computes the String representation of the features for the
    * input GeneralPath
    *
    **/    
    public static String
    featureString( GeneralPath general_path )
    {
		int       symbolic[] = GeneralPathProcessor.toSymbolic( general_path );
        ArrayList features   = GeneralPathProcessor.computeFeatureVector( 
                        		   symbolic );
		
		Util.printArrayConcisely( symbolic, "stroke" );
		
        String result = "";
        
        for ( int i=0; i<features.size(); i+=2 ) {
            result += (((Integer)features.get(i)).intValue()+1) + ":" +
                      ((Double)features.get(i+1)).intValue() + " ";
        }
        
        return result;
    }


     /**
    *
    * Converts a general path object to a symbolic representation
    *
    **/    
    public static int[]
    toSymbolic( GeneralPath general_path )
    {
        ArrayList symbols = new ArrayList();
        PathIterator path_iterator;
        path_iterator = general_path.getPathIterator( new AffineTransform() );
        
        int   type           = 0;
        float last_x         = 0;
        float last_y         = 0;
        float coefficients[] = new float[6];
        while ( !path_iterator.isDone() ) {
            type = path_iterator.currentSegment( coefficients );
        
            switch ( type ) {
                case PathIterator.SEG_CUBICTO :
                    // symbols.add( new Integer( 1000 ) );
					/*
                    symbols.addAll( 
                        convertCubic( last_x, last_y, coefficients, 4 ) );
						*/
                    last_x = coefficients[4];
                    last_y = coefficients[5];
                    break;
                case PathIterator.SEG_QUADTO :
                    last_x = coefficients[2];
                    last_y = coefficients[3];
                    break;
                case PathIterator.SEG_LINETO :
                    symbols.addAll( 
                        convertLine( last_x, last_y, coefficients, 4 ) );
                    last_x = coefficients[0];
                    last_y = coefficients[1];
                    break;
                case PathIterator.SEG_MOVETO :
                    last_x = coefficients[0];
                    last_y = coefficients[1];
                    break;
                case PathIterator.SEG_CLOSE :
                    last_x = coefficients[0];
                    last_y = coefficients[1];
                    break;
                default:
                    System.out.println( "Error in converting "   +
                                        "GeneralPath to symbolic " + 
                                        "representation no matching case..." );
            }
            path_iterator.next();
        }

        int result[] = new int[symbols.size()];
         for ( int i=0; i<result.length; i++ ) {
            result[i] = ((Integer)symbols.get(i)).intValue();
        }
        
        return result;
    }


   /**
    *
    * Compute the feature vector 
    *
    **/    
    public static ArrayList
    computeFeatureVector( int input[] )
    {        
        ArrayList result = new ArrayList();
        
        int stem[]    = new int[3];
        int occurence = 0;
        int feature   = 0;
        
        for ( stem[0] = 0; stem[0]<ALPHABET_SIZE; stem[0]++ ) {
            for ( stem[1] = 0; stem[1]<ALPHABET_SIZE; stem[1]++ ) {
                for ( stem[2] = 0; stem[2]<ALPHABET_SIZE; stem[2]++ ) {
//					if ( ( stem[0] == stem[1] ) &&
//						 ( stem[1] == stem[2] ) )
//					continue;
					 
                    occurence = stemOccurence( input, stem );
                    if ( occurence != 0 ) {
                        result.add( new Integer( feature  ) );
                        result.add( new Double( occurence ) );
                    }
                    feature++;
                }
            }
        }
         
        return result;
    }


   /**
    *
    * Compute the number of times the stem occurs in input 
    *
    **/    
    public static int
    stemOccurence( int input[], int stem[] )
    {
        int occurence = 0;

        for ( int i=0; i<input.length-stem.length+1; i++ ) {
            for ( int j=0; j<stem.length; j++ ) {
                if ( input[i+j] == stem[j] ) {
                    if ( j == stem.length-1 ) {
                        occurence++;
                    }
                } else {
                    break;
                }
            }
        }
        
        return occurence;
     }
    
    
   /**
    *
    * Converts a line to a symbolic representation
    *
    **/    
    public static ArrayList
    convertLine( float begin_x, 
                 float begin_y, 
                 float coefficients[], 
                 int   length_per_symbol )
    {
        double dx     = coefficients[0] - begin_x;
        double dy     = coefficients[1] - begin_y;
        double radian = Math.atan2( dy, dx );
        
        int    degree = (int)(radian/Math.PI*180) + 180; 
        double length = Math.sqrt( dx*dx + dy*dy );
        
        ArrayList result = new ArrayList();
        
        for ( int i=0; i<((int)length)/length_per_symbol; i++ ) {
            result.add( new Integer( degree/18 ) );
        }
         
        return result;
    }
    
    
   /**
    *
    * Converts a cubic spline to a symbolic representation
    *
    **/    
    public static ArrayList
    convertCubic( float begin_x, 
                  float begin_y, 
                  float coefficients[],
                  int   length_per_symbol )
    {

        Shape        shape            = null;
        Point        last_point       = new Point();

        shape = new CubicCurve2D.Double( last_point.x,
                                         last_point.y,
                                         coefficients[0],
                                         coefficients[1],
                                         coefficients[2],
                                         coefficients[3],
                                         coefficients[4],
                                         coefficients[5] );
        last_point.setLocation( coefficients[4], coefficients[5] );

        Vertex approximation[] = AWTUtil.flatten( shape,
                                                  0.001,
                                                  8 );
                                                  
        ArrayList samples = new ArrayList();
    
        samples.add( approximation[0] );

        double segment_length = 0.0;
        for ( int i=1; i<approximation.length; i++ ) {
            segment_length += approximation[i-1].distance( approximation[i] );
            if ( segment_length > length_per_symbol ) {
                samples.add( approximation[i] );
            }
        }
        samples.add( approximation[approximation.length-1] );
        
                
        double dx     = 0.0;
        double dy     = 0.0;
        double radian = 0.0;
        int    degree = 0;
        
        double    length = Math.sqrt( dx*dx + dy*dy );
        ArrayList result = new ArrayList();
                
        for ( int i=0; i<samples.size()-1; i++ ) {
            dx     = ((Vertex)samples.get(i+1)).x - 
                     ((Vertex)samples.get(i)).x;
            dy     = ((Vertex)samples.get(i+1)).y - 
                     ((Vertex)samples.get( i )).y;
        
            radian = Math.atan2( dy, dx );
            degree = (int)(radian/Math.PI*180) + 180;
            result.add( new Integer( degree/10 + 36 ) );
        }
         
        return result;
    }
}

/** 
  * 
  * $Log: GeneralPathProcessor.java,v $
  * Revision 1.5  2003/03/06 01:08:49  moltmans
  * Added copyright to all the files.
  *
  * Revision 1.4  2001/11/23 03:23:30  mtsezgin
  * Major reorganization.
  *
  * Revision 1.3  2001/10/18 19:48:47  mtsezgin
  * *** empty log message ***
  *
  * Revision 1.2  2001/10/12 22:26:27  mtsezgin
  * This is a general commit of all files.
  *
  * Revision 1.1  2001/06/22 19:54:13  mtsezgin
  * *** empty log message ***
  *
  * Revision 1.2  2001/04/12 19:25:55  mtsezgin
  * *** empty log message ***
  *
  * Revision 1.1.1.1  2001/03/29 16:25:00  moltmans
  * Initial directories for DRG
  *
  *  
  **/
