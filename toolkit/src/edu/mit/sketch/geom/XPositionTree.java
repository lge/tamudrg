// (c) MIT 2003.  All rights reserved.

////////////////////////////////////////////////////////////////////////////////
//                                                                            //
// AUTHOR:      Tevfik Metin Sezgin                                           //
//              Massachusetts Institute of Technology                         //
//              Department of Electrical Engineering and Computer Science     //
//              Artificial Intelligence Laboratory                            //
//                                                                            //
// E-MAIL:        mtsezgin@ai.mit.edu, mtsezgin@mit.edu                       //
//                                                                            //
// COPYRIGHT:   Tevfik Metin Sezgin                                           //
//              All rights reserved. This code can not be copied, modified,   //
//              or distributed in whole or partially without the written      //
//              permission of the author. Also see the COPYRIGHT file.        //
//                                                                            //
////////////////////////////////////////////////////////////////////////////////
package edu.mit.sketch.geom;

/** 
  * 
  * See the end of the file for the log of changes.
  * 
  * $Author: moltmans $
  * $Date: 2003/03/06 01:08:49 $   
  * $Revision: 1.3 $
  * $Headers$
  * $Id: XPositionTree.java,v 1.3 2003/03/06 01:08:49 moltmans Exp $     
  * $Name:  $   
  * $Locker:  $
  * $Source: /projects/drg/CVSROOT/drg/code/src/edu/mit/sketch/geom/XPositionTree.java,v $
  *  
  **/


import java.io.Serializable;

import java.util.ArrayList;
import java.util.SortedSet;
import java.util.TreeSet;

/**
  *
  * This class will serve as data structure that keeps the positions
  * of objects. It also supports some spatial queries.
  *
  **/
public
class      XPositionTree
implements Serializable
{
    /**
    *
    * The tree that holds the points sorted by their y positions
    *
    **/
    private TreeSet x_position_tree;
    

    /**
    *
    * The constructor.
    *
    **/    
	public 
	XPositionTree()
    {
		x_position_tree = new TreeSet( new PointXComparator() );
    }
     
         
    /**
    *
    * Add a point
    *
    **/    
    public void
    addPoint( Point point )
    {
        x_position_tree.add( point );
    }
     
         
    /**
    *
    * Add a point
	* assumes x1 < x2, y1 < y2
    *
    **/    
    public ArrayList
    pointsInside( int x1, int y1, int x2, int y2 )
    {
        ArrayList result = new ArrayList();
		SortedSet filtered_by_x = 
			x_position_tree.subSet( new Point( x1, y1 ), 
									new Point( x2, y2 ) );
		
		Object points[] = filtered_by_x.toArray();
		
		for ( int i=0; i<points.length; i++ ) {
			if ( ((Point)points[i]).y > y1 && ((Point)points[i]).y < y2 ) {
				result.add( (Point)points[i] );
			}
		}
		
		return result;
    }
}

/** 
  * 
  * $Log: XPositionTree.java,v $
  * Revision 1.3  2003/03/06 01:08:49  moltmans
  * Added copyright to all the files.
  *
  * Revision 1.2  2002/03/01 22:17:56  mtsezgin
  * *** empty log message ***
  *
  * Revision 1.1  2002/02/26 02:33:39  mtsezgin
  * Started on the spatial query infrastructure. It is not as efficient
  * as I want it to be yet.
  *
  *  
  **/
