// (c) MIT 2003.  All rights reserved.

////////////////////////////////////////////////////////////////////////////////
//                                                                            //
// AUTHOR:      Tevfik Metin Sezgin                                           //
//              Massachusetts Institute of Technology                         //
//              Department of Electrical Engineering and Computer Science     //
//              Artificial Intelligence Laboratory                            //
//                                                                            //
// E-MAIL:        mtsezgin@ai.mit.edu, mtsezgin@mit.edu                       //
//                                                                            //
// COPYRIGHT:   Tevfik Metin Sezgin                                           //
//              All rights reserved. This code can not be copied, modified,   //
//              or distributed in whole or partially without the written      //
//              permission of the author. Also see the COPYRIGHT file.        //
//                                                                            //
////////////////////////////////////////////////////////////////////////////////
package edu.mit.sketch.geom;


/** 
  * 
  * See the end of the file for the log of changes.
  * 
  * $Author: moltmans $
  * $Date: 2003/03/06 01:08:49 $   
  * $Revision: 1.3 $
  * $Headers$
  * $Id: Direction.java,v 1.3 2003/03/06 01:08:49 moltmans Exp $     
  * $Name:  $   
  * $Locker:  $
  * $Source: /projects/drg/CVSROOT/drg/code/src/edu/mit/sketch/geom/Direction.java,v $
  *  
  **/


/**
  *
  * This class lists the direction that an object's bounding box may
  * have.
  *
  **/

class Direction
{
    /**
    *             
    *             |   |
    *             |   |
    *          ___|___|___
    *             |   |
    *          XX | X | XX
    *          ___|___|___
    *             |   |
    *             |   |
    *             |   |
    *
    **/
    public static final int LEFT_RIGHT  = 0;

    /**
    *             
    *             | X |
    *             | X |
    *          ___|___|___
    *             |   |
    *             | X |   
    *          ___|___|___
    *             |   |
    *             | X |
    *             | X |
    *
    **/
    public static final int ABOVE_BELOW = 1;
    
    public static String
    toString( int relation )
    {
        switch ( relation )
        {

            case LEFT_RIGHT  : return "left_right";
            case ABOVE_BELOW : return "above_below";
            default          : return "unknown relation";
        }
    }
}


/** 
  * 
  * $Log: Direction.java,v $
  * Revision 1.3  2003/03/06 01:08:49  moltmans
  * Added copyright to all the files.
  *
  * Revision 1.2  2002/08/06 15:37:30  mtsezgin
  * Added static serialVersionUID information to the serializable geometric objects,so that serialization doesn't break unless modifications made to the class are truly incompatible changes.
  *
  * Revision 1.1.1.1  2001/03/29 16:25:00  moltmans
  * Initial directories for DRG
  *
  * Revision 1.8  2000/09/06 22:40:34  mtsezgin
  * Combinations of curves and polygons are successfully approximated
  * by straight lines and Bezier curves as appropriate. System works
  * quite reliably.
  *
  * Revision 1.2  2000/06/03 01:52:31  mtsezgin
  * *** empty log message ***
  *
  * Revision 1.1  2000/04/20 03:59:52  mtsezgin
  * *** empty log message ***
  *
  * Revision 1.2  2000/04/12 04:00:16  mtsezgin
  * *** empty log message ***
  *
  * Revision 1.1  2000/04/06 21:33:43  mtsezgin
  *
  * Spatial relation represents 9 basic relations for now. It should be extended.
  *
  * GeometricObject is extended. Implementors extended with empty stubs.
  *
  * Revision 1.3  2000/04/06 19:16:24  mtsezgin
  *
  * Modified all the classes to use my Point class which extends java.awt.Point
  * instead of directly using java.awt.Point
  *
  * Revision 1.2  2000/04/01 22:51:22  mtsezgin
  *
  * Started implementation. Also from now on using jdk 1.2
  *
  * Revision 1.1.1.1  2000/04/01 03:07:07  mtsezgin
  * Imported sources
  *
  * Revision 1.5  2000/03/31 22:41:04  mtsezgin
  *
  * Started Log tracking.
  *
  *  
  **/
