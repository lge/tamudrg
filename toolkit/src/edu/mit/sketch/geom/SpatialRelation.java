// (c) MIT 2003.  All rights reserved.

////////////////////////////////////////////////////////////////////////////////
//                                                                            //
// AUTHOR:      Tevfik Metin Sezgin                                           //
//              Massachusetts Institute of Technology                         //
//              Department of Electrical Engineering and Computer Science     //
//              Artificial Intelligence Laboratory                            //
//                                                                            //
// E-MAIL:        mtsezgin@ai.mit.edu, mtsezgin@mit.edu                       //
//                                                                            //
// COPYRIGHT:   Tevfik Metin Sezgin                                           //
//              All rights reserved. This code can not be copied, modified,   //
//              or distributed in whole or partially without the written      //
//              permission of the author. Also see the COPYRIGHT file.        //
//                                                                            //
////////////////////////////////////////////////////////////////////////////////
package edu.mit.sketch.geom;

/** 
  * 
  * See the end of the file for the log of changes.
  * 
  * $Author: moltmans $
  * $Date: 2003/10/13 19:46:36 $   
  * $Revision: 1.5 $
  * $Headers$
  * $Id: SpatialRelation.java,v 1.5 2003/10/13 19:46:36 moltmans Exp $     
  * $Name:  $   
  * $Locker:  $
  * $Source: /projects/drg/CVSROOT/drg/code/src/edu/mit/sketch/geom/SpatialRelation.java,v $
  *  
  **/


/**
  *
  * This class lists the binary spatial relations that two objects
  * may have
  *
  **/

public
class SpatialRelation
{
    /**
    * <pre>
    *             
    *             |   |
    *           X |   |
    *          ___|___|___
    *             |   |
    *             |   |
    *          ___|___|___
    *             |   |
    *             |   |
    *             |   |
    * </pre>
    *
    **/
    public static final int UPPER_LEFT  = 0;


    /**
    * <pre>
    *             
    *             |   |
    *             | X |
    *          ___|___|___
    *             |   |
    *             |   |
    *          ___|___|___
    *             |   |
    *             |   |
    *             |   |
    * </pre>
    *
    **/
    public static final int ABOVE       = 1;


    /**
    * <pre>
    *             |   |
    *             |   | X
    *          ___|___|___
    *             |   |
    *             |   |
    *          ___|___|___
    *             |   |
    *             |   |
    *             |   |
    * </pre>
    *
    **/
    public static final int UPPER_RIGHT = 2;


    /**
    *             
    * <pre>
    *             |   |
    *             |   | 
    *          ___|___|___
    *             |   |
    *           X |   |
    *          ___|___|___
    *             |   |
    *             |   |
    *             |   |
    * </pre>
    *
    **/
    public static final int LEFT        = 3;


    /**
    *             
    * <pre>
    *             |   |
    *             |   | 
    *          ___|___|___
    *             |   |
    *             | X |
    *          ___|___|___
    *             |   |
    *             |   |
    *             |   |
    * </pre>
    *
    **/
    public static final int COCENTRIC   = 4;


    /**
    *             
    * <pre>
    *             |   |
    *             |   | 
    *          ___|___|___
    *             |   |
    *             |   | X
    *          ___|___|___
    *             |   |
    *             |   |
    *             |   |
    * </pre>
    *
    **/
    public static final int RIGHT       = 5;

    /**
    *             
    * <pre>
    *             |   |
    *             |   |
    *          ___|___|___
    *             |   |
    *             |   |
    *          ___|___|___
    *             |   |
    *           X |   |
    *             |   |
    * </pre>
    *
    **/
    public static final int LOWER_LEFT  = 6;


    /**
    *             
    * <pre>
    *             |   |
    *             |   |
    *          ___|___|___
    *             |   |
    *             |   |
    *          ___|___|___
    *             |   |
    *             | X |
    *             |   |
    * </pre>
    *
    **/
    public static final int BELOW       = 7;


    /**
    * <pre>
    *             
    *             |   |
    *             |   |
    *          ___|___|___
    *             |   |
    *             |   |
    *          ___|___|___
    *             |   |
    *             |   | X
    *             |   |
    * </pre>
    *
    **/
    public static final int LOWER_RIGHT = 8;
    
    
    /**
    * To String
    **/
    public static String
    toString( int relation )
    {
        switch ( relation )
        {

            case UPPER_LEFT  : return "upper_left";
            case ABOVE       : return "above";
            case UPPER_RIGHT : return "upper_right";
            case LEFT         : return "left";
            case COCENTRIC   : return "cocentric";
            case RIGHT       : return "right";
            case LOWER_LEFT  : return "lower_left";
            case BELOW       : return "below";
            case LOWER_RIGHT : return "lower_right";
            default             : return "unknown relation";
        }
    }
    
    
    /**
    * To String
    **/
    public static int
    cartesian2screen( int relation )
    {
        switch ( relation )
        {

            case UPPER_LEFT  : return LOWER_LEFT;
            case ABOVE       : return BELOW;
            case UPPER_RIGHT : return LOWER_RIGHT;
            case LEFT         : return LEFT;
            case COCENTRIC   : return COCENTRIC;
            case RIGHT       : return RIGHT;
            case LOWER_LEFT  : return UPPER_LEFT;
            case BELOW       : return ABOVE;
            case LOWER_RIGHT : return UPPER_RIGHT;
            default             : return relation;
        }
    }
    
    
    /**
    * To String
    **/
    public static int
    screen2cartesian( int relation )
    {
        switch ( relation )
        {

            case UPPER_LEFT  : return LOWER_LEFT;
            case ABOVE       : return BELOW;
            case UPPER_RIGHT : return LOWER_RIGHT;
            case LEFT         : return LEFT;
            case COCENTRIC   : return COCENTRIC;
            case RIGHT       : return RIGHT;
            case LOWER_LEFT  : return UPPER_LEFT;
            case BELOW       : return ABOVE;
            case LOWER_RIGHT : return UPPER_RIGHT;
            default             : return relation;
        }
    }
    
    
    /**
    * To String
    **/
    public static int
    getComplement( int relation )
    {
        switch ( relation )
        {

            case UPPER_LEFT  : return LOWER_RIGHT;
            case ABOVE       : return BELOW;
            case UPPER_RIGHT : return LOWER_LEFT;
            case LEFT         : return RIGHT;
            case COCENTRIC   : return COCENTRIC;
            case RIGHT       : return LEFT;
            case LOWER_LEFT  : return UPPER_RIGHT;
            case BELOW       : return ABOVE;
            case LOWER_RIGHT : return UPPER_LEFT;
            default             : return relation;
        }
    }
}


/** 
  * 
  * $Log: SpatialRelation.java,v $
  * Revision 1.5  2003/10/13 19:46:36  moltmans
  * Removed bad line endings.
  *
  * Revision 1.4  2003/03/06 01:08:49  moltmans
  * Added copyright to all the files.
  *
  * Revision 1.3  2002/01/15 22:57:54  moltmans
  * Added some fomatting to the javadoc comments.
  *
  * Revision 1.2  2001/11/24 20:58:02  mtsezgin
  * *** empty log message ***
  *
  * Revision 1.1.1.1  2001/03/29 16:25:00  moltmans
  * Initial directories for DRG
  *
  * Revision 1.7  2000/09/06 22:40:56  mtsezgin
  * Combinations of curves and polygons are successfully approximated
  * by straight lines and Bezier curves as appropriate. System works
  * quite reliably.
  *
  * Revision 1.4  2000/06/03 01:52:35  mtsezgin
  * *** empty log message ***
  *
  * Revision 1.3  2000/04/25 22:30:40  mtsezgin
  *
  * Methods added for coordinate conversions and String conversions.
  *
  * Revision 1.2  2000/04/12 04:00:16  mtsezgin
  * *** empty log message ***
  *
  * Revision 1.1  2000/04/06 21:33:43  mtsezgin
  *
  * Spatial relation represents 9 basic relations for now. It should be extended.
  *
  * GeometricObject is extended. Implementors extended with empty stubs.
  *
  * Revision 1.3  2000/04/06 19:16:24  mtsezgin
  *
  * Modified all the classes to use my Point class which extends java.awt.Point
  * instead of directly using java.awt.Point
  *
  * Revision 1.2  2000/04/01 22:51:22  mtsezgin
  *
  * Started implementation. Also from now on using jdk 1.2
  *
  * Revision 1.1.1.1  2000/04/01 03:07:07  mtsezgin
  * Imported sources
  *
  * Revision 1.5  2000/03/31 22:41:04  mtsezgin
  *
  * Started Log tracking.
  *
  *  
  **/
