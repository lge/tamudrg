// (c) MIT 2003.  All rights reserved.

////////////////////////////////////////////////////////////////////////////////
//                                                                            //
// AUTHOR:      Tevfik Metin Sezgin                                           //
//              Massachusetts Institute of Technology                         //
//              Department of Electrical Engineering and Computer Science     //
//              Artificial Intelligence Laboratory                            //
//                                                                            //
// E-MAIL:        mtsezgin@ai.mit.edu, mtsezgin@mit.edu                       //
//                                                                            //
// COPYRIGHT:   Tevfik Metin Sezgin                                           //
//              All rights reserved. This code can not be copied, modified,   //
//              or distributed in whole or partially without the written      //
//              permission of the author. Also see the COPYRIGHT file.        //
//                                                                            //
////////////////////////////////////////////////////////////////////////////////
package edu.mit.sketch.geom;


/** 
  * 
  * See the end of the file for the log of changes.
  * 
  * $Author: hammond $
  * $Date: 2004/01/15 02:45:56 $   
  * $Revision: 1.17 $
  * $Headers$
  * $Id: GeneralPath.java,v 1.17 2004/01/15 02:45:56 hammond Exp $     
  * $Name:  $   
  * $Locker:  $
  * $Source: /projects/drg/CVSROOT/drg/code/src/edu/mit/sketch/geom/GeneralPath.java,v $
  *  
  **/

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Shape;

import java.awt.geom.AffineTransform;
import java.awt.geom.CubicCurve2D;
import java.awt.geom.FlatteningPathIterator;
import java.awt.geom.Line2D;
import java.awt.geom.PathIterator;
import java.awt.geom.QuadCurve2D;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;

import java.util.Vector;

import edu.mit.sketch.toolkit.StrokeData;
import edu.mit.sketch.util.AWTUtil;
import edu.mit.sketch.ui.Tablet;

/**
  *
  * This class represents the GeneralPath object. This is a 
  * wrapper class for enabling Serialization and extension.
  *
  **/
public
class      GeneralPath
implements Serializable,
           GeometricObject,
           Shape
{
    /**
    *
    * The general path
    *
    **/
    protected transient java.awt.geom.GeneralPath general_path;


    /**
    *
    * The original data points
    *
    **/
    private Polygon points;
    
  private Vertex m_vertices[];

  
    /**
    *
    * Time stamp of this object.
    *
    **/
    public long time_stamp;


    /**
    *
    * Graphics context for this Geometric object.
    *
    **/
    public transient Graphics2D graphics;

    /**
    * Serialversion UID added to keep serialization working
	* when modifications are result in a compatible class.
    **/    
    static final long serialVersionUID = 5941299714328631376L;

    /**
    *
    * The ranges for the segments in the general_path
    *
    **/
    private Range ranges[];

    /**
     *
     * Sets the ranges for the segments in the general_path
     *
    **/
    public void setRanges(Range[] ranges)
    {
      this.ranges = ranges;
    }

    /**
     *
     * Gets the ranges for the segments in the general_path
     *
    **/
    public Range[] getRanges()
    {
      //System.out.println("Getting Ranges");
      //for(int i = 0; i < ranges.length; i++)
      //{
      //  System.out.println("range " + i + " : " + ranges[i]);
      //}
      return ranges;
    }

    /**
     *
     * Returns the ith range for the segments in the general_path.
     *
    **/
    public Range getRange(int i)
    {
      if(i < numShapes() && i >= 0)
        return getRanges()[i];
      else
        return null;
    }

  /**
   * Gest the total number of shapes in general path
   **/
  public int numShapes(){
    return getRanges().length; 
  }

    /**
    *
    * The constructor.
    *
    **/    
    public GeneralPath()
    {
        this.general_path = new java.awt.geom.GeneralPath();
    }


    /**
    *
    * The constructor.
    *
    **/    
  public GeneralPath( Shape shape )
    {
        this.general_path = new java.awt.geom.GeneralPath( shape );
    }


    /**
    *
    * The constructor.
    *
    **/    
  public GeneralPath( int rule )
    {
        this.general_path = new java.awt.geom.GeneralPath( rule );
    }


    /**
    *
    * The constructor.
    *
    **/    
  public GeneralPath( int rule, int initial_capacity )
    {
        this.general_path = new java.awt.geom.GeneralPath( rule, 
                                                           initial_capacity );
    }


    /**
    *
    * The constructor.
    *
    **/    
  public GeneralPath( java.awt.geom.GeneralPath general_path )
    {
      // this was inconsistent with the new general path given a shape
      //        this.general_path = general_path;
      this.general_path = new java.awt.geom.GeneralPath( general_path );
    }


    /**
    *
    * For serialization of general_path
    *
    **/    
    private void 
    writeObject( ObjectOutputStream out )
    throws IOException
    {
        out.defaultWriteObject();

        PathIterator path_iterator;
        path_iterator = general_path.getPathIterator( new AffineTransform() );
        
        int   type           = 0;
        float coefficients[] = new float[6];
        while ( !path_iterator.isDone() ) {
            type = path_iterator.currentSegment( coefficients );
        
            out.writeInt( type );
            switch ( type ) {
                case PathIterator.SEG_CUBICTO :
                    out.writeFloat( coefficients[0] );
                    out.writeFloat( coefficients[1] );
                    out.writeFloat( coefficients[2] );
                    out.writeFloat( coefficients[3] );
                    out.writeFloat( coefficients[4] );
                    out.writeFloat( coefficients[5] );
                    break;
                case PathIterator.SEG_QUADTO :
                    out.writeFloat( coefficients[0] );
                    out.writeFloat( coefficients[1] );
                    out.writeFloat( coefficients[2] );
                    out.writeFloat( coefficients[3] );
                    break;
                case PathIterator.SEG_LINETO :
                    out.writeFloat( coefficients[0] );
                    out.writeFloat( coefficients[1] );
                    break;
                case PathIterator.SEG_MOVETO :
                    out.writeFloat( coefficients[0] );
                    out.writeFloat( coefficients[1] );
                    break;
                case PathIterator.SEG_CLOSE :
                    break;
                default:
                    System.out.println( "Error in serializing "   +
                                        "SerilizableGeneralPath " + 
                                        "no matching case..." );
            }
            path_iterator.next();
        }
        
        out.writeInt( PathIterator.SEG_CLOSE ); // To ensure reading works.
    }
    
    /**
    *
    * For deserialization of superclass' fields.
    *
    **/    
    private void 
    readObject( ObjectInputStream in )
    throws IOException, ClassNotFoundException
    {
        in.defaultReadObject();
        
        general_path = new java.awt.geom.GeneralPath();
        
        int   type           = in.readInt();
        float coefficients[] = new float[6];
        while ( type != PathIterator.SEG_CLOSE ) {        
            switch ( type ) {
                case PathIterator.SEG_CUBICTO :
                    coefficients[0] = in.readFloat();
                    coefficients[1] = in.readFloat();
                    coefficients[2] = in.readFloat();
                    coefficients[3] = in.readFloat();
                    coefficients[4] = in.readFloat();
                    coefficients[5] = in.readFloat();
                    general_path.curveTo( coefficients[0], coefficients[1], 
                                          coefficients[2], coefficients[3], 
                                          coefficients[4], coefficients[5] );
                    break;
                case PathIterator.SEG_QUADTO :
                    coefficients[0] = in.readFloat();
                    coefficients[1] = in.readFloat();
                    coefficients[2] = in.readFloat();
                    coefficients[3] = in.readFloat();
                    general_path.quadTo( coefficients[0], coefficients[1],
                                         coefficients[2], coefficients[3] );
                    break;
                case PathIterator.SEG_LINETO :
                    coefficients[0] = in.readFloat();
                    coefficients[1] = in.readFloat();
                    general_path.lineTo( coefficients[0], coefficients[1] );
                    break;
                case PathIterator.SEG_MOVETO :
                    coefficients[0] = in.readFloat();
                    coefficients[1] = in.readFloat();
                    general_path.moveTo( coefficients[0], coefficients[1] );
                    break;
                case PathIterator.SEG_CLOSE :
                    break;
                default:
                    System.out.println( "Error in deserializing " +
                                        "SerilizableGeneralPath " + 
                                        "no matching case..." );
            }
            type = in.readInt();
        }
    }
     
         
    /**
    *
    * Implement GeometricObject
    *
    **/    
    public String
    getType()
    {
        return "general_path";
    }
    
    
    /**
    *
    * Override toString
    *
    **/    
    public String
    toString()
    {
        return "GeneralPath ";
    }
    
    
    /**
    *
    * Paint the general path
    *
    **/
    public void
    paint( Graphics g2D )
    {
        ((Graphics2D)g2D).draw( general_path );
    }
    
    
    
    /**
    *
    * Paint the general path in segments
    *
    **/
    public void
    paintInSegments( Graphics2D g2D )
    {
        PathIterator path_iterator;
        
        path_iterator = general_path.getPathIterator( new AffineTransform() );
        
        Shape  shape          = null;
        Point  last_point     = new Point();
        Color  colors[]       = { Color.red, Color.blue, Color.green };
        double coefficients[] = new double[6];
        int    i              = 0;
        int    type           = 0;
        while ( !path_iterator.isDone() ) {
            g2D.setColor( colors[i++%colors.length] );
            
            type = path_iterator.currentSegment( coefficients );
            
            switch ( type ) {
                case PathIterator.SEG_CUBICTO :
                    shape = new CubicCurve2D.Double( last_point.x,
                                                     last_point.y,
                                                     coefficients[0],
                                                     coefficients[1],
                                                     coefficients[2],
                                                     coefficients[3],
                                                     coefficients[4],
                                                     coefficients[5] );
                    last_point.setLocation( coefficients[4], coefficients[5] );
                    break;

                case PathIterator.SEG_QUADTO :
                    shape = new QuadCurve2D.Double( last_point.x,
                                                    last_point.y,
                                                    coefficients[0],
                                                    coefficients[1],
                                                    coefficients[2],
                                                    coefficients[3] );
                    last_point.setLocation( coefficients[2], coefficients[3] );
                    break;

                case PathIterator.SEG_LINETO :
                    shape = new Line2D.Double( last_point.x,
                                               last_point.y,
                                               coefficients[0],
                                               coefficients[1] );
                    last_point.setLocation( coefficients[0], coefficients[1] );
                    g2D.setColor( Color.cyan );
                    break;

                case PathIterator.SEG_MOVETO :
                    shape = null;
                    last_point.setLocation( coefficients[0], coefficients[1] );
                    break;

                case PathIterator.SEG_CLOSE :
                    shape = null;
                    break;
                    
                default:
                    System.out.println( "Error generalPath no matching case" );
            }
            
            if ( shape != null ) {
                g2D.draw( shape );
            }
            path_iterator.next();
        }
    }
    
    
    /**
    *
    * Show convex hulls for the curves
    *
    **/
    public void
    paintConvexHulls( Graphics2D g2D )
    {
        PathIterator path_iterator;

        path_iterator = general_path.getPathIterator( new AffineTransform() );
        
        Shape  shape          = null;
        Point  last_point     = new Point();
        Color  colors[]       = { Color.red, Color.blue, Color.green };
        double coefficients[] = new double[6];
        int    i              = 0;
        int    type           = 0;
        while ( !path_iterator.isDone() ) {
            g2D.setColor( colors[i++%colors.length] );
            type = path_iterator.currentSegment( coefficients );
            
            switch ( type ) {
                case PathIterator.SEG_CUBICTO :
                    shape = new CubicCurve2D.Double( last_point.x,
                                                     last_point.y,
                                                     coefficients[0],
                                                     coefficients[1],
                                                     coefficients[2],
                                                     coefficients[3],
                                                     coefficients[4],
                                                     coefficients[5] );
                    g2D.drawLine( (int)last_point.x,    (int)last_point.y, 
                                  (int)coefficients[0], (int)coefficients[1] );
                    g2D.drawLine( (int)coefficients[0], (int)coefficients[1],
                                  (int)coefficients[2], (int)coefficients[3] );
                    g2D.drawLine( (int)coefficients[2], (int)coefficients[3],
                                  (int)coefficients[4], (int)coefficients[5] );
                    g2D.drawLine( (int)last_point.x,    (int)last_point.y, 
                                  (int)coefficients[4], (int)coefficients[5] );
                    last_point.setLocation( coefficients[4], coefficients[5] );
                    break;

                case PathIterator.SEG_QUADTO :
                    shape = new QuadCurve2D.Double( last_point.x,
                                                    last_point.y,
                                                    coefficients[0],
                                                    coefficients[1],
                                                    coefficients[2],
                                                    coefficients[3] );
                    g2D.drawLine( (int)last_point.x,    (int)last_point.y, 
                                  (int)coefficients[0], (int)coefficients[1] );
                    g2D.drawLine( (int)coefficients[0], (int)coefficients[1],
                                  (int)coefficients[2], (int)coefficients[3] );
                    g2D.drawLine( (int)last_point.x,    (int)last_point.y, 
                                  (int)coefficients[2], (int)coefficients[3] );
                    last_point.setLocation( coefficients[2], coefficients[3] );
                    break;

                case PathIterator.SEG_LINETO :
                    shape = new Line2D.Double( last_point.x,
                                               last_point.y,
                                               coefficients[0],
                                               coefficients[1] );
                    last_point.setLocation( coefficients[0], coefficients[1] );
                    break;

                case PathIterator.SEG_MOVETO :
                    shape = null;
                    last_point.setLocation( coefficients[0], coefficients[1] );
                    break;

                case PathIterator.SEG_CLOSE :
                    shape = null;
                    break;
                    
                default:
                    System.out.println( "Error generalPath no matching case" );
            }
            
            if ( shape != null ) {
                g2D.draw( shape );
            }
            path_iterator.next();
        }
    }


    /**
    *
    * Draw the object
    *
    **/
    public void
    paint()
    {
        if ( graphics == null ) // Draw only if g
            return;             // is valid
        graphics.setColor( Color.black );
        paint( graphics );
    }
    
    
    /**
    *
    * This method is used to paint the original data points that
    * forms this GeometricObject
    *
    **/
    public void
    paintOriginal( Graphics g )
    {
        points.paint( g );
    }


    /**
    *
    * Returns true if the point is within +-radius distance from
    * the curve defining the object. Returns false o/w.
    *
    **/
    public boolean
    pointIsOn( Point point, int radius )
    {            
        return (getPolygonalBounds()).pointIsOn( point, radius );
    }


    /**
    *
    * Returns true if the point is within +-radius distance from
    * the original curve defining the object. Returns false o/w.
    *
    **/
    public boolean
    pointIsOnOriginal( Point p, int radius )
    {
        if ( points == null ) 
            return false;
        else 
            return points.pointIsOn( p, radius );
    }


   /**
    *
    * Set graphics context for this Geometric object.
    * Must be set at least once before doing any drawing.
    *
    **/
    public void
    setGraphicsContext( Graphics g )
    {
        graphics = ((Graphics2D)g);
    }


    /**
    *
    * This method should return true if the input objects touch.
    * It should be optimized making use of the object type 
    * information.
    *
    **/
    public boolean
    touches( GeometricObject object )
    {
        return getPolygonalBounds().touches( object.getPolygonalBounds() );
    }


    /**
    *
    * Supplied for completeness.
    *
    **/
    public Rectangle
    getRectangularBounds()
    {
        return getPolygonalBounds().getRectangularBounds();
    }


    /**
    *
    * Returns false if the argument is not completely inside 
    * this object. Return true O/W.
    *
    **/
    public boolean
    containsGeometricObject( GeometricObject object )
    {
        return getPolygonalBounds().containsGeometricObject( object );
    }


    /**
    *
    * This method should return a polygon that fully contains the 
    * current object. The polygon is implicity closed and the last 
    * point doesn't necessarily have to be the same as the first 
    * (zeroth) point.
    *
    **/
    public Polygon 
    getPolygonalBounds()
    {
        return toPolygon().getPolygonalBounds();
    }


    /**
    *
    * This method should return the spatial relation of the input
    * parameter with respect to this object. see the SpatialRelation
    * class for a detailed list of possible spatial relations.
    * Another version of this method should be implemented for 
    * handling spatial relations where a rotated coordinate
    * system is to be used.
    *
    **/
    public int
    spatialRelation( GeometricObject object )
    {
        return getRectangularBounds().spatialRelation( object );
    }


    /**
    *
    * Returns the angle of the object WRT the x axis in radians.
    *
    **/
    public double
    getCartesianAngle()
    {
        return 0.0;
    }


    /**
    *
    * Returns the angle of the object WRT the x axis in radians.
    *
    **/
    public double
    getAngle()
    {
        return 0.0;
    }
    
    
    /**
    *
    * Sets the time stamp of the current Terminal
    *
    **/
    public void
    setTimeStamp( long time_stamp )
    {
        this.time_stamp = time_stamp;
    }
    
    
    /**
    *
    * Returns the time stamp of the current Terminal
    *
    **/
    public long
    getTimeStamp()
    {
        return time_stamp;
    }

  public void setOriginalVertices( Vertex pts[] )
  {
    setDataPoints( new Polygon( pts ) );
    m_vertices = new Vertex[pts.length];
    for ( int i = 0; i < pts.length; i++ ) {
      m_vertices[i] = pts[i];
    }
  }

  public Vertex[] getOriginalVertices()
  {
    if ( m_vertices == null ) {
      return null;
    }
    Vertex ret[] = new Vertex[m_vertices.length];
    for ( int i = 0; i < m_vertices.length; i++ ) {
      ret[i] = m_vertices[i];
    }
    return ret;
  }

    
    /**
    *
    * This method is used to set the original data points that
    * forms this GeometricObject
    *
    **/
    public void
    setDataPoints( Polygon points )
    {
        this.points = points;
    }


    /**
    *
    * This method is used to get the original data points that
    * forms this GeometricObject
    *
    **/
    public Polygon
    getDataPoints()
    {
        return points;
    }
    
    
    /**
    *
    * This method is used to get the original data points that
    * forms this GeometricObject
    *
    **/
    public Polygon
    toPolygon()
    {
        Polygon result = new Polygon( flatten( 0.001, 5 ) );

        return result;
    }
    
    
    /**
    *
    * Returns false if the objects in the input array are
    * completely inside this object. Return true O/W.
    *
    **/
    public boolean
    containsGeometricObjects( GeometricObject objects[] )
    {
        for ( int i=0; i<objects.length; i++ ) {
            if ( !containsGeometricObject( objects[i] ) )
                return false;
        }
        
        return true;
    }
    
    
    /**
    *
    * Add the arguments to the position of the object.
    *
    **/
    public void
    translate( double dx, double dy )
    {
		points.translate( dx, dy );
        int          type;
        PathIterator path_iterator;
        float        x                = (float)dx;
        float        y                = (float)dy;        
        float        coefficients[]   = new float[6];
        GeneralPath  updated_path     = new GeneralPath();
        
        path_iterator  = general_path.getPathIterator( new AffineTransform() );

        while ( !path_iterator.isDone() ) {
            type = path_iterator.currentSegment( coefficients );

            switch ( type ) {
                case PathIterator.SEG_CUBICTO :
                    updated_path.curveTo( coefficients[0]+x, coefficients[1]+y,
                                          coefficients[2]+x, coefficients[3]+y,
                                          coefficients[4]+x, coefficients[5]+y );
                    break;

                case PathIterator.SEG_QUADTO :
                    updated_path.quadTo( coefficients[0]+x, coefficients[1]+y,
                                         coefficients[2]+x, coefficients[3]+y );
                    break;

                case PathIterator.SEG_LINETO :
                    updated_path.lineTo( coefficients[0]+x, coefficients[1]+y );
                    break;

                case PathIterator.SEG_MOVETO :
                    updated_path.moveTo( coefficients[0]+x, coefficients[1]+y );
                    break;

                case PathIterator.SEG_CLOSE :
                    updated_path.closePath();
                    break;
                    
                default:
                    System.out.println( "Error: No matching case" );
            }
            path_iterator.next();
        }
        
        general_path = updated_path.general_path;
    }
    
    
    /**
    *
    * Implement methods for extension of java.awt.geom.GeneralPath
    *
    **/
    public void
    append( PathIterator pi, boolean connect )
    {
        general_path.append( pi, connect );
    }
    
    
    /**
    *
    * Implement methods for extension of java.awt.geom.GeneralPath
    *
    **/
    public void
    append( Shape s, boolean connect )
    {
        general_path.append( s, connect );
    }
    
    
    /**
    *
    * Implement methods for extension of java.awt.geom.GeneralPath
    *
    **/
    public Object
    clone()
    {
        return new GeneralPath((java.awt.geom.GeneralPath)general_path.clone());
    }
    
    
    /**
    *
    * Implement methods for extension of java.awt.geom.GeneralPath
    *
    **/
    public void
    closePath()
    {
        general_path.closePath();
    }
    
    
    /**
    *
    * Implement methods for extension of java.awt.geom.GeneralPath
    *
    **/
    public boolean
    contains( double x, double y )
    {
        return general_path.contains( x, y );
    }
    
    
    /**
    *
    * Implement methods for extension of java.awt.geom.GeneralPath
    *
    **/
    public boolean
    contains( double x, double y, double w, double h )
    {
        return general_path.contains( x, y, w, h );
    }
    
    
    /**
    *
    * Implement methods for extension of java.awt.geom.GeneralPath
    *
    **/
    public boolean
    contains( java.awt.geom.Point2D p )
    {
        return general_path.contains( p );
    }
    
    
    /**
    *
    * Implement methods for extension of java.awt.geom.GeneralPath
    *
    **/
    public boolean
    contains( java.awt.geom.Rectangle2D r )
    {
        return general_path.contains( r );
    }
    
    
    /**
    *
    * Implement methods for extension of java.awt.geom.GeneralPath
    *
    **/
    public Shape
    createTransformedShape( AffineTransform at )
    {
        return general_path.createTransformedShape( at );
    }
    
    
    /**
    *
    * Implement methods for extension of java.awt.geom.GeneralPath
    *
    **/
    public void
    curveTo( float x1, float y1, float x2, float y2, float x3, float y3 )
    {
        general_path.curveTo( x1, y1, x2, y2, x3, y3 );
    }
    
    
    /**
    *
    * Implement methods for extension of java.awt.geom.GeneralPath
    *
    **/
    public java.awt.Rectangle
    getBounds()
    {
        return general_path.getBounds();
    }
    
    
    /**
    *
    * Implement methods for extension of java.awt.geom.GeneralPath
    *
    **/
    public java.awt.geom.Rectangle2D
    getBounds2D()
    {
        return general_path.getBounds2D();
    }
    
    
    /**
    *
    * Implement methods for extension of java.awt.geom.GeneralPath
    *
    **/
    public java.awt.geom.Point2D
    getCurrentPoint()
    {
        return general_path.getCurrentPoint();
    }
    
    
    /**
    *
    * Implement methods for extension of java.awt.geom.GeneralPath
    *
    **/
    public PathIterator
    getPathIterator( AffineTransform at )
    {
        return general_path.getPathIterator( at );
    }
    
    
    /**
    *
    * Implement methods for extension of java.awt.geom.GeneralPath
    *
    **/
    public PathIterator
    getPathIterator( AffineTransform at, double flatness )
    {
        return general_path.getPathIterator( at, flatness );
    }
    
    
    /**
    *
    * Implement methods for extension of java.awt.geom.GeneralPath
    *
    **/
    public int
    getWindingRule()
    {
        return general_path.getWindingRule();
    }
    
    
    /**
    *
    * Implement methods for extension of java.awt.geom.GeneralPath
    *
    **/
    public boolean
    intersects( double x, double y, double w, double h )
    {
        return general_path.intersects( x, y, w, h );
    }
    
    
    /**
    *
    * Implement methods for extension of java.awt.geom.GeneralPath
    *
    **/
    public boolean
    intersects( java.awt.geom.Rectangle2D r )
    {
        return general_path.intersects( r );
    }
    
    
    /**
    *
    * Implement methods for extension of java.awt.geom.GeneralPath
    *
    **/
    public void
    lineTo( float x, float y )
    {
        general_path.lineTo( x, y );
    }
    
    
    /**
    *
    * Implement methods for extension of java.awt.geom.GeneralPath
    *
    **/
    public void
    moveTo( float x, float y )
    {
        general_path.moveTo( x, y );
    }
    
    
    /**
    *
    * Implement methods for extension of java.awt.geom.GeneralPath
    *
    **/
    public void
    quadTo( float x1, float y1, float x2, float y2 )
    {
        general_path.quadTo( x1, y1, x2, y2 );
    }
    
    
    /**
    *
    * Implement methods for extension of java.awt.geom.GeneralPath
    *
    **/
    public void
    reset()
    {
        general_path.reset();
    }
    
    
    /**
    *
    * Implement methods for extension of java.awt.geom.GeneralPath
    *
    **/
    public void
    setWindingRule( int rule )
    {
        general_path.setWindingRule( rule);
    }
    
    
    /**
    *
    * Implement methods for extension of java.awt.geom.GeneralPath
    *
    **/
    public void
    transform( AffineTransform at )
    {
        general_path.transform( at );
    }


    /**
    *
    * Return the number of segments in a java.awt.GeneralPath 
    * object.
    *
    **/
    public int
    getSegmentCount()
    {
        PathIterator path_iterator;
        
        path_iterator = general_path.getPathIterator( new AffineTransform() );
        
        int segments = 0;
        while ( !path_iterator.isDone() ) {
            path_iterator.next();
            segments++;
        }
        
        return segments;
    }


    /**
    *
    * Return an array of vertices corresponding to the flattened
    * version of this GeneralPath
    *
    **/
    public Vertex[]
    flatten( double flatness, int limit )
    {
        Vector       vertices;
        Vertex       approximation[];
        PathIterator path_iterator;

        vertices      = new Vector();
        path_iterator = general_path.getPathIterator( new AffineTransform() );
        
        Shape  shape           = null;
        Point  last_point      = new Point();
        double coefficients[]  = new double[6];
        int    type            = 0;
        while ( !path_iterator.isDone() ) {
            type = path_iterator.currentSegment( coefficients );
	    //            System.out.println( "general path type = " + type );
            switch ( type ) {
                case PathIterator.SEG_CUBICTO :
                    shape = new CubicCurve2D.Double( last_point.x,
                                                     last_point.y,
                                                     coefficients[0],
                                                     coefficients[1],
                                                     coefficients[2],
                                                     coefficients[3],
                                                     coefficients[4],
                                                     coefficients[5] );
                    approximation = AWTUtil.flatten( shape, flatness, limit );
                    for ( int v=1; v<approximation.length; v++ ) {
                        vertices.addElement( approximation[v] );
                    }
                    last_point.setLocation( coefficients[4], coefficients[5] );
		    //					System.out.println( "general path cubic to : " + last_point );
                    break;

                case PathIterator.SEG_QUADTO :
                    shape = new QuadCurve2D.Double( last_point.x,
                                                    last_point.y,
                                                    coefficients[0],
                                                    coefficients[1],
                                                    coefficients[2],
                                                    coefficients[3] );
                    approximation = AWTUtil.flatten( shape, flatness, limit );
                    for ( int i=1; i<approximation.length; i++ ) {
                        vertices.addElement( approximation[i] );
                    }
                    last_point.setLocation( coefficients[2], coefficients[3] );
					System.out.println( "general path quad to : " + last_point );
                    break;

                case PathIterator.SEG_LINETO :
                    shape = new Line2D.Double( last_point.x,
                                               last_point.y,
                                               coefficients[0],
                                               coefficients[1] );
                    vertices.addElement( new Vertex( last_point ) );
                    vertices.addElement( new Vertex( (int)coefficients[0], (int)coefficients[1] ) );
                    last_point.setLocation( coefficients[0], coefficients[1] );
		    //					System.out.println( "general path line to : " + last_point );
                    break;

                case PathIterator.SEG_MOVETO :
                    shape = null;
                    last_point.setLocation( coefficients[0], coefficients[1] );
		    //					System.out.println( "general path move to : " + last_point );
                    vertices.addElement( new Vertex( last_point ) );
                    break;

                case PathIterator.SEG_CLOSE :
                    shape = null;
                    break;
                    
                default:
                    System.out.println( "Error generalPath no matching case" );
            }
            
            path_iterator.next();
        }

        return Vertex.removeDuplicateVertices( Vertex.vectorToArray(vertices) );
    }



    /**
    *
    * Return a double array that has the x,y coordinates for the points on the 
    * curve after flattening it.
    *
    **/
    public double[][]
    flatten( double flatness )
    {
        Vector       x_positions = new Vector();
        Vector       y_positions = new Vector();
        PathIterator path_iterator;

        path_iterator = general_path.getPathIterator( new AffineTransform(),
						      flatness );
        
        double  coefficients[]  = new double[6];
        int     type            = 0;
        while ( !path_iterator.isDone() ) {
            type = path_iterator.currentSegment( coefficients );

            switch ( type ) {
                case PathIterator.SEG_CUBICTO :
		    x_positions.addElement( new Double( coefficients[4] ) );
		    y_positions.addElement( new Double( coefficients[5] ) );
                    break;

                case PathIterator.SEG_QUADTO :
		    x_positions.addElement( new Double( coefficients[2] ) );
		    y_positions.addElement( new Double( coefficients[3] ) );
                    break;

                case PathIterator.SEG_LINETO :
		    x_positions.addElement( new Double( coefficients[0] ) );
		    y_positions.addElement( new Double( coefficients[1] ) );
                    break;

                case PathIterator.SEG_MOVETO :
		    x_positions.addElement( new Double( coefficients[0] ) );
		    y_positions.addElement( new Double( coefficients[1] ) );
                    break;

                case PathIterator.SEG_CLOSE :
                    break;
                    
                default:
                    System.out.println( "Error generalPath no matching case" );
            }
            
	    path_iterator.next();
        }
	
	System.out.println( "x_positions.length " + x_positions.size() );
	System.out.println( "y_positions.length " + y_positions.size() );
       

	double positions[][] = new double[x_positions.size()][2];
	for ( int i=0; i<x_positions.size(); i++ ) {
	    positions[i][0] = ((Double)x_positions.get(i)).doubleValue();
	    positions[i][1] = ((Double)y_positions.get(i)).doubleValue();
	}

        return positions;
    }
    
    
    /**
    *
    * Return the beginning the position.
    *
    **/
    public Point
    getStartPosition() 
    {
        int          type;
        PathIterator path_iterator;
        double       coefficients[]   = new double[6];
        Point        result           = null;
        
        path_iterator  = general_path.getPathIterator( new AffineTransform() );

        while ( !path_iterator.isDone() && result == null ) {
            type = path_iterator.currentSegment( coefficients );

            switch ( type ) {
                case PathIterator.SEG_CUBICTO :
                    result = new Point( (int)coefficients[0], (int)coefficients[1] );
                    break;
                case PathIterator.SEG_QUADTO :
                    result = new Point( (int)coefficients[0], (int)coefficients[1] );
                    break;

                case PathIterator.SEG_LINETO :
                    result = new Point( (int)coefficients[0], (int)coefficients[1] );
                    break;

                case PathIterator.SEG_MOVETO :
                    result = new Point( (int)coefficients[0], (int)coefficients[1] );
                    break;

                case PathIterator.SEG_CLOSE :
                    System.out.println( "Segment closed before openning" );
                    return null;
                    
                default:
                    System.out.println( "Error: No matching case" );
                    return null;
            }
            
            path_iterator.next();
        }
        
        return result;
    }
    
    
    /**
    *
    * Get the LSQE for the general path by measuring the distance to
    * the vertices in the StrokeData. The ranges[] should correnspond
    * to the beginning and ending indices for the vertices that 
    * correspond to the points forming the general path. 
    *
    **/
    public double
    getLSQError2( StrokeData data, Range ranges[] ) 
    {
        Vertex vertices[]           = data.vertices;
        double cumulative_lsq_error = 0.0;
        
        int          type;
        int          segment;
        PathIterator path_iterator;
        Shape        shape            = null;
        Point        last_point       = new Point();
        double       coefficients[]   = new double[6];
        
        path_iterator  = general_path.getPathIterator( new AffineTransform() );

        segment = 0;        
        while ( !path_iterator.isDone() ) {
            type = path_iterator.currentSegment( coefficients );

            switch ( type ) {
                case PathIterator.SEG_CUBICTO :
                    shape = new CubicCurve2D.Double( last_point.x,
                                                     last_point.y,
                                                     coefficients[0],
                                                     coefficients[1],
                                                     coefficients[2],
                                                     coefficients[3],
                                                     coefficients[4],
                                                     coefficients[5] );
                    last_point.setLocation( coefficients[4], coefficients[5] );
                    segment++;
                    break;

                case PathIterator.SEG_QUADTO :
                    shape = new QuadCurve2D.Double( last_point.x,
                                                    last_point.y,
                                                    coefficients[0],
                                                    coefficients[1],
                                                    coefficients[2],
                                                    coefficients[3] );
                    last_point.setLocation( coefficients[2], coefficients[3] );
                    segment++;
                    break;

                case PathIterator.SEG_LINETO :
                    shape = new Line( last_point.x,
                                      last_point.y,
                                      coefficients[0],
                                      coefficients[1] );
                    last_point.setLocation( coefficients[0], coefficients[1] );
                    segment++;
                    break;

                case PathIterator.SEG_MOVETO :
                    shape = null;
                    last_point.setLocation( coefficients[0], coefficients[1] );
                    break;

                case PathIterator.SEG_CLOSE :
                    shape = null;
                    break;
                    
                default:
                    System.out.println( "Error: No matching case" );
            }
            
            if ( ( type == PathIterator.SEG_CUBICTO ) ||
                 ( type == PathIterator.SEG_QUADTO  ) ) {
                double lsq_error = Double.MAX_VALUE;
                Range  range     = ranges[segment-1];
                
                lsq_error = AWTUtil.getCurveLSQEror( shape, data, range );
                
        if ( Tablet.debug ) {
                    System.out.println( "LSQ Error for segment " + (segment-1) + 
                                        " = " + lsq_error );
                }
                cumulative_lsq_error += lsq_error*( range.max - range.min );
            }
            
            if ( ( type == PathIterator.SEG_LINETO ) ) {
                Line line = (Line)shape;
                
                double lsq_error = 0.0;
                Range  range     = ranges[segment-1];
                
                lsq_error = line.getLSQError( data, range );
                
        if ( Tablet.debug ) {
                    System.out.println( "LSQ Error for line " + (segment-1) + 
                                        " = " + lsq_error );
                }
                cumulative_lsq_error += lsq_error*( range.max - range.min );
            }
            
            path_iterator.next();
        }

        return Math.abs( cumulative_lsq_error /
                         (ranges[ranges.length-1].max-ranges[0].min) );
    }
    
    
    /**
    *
    * Get the LSQE for the general path by measuring the distance to
    * the vertices in the StrokeData. This is more costly that the 
	* getLSQError2
    *
    **/
    public double
    getLSQError( StrokeData data ) 
    {
		Polygon polygon = toPolygon();
		
//  		System.out.println( "polygon" + polygon );
		
//  		System.out.println( "polygon length = " + polygon.npoints  );
		
		return AWTUtil.leastSquaresForPolygon( polygon, data.vertices );
    }

    
    /**
    *
    * return true if all the pieces are Bezier curves
    *
    **/
    public boolean
    isAllCurves()
    {
        double      coefficients[] = new double[6];
		PathIterator path_iterator = 
			general_path.getPathIterator( new AffineTransform() );

        while ( !path_iterator.isDone() ) {
	        if ( path_iterator.currentSegment( coefficients ) == 
				 PathIterator.SEG_LINETO ) {
				return false;
            }
			
			path_iterator.next();
		}
		return true;
    }

    
    /**
    *
    * getGeneralPathRanges.
    *
    **/
    public Range[]
    getGeneralPathRanges( StrokeData  data )
    {
        double       coefficients[] = 
            new double[6];
        PathIterator path_iterator  =
            general_path.getPathIterator( new AffineTransform() );
        Vertex       vertices[]     = 
            data.vertices;
        Range        ranges[]       = 
            new Range[ getSegmentCount() - 1 ];
        
        for ( int i=0; i<ranges.length; i++ )
            ranges[i] = new Range( 0, 0 );
        
        int    type        = 0;
        int    j           = 0;
        Vertex next_vertex = new Vertex();
        System.out.println( "Get ranges of " + ranges.length + " segments" );
        for ( int i=0; i<ranges.length-1; i++ ) {
            type = path_iterator.currentSegment( coefficients );
            
            while ( ( type != PathIterator.SEG_CUBICTO ) &&
                    ( type != PathIterator.SEG_QUADTO  ) &&
                    ( type != PathIterator.SEG_LINETO  ) ) {
                path_iterator.next();
                type = path_iterator.currentSegment( coefficients );
            }
            
            switch ( type ) {
                case PathIterator.SEG_CUBICTO :
                    next_vertex.x = (int)coefficients[4];
                    next_vertex.y = (int)coefficients[5];
                    break;
                case PathIterator.SEG_QUADTO :
                    next_vertex.x = (int)coefficients[2];
                    next_vertex.y = (int)coefficients[3];
                    break;
                case PathIterator.SEG_LINETO :
                    next_vertex.x = (int)coefficients[0];
                    next_vertex.y = (int)coefficients[1];
                    break;
                default :
                    break;
            }
            
            while ( ( vertices[j].x != next_vertex.x ) &&
                    ( vertices[j].y != next_vertex.y ) ) {
                j++;
            }
            
            ranges[ i ].max = j;
            ranges[i+1].min = j;
            
            path_iterator.next();
        }
        
        ranges[       0       ].min = 0;
        ranges[ranges.length-1].max = vertices.length;
        
        return ranges;
    }
    
    
    /**
    * 
    * Combines the ends of the polygon if they are sufficiently close.
    * This is still not working right. 
    *
    **/
    public void
    combineEndPoints( double error )
    {
        int          type;
        int          next_type           = 0;
        PathIterator path_iterator;
        float        next_coefficients[] = new float[6];
        float        coefficients[]      = new float[6];
        GeneralPath  updated_path        = new GeneralPath();
        Point        beginning_point     = getStartPosition();
        boolean      debug               = false;
                
        path_iterator  = general_path.getPathIterator( new AffineTransform() );



        while ( !path_iterator.isDone() ) {
            if ( debug ) {
                System.out.println( "Entering" );
            }
            type      = path_iterator.currentSegment( coefficients );
            path_iterator.next();
            if ( !path_iterator.isDone() )
                next_type = path_iterator.currentSegment( next_coefficients );
            
            switch ( type ) {
                case PathIterator.SEG_CUBICTO :
                    if ( debug ) {
                        System.out.println("CUBIC");
                    }
                    if ( ( ( next_type == PathIterator.SEG_CLOSE                            ) ||
                           ( path_iterator.isDone()                                         ) ) &&
                         ( beginning_point.distance( coefficients[4], coefficients[5] ) < error ) ) {
                        updated_path.curveTo( coefficients[0], coefficients[1], coefficients[2], coefficients[3], beginning_point.x, beginning_point.y );
                    } else {
                        updated_path.curveTo( coefficients[0], coefficients[1], coefficients[2], coefficients[3], coefficients[4], coefficients[5] );
                        System.out.println( "Distance" + beginning_point.distance( coefficients[4], coefficients[5] ) + " error " + error );
                    }
                    break;

                case PathIterator.SEG_QUADTO :
                    if ( debug ) {
                        System.out.println("QUAD");
                    }
                    if ( ( ( next_type == PathIterator.SEG_CLOSE                            ) ||
                           ( path_iterator.isDone()                                         ) ) &&
                         ( beginning_point.distance( coefficients[2], coefficients[3] ) < error ) ) {
                        updated_path.quadTo( coefficients[0], coefficients[1], beginning_point.x, beginning_point.y );
                    } else {
                        updated_path.quadTo( coefficients[0], coefficients[1], coefficients[2], coefficients[3] );
                        if ( debug ) {
                            System.out.println( "Distance" + beginning_point.distance( coefficients[2], coefficients[3] ) + " error " + error );
                        }
                    }
                    break;

                case PathIterator.SEG_LINETO :
                    if ( debug ) {
                        System.out.println("LINE");
                    }
                    if ( ( ( next_type == PathIterator.SEG_CLOSE                            ) ||
                           ( path_iterator.isDone()                                         ) ) &&
                         ( beginning_point.distance( coefficients[0], coefficients[1] ) < error ) ) {
                        updated_path.lineTo( beginning_point.x, beginning_point.y );
                    } else {
                        updated_path.lineTo( coefficients[0], coefficients[1] );
                        if ( debug ) {
                            System.out.println( "Distance" + beginning_point.distance( coefficients[0], coefficients[1] ) + " error " + error );
                        }
                    }
                    break;

                case PathIterator.SEG_MOVETO :
                    if ( debug ) {
                        System.out.println("MOVETO");
                    }
                    updated_path.moveTo( (float)coefficients[0], (float)coefficients[1] );
                    break;

                case PathIterator.SEG_CLOSE :
                    if ( debug ) {
                        System.out.println("CLOSE");
                    }
                    updated_path.closePath();
                    break;
                    
                default:
                    if ( debug ) {
                        System.out.println( "Error: No matching case" );
                    }
            }

            if ( debug ) {
                for ( int i=0; i<6; i++ ) System.out.print( coefficients[i] + " " );
                System.out.println( "\nbeginning_point" + beginning_point + "path_iterator.isDone() = "  + path_iterator.isDone() );
                System.out.println( "Exiting" );
            }
        }

        
        reset();
        updated_path.closePath();
        append( updated_path, false );
    }

  public final GeometricObject copy()
  {
    System.out.println("in copy");
    GeneralPath path = new GeneralPath( general_path );
    if( points != null ) {
      path.points = (Polygon)points.copy();
    }
    path.time_stamp = time_stamp;
    return path;    
  }
  
}

/** 
  * 
  * $Log: GeneralPath.java,v $
  * Revision 1.17  2004/01/15 02:45:56  hammond
  * added one more thread and simplified general path processing
  *
  * Revision 1.16  2003/06/26 19:57:14  calvarad
  * Lots of bug fixes
  *
  * Revision 1.15  2003/06/16 17:01:13  mtsezgin
  * *** empty log message ***
  *
  * Revision 1.14  2003/05/07 20:25:11  mtsezgin
  *
  * Removed the control M madness.
  *
  * Revision 1.13  2003/03/06 01:08:49  moltmans
  * Added copyright to all the files.
  *
  * Revision 1.12  2002/11/25 02:43:26  hammond
  * made ranges accessible
  *
  * Revision 1.11  2002/08/14 18:35:56  moltmans
  * Added a copy method to Geometric primitives so that we can make deep
  * copies of objects.  The copy method is careful to not reuse any data.
  *
  * Revision 1.10  2002/08/06 15:37:30  mtsezgin
  * Added static serialVersionUID information to the serializable geometric objects,so that serialization doesn't break unless modifications made to the class are truly incompatible changes.
  *
  * Revision 1.9  2002/07/23 21:00:06  moltmans
  * Commented out some debug code that was throwing errors.
  *
  * Revision 1.8  2002/07/22 20:56:31  mtsezgin
  * Fixed minor bugs.
  *
  * Revision 1.7  2002/07/21 00:31:38  mtsezgin
  * Changed the representation for Arc. Now it is represented by a piece of
  * circle. There are methods for computing the center of the circle as well
  * as its radius.Ghie
  *
  * Revision 1.6  2002/07/10 17:49:17  mtsezgin
  * *** empty log message ***
  *
  * Revision 1.5  2001/11/23 03:23:30  mtsezgin
  * Major reorganization.
  *
  * Revision 1.4  2001/10/18 19:48:46  mtsezgin
  * *** empty log message ***
  *
  * Revision 1.3  2001/10/12 22:26:27  mtsezgin
  * This is a general commit of all files.
  *
  * Revision 1.2  2001/04/12 19:25:55  mtsezgin
  * *** empty log message ***
  *
  * Revision 1.1.1.1  2001/03/29 16:25:00  moltmans
  * Initial directories for DRG
  *
  *  
  **/
