// (c) MIT 2003.  All rights reserved.

////////////////////////////////////////////////////////////////////////////////
//                                                                            //
// AUTHOR:      Tevfik Metin Sezgin                                           //
//              Massachusetts Institute of Technology                         //
//              Department of Electrical Engineering and Computer Science     //
//              Artificial Intelligence Laboratory                            //
//                                                                            //
// E-MAIL:        mtsezgin@ai.mit.edu, mtsezgin@mit.edu                       //
//                                                                            //
// COPYRIGHT:   Tevfik Metin Sezgin                                           //
//              All rights reserved. This code can not be copied, modified,   //
//              or distributed in whole or partially without the written      //
//              permission of the author. Also see the COPYRIGHT file.        //
//                                                                            //
////////////////////////////////////////////////////////////////////////////////
package edu.mit.sketch.geom;

/** 
  * 
  * See the end of the file for the log of changes.
  * 
  * $Author: hammond $
  * $Date: 2004/01/10 03:02:25 $   
  * $Revision: 1.16 $
  * $Headers$
  * $Id: Arc.java,v 1.16 2004/01/10 03:02:25 hammond Exp $     
  * $Name:  $   
  * $Locker:  $
  * $Source: /projects/drg/CVSROOT/drg/code/src/edu/mit/sketch/geom/Arc.java,v $
  *  
  **/




import Jama.Matrix;
import edu.mit.sketch.geom.Ellipse;
import edu.mit.sketch.geom.GeometryUtil;
import edu.mit.sketch.geom.Point;
import edu.mit.sketch.geom.Range;
import edu.mit.sketch.geom.Vertex;
import edu.mit.sketch.toolkit.Blackboard;
import edu.mit.sketch.toolkit.StrokeData;
import edu.mit.sketch.ui.Tablet;
import edu.mit.sketch.util.AWTUtil;
import edu.mit.sketch.util.AWTUtil;
import edu.mit.sketch.util.GraphicsUtil;
import edu.mit.sketch.util.Util;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics2D;
import java.awt.Graphics;
import java.awt.Shape;
import java.awt.geom.AffineTransform;
import java.awt.geom.Arc2D;
import java.awt.geom.CubicCurve2D;
import java.awt.geom.Line2D;
import java.awt.geom.PathIterator;
import java.awt.geom.QuadCurve2D;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.util.Arrays;

/**
  *
  * This class represents an arc described by its end-points p, q.
  *
  **/
public class Arc //extends Arc2D.Double
implements GeometricObject,
           Serializable
{
  /**
   *
   * The original data points
   *
   **/
  private Polygon points;

  
  
  /**
   *
   * start angle in radians
   *
   **/
  public double start;
    

  /**
   *
   * extent in radians
   *
   **/
  public double extent;
    

  /**
   *
   * Time stamp of this object.
   *
   **/
  public long time_stamp;


  /**
   *
   * Graphics context for this Geometric object.
   *
   **/
  public transient Graphics graphics;

  private boolean parameters_computed = false;

  private Point center;
	
  private double radius;

  private Vertex vertices[];
    
  private boolean clockwise;
	

  /**
   * Serialversion UID added to keep serialization working
   * when modifications are result in a compatible class.
   **/    
  static final long serialVersionUID = 9059393618290047379L;

  /**
   *
   * Constructor that takes a stroke data. There must be at least 3 points in 
   * stroke_data
   *
   **/
  protected Arc( StrokeData stroke_data )
  {
    this( stroke_data.vertices );
  }
	
  /**
   *
   * Constructor that takes vertices. There must be at least 3 points in 
   * vertices.
   *
   **/
  protected Arc( Vertex vertices[] )
  {
    this.vertices = vertices;
		
    points   = new Polygon( vertices );
		
    computeParameters();
  }

  public static Arc fitArc(Vertex[] vertices)
  {
    try {
      return new Arc(vertices);
    }
    catch( RuntimeException e ) {
      return null;
    }
  }

  public static Arc fitArc(StrokeData strokeData)
  {
    try {
      return new Arc(strokeData);
    }
    catch( RuntimeException e ) {
      return null;
    }
  }
  


  /**
   *
   * compute center and radius for the circle that best 
   * matches this arc in a least squares sense
   *
   **/    
  private void
    computeParameters()
  {
    //	try {
    double A[][] = new double[vertices.length][3];
    double B[][] = new double[vertices.length][1];
	    
    for ( int i=0; i<A.length; i++ ) {
      A[i][0] = 2*vertices[i].x;
      A[i][1] = 2*vertices[i].y;
      A[i][2] = 1;
      B[i][0] = vertices[i].x*vertices[i].x + 
        vertices[i].y*vertices[i].y;
    }
	    
    Matrix matrixA = new Matrix( A );
    Matrix matrixB = new Matrix( B );
		
	    
    Matrix matrixC = matrixA.solve( matrixB );
	    
    double C[][]   = matrixC.getArray();
	    
    center         = new Point( (int)C[0][0], (int)C[1][0] );
	    
    radius         = Math.sqrt( C[0][0]*C[0][0] + 
                                C[1][0]*C[1][0] + 
                                C[2][0] );
	    
    Point reference_point = vertices[2];
    Point begining_point  = vertices[0];
    Point ending_point    = vertices[vertices.length-1];
	    
    double x0 = begining_point.x  - center.x;
    double y0 = begining_point.y  - center.y;
    double x1 = reference_point.x - begining_point.x;
    double y1 = reference_point.y - begining_point.y;
	    
    double cross_product = x0*y1-x1*y0;
	    
    if ( cross_product > 0 ) { // Find the cw/ccw order
      //System.out.println( "clockwise" );
      begining_point = vertices[vertices.length-1];
      ending_point   = vertices[0];
      clockwise = true;
    } else {
      //System.out.println( "counter clockwise" );
      clockwise = false;
    }
	    
    int    dx;
    int    dy;
    double theta1;
    double theta2;
    dx     = (int)(begining_point.x-center.x);
    dy     = (int)(begining_point.y-center.y);
    theta1 = Math.atan2( -dy, dx );
    dx     = (int)(ending_point.x-center.x);
    dy     = (int)(ending_point.y-center.y);
    theta2 = Math.atan2( -dy, dx );
	    
    theta1 = ( theta1 < 0      ) ? theta1 + Math.PI*2: theta1;
    theta2 = ( theta2 < 0      ) ? theta2 + Math.PI*2: theta2;
    theta2 = ( theta2 < theta1 ) ? theta2 + Math.PI*2: theta2;
	    
    start          = theta1;
    extent         = theta2-theta1;
	    
    parameters_computed = true;
    // 	} catch( Exception e ) {
    // 	    System.err.println( "Exception caught in Arc.java " + e );
    // 	    System.err.println( "Input to Arc.java must be checked for line before passing in" );
    // 	    e.printStackTrace();
    // 	    radius = 1;
    // 	    start  = 0;
    // 	    extent = Math.PI;
    // 	    center = new Point(0, 0);
    // 	    parameters_computed = true;
    // 	}
  }


  /**
   *
   * returns radius of the cirle that best matches this arc in a least
   * squares sense
   *
   **/    
  public double
    getRadius()
  {
    if ( !parameters_computed ) {
      computeParameters();
    }
		
    return radius;
  }


  /**
   *
   * returns center of the cirle that best matches this arc in a least
   * squares sense
   *
   **/    
  public Point
    getCenter()
  {
    if ( !parameters_computed ) {
      computeParameters();
    }
		
    return center;
  }

  /**
   *
   * returns the head point of the arc
   *
   **/    
  public Point
    getHeadPoint()	
  {
    Point returnme = new Point (points.xpoints[0], 
                                points.ypoints[0] );  

    return returnme;
  }

  /**
   *
   * returns the tail point of the arc
   *
   **/    
  public Point
    getTailPoint()	
  {
    int i = points.xpoints.length-1;
	
    Point returnme = new Point (points.xpoints[i], 
                                points.ypoints[i] );  

    return returnme;
  }

  public boolean getClockwise()
  {
    return clockwise;
  }

  /**
   *
   * Implement GeometricObject
   *
   **/    
  public String
    getType()
  {
    return "arc";
  }
    
    
  /**
   *
   * Override toString
   *
   **/    
  public String
    toString()
  {
    return "Arc " + super.toString();
  }
    
    
  /**
   *
   * Draw the object
   *
   **/
  public void
    paint()
  {
    graphics.setColor( Color.black );
    paint( graphics );
  }
    
    
  /**
   *
   * Draw the object
   *
   **/
  public void
    paint( Graphics g )
  {
    g.drawArc( (int)(center.x-radius),
               (int)(center.y-radius),
               (int)radius*2,
               (int)radius*2,
               (int)Math.toDegrees( start ),
               (int)Math.toDegrees( extent) );
  }
    
    
  /**
   *
   * This method is used to paint the original data points that
   * forms this GeometricObject
   *
   **/
  public void
    paintOriginal( Graphics g )
  {
    points.paint( g );
  }
   
    
  /**
   *
   * Returns true if the point is within +-radius distance from
   * the curve defining the object. Returns false o/w.
   *
   **/
  public boolean
    pointIsOn( Point p, int radius )
  {
    int    dx    = p.x-(int)center.x;
    int    dy    = p.y-(int)center.y;
    double theta = Math.atan2( -dy, dx );
	
    int i = points.xpoints.length-1;

    if ( p.distance( points.xpoints[0], points.ypoints[0] ) < radius ||
         p.distance( points.xpoints[i], points.ypoints[i] ) < radius ) {
      return true; 
    }
	
    double distance = Math.sqrt( dx*dx + dy*dy );

    theta = ( theta < start)   ? theta + Math.PI*2: theta;

    if ( theta    >= start        && 
         theta    <= start+extent &&
         (Math.abs(distance-this.radius) < radius)){
      //	     distance <  radius ) {
      return true; 
    }
		
    return false;
  }


  /**
   *
   * Returns true if the point is within +-radius distance from
   * the original curve defining the object. Returns false o/w.
   *
   **/
  public boolean
    pointIsOnOriginal( Point p, int radius )
  {
    return points.pointIsOn( p, radius );
  }


  /**
   *
   * Set graphics context for this Geometric object.
   * Must be set at least once before doing any drawing.
   *
   **/
  public void
    setGraphicsContext( Graphics g )
  {
    graphics = g;
  }


  /**
   *
   * This method should return true if the input objects touch.
   * It should be optimized making use of the object type 
   * information.
   *
   **/
  public boolean
    touches( GeometricObject object )
  {
    return getPolygonalBounds().touches( object.getPolygonalBounds() );
  }


  /**
   *
   * Supplied for completeness.
   *
   **/
  public Rectangle
    getRectangularBounds()
  {
    return points.getRectangularBounds();
  }


  /**
   *
   * Returns false if the argument is not completely inside 
   * this object. Return true O/W.
   *
   **/
  public boolean
    containsGeometricObject( GeometricObject object )
  {
    return false;
  }


  /**
   *
   * This method should return a polygon that corresponds to this
   * object. The polygon is implicity closed and the last 
   * point doesn't necessarily have to be the same as the first 
   * (zeroth) point. The returned polygon is a liberal 
   * approximation to the real shape of the object. 
   *
   * Known eksik: This should be refined to return a more 
   * conservative result.
   *
   **/
  public Polygon 
    getPolygonalBounds()
  {
    return points.getPolygonalBounds();
  }


  /**
   *
   * This method should return the spatial relation of the input
   * parameter with respect to this object. see the SpatialRelation
   * class for a detailed list of possible spatial relations.
   * Another version of this method should be implemented for 
   * handling spatial relations where a rotated coordinate
   * system is to be used.
   *
   **/
  public int
    spatialRelation( GeometricObject object )
  {
    return getRectangularBounds().spatialRelation( object );
  }
    
    
  /**
   *
   * Sets the time stamp of the current Terminal
   *
   **/
  public void
    setTimeStamp( long time_stamp )
  {
    this.time_stamp = time_stamp;
  }
    
  /**
   *
   * Returns the time stamp of the current Terminal
   *
   **/
  public long
    getTimeStamp()
  {
    return time_stamp;
  }
    

  public void setOriginalVertices( Vertex pts[] )
  {
    setDataPoints( new Polygon( pts ) );
    vertices = new Vertex[pts.length];
    for ( int i = 0; i < pts.length; i++ ) {
      vertices[i] = pts[i];
    }
  }

  public Vertex[] getOriginalVertices()
  {
    if ( vertices == null ) {
      return null;
    }
    Vertex ret[] = new Vertex[vertices.length];
    for ( int i = 0; i < vertices.length; i++ ) {
      ret[i] = vertices[i];
    }
    return ret;
  }

  
  /**
   *
   * This method is used to set the original data points that
   * forms this GeometricObject
   *
   **/
  public void
    setDataPoints( Polygon points )
  {
    this.points = points;
  }
    
    
  /**
   *
   * This method is used to get the original data points that
   * forms this GeometricObject
   *
   **/
  public Polygon
    getDataPoints()
  {
    return points;
  }
    
  /**
   *
   * Add the arguments to the (x, y) position of the object.
   *
   **/
  public void
    translate( double x, double y )
  {
    center.translate( x,y );

    if ( points != null ){
      points.translate( x, y );
    }

    for(int i = 0; i < vertices.length; i++){
      vertices[i].translate(x,y);
    }

  }


  /**
   *
   * Returns false if the objects in the input array are
   * completely inside this object. Return true O/W.
   *
   **/
  public boolean
    containsGeometricObjects( GeometricObject objects[] )
  {
    return false;
  }


  /**
   *
   * Return the LSQ error.
   *
   **/
  public double
    leastSquaresError()
  {
    double error = 0.0;
		
    for ( int i=0; i<vertices.length; i++ ) {
      double distance = vertices[i].distance( center ) - radius;
      error += distance*distance;
    }
		
    return error;
  }

  public final GeometricObject copy()
  {
    Vertex[] newVertices = new Vertex[vertices.length];
    for( int i = 0; i < vertices.length; i++ ) {
      newVertices[i] = (Vertex)vertices[i].copy();
    }

    Arc arc = new Arc( newVertices );
    arc.time_stamp = time_stamp;
    
    if( points != null ) {
      arc.points = (Polygon)points.copy();
    }
    
    return arc;
  }
  
  
}

/** 
  * 
  * $Log: Arc.java,v $
  * Revision 1.16  2004/01/10 03:02:25  hammond
  * no real edits
  *
  * Revision 1.15  2003/11/05 20:31:29  moltmans
  * Fixed matrix rank deficency bug in arc and spiral.
  *
  * Revision 1.14  2003/06/26 19:57:14  calvarad
  * Lots of bug fixes
  *
  * Revision 1.13  2003/06/16 17:01:13  mtsezgin
  * *** empty log message ***
  *
  * Revision 1.12  2003/06/10 02:53:49  mtsezgin
  *
  * Added code to resume arc creation with lines as input. Users should check for
  * linearity before creating arcs.
  *
  * Revision 1.11  2003/04/21 16:40:45  calvarad
  * It doesn't really run, but for now at least it compiles
  *
  * Revision 1.10  2003/03/06 01:08:49  moltmans
  * Added copyright to all the files.
  *
  * Revision 1.9  2002/11/25 02:43:26  hammond
  * made ranges accessible
  *
  * Revision 1.8  2002/09/12 21:43:45  rebecca8
  * *** empty log message ***
  *
  * Revision 1.7  2002/08/14 18:35:56  moltmans
  * Added a copy method to Geometric primitives so that we can make deep
  * copies of objects.  The copy method is careful to not reuse any data.
  *
  * Revision 1.6  2002/08/06 15:37:30  mtsezgin
  * Added static serialVersionUID information to the serializable geometric objects,so that serialization doesn't break unless modifications made to the class are truly incompatible changes.
  *
  * Revision 1.5  2002/07/22 20:56:31  mtsezgin
  * Fixed minor bugs.
  *
  * Revision 1.4  2002/07/21 00:31:35  mtsezgin
  * Changed the representation for Arc. Now it is represented by a piece of
  * circle. There are methods for computing the center of the circle as well
  * as its radius.Ghie
  *
  * Revision 1.3  2002/07/15 21:44:44  mtsezgin
  * The change wasn't that simple anyways...
  *
  * Revision 1.2  2002/07/15 21:27:45  mtsezgin
  * Fixed a small bug related to approximation.
  *
  * Revision 1.1  2002/07/09 20:38:05  mtsezgin
  * Class representing an Arc
  *
  * Revision 1.4  2002/06/09 23:56:47  mtsezgin
  * added getP1, getP2
  *
  * Revision 1.3  2002/05/24 17:33:07  mtsezgin
  * Added utility functions.
  *
  * Revision 1.2  2001/11/23 03:23:30  mtsezgin
  * Major reorganization.
  *
  * Revision 1.1.1.1  2001/03/29 16:25:00  moltmans
  * Initial directories for DRG
  *
  * Revision 1.23  2000/09/06 22:40:38  mtsezgin
  * Combinations of curves and polygons are successfully approximated
  * by straight lines and Bezier curves as appropriate. System works
  * quite reliably.
  *
  * Revision 1.17  2000/06/08 03:14:30  mtsezgin
  *
  * Made the class Serializable for supporting saving and loading
  * designs. Both the object attributes, and the original data points
  * are stored and restored.
  *
  * Revision 1.16  2000/06/03 01:52:33  mtsezgin
  * *** empty log message ***
  *
  * Revision 1.15  2000/06/02 21:11:14  mtsezgin
  * *** empty log message ***
  *
  * Revision 1.14  2000/05/24 02:48:51  mtsezgin
  *
  * Fixed a bug in getIntersection( Line line )
  *
  * Revision 1.12  2000/05/03 23:26:45  mtsezgin
  * *** empty log message ***
  *
  * Revision 1.11  2000/04/28 04:45:03  mtsezgin
  *
  * Now each GeometricObject keeps the mouse input that was previously
  * discarded. User can switch between seeing the recognized mode and
  * the raw mode. setDataPoints( Polygon points ) and getDataPoints()
  * are added to GeometricObject, and all the implementors are modified
  * accordingly.
  *
  * Revision 1.10  2000/04/25 22:16:36  mtsezgin
  *
  * Fixed some bugs and added the getAngle and other angle related
  * funtions. Also getBounds changed to getRectangularBounds which returns
  * a Rectangle.
  *
  * Revision 1.9  2000/04/13 06:24:08  mtsezgin
  *
  * The current version of the program recognized Crosses, and Shades.
  * Implementors of Terminal and their descendants were modified to
  * implement the changes in GeometricObject.
  *
  * Revision 1.8  2000/04/12 04:00:16  mtsezgin
  * *** empty log message ***
  *
  * Revision 1.7  2000/04/11 01:41:45  mtsezgin
  * *** empty log message ***
  *
  * Revision 1.6  2000/04/11 00:41:46  mtsezgin
  *
  * Now the whole package succesfully parses a motor.
  *
  * Revision 1.5  2000/04/07 04:28:54  mtsezgin
  *
  * Added Rotatable interface. Rectangle and Line are Rotatable for now, but
  * Rectangle should be modified to have an angle field. Also other rotatable
  * classes should also implement Rotatable interface.
  *
  * Revision 1.4  2000/04/06 21:33:42  mtsezgin
  *
  * Spatial relation represents 9 basic relations for now. It should be extended.
  *
  * GeometricObject is extended. Implementors extended with empty stubs.
  *
  * Revision 1.3  2000/04/06 19:16:23  mtsezgin
  *
  * Modified all the classes to use my Point class which extends java.awt.Point
  * instead of directly using java.awt.Point
  *
  * Revision 1.2  2000/04/01 22:51:21  mtsezgin
  *
  * Started implementation. Also from now on using jdk 1.2
  *
  * Revision 1.1.1.1  2000/04/01 03:07:07  mtsezgin
  * Imported sources
  *
  * Revision 1.2  2000/03/31 22:41:03  mtsezgin
  *
  * Started Log tracking.
  *
  *  
  **/
