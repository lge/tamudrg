// (c) MIT 2003.  All rights reserved.

package edu.mit.sketch.geom;

import Jama.Matrix;
import edu.mit.sketch.geom.Ellipse;
import edu.mit.sketch.geom.GeometryUtil;
import edu.mit.sketch.geom.Point;
import edu.mit.sketch.geom.PolarPoint;
import edu.mit.sketch.geom.Range;
import edu.mit.sketch.geom.Vertex;
import edu.mit.sketch.toolkit.Blackboard;
import edu.mit.sketch.toolkit.StrokeData;
import edu.mit.sketch.ui.Tablet;
import edu.mit.sketch.util.AWTUtil;
import edu.mit.sketch.util.AWTUtil;
import edu.mit.sketch.util.GraphicsUtil;
import edu.mit.sketch.util.Util;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics2D;
import java.awt.Graphics;
import java.awt.Shape;
import java.awt.geom.AffineTransform;
import java.awt.geom.CubicCurve2D;
import java.awt.geom.Line2D;
import java.awt.geom.PathIterator;
import java.awt.geom.QuadCurve2D;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.util.Arrays;


public class  Spiral
    implements GeometricObject,
	       Serializable
{
    /**
     * The original data points
     **/
    private Polygon points;
    
  private Vertex m_vertices[];
  
  /**
     * Time stamp of this object.
     **/
    public long time_stamp;
    
    /**
     * Graphics context for this Geometric object.
     **/
    public transient Graphics graphics;
    
    private boolean parameters_computed = false;
    
    private Point center;
    
    //start points
    private double r_0;
    private double theta_0;

    //end points
    private double radius;
    private double extent;
    
    private Vertex vertices[];
    private PolarPoint polarPoints[];
    private Vertex idealPoints[];
    private Polygon idealPoly;
    
    private boolean clockwise;
    private double parameter;

    /**
     * Serialversion UID added to keep serialization working
     * when modifications are result in a compatible class.
     **/ 

    //TODO: huh?
    static final long serialVersionUID = 9059393618290047379L;


    /**
     *
     * Constructor that takes a stroke data. 
     * There must be at least 3 points in stroke_data
     *
     **/
    protected Spiral( StrokeData stroke_data )
    {
	this( stroke_data.vertices );
    }
    
    /**
     *
     * Constructor that takes vertices. There must be at least 3 points in 
     * vertices.
     *
     **/
    protected Spiral( Vertex vertices[] )
    {
	this.vertices = vertices;
	
	points = new Polygon( vertices );
	
	computeParameters();
    }

  public static Spiral fitSpiral(StrokeData strokeData) 
  {
    try {
      return new Spiral(strokeData);
    }
    catch( RuntimeException e ) {
      return null;
    }
  }

  public static Spiral fitSpiral(Vertex[] vertices) 
  {
    try {
      return new Spiral(vertices);
    }
    catch( RuntimeException e ) {
      return null;
    }
  }
  
    
  
    private void computeParameters(){

	center = getRectangularBounds().getCenter();

	int spiralInc;
	int spiralStart;
	int spiralEnd;
	
	if (vertices[0].distance(center) > 
	    vertices[vertices.length-1].distance(center)){

	    Vertex[] temp = new Vertex[vertices.length];

	    for(int i = 1; i <= vertices.length; i++){
		temp[i-1] = vertices[vertices.length - i];
	    }

	    vertices = temp;
	    
	}


	int arc_index;
	if ( vertices.length > 50 ) {
	    arc_index = vertices.length/5;
	} else {
	    if ( vertices.length > 8 ) {
		arc_index = 8;
	    } else {
		arc_index = vertices.length;
	    }
	}

	Vertex arc[] = new Vertex[arc_index];

	for(int i = 0; i<arc_index; i++){
	    arc[i] = vertices[i];
	}

	clockwise = true;
	
        Arc arcFit = new Arc(arc);
        center = arcFit.getCenter();
        clockwise = arcFit.getClockwise();
	    
//	System.out.println("Center(after) is " + center);

	radius = center.distance(vertices[vertices.length -1]);

	// Archimedes spiral has equation r=r_0 + a(theta-theta_0).  Sprial is 
	// determined by the value of a.  Find the r and theta for each data 
	// point and then do a linear least squares fitting to find the best a.  
	// Thetas need to increase around the circle.  Negative a's?  Offset?
        
	r_0 = center.distance(vertices[0]);
	theta_0 = Math.atan2((vertices[0].y - center.y),
			     (vertices[0].x - center.x));
	
	//	System.out.println("****************************************************");

	//(distance from center to point) - (initial radius)
	double radii[][] = new double[vertices.length][1];
	
	//(angle of point) - (initial angle)
	double angles[][] = new double[vertices.length][1];
	
	double lastTheta = 0;

	polarPoints = new PolarPoint[vertices.length];

	int i; 
	
	double a = 0;

	for(i = 0; i < vertices.length; i++){

	    radii[i][0] = vertices[i].distance(center) - r_0;

	    angles[i][0] = Math.atan2((vertices[i].y - center.y),
				      (vertices[i].x - center.x));

	    angles[i][0] -= theta_0;

	    if (clockwise){
		while (angles[i][0] - lastTheta < -.2){
		    angles[i][0] += 2 * Math.PI;
		
		}
	    }
	    else{
		while (angles[i][0] - lastTheta > .2){
		    angles[i][0] -= 2 * Math.PI;
		}
	    }

	    lastTheta = angles[i][0];

	    if (angles[i][0] != 0)
		a += radii[i][0]/angles[i][0];
	
	    polarPoints[i] = new PolarPoint(radii[i][0], angles[i][0]);
	}

	extent = angles[i-1][0];

	Matrix matrixA = new Matrix(angles);
	Matrix matrixb = new Matrix(radii);
	
	Matrix matrixx = matrixA.solve(matrixb);

	parameter = matrixx.get(0,0);

	//Find the ideal points given this parameter.


	
	//	System.out.println("EXTENT IS: " + extent);
	double range = Math.abs(extent - theta_0);

	int numberOfPoints = (int)((range) * 10) + 1;

	idealPoints = new Vertex[numberOfPoints];

	double theta = theta_0;

	for(int j = 0; j < numberOfPoints; j++){
	    
	    //double theta = polarPoints[i].theta;
	    
	    if(clockwise){
		theta = theta_0 + ((double)j)/10;
	    }
	    else{
		theta = theta_0 - ((double)j)/10;
	    }

	    double radius = r_0 + parameter*(theta - theta_0);
	    
	    idealPoints[j] = new Vertex((int)(radius*Math.cos(theta) + center.x),
					(int)(radius*Math.sin(theta) + center.y));
	}

	//	System.out.println("THETA IS: " + theta);

	idealPoly = new Polygon(idealPoints);

	parameters_computed = true;

    }


    
    public double leastSquaresError()
    {
	double returnMe = 0;
	double returnMe2 = 0;

	for (int i = 0; i < vertices.length; i++){

	    int index = idealPoly.closestNeighborIndex(vertices[i]);
	    returnMe += Math.pow(idealPoly.pointAt(index).distance(vertices[i]),2);

	    double theta = polarPoints[i].theta;
	    double radius = polarPoints[i].radius;
	    if(theta != 0){
		double estimated = radius/theta;
		double error = estimated - parameter;
		returnMe2 += Math.pow(error, 2);
	    }
	}
	
	return returnMe2/vertices.length;
	    
    }

    public Point getCenter(){
	return center;
    }

    public boolean getClockwise(){
	return clockwise;
    }

    public double getRotations(){
	
	return Math.abs(extent/(2*Math.PI));
    }


    public void translate(double x, double y){

	center.translate( x,y );
	
        if ( points != null ){
            points.translate( x, y );
	}
	
	if ( idealPoly != null){
	    idealPoly.translate(x,y);
	}

	for(int i = 0; i < vertices.length; i++){
	    vertices[i].translate(x,y);
	    //	    polarPoints[i].translate(x,y);
	}
	
	for(int i = 0; i < idealPoints.length; i++){
	    idealPoints[i].translate(x,y);
	}

    }
    public boolean pointIsOn(Point p, int offset){

	if(!parameters_computed){
	    computeParameters();
	}
	return idealPoly.pointIsOn(p, offset);

    }

    public boolean pointIsOnOriginal( Point p, int radius )
    {
        return points.pointIsOn( p, radius );
    }

    public Polygon getPolygonalBounds()
    {
        return points.getPolygonalBounds();
    }

    public boolean touches( GeometricObject object )
    {
        return getPolygonalBounds().touches( object.getPolygonalBounds() );
    }

    public int spatialRelation( GeometricObject object )
    {
        return getRectangularBounds().spatialRelation( object );
    }

    public Rectangle getRectangularBounds()
    {
        return points.getRectangularBounds();
    }

  public void setOriginalVertices( Vertex pts[] )
  {
    setDataPoints( new Polygon( pts ) );
    m_vertices = new Vertex[pts.length];
    for ( int i = 0; i < pts.length; i++ ) {
      m_vertices[i] = pts[i];
    }
  }

  public Vertex[] getOriginalVertices()
  {
    if ( m_vertices == null ) {
      return null;
    }
    Vertex ret[] = new Vertex[m_vertices.length];
    for ( int i = 0; i < m_vertices.length; i++ ) {
      ret[i] = m_vertices[i];
    }
    return ret;
  }

  
    public void setDataPoints( Polygon points )
    {
        this.points = points;
    }

    public Polygon getDataPoints(  )
    {
        return points;
    }

    public final GeometricObject copy()
    {
	Vertex[] newVertices = new Vertex[vertices.length];
	for( int i = 0; i < vertices.length; i++ ) {
	    newVertices[i] = (Vertex)vertices[i].copy();
	}
	
	Spiral spiral = new Spiral( newVertices );
	spiral.time_stamp = time_stamp;
	
	if( points != null ) {
	    spiral.points = (Polygon)points.copy();
	}
	
	return spiral;
    }

    public void setGraphicsContext( Graphics g )
    {
        graphics = g;
    }
 
        
    /**
     *
     * Sets the time stamp of the current Terminal
     *
     **/
    public void setTimeStamp( long time_stamp )
    {
        this.time_stamp = time_stamp;
    }
    
    /**
     *
     * Returns the time stamp of the current Terminal
     *
     **/
    public long getTimeStamp()
    {
        return time_stamp;
    }
   
        /**
    *
    * Override toString
    *
    **/    
    public String toString()
    {
        return "Spiral " + super.toString();
    }
    
    
    /**
    *
    * Draw the object
    *
    **/
    public void
    paint()
    {
        graphics.setColor( Color.black );
        paint( graphics );
    }
    
    
    /**
    *
    * Draw the object
    *
    **/
    public void
    paint( Graphics g )
    {
	if(!parameters_computed){
	    computeParameters();
	}

	idealPoly.paint(g);
    }
    
    
    /**
    *
    * This method is used to paint the original data points that
    * forms this GeometricObject
    *
    **/
    public void
    paintOriginal( Graphics g )
    {
        points.paint( g );
    }

    public String getType()
    {
        return "spiral";
    }


    public boolean containsGeometricObject( GeometricObject object )
    {
        return false;
    }
    
    public boolean containsGeometricObjects( GeometricObject objects[] )
    {
	return false;
    }

}
