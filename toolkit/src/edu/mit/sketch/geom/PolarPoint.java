// (c) MIT 2003.  All rights reserved.

////////////////////////////////////////////////////////////////////////////////
//                                                                            //
// AUTHOR:      Tevfik Metin Sezgin                                           //
//              Massachusetts Institute of Technology                         //
//              Department of Electrical Engineering and Computer Science     //
//              Artificial Intelligence Laboratory                            //
//                                                                            //
// E-MAIL:        mtsezgin@ai.mit.edu, mtsezgin@mit.edu                       //
//                                                                            //
// COPYRIGHT:   Tevfik Metin Sezgin                                           //
//              All rights reserved. This code can not be copied, modified,   //
//              or distributed in whole or partially without the written      //
//              permission of the author. Also see the COPYRIGHT file.        //
//                                                                            //
////////////////////////////////////////////////////////////////////////////////
package edu.mit.sketch.geom;

import java.io.Serializable;

/** 
  * 
  * See the end of the file for the log of changes.
  * 
  * $Author: olyav $
  * $Date: 2003/05/12 17:21:51 $   
  * $Revision: 1.3 $
  * $Headers$
  * $Id: PolarPoint.java,v 1.3 2003/05/12 17:21:51 olyav Exp $     
  * $Name:  $   
  * $Locker:  $
  * $Source: /projects/drg/CVSROOT/drg/code/src/edu/mit/sketch/geom/PolarPoint.java,v $
  *  
  **/


/**
 *
 * Polar Point object that stores an angle and a radius.
 *
 **/

public class PolarPoint implements Serializable
{
  public double theta;
  public double radius;
    
    /**
       *
     * Polar Point constructor
      *
      **/
  public PolarPoint()
    {
        this( 0.0, 0.0 );
    }
    
    /**
      *
      * Polar Point constructor
      *
      **/
  public PolarPoint( double radius, double theta )
    {
        this.theta  = theta;
        this.radius = radius;
    }
    
    /**
      *
      * Simple printing
      *
      **/
    public String
    toString()
    {
        return "Theta = " + theta + " radius = " + radius;
    }
}


/** 
  * 
  * $Log: PolarPoint.java,v $
  * Revision 1.3  2003/05/12 17:21:51  olyav
  * Made PolarPoint serializable. Added a methods to Ellipse and Line to be able to retrieve their center.
  *
  * Revision 1.2  2003/03/06 01:08:49  moltmans
  * Added copyright to all the files.
  *
  * Revision 1.1.1.1  2001/03/29 16:25:00  moltmans
  * Initial directories for DRG
  *
  * Revision 1.4  2000/09/06 22:40:52  mtsezgin
  * Combinations of curves and polygons are successfully approximated
  * by straight lines and Bezier curves as appropriate. System works
  * quite reliably.
  *
  * Revision 1.1.1.1  2000/04/01 03:07:07  mtsezgin
  * Imported sources
  *
  * Revision 1.2  2000/03/31 22:41:04  mtsezgin
  *
  * Started Log tracking.
  *
  *  
  **/
