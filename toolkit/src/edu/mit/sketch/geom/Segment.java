// (c) MIT 2003.  All rights reserved.

////////////////////////////////////////////////////////////////////////////////
//                                                                            //
// AUTHOR:      Tevfik Metin Sezgin                                           //
//              Massachusetts Institute of Technology                         //
//              Department of Electrical Engineering and Computer Science     //
//              Artificial Intelligence Laboratory                            //
//                                                                            //
// E-MAIL:        mtsezgin@ai.mit.edu, mtsezgin@mit.edu                       //
//                                                                            //
// COPYRIGHT:   Tevfik Metin Sezgin                                           //
//              All rights reserved. This code can not be copied, modified,   //
//              or distributed in whole or partially without the written      //
//              permission of the author. Also see the COPYRIGHT file.        //
//                                                                            //
////////////////////////////////////////////////////////////////////////////////
package edu.mit.sketch.geom;


/** 
  * 
  * See the end of the file for the log of changes.
  * 
  * $Author: moltmans $
  * $Date: 2003/03/06 01:08:49 $   
  * $Revision: 1.5 $
  * $Headers$
  * $Id: Segment.java,v 1.5 2003/03/06 01:08:49 moltmans Exp $     
  * $Name:  $   
  * $Locker:  $
  * $Source: /projects/drg/CVSROOT/drg/code/src/edu/mit/sketch/geom/Segment.java,v $
  *  
  **/

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;

import java.io.Serializable;

import java.util.Comparator;
import java.util.ArrayList;

import edu.mit.sketch.util.Util;

/**
  *
  * This class represents a Point described by a 
  * java.awt.geom.Line2D.Double
  *
  **/
public
class   Segment
extends Range
{
    /**
    *
    * Significance of this segment
    *
    **/
    public double significance;
    

    /**
    *
    * true if included 
    *
    **/
    public boolean included;


    /**
    *
    * left subsegment
    *
    **/
    public Segment parent;


    /**
    *
    * left subsegment
    *
    **/
    public Segment left;


    /**
    *
    * right subsegment
    *
    **/
    public Segment right;


    /**
    *
    * vertices
    *
    **/
    public Vertex vertices[];


    /**
    *
    * SegmentCollection
    *
    **/
    public SegmentCollection accumulated_segments = new SegmentCollection();


    /**
    *
    * The constructor.
    *
    **/    
    public Segment()
    {
    }


    /**
    *
    * The constructor.
    *
    **/    
    public Segment( Vertex vertices[], int min, int max )
    {
        this.min       = min;
        this.max      = max;
        this.vertices = vertices;
    }
    
    
    /**
    *
    * Override toString
    *
    **/    
    public String
    toString()
    {
        return "Segment [" + min + ", " + max + "] significance = " + 
               significance + " included = " + included;
    }
    
    
    /**
    *
    * subdivide at the index with the maximum deviation
    * return false if cannot be divided any further.
    *
    **/    
    public boolean
    subdivide()
    {
        if ( ( (max - min) < 5                             ) || 
             ( vertices[min].distance( vertices[max] ) < 4 ) ) {
            return false;
        }
        
        int max_index = adjustSignificance();
        
        if ( significance == Double.MAX_VALUE ) 
            return false;
        
        left  = new Segment( vertices, min,       max_index );
        right = new Segment( vertices, max_index, max       );
        
        left.parent  = this;        
        right.parent = this;        

        System.out.println( "divided segment [ " + min       + 
                            ", "                 + max_index +
                            ", "                 + max       + 
                            " ] significance = " + significance );
        
        return true;
    }
    
    
    /**
    *
    * build tree
    *
    **/    
    public void
    buildTree()
    {
        if ( subdivide() == false )
            return;
        
        left.buildTree();
        right.buildTree();
    }
    
    
    /**
    *
    * accumulate segments
    *
    **/    
    public void
    accumulateSegments()
    {
        if ( ( left == null ) && ( right == null ) ) {
            accumulated_segments.add( this );
            return;
        }

        if ( right == null ) {
            left.accumulateSegments();
            
            if ( significance > left.accumulated_segments.max_significance ) {
                accumulated_segments.add( this );
            } else {
                accumulated_segments.addAll( left.accumulated_segments );
            }
            return;
        }

        if ( left == null ) {
            right.accumulateSegments();
            
            if ( significance > right.accumulated_segments.max_significance ) {
                accumulated_segments.add( this );
            } else {
                accumulated_segments.addAll( right.accumulated_segments );
            }
            return;
        }

        left.accumulateSegments();
        right.accumulateSegments();
        
        if ( ( significance < right.accumulated_segments.max_significance ) ||
             ( significance < left.accumulated_segments.max_significance  ) ) {
            accumulated_segments.addAll( left.accumulated_segments );
            accumulated_segments.addAll( right.accumulated_segments );
        } else {
            accumulated_segments.add( this );
        }
    }
        
        
    /**
    *
    * override to preserve maximum significance of the segments in 
    * this collection
    *
    **/
    public int[]
    getFeaturePointIndices() 
    {
        buildTree();
        accumulateSegments();
        
        int indices[] = new int[accumulated_segments.size()+1];
        
        
        indices[0] = 0;
        for ( int i=1; i<indices.length; i++ ) {
            indices[i] = ((Segment)accumulated_segments.get( i-1 )).max;
        }
        
            
        Util.printArray( indices, "final index " );
        
        return indices;
    }
        
        
    /**
    *
    * override to preserve maximum significance of the segments in 
    * this collection
    *
    **/
    public int
    adjustSignificance() 
    {
        int    max_index    = min;
        double distance     = 0.0;
        double max_distance = 0.0;
        Line line      = new Line( vertices[min], vertices[max] );
        for ( int i=min; i<=max; i++ ) {
            distance = line.ptSegDist( vertices[i] );
            if ( distance > max_distance ) {
                max_index    = i;
                max_distance = distance;
            }
        }

        distance = line.ptSegDist( vertices[max_index] );

        if ( distance == 0.0 ) {
            significance = Double.MAX_VALUE;
        } else {
            significance = vertices[min].distance(vertices[max])/distance;
        }
        
        return max_index;
    }

    class   SegmentCollection
    extends ArrayList
    {
        /**
        *
        * maximum significance of the segments in this collection
        *
        **/
        double max_significance;
        
        
        /**
        *
        * constructor
        *
        **/
        public SegmentCollection()
        {
        }
        
        
        /**
        *
        * override to preserve maximum significance of the segments in 
        * this collection
        *
        **/
        public void
        add( Segment segment ) 
        {
            super.add( segment );

            if ( segment.max > max )
                max = segment.max;

            if ( segment.min > min )
                min = segment.min;
            
            adjustSignificance();            
            max_significance = significance;
        }
        
        
        /**
        *
        * override to preserve maximum significance of the segments in 
        * this collection
        *
        **/
        public void
        add( int index, Segment segment ) 
        {
            super.add( index, segment );

            if ( segment.max > max )
                max = segment.max;

            if ( segment.min > min )
                min = segment.min;
            
            adjustSignificance();
            max_significance = significance;
        }
        
        
        /**
        *
        * override to preserve maximum significance of the segments in 
        * this collection
        *
        **/
        public boolean
        addAll( SegmentCollection collection ) 
        {
            boolean result = super.addAll( collection );

            if ( collection.getMax() > max )
                max = collection.getMax();

            if ( collection.getMin() > min )
                min = collection.getMin();
            
            adjustSignificance();
            max_significance = significance;
            
            return result;
        }
        
        
        /**
        *
        * override to preserve maximum significance of the segments in 
        * this collection
        *
        **/
        public boolean
        addAll( int index, SegmentCollection collection ) 
        {
            boolean result = super.addAll( index, collection );

            if ( collection.getMax() > max )
                max = collection.getMax();

            if ( collection.getMin() > min )
                min = collection.getMin();
            
            adjustSignificance();
            max_significance = significance;
            
            return result;
        }
        
        
        /**
        *
        * min
        *
        **/
        public int
        getMin() 
        {
            return min;
        }
        
        
        /**
        *
        * max
        *
        **/
        public int
        getMax() 
        {
            return max;
        }
    }
}
/** 
  * 
  * $Log: Segment.java,v $
  * Revision 1.5  2003/03/06 01:08:49  moltmans
  * Added copyright to all the files.
  *
  * Revision 1.4  2001/11/23 03:23:31  mtsezgin
  * Major reorganization.
  *
  * Revision 1.3  2001/10/12 22:26:27  mtsezgin
  * This is a general commit of all files.
  *
  * Revision 1.2  2001/04/12 19:25:55  mtsezgin
  * *** empty log message ***
  *
  * Revision 1.1  2001/04/05 19:18:28  uid6752
  * Segments for partitioning.
  *
  * Revision 1.1.1.1  2001/03/29 16:25:00  moltmans
  * Initial directories for DRG
  *
  * Revision 1.18  2000/09/06 22:40:51  mtsezgin
  * Combinations of curves and polygons are successfully approximated
  * by straight lines and Bezier curves as appropriate. System works
  * quite reliably.
  *
  * Revision 1.12  2000/06/08 03:14:30  mtsezgin
  *
  * Made the class Serializable for supporting saving and loading
  * designs. Both the object attributes, and the original data points
  * are stored and restored.
  *
  * Revision 1.11  2000/06/03 01:52:34  mtsezgin
  * *** empty log message ***
  *
  * Revision 1.10  2000/05/03 23:26:46  mtsezgin
  * *** empty log message ***
  *
  * Revision 1.9  2000/04/28 04:45:04  mtsezgin
  *
  * Now each GeometricObject keeps the mouse input that was previously
  * discarded. User can switch between seeing the recognized mode and
  * the raw mode. setDataPoints( Polygon points ) and getDataPoints()
  * are added to GeometricObject, and all the implementors are modified
  * accordingly.
  *
  * Revision 1.8  2000/04/25 22:18:57  mtsezgin
  *
  * The getBounds changed to getRectangularBounds which returns a Rectangle.
  *
  * Revision 1.7  2000/04/17 07:02:30  mtsezgin
  *
  * Finally made the Rectangle really rotatable.
  *
  * Revision 1.6  2000/04/13 06:24:08  mtsezgin
  *
  * The current version of the program recognized Crosses, and Shades.
  * Implementors of Terminal and their descendants were modified to
  * implement the changes in GeometricObject.
  *
  * Revision 1.5  2000/04/11 01:41:46  mtsezgin
  * *** empty log message ***
  *
  * Revision 1.4  2000/04/11 00:41:47  mtsezgin
  *
  * Now the whole package succesfully parses a motor.
  *
  * Revision 1.3  2000/04/07 04:28:54  mtsezgin
  *
  * Added Rotatable interface. Rectangle and Line are Rotatable for now, but
  * Rectangle should be modified to have an angle field. Also other rotatable
  * classes should also implement Rotatable interface.
  *
  *
  **/
