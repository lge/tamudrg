// (c) MIT 2003.  All rights reserved.

////////////////////////////////////////////////////////////////////////////////
//                                                                            //
// AUTHOR:      Tevfik Metin Sezgin                                           //
//              Massachusetts Institute of Technology                         //
//              Department of Electrical Engineering and Computer Science     //
//              Artificial Intelligence Laboratory                            //
//                                                                            //
// E-MAIL:        mtsezgin@ai.mit.edu, mtsezgin@mit.edu                       //
//                                                                            //
// COPYRIGHT:   Tevfik Metin Sezgin                                           //
//              All rights reserved. This code can not be copied, modified,   //
//              or distributed in whole or partially without the written      //
//              permission of the author. Also see the COPYRIGHT file.        //
//                                                                            //
////////////////////////////////////////////////////////////////////////////////
package edu.mit.sketch.geom;

/** 
  * 
  * See the end of the file for the log of changes.
  * 
  * $Author: moltmans $
  * $Date: 2003/03/06 01:08:49 $   
  * $Revision: 1.3 $
  * $Headers$
  * $Id: SpatialDatabase.java,v 1.3 2003/03/06 01:08:49 moltmans Exp $     
  * $Name:  $   
  * $Locker:  $
  * $Source: /projects/drg/CVSROOT/drg/code/src/edu/mit/sketch/geom/SpatialDatabase.java,v $
  *  
  **/

import java.io.Serializable;

import java.util.ArrayList;
import java.util.SortedSet;
import java.util.TreeSet;

/**
  *
  * This class will serve as data structure that keeps the positions
  * of objects. It also supports some spatial queries.
  *
  **/
public
class      SpatialDatabase
implements Serializable
{
    /**
    *
    * The tree that holds the points sorted by their y positions
    *
    **/
    private XPositionTree x_position_tree;
    

    /**
    *
    * The constructor.
    *
    **/    
	public 
	SpatialDatabase()
    {
		x_position_tree = new XPositionTree();
    }
     
         
    /**
    *
    * Add a point
    *
    **/    
    public void
    addPoint( Point point )
    {
        x_position_tree.addPoint( point );
    }
     
         
    /**
    *
    * get the points inside
    *
    **/    
    public ArrayList
    pointsInside( int x1, int y1, int x2, int y2 )
    {
        return x_position_tree.pointsInside( x1, y1, x2, y2 );
    }
     
         
    /**
    *
    * get the objects inside the focus rectangle. This
	* must be implemented to do the right thing.
    *
    **/    
    public Object[]
    getObjects( Rectangle focus )
    {
        return x_position_tree.pointsInside( (int)focus.x, 
											 (int)focus.y, 
											 (int)(focus.x + focus.getWidth()), 
											 (int)(focus.y + focus.getHeight()) ).toArray();
    }
}

/** 
  * 
  * $Log: SpatialDatabase.java,v $
  * Revision 1.3  2003/03/06 01:08:49  moltmans
  * Added copyright to all the files.
  *
  * Revision 1.2  2002/05/24 17:33:07  mtsezgin
  * Added utility functions.
  *
  * Revision 1.1  2002/02/26 02:33:37  mtsezgin
  * Started on the spatial query infrastructure. It is not as efficient
  * as I want it to be yet.
  *
=  *
  *  
  **/
