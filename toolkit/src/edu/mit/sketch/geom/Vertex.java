// (c) MIT 2003.  All rights reserved.

////////////////////////////////////////////////////////////////////////////////
//                                                                            //
// AUTHOR:      Tevfik Metin Sezgin                                           //
//              Massachusetts Institute of Technology                         //
//              Department of Electrical Engineering and Computer Science     //
//              Artificial Intelligence Laboratory                            //
//                                                                            //
// E-MAIL:        mtsezgin@ai.mit.edu, mtsezgin@mit.edu                       //
//                                                                            //
// COPYRIGHT:   Tevfik Metin Sezgin                                           //
//              All rights reserved. This code can not be copied, modified,   //
//              or distributed in whole or partially without the written      //
//              permission of the author. Also see the COPYRIGHT file.        //
//                                                                            //
////////////////////////////////////////////////////////////////////////////////
package edu.mit.sketch.geom;

/** 
  * 
  * See the end of the file for the log of changes.
  * 
  * $Author: moltmans $
  * $Date: 2003/03/06 01:08:49 $   
  * $Revision: 1.8 $
  * $Headers$
  * $Id: Vertex.java,v 1.8 2003/03/06 01:08:49 moltmans Exp $     
  * $Name:  $   
  * $Locker:  $
  * $Source: /projects/drg/CVSROOT/drg/code/src/edu/mit/sketch/geom/Vertex.java,v $
  *  
  **/


import java.io.Serializable;

import java.util.Comparator;
import java.util.Vector;
import java.util.ArrayList;


/**
  *
  * This class represents a Vertex described by a Point and its
  * certainty.
  *
  **/
public
class      Vertex
extends    Point
implements Serializable,
           Comparator
{
    /**
    *
    * The index
    *
    **/    
  public int index;


    /**
    *
    * The certainty
    *
    **/    
  public double certainty;
    
    /**
    * Serialversion UID added to keep serialization working
	* when modifications are result in a compatible class.
    **/    
	static final long serialVersionUID = -9083123633509598859L;

    /**
    *
    * The constructor.
    *
    **/    
  public Vertex()
    {
        super();
        certainty = 0.0;
    }


    /**
    *
    * The constructor.
    *
    **/    
  public Vertex( java.awt.Point point )
    {
        super( point );

        if ( point instanceof Vertex ) {
            index      = ((Vertex)point).index;
            certainty  = ((Vertex)point).certainty;
            time_stamp = ((Vertex)point).getTimeStamp();
        } else {
            if ( point instanceof Point ) {
                time_stamp = ((Point)point).getTimeStamp();
            } else {
                certainty = 0.0;
            }
        }
    }


    /**
    *
    * The constructor.
    *
    **/    
  public Vertex( int x, int y )
    {
        super( x, y );
        certainty = 0.0;
    }
    

    /**
    *
    * The constructor.
    *
    **/    
  public Vertex( double certainty )
    {
        super();
        this.certainty = certainty;
    }


    /**
    *
    * The constructor.
    *
    **/    
  public Vertex( java.awt.Point point, double certainty )
    {
        super( point );
        this.certainty = certainty;
        
        if ( point instanceof Vertex ) {
            index     = ((Vertex)point).index;
        }
    }


    /**
    *
    * The constructor.
    *
    **/    
  public Vertex( int x, int y, double certainty )
    {
        super( x, y );
        this.certainty = certainty;
    }
    
    
    /**
    *
    * The constructor.
    *
    **/    
  public Vertex( Vertex vertex )
    {
        this( vertex.x, vertex.y, vertex.certainty );
        index = vertex.index;
        this.time_stamp = vertex.getTimeStamp();
    }
    
    
    /**
    *
    * Set index
    *
    **/    
    public void
    setIndex( int index )
    {
        this.index = index;
    }
    
    
    /**
    *
    * Override toString
    *
    **/    
    public String
    toString()
    {
        return "Vertex ( " + x + ", " + y + " ) c = " + 
            ( certainty + "              " ).substring( 0, 5 )   +
            ", i = " + index + " t = " + time_stamp;
    }
    
    
    /**
    *
    * Implement Comparator
    *
    **/    
    public int
    compare( Object o1, Object o2 )
    {
        if ( (((Vertex)o1).certainty-((Vertex)o2).certainty) < 0 )
            return -1;
        if ( (((Vertex)o1).certainty-((Vertex)o2).certainty) > 0 )
            return 1;
        return 0;
    }
    
    
    /**
    *
    * Implement Comparator
    *
    **/    
    public boolean
    equals( Object o1, Object o2 )
    {
        return ( ((Vertex)o1).certainty == ((Vertex)o2).certainty );
    }
    
    
    /**
    *
    * Remove repeating vertices
    *
    **/    
    public static Vertex[]
    removeDuplicateVertices( Vertex vertices[] )
    {
        Vertex copy[] = new Vertex[vertices.length];
        
        ArrayList list = new ArrayList();
        
        for ( int i=0; i< vertices.length-1; i++ ) {
            if ( ( vertices[i].x != vertices[i+1].x ) ||
                 ( vertices[i].y != vertices[i+1].y ) ) {
                list.add( vertices[i] );
            }
        }
        
        list.add( vertices[vertices.length-1] );
        
        return arrayListToArray( list );
    }
    
    
    /**
    *
    * For obtaining a complete copy of an array of Vertex objects.
    *
    **/    
    public static Vertex[]
    cloneVertices( Vertex vertices[] )
    {
        Vertex copy[] = new Vertex[vertices.length];
        for ( int i=0; i< vertices.length; i++ ) {
            copy[i] = new Vertex( vertices[i] );
            copy[i].setTimeStamp( vertices[i].getTimeStamp() );
        }
        
        return copy;
    }
    
    
    /**
    *
    * Appends the input vertices with the argument vertex
    *
    **/    
    public static Vertex[]
    appendVertices( Vertex vertices[], Vertex vertex )
    {
        Vertex result[] = new Vertex[vertices.length+1];
        for ( int i=0; i<vertices.length; i++ ) {
            result[i] = vertices[i];
        }
        result[result.length-1] = vertex;
        
        return result;
    }
    
    
    /**
    *
    * Returns an array containing the elements of the Vector.
    *
    **/    
    public static Vertex[]
    arrayListToArray( ArrayList vertices )
    {
        Vertex result[] = new Vertex[vertices.size() ];
        for ( int i=0; i< result.length; i++ ) {
            result[i] = (Vertex)( vertices.get( i ) );
        }
        
           return result;
    }
    
    
    /**
    *
    * Returns an array containing the elements of the Vector.
    *
    **/    
    public static Vertex[]
    vectorToArray( Vector vertices )
    {
        Vertex result[] = new Vertex[vertices.size() ];
        for ( int i=0; i< result.length; i++ ) {
            result[i] = (Vertex)( vertices.elementAt( i ) );
        }
        
           return result;
    }

  public final GeometricObject copy() 
  {
    Vertex vertex = new Vertex(this);
    if( getDataPoints() != null ) {
      vertex.setDataPoints( (Polygon)getDataPoints().copy() );
    }
    return vertex;    
  }
  
}
/** 
  * 
  * $Log: Vertex.java,v $
  * Revision 1.8  2003/03/06 01:08:49  moltmans
  * Added copyright to all the files.
  *
  * Revision 1.7  2002/08/14 18:35:55  moltmans
  * Added a copy method to Geometric primitives so that we can make deep
  * copies of objects.  The copy method is careful to not reuse any data.
  *
  * Revision 1.6  2002/08/06 15:37:30  mtsezgin
  * Added static serialVersionUID information to the serializable geometric objects,so that serialization doesn't break unless modifications made to the class are truly incompatible changes.
  *
  * Revision 1.5  2002/07/22 20:56:31  mtsezgin
  * Fixed minor bugs.
  *
  * Revision 1.4  2001/11/23 03:23:31  mtsezgin
  * Major reorganization.
  *
  * Revision 1.3  2001/10/12 22:26:27  mtsezgin
  * This is a general commit of all files.
  *
  * Revision 1.2  2001/04/12 19:25:55  mtsezgin
  * *** empty log message ***
  *
  * Revision 1.1.1.1  2001/03/29 16:25:00  moltmans
  * Initial directories for DRG
  *
  * Revision 1.6  2001/03/23 00:00:22  mtsezgin
  * A major release with added files. Includes scale space method.
  *
  * Revision 1.3  2000/09/06 22:41:09  mtsezgin
  * Combinations of curves and polygons are successfully approximated
  * by straight lines and Bezier curves as appropriate. System works
  * quite reliably.
  *
  * Revision 1.12  2000/06/08 03:14:30  mtsezgin
  *
  * Made the class Serializable for supporting saving and loading
  * designs. Both the object attributes, and the original data points
  * are stored and restored.
  *
  * Revision 1.11  2000/06/03 01:52:34  mtsezgin
  * *** empty log message ***
  *
  * Revision 1.10  2000/05/03 23:26:46  mtsezgin
  * *** empty log message ***
  *
  * Revision 1.9  2000/04/28 04:45:04  mtsezgin
  *
  * Now each GeometricObject keeps the mouse input that was previously
  * discarded. User can switch between seeing the recognized mode and
  * the raw mode. setDataPoints( Polygon points ) and getDataPoints()
  * are added to GeometricObject, and all the implementors are modified
  * accordingly.
  *
  * Revision 1.8  2000/04/25 22:18:57  mtsezgin
  *
  * The getBounds changed to getRectangularBounds which returns a Rectangle.
  *
  * Revision 1.7  2000/04/17 07:02:30  mtsezgin
  *
  * Finally made the Rectangle really rotatable.
  *
  * Revision 1.6  2000/04/13 06:24:08  mtsezgin
  *
  * The current version of the program recognized Crosses, and Shades.
  * Implementors of Terminal and their descendants were modified to
  * implement the changes in GeometricObject.
  *
  * Revision 1.5  2000/04/11 01:41:46  mtsezgin
  * *** empty log message ***
  *
  * Revision 1.4  2000/04/11 00:41:47  mtsezgin
  *
  * Now the whole package succesfully parses a motor.
  *
  * Revision 1.3  2000/04/07 04:28:54  mtsezgin
  *
  * Added Rotatable interface. Rectangle and Line are Rotatable for now, but
  * Rectangle should be modified to have an angle field. Also other rotatable
  * classes should also implement Rotatable interface.
  *
  *
  **/
