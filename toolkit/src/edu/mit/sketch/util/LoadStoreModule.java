// (c) MIT 2003.  All rights reserved.

////////////////////////////////////////////////////////////////////////////////
//                                                                            //
// AUTHOR:      Tevfik Metin Sezgin                                           //
//              Massachusetts Institute of Technology                         //
//              Department of Electrical Engineering and Computer Science     //
//              Artificial Intelligence Laboratory                            //
//                                                                            //
// E-MAIL:        mtsezgin@ai.mit.edu, mtsezgin@mit.edu                       //
//                                                                            //
// COPYRIGHT:   Tevfik Metin Sezgin                                           //
//              All rights reserved. This code can not be copied, modified,   //
//              or distributed in whole or partially without the written      //
//              permission of the author. Also see the COPYRIGHT file.        //
//                                                                            //
////////////////////////////////////////////////////////////////////////////////
package edu.mit.sketch.util;

/**
  * DON'T USE THIS FOR STORING STROKE OBJECTS.  PLEASE USE THE NEW
  * StrokeReader AND StrokeWriter CLASSES INSTEAD.
  *
  * See the end of the file for the log of changes.
  *
  * $Author: mtsezgin $
  * $Date: 2003/10/13 03:10:57 $
  * $Revision: 1.9 $
  * $Headers$
  * $Id: LoadStoreModule.java,v 1.9 2003/10/13 03:10:57 mtsezgin Exp $
  * $Name:  $
  * $Locker:  $
  * $Source: /projects/drg/CVSROOT/drg/code/src/edu/mit/sketch/util/LoadStoreModule.java,v $
  *
  **/



import edu.mit.sketch.geom.Ellipse;
import edu.mit.sketch.geom.GeneralPath;
import edu.mit.sketch.geom.Line;
import edu.mit.sketch.geom.Point;
import edu.mit.sketch.geom.Polygon;
import edu.mit.sketch.geom.Rectangle;
import edu.mit.sketch.geom.Vertex;
import edu.mit.sketch.grammar.me.Cross;
import edu.mit.sketch.grammar.me.Ground;
import edu.mit.sketch.grammar.me.Motor;
import edu.mit.sketch.grammar.me.Pin;
import edu.mit.sketch.grammar.me.Shade;
import edu.mit.sketch.grammar.me.Spring;
import edu.mit.sketch.toolkit.StrokeData;
import edu.mit.sketch.toolkit.StrokeWriter;
import java.io.BufferedReader;
import java.io.EOFException;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.io.StreamTokenizer;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Vector;

/**
 *
 * This class loads and stores objects used by the ObjectManager. See
 * {@link #loadTerminalsFromFile(String)} and
 * {@link #storeTerminalsInFile( String filename, Vector terminals)}
 * functions.
 **/

public
class LoadStoreModule
{
    /**
    *
    * Load Terminals and NonTerminals from the given file, and
    * return them in a Vector
    *
    **/
    public static Vector
    loadTerminalsFromFile( String file_name )
    {
        Object            read_object;
        FileInputStream   fis = null;
        ObjectInputStream ois = null;

        Vector objects = new Vector();

        try {
            fis = new FileInputStream( file_name );
            ois = new ObjectInputStream( fis );
            
            
            System.out.print( "Loading" );
            while ( true ) {
                read_object = ois.readObject();
                System.out.print( "." );

                if ( read_object instanceof Cross ) {
                    objects.add( (Cross)(read_object) );
                    continue;
                }
                
                if ( read_object instanceof Ellipse ) {
                    objects.add( (Ellipse)(read_object) );
                    continue;
                }
                
                if ( read_object instanceof Ground ) {
                    objects.add( (Ground)(read_object) );
                    continue;
                }
                
                if ( read_object instanceof Line ) {
                    objects.add( (Line)(read_object) );
                    continue;
                }
        
                if ( read_object instanceof Motor ) {
                    objects.add( (Motor)(read_object) );
                    continue;
                }
        
                if ( read_object instanceof Pin ) {
                    objects.add( (Pin)(read_object) );
                    continue;
                }
        
                if ( read_object instanceof Point ) {
                    objects.add( (Point)(read_object) );
                    continue;
                }
        
                if ( read_object instanceof Polygon ) {
                    objects.add( (Polygon)(read_object) );
                    continue;
                }
        
                if ( read_object instanceof Rectangle ) {
                    objects.add( (Rectangle)(read_object) );
                    continue;
                }
        
                if ( read_object instanceof Shade ) {
                    objects.add( (Shade)(read_object) );
                    continue;
                }
        
                if ( read_object instanceof Spring ) {
                    objects.add( (Spring)(read_object) );
                    continue;
                }
                
                if ( read_object instanceof GeneralPath ) {
                    objects.add( (GeneralPath)(read_object) );
                    continue;
                }
                
                System.out.println( "Unresolved object" + read_object );
            }
        } catch ( EOFException e ) {
            try {
                ois.close();
            } catch( Exception e2 ) {
                e2.printStackTrace(); 
            }
        } catch ( Exception e ) {
                e.printStackTrace(); 
        }
        
        System.out.println( "" );
        return objects;
    }


    /**
    *
    * Store Terminals and NonTerminals in objects to the given file.
    *
    **/
    public static void
    storeTerminalsInFile( String file_name, Vector objects )
    {
        System.out.print( "Storing" );
        try {
            FileOutputStream   fos = new FileOutputStream( file_name );
            ObjectOutputStream oos = new ObjectOutputStream( fos );

            
            for ( int i=0; i<objects.size(); i++ ) {
                System.out.print( "." );
                Object output_object = objects.elementAt( i );
                
                if ( output_object instanceof Cross )
                    oos.writeObject( (Cross)(output_object) );
        
                if ( output_object instanceof Ellipse )
                    oos.writeObject( (Ellipse)(output_object) );
        
                if ( output_object instanceof Ground )
                    oos.writeObject( (Ground)(output_object) );
        
                if ( output_object instanceof Line )
                    oos.writeObject( (Line)(output_object) );
        
                if ( output_object instanceof Motor )
                    oos.writeObject( (Motor)(output_object) );
        
                if ( output_object instanceof Pin )
                    oos.writeObject( (Pin)(output_object) );
        
                if ( output_object instanceof Point )
                    oos.writeObject( (Point)(output_object) );
        
                if ( output_object instanceof Polygon )
                    oos.writeObject( (Polygon)(output_object) );
        
                if ( output_object instanceof Rectangle )
                    oos.writeObject( (Rectangle)(output_object) );
        
                if ( output_object instanceof Shade )
                    oos.writeObject( (Shade)(output_object) );
        
                if ( output_object instanceof Spring )
                    oos.writeObject( (Spring)(output_object) );

                if ( output_object instanceof GeneralPath )
                    oos.writeObject( (GeneralPath)(output_object) );
            }
            oos.close();
        } catch( Exception e ) { 
            e.printStackTrace(); 
        }
        System.out.println( "" );
    }
    
    
    /**
    *
    * Load arrays of vertices from the given file, and
    * return them in a Vector
    *
    **/
    public static Vector
    loadVerticesFromFile( String file_name )
    {
        Object            read_object;
        FileInputStream   fis = null;
        ObjectInputStream ois = null;

        Vector objects = new Vector();

        System.out.print( "Loading vertices from " + file_name );
        try {
            fis = new FileInputStream( file_name );
            ois = new ObjectInputStream( fis );
            
            
            System.out.print( "Loading" );
            while ( true ) {
                read_object = ois.readObject();
                System.out.print( "+" );

                if ( read_object instanceof Vertex[] ) {
                    objects.add( (Vertex[])(read_object) );
                    continue;
                }
                
                System.out.println( "Unresolved object" + read_object );
            }
        } catch ( EOFException e ) {
      System.out.print( " done" );
        } catch ( Exception e ) {
                e.printStackTrace(); 
        } finally {
      try {
        System.out.println( "Closing file" );
        ois.close();
      } catch( Exception e2 ) {
        e2.printStackTrace(); 
      }
    }
    
        
        System.out.println( "" );
        return objects;
    }

    
    
    /**
    *
    * Load matrix from file (rows of numbers)
    *
    **/
    public static double[][]
    loadMatrixFromFile( String file_name )
    {
        int rows = 0;
        ArrayList values = new ArrayList();
        try {
            FileInputStream fis = new FileInputStream( file_name );

            StreamTokenizer st =
                new StreamTokenizer(
                    new BufferedReader(
                        new InputStreamReader( fis )));

            System.out.println( "reading " + file_name );

            st.resetSyntax();
            st.eolIsSignificant( true );
            st.whitespaceChars( ' ', ' ' );
            st.wordChars( '0', '0' );
            st.wordChars( '1', '1' );
            st.wordChars( '2', '2' );
            st.wordChars( '3', '3' );
            st.wordChars( '4', '4' );
            st.wordChars( '5', '5' );
            st.wordChars( '6', '6' );
            st.wordChars( '7', '7' );
            st.wordChars( '8', '8' );
            st.wordChars( '9', '9' );
            st.wordChars( 'e', 'e' );
            st.wordChars( 'E', 'E' );
            st.wordChars( '+', '+' );
            st.wordChars( '-', '-' );
            st.wordChars( '.', '.' );

            while ( st.nextToken() != st.TT_EOF ) {
                if ( st.ttype == st.TT_EOL   ) {
                    rows++;
                    System.out.print( "\n\n\n" );
                    continue;
                }
                if ( st.ttype == st.TT_WORD ) {
                    System.out.print( st.sval + " " );
                    values.add( new Double( st.sval ) );
                    continue;
                }
            }
        } catch( Exception e ) {
            System.err.println( e );
        }
        
        double result[][] = new double[rows][values.size()/rows];
        
        for ( int i=0; i<result.length; i++ ) {
            for ( int j=0; j<result[i].length; j++ ) {
                result[i][j] = ((Double)values.get( i*result[i].length + j )).doubleValue(); 
            }
        }
    
        return result;
    }

    
    
    /**
    *
    * Load arrays of vertices from the given file, and
    * return them in a Vector
    *
    **/
    public static ArrayList
    loadRawVerticesFromFile( String file_name )
    {
        ArrayList strokes = new ArrayList();
        try {
            FileInputStream fis = new FileInputStream( file_name );

            StreamTokenizer st =
                new StreamTokenizer(
                    new BufferedReader(
                        new InputStreamReader( fis )));

            System.out.println( "reading " + file_name );

            st.parseNumbers();
            st.wordChars( '#', '#' );
            int i = 0;
            ArrayList points = new ArrayList();
            while ( st.nextToken() != st.TT_EOF ) {
                if ( st.ttype == st.TT_WORD   ) {
                    if ( st.sval.equals( "#" ) ) {
                        strokes.add( Point.arrayListToPoints( points ) );
                        points = new ArrayList();
                    }
                }
                if ( st.ttype == st.TT_NUMBER ) {
                    int x = (int)st.nval;
                    st.nextToken();
                    int y = (int)st.nval;
                    st.nextToken();
                    long t = (long)st.nval;
                    points.add( new Point( x, y, t ) );
                }
            }
        } catch( Exception e ) {
            System.err.println( e );
        }
    
        return strokes;
    }

    /**
    *
    * Store arrays of vertices in objects to the given file.
    *
    **/
    public static void
    storeRawVerticesInFile( String file_name, Vector objects )
    {
        System.out.print( "Storing vertices in " + file_name );
        try {
            FileOutputStream   fos = new FileOutputStream( file_name );
            
            for ( int i=0; i<objects.size(); i++ ) {
                System.out.print( "+" );
                
                if ( objects.elementAt( i ) instanceof Vertex[] ) {
                    Vertex vertices[] = (Vertex[])(objects.elementAt(i));
                    for ( int j=0; j<vertices.length; j++ ) {
                        {
                            fos.write( (vertices[j].x + " " + vertices[j].y + " " + vertices[j].time_stamp + "\n").getBytes() );
                        }
                    }
                    fos.write( ("#\n").getBytes() );
                }
            }
            fos.close();
        } catch( Exception e ) { 
            e.printStackTrace(); 
        }
        System.out.println( "" );
    }
    
    
    /**
    *
    * load segmentation: object class, object length \n
    * result is returned as an arraylist of arrays of length 2
    *
    **/
    public static ArrayList
    loadSegmentationForClass( String file_name )
    {
        ArrayList result = new ArrayList();

        try {
            FileInputStream fis = new FileInputStream( file_name );

            StreamTokenizer st =
                new StreamTokenizer(
                    new BufferedReader(
                        new InputStreamReader( fis )));

            System.out.println( "reading " + file_name );

            st.parseNumbers();
            int i = 0;
            ArrayList points = new ArrayList();
            while ( st.nextToken() != st.TT_EOF ) {
                int pair[] = new int[2];
                if ( st.ttype == st.TT_NUMBER ) {
                    pair[0]  = (int)st.nval;
                    st.nextToken();
                    pair[1]  = (int)st.nval;
                }
                result.add( pair );
            }
        } catch( Exception e ) {
            System.err.println( e );
        }
    
        return result;
    }
    
    
    /**
    *
    * save strings, (one per line)
    *
    **/
    public static void
    saveStrings( String file_name, String strings[] )
    {
        String all_strings = "";
        
        for ( int i=0; i<strings.length; i++ ) {
            all_strings += strings[i] + "\n";
        }
        
        storeStringInFile( file_name, all_strings );
    }
    
    
    /**
    *
    * load a file with a string in each line, and return the 
    * contents in a String[]
    *
    **/
    public static String[]
    loadStrings( String file_name )
    {
        ArrayList result = new ArrayList();

        try {
            FileInputStream fis = new FileInputStream( file_name );

            StreamTokenizer st =
                new StreamTokenizer(
                    new BufferedReader(
                        new InputStreamReader( fis )));

            System.out.println( "reading " + file_name );

            int i = 0;
            ArrayList points = new ArrayList();
            while ( st.nextToken() != st.TT_EOF ) {
		System.out.println( "type = " + st.ttype );
                if ( st.ttype == st.TT_WORD ) {
                    result.add( st.sval );
		    System.out.println( st.sval );
                }
                if ( st.ttype == st.TT_NUMBER ) {
		    System.out.println( st.nval );		  
                }
                if ( st.ttype == st.TT_EOF ) {
		    System.out.println( "EOF reached" );		  
		}
                if ( st.ttype == st.TT_EOL ) {
		    System.out.println( "EOL reached" );		  
		}

	    }
        } catch( Exception e ) {
            System.out.println( "error in reading" );
            System.err.println( e );
        }
    
        String output[] = new String[result.size()];

        for ( int i=0; i<output.length; i++ ) {
            output[i] = ((String)result.get(i));
        }    
        
        return output;
    }


    /**
    *
    * save segmentation: object class, object length \n
    *
    **/
    public static void
    saveSegmentationForClass( String file_name, int ending_indices[], int object_ids[] )
    {
        System.out.print( "Storing segmentation data in " + file_name );
        try {
            FileOutputStream   fos = new FileOutputStream( file_name );
            
            fos.write( (object_ids[0] + " " + ending_indices[0] + "\n").getBytes() );
            for ( int i=1; i<ending_indices.length; i++ ) {
                System.out.print( "*" );
                fos.write( (object_ids[i] + " " + (ending_indices[i]-ending_indices[i-1]) + "\n").getBytes() );
            }
            fos.close();
        } catch( Exception e ) { 
            e.printStackTrace(); 
        }
        System.out.println( "" );
    }


    /**
    *
    * Store arrays of vertices in objects to the given file.
    *
    **/
    public static void
    storeVerticesInFile( String file_name, Vector objects )
    {
      System.out.print( "Storing vertices in " + file_name );
      FileOutputStream   fos = null;
      ObjectOutputStream oos = null;
      
      try {
    fos = new FileOutputStream( file_name );
    oos = new ObjectOutputStream( fos );

            
    for ( int i=0; i<objects.size(); i++ ) {
      System.out.print( "+" );
      Object output_object = objects.elementAt( i );
                
      if ( output_object instanceof Vertex[] )
        oos.writeObject( (Vertex[])(output_object) );
    }
      } catch( Exception e ) { 
    e.printStackTrace(); 
      } finally {
    if ( oos != null ) {
      try {
        oos.close();
      }
      catch( IOException e) {
        e.printStackTrace();
      }          
    } 
      }
    
      System.out.println( "" );
    }


    /**
    *
    * Store string to the given file.
    *
    **/
    public static void
    storeStringInFile( String file_name, String string )
    {
        System.out.println( "Storing string in " + file_name );
        try {
            FileOutputStream   fos = new FileOutputStream( file_name );
            
            fos.write( string.getBytes() );
            fos.close();
        } catch( Exception e ) { 
            e.printStackTrace(); 
        }
    }


    /**
    *
    * Append the string the the file
    *
    **/
    public static void
    appendStringToFile( String file_name, String string )
    {
        System.out.println( "Appending string to " + file_name );
        try {
            FileOutputStream   fos = new FileOutputStream( file_name,
                                                           true );
            
            fos.write( string.getBytes() );
            fos.close();
        } catch( Exception e ) { 
            e.printStackTrace(); 
        }
    }

  /**
   * Convert from this file format to the newer file format read and
   * written by DataCollector.
   **/
  public static void main( String[] args ) {
    // load the verts in the old format
     List list = loadRawVerticesFromFile( args[0] );

     // convert to a list of stroke data objects
     List strokes = new ArrayList( list.size() );
     Iterator it = list.iterator();
     while( it.hasNext() ) {
       System.out.println("Data object");
       
       Point[] verts = (Point[])it.next();
       strokes.add( new StrokeData( verts ) );
     }
     
     File out = new File( args[1] );
     FileWriter writer = null;
     try {
       writer = new FileWriter( out );
       StrokeWriter strokeWriter = new StrokeWriter( writer, 1.0,
                             out.getName() );
       strokeWriter.writeStrokes( strokes );
       writer.flush();
    }
    catch( IOException e ) {
      System.err.println( "Couldn't open file to write stroke data: " +
              out.getName() );
      return;
    }
    finally {
      try {
    writer.close();
      } catch( IOException e ) {
    System.err.println( "Trouble closing the file: " + out.getName() );
      }
      
    }
  }
  
}


/** 
  * 
  * $Log: LoadStoreModule.java,v $
  * Revision 1.9  2003/10/13 03:10:57  mtsezgin
  * *** empty log message ***
  *
  * Revision 1.8  2003/06/26 19:57:15  calvarad
  * Lots of bug fixes
  *
  * Revision 1.7  2003/05/07 21:29:44  mtsezgin
  *
  * Added Utility function in AWTUtil
  *
  * Revision 1.6  2003/03/06 01:08:54  moltmans
  * Added copyright to all the files.
  *
  * Revision 1.5  2002/07/22 21:06:09  mtsezgin
  * Added more utility methods.
  *
  * Revision 1.4  2002/05/24 17:51:20  moltmans
  * Added the comment about deprecating the class.
  *
  * Revision 1.3  2002/05/24 17:49:08  moltmans
  * Fixed some confusion that resulted in 2 different LoadStoreModule
  * classes (in util and toolkit).  We now only use the one in util.  But
  * in fact LoadStoreModule is deprecated anyway.  Please use StrokeReader
  * and StrokeWriter instead.
  *
  * Revision 1.2  2002/05/24 17:18:49  mtsezgin
  * *** empty log message ***
  *
  * Revision 1.1  2001/11/23 03:24:33  mtsezgin
  * *** empty log message ***
  *
  * Revision 1.2  2001/10/12 22:25:52  mtsezgin
  * This is a commit of all files.
  * Shoapid
  * vi sux:q
  *
  * Revision 1.1.1.1  2001/03/29 16:25:01  moltmans
  * Initial directories for DRG
  *
  * Revision 1.7  2000/09/06 22:40:41  mtsezgin
  * Combinations of curves and polygons are successfully approximated
  * by straight lines and Bezier curves as appropriate. System works
  * quite reliably.
  *
  *
  **/
