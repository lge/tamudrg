// (c) MIT 2003.  All rights reserved.

////////////////////////////////////////////////////////////////////////////////
//                                                                            //
// AUTHOR:      Tevfik Metin Sezgin                                           //
//              Massachusetts Institute of Technology                         //
//              Department of Electrical Engineering and Computer Science     //
//              Artificial Intelligence Laboratory                            //
//                                                                            //
// E-MAIL:        mtsezgin@ai.mit.edu, mtsezgin@mit.edu                       //
//                                                                            //
// COPYRIGHT:   Tevfik Metin Sezgin                                           //
//              All rights reserved. This code can not be copied, modified,   //
//              or distributed in whole or partially without the written      //
//              permission of the author. Also see the COPYRIGHT file.        //
//                                                                            //
////////////////////////////////////////////////////////////////////////////////
// Cleaned -Metin
package edu.mit.sketch.util;


/** 
  * 
  * See the end of the file for the log of changes.
  * 
  * $Id: AWTUtil.java,v 1.9 2003/11/05 01:42:02 moltmans Exp $     
  * $Name:  $   
  * $Locker:  $
  * $Source: /projects/drg/CVSROOT/drg/code/src/edu/mit/sketch/util/AWTUtil.java,v $
  *  
  **/


import java.awt.Color;
import java.awt.Component;
import java.awt.Container;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.Shape;

import java.awt.geom.AffineTransform;
import java.awt.geom.GeneralPath;
import java.awt.geom.PathIterator;
import java.awt.geom.FlatteningPathIterator;

import java.util.ArrayList;
import java.util.Vector;

import edu.mit.sketch.geom.Polygon;
import edu.mit.sketch.geom.Vertex;
import edu.mit.sketch.geom.Point;
import edu.mit.sketch.geom.Line;
import edu.mit.sketch.geom.Point;
import edu.mit.sketch.geom.GeometryUtil;
import edu.mit.sketch.geom.Range;
import edu.mit.sketch.toolkit.StrokeData;
import edu.mit.sketch.ui.Tablet;
import edu.mit.sketch.system.Logging;
import org.apache.log4j.Logger;

/**
  *
  * Implements some methods not found in jdk 1.1
  *
  **/
public class
AWTUtil
{
  private static final Logger LOG = Logging.getSubLogger( Logging.TOOLKIT, "Util" );
    /**
    *
    * Array of 8 colors for in-loop alternated coloring.
    *
    **/
    public static final Color colors[] = { Color.red,
                                           Color.green,
                                           Color.blue,
                                           Color.orange,
                                           Color.cyan,
                                           Color.gray,
                                           Color.magenta,
                                           Color.pink,
                                           Color.darkGray,
                                           Color.yellow };
                        
    /**
    *
    * Array of 2 colors.
    *
    **/
    public static final Color colors2[] = { Color.red,
                                            Color.blue };
                        
    /**
    *
    * Get all the sub componets in this container.
    *
    **/
    public static Component[]
    getAllComponentsRecursively( Container container )
    {
        Component first_layer[];
        Vector    components = new Vector();
        
        first_layer = container.getComponents();
        for ( int i=0; i<first_layer.length; i++ ) {
            components.add( first_layer[i] );
            if ( first_layer[i] instanceof Container ) {
                Component second_layer[] = getAllComponentsRecursively(
                    ((Container)first_layer[i]) );
                for ( int j=0; j<second_layer.length; j++ ) {
                    components.add( second_layer[j] );
                }
            }
        }
        
        Object    objects[] = components.toArray();
        Component result[]  = new Component[objects.length];
        for ( int i=0; i<objects.length; i++ ) {
            result[i] = (Component)objects[i];
        }
        
        return result;
    }


    /**
    *
    * Return an array of vertices corresponding to the flattened
    * version of the input.
    *
    **/
    public static Vertex[]
    flatten( Shape shape, double flatness, int limit )
    {
        Vector                 vertices  = new Vector();
        FlatteningPathIterator flattener = new FlatteningPathIterator(
                                                   shape.getPathIterator(
                                                       new AffineTransform() ),
                                                       flatness,
                                                       limit );
                            
            
        double coordinates[] = new double[6];
        
        while ( !flattener.isDone() ) {
            flattener.currentSegment( coordinates );
            vertices.addElement( new Vertex( (int)coordinates[0], 
                                             (int)coordinates[1] ) );
            flattener.next();
        }
        
        return Vertex.vectorToArray( vertices );
    }
    
    
    /**
    *
    * Get the LSQE for the curve by measuring the distance to 
    * the vertices in range. It is only valid to pass curves in.
    *
    **/
    public static double
    getCurveLSQEror( Shape shape, StrokeData data, Range range ) {
        Vertex   vertices[]               = data.vertices;
        Vertex   approximation_vertices[] = AWTUtil.flatten( shape,
                                                             0.001,
                                                             6 );
        double lsq_error = 0.0;
        
        for ( int i=range.min; i<range.max; i++ ) {
            double min_distance = Double.MAX_VALUE;
            double lsq_distance = Double.MAX_VALUE;
            int    min_index    = 0;
            for ( int j=0; j<approximation_vertices.length; j++ ) {
                lsq_distance = 
                    vertices[i].distanceSq( approximation_vertices[j] );
                if ( lsq_distance < min_distance ) {
                     min_distance = lsq_distance;
                     min_index    = j;
                }
            }
            
            lsq_error += min_distance;
        }
        
        lsq_error /= ( range.max - range.min);
        
        return lsq_error;
    }
    
        
    /**
    *
    * Calculate the lsq error. points is the actual data points,
    * vertices is the match 
    *
    **/
    public static double
    leastSquaresForPolygon( Vertex vertices[], Point points[] )
    {
        double lsq_error     = 0.0;
        double segment_error = 0.0;
            
        Vertex v1;
        Vertex v2;
        Point  p;

        if ( ( vertices == null ) || ( vertices.length < 2 ) ) {
            return Double.MAX_VALUE;
        }
    
        for ( int i=0; i<vertices.length-1; i++ ) {
            v1 = vertices[i];
            v2 = vertices[i+1];

            segment_error = 0.0;            
            for ( int j=v1.index; j<v2.index; j++ ) {
                p = points[j];
                segment_error += Line.ptSegDistSq( v1.x, v1.y, 
                                                   v2.x, v2.y, 
                                                   p.x,  p.y );
                //System.out.println( "Polygon to line " + j  ); 
            }
            lsq_error += segment_error;
        }


        return lsq_error/points.length;
    }
    
        
    /**
    *
    * Calculate the lsq error. points is the actual data points,
    * polygon is the match 
    *
    **/
    public static double
    leastSquaresForPolygon( Polygon polygon, Point points[] )
    {
        double lsq_error   = 0.0;
        double point_error = 0.0;
            
        Point  p;
        int    min_index;
    
        for ( int i=0; i<points.length; i++ ) {
            p           = points[i];
            point_error = Double.MAX_VALUE;            
            min_index   = 0;
            for ( int j=0; j<polygon.npoints-1; j++ ) {
                double distance = Line.ptSegDistSq( polygon.xpoints[j],   polygon.ypoints[j], 
                                                    polygon.xpoints[j+1], polygon.ypoints[j+1],
                                                    p.x,  p.y ); 
                                                    
                if ( distance < point_error ) {
                    point_error = distance;
                    min_index   = j;
                }
            }
            lsq_error += point_error;
        }


        return lsq_error/points.length;
    }
    
        
    /**
    *
    * calculate the lsq error. points is the actual data points
    *
    **/
    public static double
    leastSquaresForLine( Line line, Point points[] )
    {
        double lsq_error     = 0.0;

        for ( int i=0; i<points.length; i++ ) {
            lsq_error += Line.ptSegDistSq( line.x1,     line.y1, 
                                           line.x2,     line.y2, 
                                           points[i].x, points[i].y );
        }

        return lsq_error/points.length;
    }
    
    
    /**
    *
    * Paint f vs g in the frame using the input color and affine 
    * transform. f is on the horizontal axis.
    *
    **/
    public static void
    paintFvsG( double          f[], 
               double          g[], 
               Color           color,
               Image           image,
               AffineTransform transform )
    {
        Graphics2D graphics = (Graphics2D)image.getGraphics();
        int        length   = ( f.length < g.length ) ? f.length : g.length;
        double     input[]  = new double[length*2];

        for ( int i=0; i<length; i++ ) {
            input[ i*2 ] = f[i];
            input[i*2+1] = g[i];
        }
        
        transform.transform( input, 0, input, 0, length );
        graphics.setColor( color );

        for ( int i=0; i<length-1; i++ ) {
            graphics.drawLine( (int)input[i*2],   (int)input[i*2+1],
                               (int)input[i*2+2], (int)input[i*2+3] );
        }
        
        // Axis
        int max_window_size = 1000;
        input               = new double[10];
        
        input[0] = -max_window_size;
        input[1] = 0;
        
        input[2] = max_window_size;
        input[3] = 0;
        
        input[4] = 0;
        input[5] = 0;
        
        input[6] = 0;
        input[7] = max_window_size;
        
        input[8] = 0;
        input[9] = -max_window_size;
        
        transform.transform( input, 0, input, 0, 5 );

        graphics.setColor( Color.black );
        
        for ( int i=0; i<4; i++ ) {
            graphics.drawLine( (int)input[i*2],   (int)input[i*2+1],
                               (int)input[i*2+2], (int)input[i*2+3] );
        }
    }
    
    
    /**
    *
    * Paint f vs g in the frame using the input color and affine 
    * transform. f is on the horizontal axis.
    *
    **/
    public static void
    paintFvsG( double          f[], 
               double          g[], 
               Color           color,
               Component       component,
               AffineTransform transform )
    {
        Graphics2D graphics = (Graphics2D)component.getGraphics();
        int        length   = ( f.length < g.length ) ? f.length : g.length;
        double     input[]  = new double[length*2];

        for ( int i=0; i<length; i++ ) {
            input[ i*2 ] = f[i];
            input[i*2+1] = g[i];
        }
        
        transform.transform( input, 0, input, 0, length );
        graphics.setColor( color );

        for ( int i=0; i<length-1; i++ ) {
            graphics.drawLine( (int)input[i*2],   (int)input[i*2+1],
                               (int)input[i*2+2], (int)input[i*2+3] );
        }
        
        // Axis
        int max_window_size = (int)( component.getSize().width/
                                     transform.getShearX() +
                                       component.getSize().height/
                                     transform.getShearY() );
        input               = new double[10];
        
        input[0] = -max_window_size;
        input[1] = 0;
        
        input[2] = max_window_size;
        input[3] = 0;
        
        input[4] = 0;
        input[5] = 0;
        
        input[6] = 0;
        input[7] = max_window_size;
        
        input[8] = 0;
        input[9] = -max_window_size;
        
        transform.transform( input, 0, input, 0, 5 );

        graphics.setColor( Color.black );
        
        for ( int i=0; i<4; i++ ) {
            graphics.drawLine( (int)input[i*2],   (int)input[i*2+1],
                               (int)input[i*2+2], (int)input[i*2+3] );
        }
    }
    
    
    /**
    *
    * Paint line.
    *
    **/
    public static void
    paintLine( Line            line,
               Color           color,
               Image           image,
               AffineTransform transform )
    {
        Graphics2D graphics = (Graphics2D)image.getGraphics();
        double     input[]  = new double[4];

        input[0] = line.x1;
        input[1] = line.y1;
        input[2] = line.x2;
        input[3] = line.y2;
        
        transform.transform( input, 0, input, 0, 2 );
        graphics.setColor( color );

        graphics.drawLine( (int)input[0], (int)input[1],
                           (int)input[2], (int)input[3] );
    }
    
    
    /**
    *
    * Paint line.
    *
    **/
    public static void
    paintLine( Line            line,
               Color           color,
               Component       component,
               AffineTransform transform )
    {
        Graphics2D graphics = (Graphics2D)component.getGraphics();
        double     input[]  = new double[4];

        input[0] = line.x1;
        input[1] = line.y1;
        input[2] = line.x2;
        input[3] = line.y2;
        
        transform.transform( input, 0, input, 0, 2 );
        graphics.setColor( color );

        graphics.drawLine( (int)input[0], (int)input[1],
                           (int)input[2], (int)input[3] );
    }
    
    
    /**
    *
    * Paint f vs g in the frame using the input color and affine 
    * transform. f is on the horizontal axis. Use dots.
    *
    **/
    public static void
    paintDots( double          f[], 
               int             g[], 
               Color           color,
               Image           image,
               AffineTransform transform )
    {
        Graphics2D graphics = (Graphics2D)image.getGraphics();
        int        length   = ( f.length < g.length ) ? f.length : g.length;
        double     input[]  = new double[length*2];

        for ( int i=0; i<length; i++ ) {
            input[ i*2 ] = f[i];
            input[i*2+1] = g[i];
        }
        
        transform.transform( input, 0, input, 0, length );
        graphics.setColor( color );

        for ( int i=0; i<length; i++ ) {
            graphics.drawOval( (int)input[i*2], (int)input[i*2+1],
                               1,               1 );
        }
    }
    
    
    /**
    *
    * Paint f vs g in the frame using the input color and affine 
    * transform. f is on the horizontal axis. Use dots.
    *
    **/
    public static void
    paintDots( double          f[], 
               int             g[], 
               Color           color,
               Component       component,
               AffineTransform transform )
    {
        Graphics2D graphics = (Graphics2D)component.getGraphics();
        int        length   = ( f.length < g.length ) ? f.length : g.length;
        double     input[]  = new double[length*2];

        for ( int i=0; i<length; i++ ) {
            input[ i*2 ] = f[i];
            input[i*2+1] = g[i];
        }
        
        transform.transform( input, 0, input, 0, length );
        graphics.setColor( color );

        for ( int i=0; i<length; i++ ) {
            graphics.drawOval( (int)input[i*2], (int)input[i*2+1],
                               1,               1 );
        }
    }
    
    
    /**
    *
    * Paint f vs g in the frame using the input color and affine 
    * transform. f is on the horizontal axis. Use dots.
    *
    **/
    public static void
    paintDots( double          f[], 
               double          g[], 
               Color           color,
               Image           image,
               AffineTransform transform,
               int             brush_size )
    {
        Graphics2D graphics = (Graphics2D)image.getGraphics();
        int        length   = ( f.length < g.length ) ? f.length : g.length;
        double     input[]  = new double[length*2];

        for ( int i=0; i<length; i++ ) {
            input[ i*2 ] = f[i];
            input[i*2+1] = g[i];
        }
        
        transform.transform( input, 0, input, 0, length );
        graphics.setColor( color );

        for ( int i=0; i<length; i++ ) {
            graphics.drawOval( (int)input[i*2], (int)input[i*2+1],
                               brush_size,      brush_size         );
        }
    }
    
    
    /**
    *
    * Paint f vs g in the frame using the input color and affine 
    * transform. f is on the horizontal axis. Use dots.
    *
    **/
    public static void
    paintDots( double          f[], 
               double          g[], 
               Color           color,
               Component       component,
               AffineTransform transform,
               int             brush_size )
    {
        Graphics2D graphics = (Graphics2D)component.getGraphics();
        int        length   = ( f.length < g.length ) ? f.length : g.length;
        double     input[]  = new double[length*2];

        for ( int i=0; i<length; i++ ) {
            input[ i*2 ] = f[i];
            input[i*2+1] = g[i];
        }
        
        transform.transform( input, 0, input, 0, length );
        graphics.setColor( color );

        for ( int i=0; i<length; i++ ) {
            graphics.drawOval( (int)input[i*2], (int)input[i*2+1],
                               brush_size,      brush_size         );
        }
    }
    
    
    /**
    *
    * Paint f vs g in the frame using the input color and affine 
    * transform. f is on the horizontal axis. Use dots.
    *
    **/
    public static void
    paintDots( int             f[], 
               double          g[], 
               Color           color,
               Image           image,
               AffineTransform transform,
               int             brush_size )
    {
        Graphics2D graphics = (Graphics2D)image.getGraphics();
        int        length   = ( f.length < g.length ) ? f.length : g.length;
        double     input[]  = new double[length*2];

        for ( int i=0; i<length; i++ ) {
            input[ i*2 ] = f[i];
            input[i*2+1] = g[i];
        }
        
        transform.transform( input, 0, input, 0, length );
        graphics.setColor( color );

        for ( int i=0; i<length; i++ ) {
            graphics.drawOval( (int)input[i*2], (int)input[i*2+1],
                               brush_size,      brush_size         );
        }
    }
    
    
    /**
    *
    * Paint f vs g in the frame using the input color and affine 
    * transform. f is on the horizontal axis. Use dots.
    *
    **/
    public static void
    paintDots( int             f[], 
               double          g[], 
               Color           color,
               Component       component,
               AffineTransform transform,
               int             brush_size )
    {
        Graphics2D graphics = (Graphics2D)component.getGraphics();
        int        length   = ( f.length < g.length ) ? f.length : g.length;
        double     input[]  = new double[length*2];

        for ( int i=0; i<length; i++ ) {
            input[ i*2 ] = f[i];
            input[i*2+1] = g[i];
        }
        
        transform.transform( input, 0, input, 0, length );
        graphics.setColor( color );

        for ( int i=0; i<length; i++ ) {
            graphics.drawOval( (int)input[i*2], (int)input[i*2+1],
                               brush_size,      brush_size         );
        }
    }
    
    
    /**
    *
    * Paint f vs g in the frame using the input color and affine 
    * transform. f is on the horizontal axis. Use dots.
    *
    **/
    public static void
    paintDots( int             f[], 
               double          g[], 
               Color           color,
               Image           image,
               AffineTransform transform )
    {
        Graphics2D graphics = (Graphics2D)image.getGraphics();
        int        length   = ( f.length < g.length ) ? f.length : g.length;
        double     input[]  = new double[length*2];

        for ( int i=0; i<length; i++ ) {
            input[ i*2 ] = f[i];
            input[i*2+1] = g[i];
        }
        
        transform.transform( input, 0, input, 0, length );
        graphics.setColor( color );

        for ( int i=0; i<length; i++ ) {
            graphics.drawLine( (int)input[i*2],   (int)input[i*2+1],
                               (int)input[i*2], (int)input[i*2+1] );
        }
    }
    
    
    /**
    *
    * Paint f vs g in the frame using the input color and affine 
    * transform. f is on the horizontal axis. Use dots.
    *
    **/
    public static void
    paintDots( int             f[], 
               double          g[], 
               Color           color,
               Component       component,
               AffineTransform transform )
    {
        Graphics2D graphics = (Graphics2D)component.getGraphics();
        int        length   = ( f.length < g.length ) ? f.length : g.length;
        double     input[]  = new double[length*2];

        for ( int i=0; i<length; i++ ) {
            input[ i*2 ] = f[i];
            input[i*2+1] = g[i];
        }
        
        transform.transform( input, 0, input, 0, length );
        graphics.setColor( color );

        for ( int i=0; i<length; i++ ) {
            graphics.drawLine( (int)input[i*2],   (int)input[i*2+1],
                               (int)input[i*2], (int)input[i*2+1] );
        }
    }
    
    
    /**
    *
    * Paint f vs g in the frame using the input color and affine 
    * transform. f is on the horizontal axis. Use dots.
    *
    **/
    public static void
    paintDots( int             f[], 
               int             g[], 
               Color           color,
               Image           image,
               AffineTransform transform )
    {
        Graphics2D graphics = (Graphics2D)image.getGraphics();
        int        length   = ( f.length < g.length ) ? f.length : g.length;
        double     input[]  = new double[length*2];

        for ( int i=0; i<length; i++ ) {
            input[ i*2 ] = f[i];
            input[i*2+1] = g[i];
        }
        
        transform.transform( input, 0, input, 0, length );
        graphics.setColor( color );

        for ( int i=0; i<length; i++ ) {
            graphics.drawLine( (int)input[i*2],   (int)input[i*2+1],
                               (int)input[i*2], (int)input[i*2+1] );
        }
    }
    
    
    /**
    *
    * Paint f vs g in the frame using the input color and affine 
    * transform. f is on the horizontal axis. Use dots.
    *
    **/
    public static void
    paintDots( int             f[], 
               int             g[], 
               Color           color,
               Component       component,
               AffineTransform transform )
    {
        Graphics2D graphics = (Graphics2D)component.getGraphics();
        int        length   = ( f.length < g.length ) ? f.length : g.length;
        double     input[]  = new double[length*2];

        for ( int i=0; i<length; i++ ) {
            input[ i*2 ] = f[i];
            input[i*2+1] = g[i];
        }
        
        transform.transform( input, 0, input, 0, length );
        graphics.setColor( color );

        for ( int i=0; i<length; i++ ) {
            graphics.drawLine( (int)input[i*2],   (int)input[i*2+1],
                               (int)input[i*2], (int)input[i*2+1] );
        }
    }
    
    
    /**
    *
    * Paint x vs y in the frame using the input color and affine 
    * transform. x is on the horizontal axis. Use dots.
    *
    **/
    public static void
    paintDot( int              x, 
              int              y, 
              Color           color,
              Image           image,
              AffineTransform transform )
    {
        Graphics2D graphics = (Graphics2D)image.getGraphics();
        double     input[]  = new double[2];

        input[0] = x;
        input[1] = y;
        
        transform.transform( input, 0, input, 0, 1 );
        
        graphics.setColor( color );
        graphics.drawLine( (int)input[0], (int)input[1],
                           (int)input[0], (int)input[1] );
    }
    
    
    /**
    *
    * Paint x vs y in the frame using the input color and affine 
    * transform. x is on the horizontal axis. Use dots.
    *
    **/
    public static void
    paintDot( int              x, 
              int              y, 
              Color           color,
              Component       component,
              AffineTransform transform )
    {
        Graphics2D graphics = (Graphics2D)component.getGraphics();
        double     input[]  = new double[2];

        input[0] = x;
        input[1] = y;
        
        transform.transform( input, 0, input, 0, 1 );
        
        graphics.setColor( color );
        graphics.drawLine( (int)input[0], (int)input[1],
                           (int)input[0], (int)input[1] );
    }
    
    
    /**
    *
    * Paint x vs y in the frame using the input color and affine 
    * transform. x is on the horizontal axis. Use dots.
    *
    **/
    public static void
    paintDot( double          x, 
              double          y, 
              Color           color,
              Image           image,
              AffineTransform transform )
    {
        Graphics2D graphics = (Graphics2D)image.getGraphics();
        double     input[]  = new double[2];

        input[0] = x;
        input[1] = y;
        
        transform.transform( input, 0, input, 0, 1 );
        
        graphics.setColor( color );
        graphics.drawLine( (int)input[0], (int)input[1],
                           (int)input[0], (int)input[1] );
    }
    
    
    /**
    *
    * Paint x vs y in the frame using the input color and affine 
    * transform. x is on the horizontal axis. Use dots.
    *
    **/
    public static void
    paintDot( double          x, 
              double          y, 
              Color           color,
              Component       component,
              AffineTransform transform )
    {
        Graphics2D graphics = (Graphics2D)component.getGraphics();
        double     input[]  = new double[2];

        input[0] = x;
        input[1] = y;
        
        transform.transform( input, 0, input, 0, 1 );
        
        graphics.setColor( color );
        graphics.drawLine( (int)input[0], (int)input[1],
                           (int)input[0], (int)input[1] );
    }
    
    
    /**
    *
    * Paint f vs g in the frame using the input color and affine 
    * transform. f is on the horizontal axis.
    *
    **/
    public static void
    paintFvsG( long            f[], 
               double          g[], 
               Color           color,
               Image           image,
               AffineTransform transform )
    {
        double f_copy[] = new double[f.length];
        
        for ( int i=0; i<f.length; i++ ) {
            f_copy[i] = (double)f[i];
        }
        paintFvsG( f_copy, g, color, image, transform );        
    }
    
    
    /**
    *
    * Paint f vs g in the frame using the input color and affine 
    * transform. f is on the horizontal axis.
    *
    **/
    public static void
    paintFvsG( long            f[], 
               double          g[], 
               Color           color,
               Component       component,
               AffineTransform transform )
    {
        double f_copy[] = new double[f.length];
        
        for ( int i=0; i<f.length; i++ ) {
            f_copy[i] = (double)f[i];
        }
        paintFvsG( f_copy, g, color, component, transform );        
    }


    /**
    * 
    * Make array of vertices from indices. The second argument should be 
    * an array of vertices whose subset is indexed by the indices.
    *
    **/
    public static Vertex[]
    makeVertices( int indices[], Vertex in_vertices[] ) 
    {
	Vertex out_vertices[] = new Vertex[indices.length];

	for ( int i=0; i<indices.length; i++ ) {
	    for ( int j=0; j<in_vertices.length; j++ ) {
		if ( indices[i] == in_vertices[j].index ) {
		    out_vertices[i] = in_vertices[j];
		    break;
		}
	    }
	}

	return out_vertices;
    }


    /**
    * 
    * Get indices of the array of vertices.
    *
    **/
    public static int[]
    getIndices( Vertex input_vertices[] ) 
    {
	int indices[];
	indices = new int[input_vertices.length];

	for ( int i=0; i<indices.length; i++ )
	    indices[i] = input_vertices[i].index;

	return indices;
    }


    /**
    * 
    * Simplify the polygon to get rid of consecutive lines which are
    * sufficiently parallel.
    *
    **/
    public static Vertex[]
    simplifyPolygon( Vertex input_vertices[], double turn_angle_treshold ) 
    {
        double theta     = 0.0;
        double old_theta = 0.0;
        
        if ( (input_vertices == null) || (input_vertices.length < 2) ) {
            System.err.println("Error: simplifyPolygon no polygon detected.");
            return null;
        }
        
        double  intermediate_angles[] = 
            GeometryUtil.getIntermediateAngles( input_vertices );
        boolean include[]             = 
            new boolean[intermediate_angles.length+1];
        
        include[0] = true;
        old_theta  = intermediate_angles[0];

        int deleted_points = 0;
        for ( int i=1; i<intermediate_angles.length; i++ ) {
            if ( Math.abs( old_theta - intermediate_angles[i] ) < 
                 turn_angle_treshold ) {
                deleted_points++;
                include[i] = false;
            } else {
                include[i] = true;
            }
            old_theta = intermediate_angles[i];
        }
        include[include.length-1] = true;
        if ( !Tablet.very_quiet ) {
            System.out.println( "Simplification over" );
            System.out.println( "Deleted points = " +  deleted_points );
        }

        Vertex new_polygon[] = new Vertex[include.length - deleted_points];
        if ( !Tablet.very_quiet ) {
            System.out.println( "Created new polygon with " + 
                                 new_polygon.length + " points");
        }
        int index = 0;
        for ( int i=0; i<include.length; i++ ) {
            if ( include[i] ) {
                new_polygon[index] = new Vertex( input_vertices[i] );
                index++;
            }
        }
        
        return new_polygon;
    }


    /**
    * 
    * Simplify the polygon to get rid of consecutive lines which are
    * sufficiently parallel. This takes the direction of the parallel
    * lines into consideration. This does the job by dot product.
    *
    **/
    public static Vertex[]
    simplifyPolygon2( Vertex input_vertices[], double turn_angle_treshold ) 
    {
        ArrayList result = new ArrayList();
        double theta     = 0.0;
        double old_theta = 0.0;
        
        if ( (input_vertices == null) || (input_vertices.length < 2) ) {
            System.err.println("Error: simplifyPolygon2 no polygon detected.");
            return null;
        }
        
        double x0;
        double y0;
        double x1;
        double y1;
        
        result.add( input_vertices[0] );
        
        for ( int i=0; i<input_vertices.length-2; i++ ) {
            x0 = input_vertices[i+1].x - input_vertices[i].x;
            y0 = input_vertices[i+1].y - input_vertices[i].y;
            x1 = input_vertices[i+2].x - input_vertices[i+1].x;
            y1 = input_vertices[i+2].y - input_vertices[i+1].y;
            
            
            double dot_product = x0*x1+y0*y1;
            double norms       = Math.sqrt( ( x0*x0+y0*y0 ) * ( x1*x1+y1*y1 ) );
            if ( dot_product < norms*Math.cos( turn_angle_treshold ) ) {
                result.add( input_vertices[i+1] );
            }
        }
        
        result.add( input_vertices[input_vertices.length-1] );

        Vertex new_polygon[] = new Vertex[result.size()];
        for ( int i=0; i<new_polygon.length; i++ ) {
            new_polygon[i] = (Vertex)(result.get( i ));
        }
        
        return new_polygon;
    }
}


/** 
  * 
  * $Log: AWTUtil.java,v $
  * Revision 1.9  2003/11/05 01:42:02  moltmans
  * Found more ^M's  They should all be gone now... Again...  For good?
  *
  * Revision 1.8  2003/08/08 19:28:22  moltmans
  * Updated debug message to match method name.
  *
  * Revision 1.7  2003/06/26 19:57:15  calvarad
  * Lots of bug fixes
  *
  * Revision 1.6  2003/05/07 21:29:44  mtsezgin
  *
  * Added Utility function in AWTUtil
  *
  * Revision 1.5  2003/03/06 01:08:54  moltmans
  * Added copyright to all the files.
  *
  * Revision 1.4  2002/07/23 21:00:08  moltmans
  * Commented out some debug code that was throwing errors.
  *
  * Revision 1.3  2002/07/22 21:06:09  mtsezgin
  * Added more utility methods.
  *
  * Revision 1.2  2002/05/24 17:33:41  mtsezgin
  * Added more utility functions.
  *
  * Revision 1.1  2001/11/23 03:24:32  mtsezgin
  * *** empty log message ***
  *
  * Revision 1.4  2001/11/13 17:54:41  mtsezgin
  * *** empty log message ***
  *
  * Revision 1.3  2001/10/12 23:32:29  mtsezgin
  * Turned off printing...
  *
  * Revision 1.2  2001/10/12 22:25:52  mtsezgin
  * This is a commit of all files.
  * Shoapid
  * vi sux:q
  *
  * Revision 1.1.1.1  2001/03/29 16:25:00  moltmans
  * Initial directories for DRG
  *
  * Revision 1.4  2000/09/20 20:07:23  mtsezgin
  * This is a working version with curve recognition and curve
  * refinement. The GeneralPath approximation is refined if needed
  * to result in a better fit.
  *
  * Revision 1.3  2000/09/06 22:40:32  mtsezgin
  * Combinations of curves and polygons are successfully approximated
  * by straight lines and Bezier curves as appropriate. System works
  * quite reliably.
  *
  * Revision 1.4  2000/06/03 01:52:33  mtsezgin
  * *** empty log message ***
  *
  * Revision 1.3  2000/04/28 04:45:03  mtsezgin
  *
  * Now each GeometricObject keeps the mouse input that was previously
  * discarded. User can switch between seeing the recognized mode and
  * the raw mode. setDataPoints( Polygon points ) and getDataPoints()
  * are added to GeometricObject, and all the implementors are modified
  * accordingly.
  *
  * Revision 1.2  2000/04/25 22:15:29  mtsezgin
  *
  * Added more utility functions.
  *
  * Revision 1.1.1.1  2000/04/01 03:07:07  mtsezgin
  * Imported sources
  *
  * Revision 1.2  2000/03/31 22:41:03  mtsezgin
  *
  * Started Log tracking.
  *
  *  
  **/
