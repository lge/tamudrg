// (c) MIT 2003.  All rights reserved.

////////////////////////////////////////////////////////////////////////////////
//                                                                            //
// AUTHOR:      Tevfik Metin Sezgin                                           //
//              Massachusetts Institute of Technology                         //
//              Department of Electrical Engineering and Computer Science     //
//              Artificial Intelligence Laboratory                            //
//                                                                            //
// E-MAIL:        mtsezgin@ai.mit.edu, mtsezgin@mit.edu                       //
//                                                                            //
// COPYRIGHT:   Tevfik Metin Sezgin                                           //
//              All rights reserved. This code can not be copied, modified,   //
//              or distributed in whole or partially without the written      //
//              permission of the author. Also see the COPYRIGHT file.        //
//                                                                            //
////////////////////////////////////////////////////////////////////////////////
package edu.mit.sketch.util;

/**
  * See the end of the file for the log of changes.
  *
  * $Author: mtsezgin $
  * $Date: 2003/05/07 21:29:44 $
  * $Revision: 1.3 $
  * $Headers$
  * $Id: ShortestPathModule.java,v 1.3 2003/05/07 21:29:44 mtsezgin Exp $
  * $Name:  $
  * $Locker:  $
  * $Source: /projects/drg/CVSROOT/drg/code/src/edu/mit/sketch/util/ShortestPathModule.java,v $
  *
  **/



import java.io.BufferedReader;
import java.io.EOFException;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.io.StreamTokenizer;
import java.util.ArrayList;

/**
 *
 * This class can read and write graphs. It can find 
 * k-shortest paths in a graph.
 *
 **/

public
class ShortestPathModule
{
    private static boolean windows_platform = 
        System.getProperty( "os.name" ).indexOf( "Win" ) != -1;

    /**
    *
    * Load shortest path descriptions into an ArrayList of 
    * int[]
    *
    **/
    public static ArrayList
    loadShortestPathsFromFile( String file_name )
    {
        ArrayList path  = new ArrayList();
        ArrayList paths = new ArrayList();
        System.out.println( "Trying to open " + file_name );
        try {
            FileInputStream fis = new FileInputStream( file_name );

            System.out.println( "Created fis " );
            StreamTokenizer st =
                new StreamTokenizer(
                    new BufferedReader(
                        new InputStreamReader( fis )));

            System.out.println( "Created st " );

            st.parseNumbers();
            st.eolIsSignificant( true );
            int i = 0;

            while ( true ) {
                st.nextToken();
                
                if  ( st.ttype == st.TT_NUMBER  ) {
                    //System.out.println( "Got a number" + st.nval );
                    path.add( new Integer( (int)st.nval ) );
                }
                
                if  ( st.ttype == st.TT_EOL  ) {
                    paths.add( Util.arrayListToIntArray( path ) );
                    path = new ArrayList();
                }

                if  ( st.ttype == st.TT_EOF  ) {
                    if ( path.size() > 0 )
                        paths.add( Util.arrayListToIntArray( path ) );
                    System.out.println( "Finished reading. EOF reached." );
                    break;
                }
            }
        } catch( Exception e ) {
            System.err.println( "Failed file open" );
            System.err.println( e );
        }
    
        return paths;
    }
    

    /**
    *
    * Store given greph in a format that the the shortest path 
    * executable understands. Beware, this method works around the 
    * c++ scientific notation parsing error in a dirty way. Specialized
    * to work with the k shortest path algorithm executable
    *
    **/
    public static void
    storeGraph( String file_name, double graph[][] )
    {
        System.out.println( "Storing graph in " + file_name );
        try {
            FileOutputStream   fos = new FileOutputStream( file_name );
            fos.write( (graph.length + "\n").getBytes() );
            
            for ( int i=0; i<graph.length; i++ ) {
                for ( int j=0; j<graph[i].length; j++ ) {
                    if ( graph[i][j] > 0.001 && graph[i][j] < 10000 ) {
                        fos.write( ( "(" + i + " " + j + " " + 
                        (graph[i][j] + "       " ).substring(0,6) +
                        ") " ).getBytes() );
                    } else {
                        if ( graph[i][j] != Double.MAX_VALUE ) {
                            if ( graph[i][j] <= .0001 ) {
                                fos.write( ( "(" + i + " " + j + " 0.00001) " ).getBytes() ); // For avoiding stupid c++ scientific notation parsing 
                            } else {
                                fos.write( ( "(" + i + " " + j + "10000) " ).getBytes() );
                            }
                        }
                    }
                }
            }
            fos.write( ( "\n" ).getBytes() );
            fos.close();
        } catch( Exception e ) { 
            e.printStackTrace(); 
        }
    }
    

    /**
    *
    * Store given greph in a format that the the shortest path 
    * executable understands. Run the right executable depending on
    * the platform. Read and return the result.
    *
    **/
    public static ArrayList
    getKShortestPaths( double graph[][], int source, int destination, int k )
    {
        
        
        try {
            String command;
        
            
            if ( windows_platform ) { // windows
                Runtime.getRuntime().exec( "g:\\drg\\lib\\win32\\cleanup.bat" ).waitFor();
                storeGraph( "g:\\drg\\tmp_graph", graph );
                command = "g:\\drg\\lib\\win32\\kshortestpath.exe " +
                    source      + " " +
                    destination + " " +
                    k           + " < g:\\drg\\tmp_graph > g:\\drg\\tmp_graph_shortest_paths\n";
                LoadStoreModule.storeStringInFile( "g:\\drg\\lib\\win32\\command.bat", command );

                Runtime.getRuntime().exec( "g:\\drg\\lib\\win32\\command.bat" ).waitFor();
                return loadShortestPathsFromFile( "g:\\drg\\tmp_graph_shortest_paths" );
            } else { // Assume unix
                storeGraph( "g:\\drg\\tmp_graph", graph );
                command = "~/drg/lib/win32/ktest " +
                    source      + " " +
                    destination + " " +
                    k           + " < ~/drg/tmp_graph > ~/drg/tmp_graph_shortest_paths";
                
                System.out.println( command );
                Runtime.getRuntime().exec( command ).waitFor();
                return loadShortestPathsFromFile( "~/drg/tmp_graph_shortest_paths" );
            }
        } catch ( Exception e ) {
            System.out.println( e );
        }
        
        return null;
    }
    

    /**
    *
    * Store given greph in a format that the the shortest path 
    * executable understands. Run the right executable depending on
    * the platform. Read and return the result.
    *
    **/
    public static int[]
    getShortestPath( double graph[][], int source, int destination )
    {
        ArrayList paths = getKShortestPaths( graph, source, destination, 1 );
        System.out.println( "there are " + paths.size() + " paths" );
        return (int[])(paths.get(0));
    }
}


/** 
  * 
  * $Log: ShortestPathModule.java,v $
  * Revision 1.3  2003/05/07 21:29:44  mtsezgin
  *
  * Added Utility function in AWTUtil
  *
  * Revision 1.2  2003/03/06 01:08:54  moltmans
  * Added copyright to all the files.
  *
  * Revision 1.1  2002/07/22 22:11:31  mtsezgin
  * This class finds the k-shortest paths.
  *
  *
  **/
