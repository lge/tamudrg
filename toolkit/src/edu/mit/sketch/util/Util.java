// (c) MIT 2003.  All rights reserved.

////////////////////////////////////////////////////////////////////////////////
//                                                                            //
// AUTHOR:      Tevfik Metin Sezgin                                           //
//              Massachusetts Institute of Technology                         //
//              Department of Electrical Engineering and Computer Science     //
//              Artificial Intelligence Laboratory                            //
//                                                                            //
// E-MAIL:        mtsezgin@ai.mit.edu, mtsezgin@mit.edu                       //
//                                                                            //
// COPYRIGHT:   Tevfik Metin Sezgin                                           //
//              All rights reserved. This code can not be copied, modified,   //
//              or distributed in whole or partially without the written      //
//              permission of the author. Also see the COPYRIGHT file.        //
//                                                                            //
////////////////////////////////////////////////////////////////////////////////
package edu.mit.sketch.util;

/** 
  * 
  * See the end of the file for the log of changes.
  * 
  * $Author: mtsezgin $
  * $Date: 2003/05/07 21:29:44 $   
  * $Revision: 1.5 $
  * $Headers$
  * $Id: Util.java,v 1.5 2003/05/07 21:29:44 mtsezgin Exp $     
  * $Name:  $   
  * $Locker:  $
  * $Source: /projects/drg/CVSROOT/drg/code/src/edu/mit/sketch/util/Util.java,v $
  *  
  **/


import java.awt.Component;
import java.awt.Container;
import java.text.DecimalFormat;
import java.text.NumberFormat;

import java.util.ArrayList;
import java.util.Vector;

/**
  *
  * Implements some methods not found in jdk 1.1
  *
  **/
public class
Util
{
    /**
    *
    * Print strings
    *
    **/
    public static void
    printStrings( String strings[] )
    {
        for ( int i=0; i<strings.length; i++ ) {
            System.out.println( strings[i] );
        }
    }


    /**
    *
    * Print array
    *
    **/
    public static void
    printArray( Object[] object, String name )
    {
        for ( int i=0; i<object.length; i++ ) {
            System.out.println( name + "[" + i + "] = " + object[i] );
        }
    }


    /**
    *
    * Print array
    *
    **/
    public static void
    printArray3D( double data[][][], String name )
    {
	NumberFormat formatter = new DecimalFormat("#.0000");
	for ( int i=0; i<data.length; i++ ) {
            System.out.println( name + "[" + i + "]" );
	    for ( int j=0; j<data[i].length; j++ ) {
		for ( int k=0; k<data[i][j].length; k++ ) {
		    String s = formatter.format( data[i][j][k] );
		    System.out.print( s + " " );
		}
		System.out.println();
	    }
	    System.out.println();
        }
    }


    /**
    *
    * Print array
    *
    **/
    public static String
    printArrayConciselyToString( int data[], String name )
    {
        String s = "";
        for ( int i=0; i<data.length-1; i++ ) {
            s += i +"->" + data[i] + ", ";
        }
        if ( data.length != 0 )
            s += (data.length-1) + "->"+ data[data.length-1];
        else
            System.out.println( "ERROR: ARRAY HAS 0 LENGTH" );
            
        return s;
    }


    /**
    *
    * Print array
    *
    **/
    public static void
    printArrayConcisely( int data[], String name )
    {
        System.out.print( name + " " );
        for ( int i=0; i<data.length-1; i++ ) {
            System.out.print( i +"->" + data[i] + ", ");
        }
        if ( data.length != 0 )
            System.out.println( (data.length-1) + "->"+ data[data.length-1] );
        else
            System.out.println( "ERROR: ARRAY HAS 0 LENGTH" );
    }


    /**
    *
    * Print array
    *
    **/
    public static void
    printArrayConcisely( double data[], String name )
    {
        System.out.print( name + " " );
        for ( int i=0; i<data.length-1; i++ ) {
            System.out.print( i + "->" + data[i] + ", ");
        }
        System.out.println( (data.length-1) + "->"+ data[data.length-1] );
        if ( data.length != 0 )
            System.out.println( (data.length-1) + "->"+ data[data.length-1] );
        else
            System.out.println( "ERROR: ARRAY HAS 0 LENGTH" );
    }


    /**
    *
    * Print array
    *
    **/
    public static void
    print2DArray( double data[][], String name )
    {
        System.out.println( name + "[" + data.length + "][" + data[0].length  + "]" );
        for ( int i=0; i<data.length; i++ ) {
            System.out.print( name + "[" + i + "]" );
            for ( int j=0; j<data[i].length; j++ ) {
                System.out.print( data[i][j] + " " );
            }
            System.out.print( "\n" );
        }
    }


    /**
    *
    * Print arraylist
    *
    **/
    public static void
    printArrayList( ArrayList data, String name )
    {
        for ( int i=0; i<data.size(); i++ ) {
            System.out.println( name + ".get(" + i + ") = " + data.get(i) );
        }
    }


    /**
    *
    * Print array
    *
    **/
    public static void
    printArray( double data[], String name )
    {
        for ( int i=0; i<data.length; i++ ) {
            System.out.println( name + "[" + i + "] = " + data[i] );
        }
    }


    /**
    *
    * Print array
    *
    **/
    public static void
    printArray( int data[], String name )
    {
        for ( int i=0; i<data.length; i++ ) {
            System.out.println( name + "[" + i + "] = " + data[i] );
        }
    }


    /**
    *
    * Print array
    *
    **/
    public static void
    printArray( long data[], String name )
    {
        for ( int i=0; i<data.length; i++ ) {
            System.out.println( name + "[" + i + "] = " + data[i] );
        }
    }


    /**
    *
    * Print arrays side by side
    *
    **/
    public static void
    printArrays( double data1[], String name1, double data2[], String name2 )
    {
        for ( int i=0; i<data1.length; i++ ) {
            System.out.println( name1 + "[" + i + "] = " + data1[i] + " " +
                                name2 + "[" + i + "] = " + data2[i] );
        }
    }


    /**
    *
    * Print arrays side by side
    *
    **/
    public static void
    printArrays( double data1[], String name1, long data2[], String name2 )
    {
        for ( int i=0; i<data1.length; i++ ) {
            System.out.println( name1 + "[" + i + "] = " + data1[i] + " " +
                                name2 + "[" + i + "] = " + data2[i] );
        }
    }


    /**
    *
    * Print arrays side by side
    *
    **/
    public static void
    printArrays( long data1[], String name1, double data2[], String name2 )
    {
        for ( int i=0; i<data1.length; i++ ) {
            System.out.println( name1 + "[" + i + "] = " + data1[i] + " " +
                                name2 + "[" + i + "] = " + data2[i] );
        }
    }


    /**
    *
    * Print arrays side by side
    *
    **/
    public static void
    printArrays( int data1[], String name1, int data2[], String name2 )
    {
        for ( int i=0; i<data1.length; i++ ) {
            System.out.println( name1 + "[" + i + "] = " + data1[i] + " " +
                                name2 + "[" + i + "] = " + data2[i] );
        }
    }


    /**
    *
    * Print arrays side by side
    *
    **/
    public static void
    printArrays( long data1[], String name1, long data2[], String name2 )
    {
        for ( int i=0; i<data1.length; i++ ) {
            System.out.println( name1 + "[" + i + "] = " + data1[i] + " " +
                                name2 + "[" + i + "] = " + data2[i] );
        }
    }


    /**
    *
    * Print to string array
    *
    **/
    public static String[]
    printToStringArray( double data[], String name )
    {
        String result[] = new String[data.length];
        for ( int i=0; i<data.length; i++ ) {
            result[i] = name + "[" + i + "] = " + data[i];
        }
        
        return result;
    }


    /**
    *
    * Print to string array
    *
    **/
    public static int[]
    getPositiveZeroCrossingIndices( double data[] )
    {
        Vector  indices = new Vector();
        boolean below   = ( data[0] < 0  );

        for ( int i=1; i<data.length; i++ ) {
            if ( below ) {
                if ( data[i] > 0 ) {
                    indices.addElement( new Integer( i ) );
                    below = false;
                }
            } else {
                below = data[i] < 0;
            }
        }
        
        int result[] = new int[indices.size()];
        for ( int i=0; i<indices.size(); i++ ) {
            result[i] = ((Integer)indices.elementAt( i )).intValue();
        }
        
        return result;
    }


    /**
    *
    * Print to string array
    *
    **/
    public static int[]
    getNegativeZeroCrossingIndices( double data[] )
    {
        Vector  indices = new Vector();
        boolean above   = ( data[0] > 0  );

        for ( int i=1; i<data.length; i++ ) {
            if ( above ) {
                if ( data[i] < 0 ) {
                    indices.addElement( new Integer( i ) );
                    above = false;
                }
            } else {
                above = data[i] > 0;
            }
        }
        
        int result[] = new int[indices.size()];
        for ( int i=0; i<indices.size(); i++ ) {
            result[i] = ((Integer)indices.elementAt( i )).intValue();
        }
        
        return result;
    }


    /**
    *
    * Print to string array
    *
    **/
    public static int[]
    getZeroCrossingIndices( double data[] )
    {
        Vector indices = new Vector();
        

        boolean above            = ( data[0] > 0  );

        for ( int i=1; i<data.length; i++ ) {
            if ( above && ( data[i] < 0 ) ) {
                indices.addElement( new Integer( i ) );
                above = !above;
            }
            if ( !above && ( data[i] > 0 ) ) {
                indices.addElement( new Integer( i ) );
                above = !above;
            }
        }
        
        int result[] = new int[indices.size()];
        for ( int i=0; i<indices.size(); i++ ) {
            result[i] = ((Integer)indices.elementAt( i )).intValue();
        }
        
        return result;
    }


    /**
    *
    * Print to string array
    *
    **/
    public static int[]
    getLocalMinimaIndices( double data[] )
    {
        Vector indices = new Vector();
        

        int     min_index        = 0;
        int     last_added_index = -1;
        double  min              = data[0];

        for ( int i=1; i<data.length; i++ ) {
            if ( data[i] < min ) {
                min_index = i;
                min       = data[i];
            } else {
                if ( last_added_index != min_index ) {
                    indices.addElement( new Integer( min_index ) );
                    last_added_index = min_index;
                }
                min = data[i];
            }
        }
        
        if ( data[data.length-1] < data[data.length-2] )
            indices.addElement( new Integer( data.length-1 ) );
        
        int result[] = new int[indices.size()];
        for ( int i=0; i<indices.size(); i++ ) {
            result[i] = ((Integer)indices.elementAt( i )).intValue();
        }
        
        return result;
    }


    /**
    *
    * Print to string array
    *
    **/
    public static void
    printTrainingData( ArrayList training_data[] )
    {
        for ( int i=0; i<training_data.length; i++ ) {
            ArrayList all_data_for_ith_hmm = training_data[i];
            System.out.println("Data for HMM " + i );
            for ( int j=0; j<all_data_for_ith_hmm.size(); j++ ) {
                ArrayList a_data_for_ith_hmm = (ArrayList)all_data_for_ith_hmm.get( j );
                for ( int k=0; k<a_data_for_ith_hmm.size(); k++ ) {
                    System.out.print( a_data_for_ith_hmm.get( k ) );
                    if ( k == a_data_for_ith_hmm.size()-1 ) {
                        System.out.print( ";\n" );
                    } else {
                        System.out.print( "," );
                    }
                }
            }
        }
    }


    /**
    *
    * Used for training data format conversion
    *
    **/
    public static int[][]
    arrayList2DtoArray( ArrayList data )
    {
        int result[][] = new int[data.size()][((ArrayList)data.get(0)).size()];

        for ( int i=0; i<result.length; i++ ) {
            ArrayList array = (ArrayList)data.get(i);
            for ( int j=0; j<result[0].length; j++ ) {
                result[i][j] = ((Integer)array.get(j)).intValue();
            }
        }
        
        return result;
    }


    /**
    *
    * Used for training data format conversion
    *
    **/
    public static int[]
    arrayListToIntArray( ArrayList data )
    {
        int result[] = new int[data.size()];

        for ( int i=0; i<result.length; i++ ) {
            result[i] = ((Integer)(data.get(i))).intValue();
        }
        
        return result;
    }


    /**
    *
    * Print to string array
    *
    **/
    public static void
    printTrainingData( ArrayList training_data )
    {
        for ( int i=0; i<training_data.size(); i++ ) {
            ArrayList all_data_for_ith_hmm = (ArrayList)training_data.get( i );
            System.out.println("Data for HMM " + i );
            for ( int j=0; j<all_data_for_ith_hmm.size(); j++ ) {
                ArrayList a_data_for_ith_hmm = (ArrayList)all_data_for_ith_hmm.get( j );
                for ( int k=0; k<a_data_for_ith_hmm.size(); k++ ) {
                    System.out.print( a_data_for_ith_hmm.get( k ) );
                    if ( k == a_data_for_ith_hmm.size()-1 ) {

                        System.out.print( ";\n" );
                    } else {
                        System.out.print( "," );
                    }
                }
            }
        }
    }


    /**
    *
    * Print to string array
    *
    **/
    public static String[]
    printToStringArray( long data[], String name )
    {
        String result[] = new String[data.length];
        for ( int i=0; i<data.length; i++ ) {
            result[i] = name + "[" + i + "] = " + data[i];
        }
        
        return result;
    }


    /**
    *
    * Print doubly embedded ArrayList into a string representing
    * the ArrayList in ml format
    *
    **/
    public static String
    arrayListToMLCellMatrix( ArrayList data )
    {
        String result = "{";
        for ( int i=0; i<data.size()-1; i++ ) {
            result += "[";
            ArrayList ith_data = (ArrayList) (data.get(i));
            for ( int j=0; j<ith_data.size()-1; j++ ) {
                result += ((Integer)(ith_data.get(j))).intValue() + ", ";
            }
            result += ((Integer)(ith_data.get(ith_data.size()-1))).intValue();
            result += "], ";
        }

        ArrayList last_data = (ArrayList) (data.get(data.size()-1));
        result += "[";
        for ( int j=0; j<last_data.size()-1; j++ ) {
            result += ((Integer)(last_data.get(j))).intValue() + ", ";
        }
        result += ((Integer)(last_data.get(last_data.size()-1))).intValue();
        result += "]}";

        return result;
    }


    /**
    *
    * Print array in ml format
    *
    **/
    public static String
    array2DToMLMatrix( int data[][] )
    {
        String result = "[";
        for ( int i=0; i<data.length-1; i++ ) {
            for ( int j=0; j<data[i].length-1; j++ ) {
                result += data[i][j] + ", ";
            }
            result += data[i][data[i].length-1];
            result += ";";
        }

        int i = data.length-1;
        for ( int j=0; j<data[i].length-1; j++ ) {
            result += data[i][j] + ", ";
        }
        
        result += data[i][data[i].length-1] + "]";

        
        return result;
    }


    /**
    *
    * Print array in ml format
    *
    **/
    public static String
    arrayToMLMatrix( int data[] )
    {
        String result = "[";
        for ( int i=0; i<data.length-1; i++ ) {
            result += data[i] + ", ";
        }

        int i = data.length-1;
            result += data[i] + "]";
        
        return result;
    }

    
    /**
    * 
    * Convolve input using the filter. The length of the filter
    * must be less than or equal to that of input, and it should be 
    * and odd number. The center factor in the filter corresponds
    * to the current element in the input. The end points are 
    * hadled separately and scaled.
    *
    **/
    public static double[]
    convolve( double input[], double filter[] )
    {
        double filter_sum = 0.0;
        double result[]   = new double[input.length];
        int    l          = (filter.length-1)/2;

        for ( int i=0; i<filter.length; i++ ) {
            filter_sum += filter[i];
        }

        for ( int i=l; i<input.length-l; i++ ) {
            result[i] = 0.0;
            for ( int j=0; j<filter.length; j++ ) {
                result[i] += filter[j]*input[i-l+j];
            }
            result[i] /= filter_sum;
        }
        
        double partial_sum = 0.0;
        for ( int i=0; i<l; i++ ) {
            partial_sum = 0.0;
            result[i]   = 0.0;
            for ( int j=0; j<filter.length; j++ ) {
                if ( i-l+j >= 0 ) {
                    result[i]   += filter[j]*input[i-l+j];
                    partial_sum += filter[j];
                }
            }
            result[i] /= partial_sum;
        }
        for ( int i=input.length-l; i<input.length; i++ ) {
            partial_sum = 0.0;
            result[i]   = 0.0;
            for ( int j=0; j<filter.length; j++ ) {
                if ( i-l+j < input.length ) {
                    result[i]   += filter[j]*input[i-l+j];
                    partial_sum += filter[j];
                }
            }
            result[i] /= partial_sum;
        }
        
        return result;
    }
}


/** 
  * 
  * $Log: Util.java,v $
  * Revision 1.5  2003/05/07 21:29:44  mtsezgin
  *
  * Added Utility function in AWTUtil
  *
  * Revision 1.4  2003/03/06 01:08:54  moltmans
  * Added copyright to all the files.
  *
  * Revision 1.3  2002/07/22 21:06:09  mtsezgin
  * Added more utility methods.
  *
  * Revision 1.2  2002/05/24 17:33:41  mtsezgin
  * Added more utility functions.
  *
  * Revision 1.1  2001/11/23 03:24:33  mtsezgin
  * *** empty log message ***
  *
  * Revision 1.3  2001/10/18 19:52:06  mtsezgin
  * *** empty log message ***
  *
  * Revision 1.2  2001/10/12 22:25:53  mtsezgin
  * This is a commit of all files.
  * Shoapid
  * vi sux:q
  *
  * Revision 1.1.1.1  2001/03/29 16:25:01  moltmans
  * Initial directories for DRG
  *
  * Revision 1.3  2000/09/06 22:41:09  mtsezgin
  * Combinations of curves and polygons are successfully approximated
  * by straight lines and Bezier curves as appropriate. System works
  * quite reliably.
  *
  * Revision 1.4  2000/06/03 01:52:33  mtsezgin
  * *** empty log message ***
  *
  * Revision 1.3  2000/04/28 04:45:03  mtsezgin
  *
  * Now each GeometricObject keeps the mouse input that was previously
  * discarded. User can switch between seeing the recognized mode and
  * the raw mode. setDataPoints( Polygon points ) and getDataPoints()
  * are added to GeometricObject, and all the implementors are modified
  * accordingly.
  *
  * Revision 1.2  2000/04/25 22:15:29  mtsezgin
  *
  * Added more utility functions.
  *
  * Revision 1.1.1.1  2000/04/01 03:07:07  mtsezgin
  * Imported sources
  *
  * Revision 1.2  2000/03/31 22:41:03  mtsezgin
  *
  * Started Log tracking.
  *
  *  
  **/
