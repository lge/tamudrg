// (c) MIT 2003.  All rights reserved.

package edu.mit.sketch.fig;

import edu.mit.sketch.geom.Ellipse;
import edu.mit.sketch.geom.GeometricObject;
import edu.mit.sketch.geom.Line;
import edu.mit.sketch.geom.Point;
import edu.mit.sketch.geom.Polygon;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.io.Writer;
import java.lang.StringBuffer;
import org.apache.log4j.BasicConfigurator;
import org.apache.log4j.Logger;


/**
 * 
 * <p>
 * Created:   Tue Apr 16 19:57:58 2002<br>
 * Copyright: Copyright (C) 2002 by MIT.  All rights reserved.<br>
 * 
 * @author <a href="mailto:moltmans@ai.mit.edu">Michael Oltmans</a>
 * @version $Id: FigDocument.java,v 1.4 2003/03/06 01:08:48 moltmans Exp $
 **/

public class FigDocument {
  private static Logger m_logger = Logger.getLogger( FigDocument.class );

  private static int m_scale = 10;

  private Writer m_document;

  public FigDocument( Writer w )
  {
    m_document = w;
    printHeader();
  }

  private void printHeader() 
  {
    println( "#FIG 3.2" );
    println( "Landscape" );
    println( "Center" );
    println( "Inches" );
    println( "Letter"   );
    println( "100.00" );
    println( "Single" );
    println( "-2" );
    println( "1200 2" );
  }

  public void render( GeometricObject shape )
  {
    if( shape instanceof Ellipse ) {
      render( (Ellipse)shape );
    }
    else if( shape instanceof Polygon ) {
      render( (Polygon)shape );
    }
    else if( shape instanceof Line ) {
      render( (Line)shape );
    }
    
    
  }
  

  public void render( Ellipse e )
  {
    println( "1 "   // the object code
	     + "2 " // use elliptical diameters definition
	     + "0 " // line style is solid
	     + "1 " // thickness
	     + "0 " // pen color
	     + "7 " // fill color
	     + "50 " // depth
	     + "0 "  // pen style
	     + "-1 " // area fill (-1 = no fill)
	     + "0.000 " // style value (1/80 inch)
	     + "1 " // direction (always 1)
	     + "0.000 " // angle
	     + (int)e.getCenterX()*m_scale + " " // center_x
	     + (int)e.getCenterY()*m_scale + " " // center_y
	     + (int)(e.getWidth()*m_scale / 2.0) + " "   // radius_x
	     + (int)(e.getHeight()*m_scale / 2.0) + " "  // radius_y
	     + (int)e.getMinX()*m_scale + " "    // start_x
	     + (int)e.getMinY()*m_scale + " "    // start_y
	     + (int)e.getMaxX()*m_scale + " "    // end_x
	     + (int)e.getMaxY()*m_scale          // end_y
 	     );
  }

  public void render( Polygon p )
  {
    boolean isClosed = p.pointAt( 0 ).equals( p.pointAt( p.npoints - 1 ) );
    

    println( "2 " // object code
	     + (isClosed ? "3 " : "1 ") // sub type: assume polyline for all
	     + "0 " // solid line style
	     + "1 " // line thickness
	     + "0 " // pen color
	     + "7 " // fill color
	     + "50 " // depth
	     + "0 " // pen style
	     + "-1 " // area fill (-1 = no fill)
	     + "0.000 " // style value (1/80 inch)
	     + "0 " // join style
	     + "0 " // cap style = butt (that's what the docs call it...???)
	     + "-1 " // radius not used except for arc-boxes
	     + "0 " // no forward arrow
	     + "0 " // no backward arrow
	     + p.npoints
	     );

    // make it look nice:
    print( "\t " );
    for( int i = 0; i < p.npoints; i++ ) {
      Point point = p.pointAt( i );
      print( (int)point.getX()*m_scale + " " + (int)point.getY()*m_scale + " " );
    }
    println( "" );
  }

  public void render( Line line ) 
  {
    Polygon p = new Polygon();
    p.addPoint( (int)line.x1, (int)line.y1 );
    p.addPoint( (int)line.x2, (int)line.y2 );
    render( p );
  }
  
  private void print( String string )
  {
    try {
      m_document.write( string );
    } catch( IOException e ) {
      m_logger.error( "Trouble writing to the writer" );
    }
  }
  
  /**
   * This method is needed because xfig doesn't like having DOS line
   * endings.  Thereofore we need to make sure to do our own line
   * terminations.
   **/
  private void println( String string ) 
  {
    print( string + "\n" );
  }
  
  public static void main( String[] args ) {
    // this is a workaround for a bug in java 1.4 where file locking
    // over NFS on fails for the user's java preferences file, causing
    // annoying messages and a file descriptor leak.
    // (see http://developer.java.sun.com/developer/bugParade/bugs/4673298.html)
    
    // this workaround was posted in the java forums at
    // http://forum.java.sun.com/thread.jsp?forum=27&thread=231280

    // it sets the sync interval to > 600 hours so that java doesn't
    // attempt to lock the file.

    System.setProperty("java.util.prefs.syncInterval","2000000");

    BasicConfigurator.configure();
    Ellipse e1 = new Ellipse(2400, 1200, 1200, 3600);
    Ellipse e2 = new Ellipse(3000, 2400, 600, 1800);
    Line l1 = new Line( 2400, 1200, 3600, 4800 );
    Polygon p1 = new Polygon();
    p1.addPoint( 2400, 1200 );
    p1.addPoint( 2400, 4800 );
    p1.addPoint( 3600, 4800 );
    p1.addPoint( 3600, 1200 );
    p1.addPoint( 2400, 1200 );
    
    Writer w;
    try {
      if( args.length != 1 ) {
	System.out.println("Must give a single file name to write the figure to");
	System.exit(1);
      }
      
      w = new FileWriter( args[0] );
      FigDocument d = new FigDocument( w );
      d.render( e1 );
      d.render( e2 );
      d.render( p1 );
      d.render( l1 );
      w.flush();
      w.close();
    } catch( IOException ex ) {
      System.err.println("Trouble opening file");
      ex.printStackTrace();
    }
    
  }

  
}// FigDocument
