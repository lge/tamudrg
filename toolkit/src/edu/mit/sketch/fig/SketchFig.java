// (c) MIT 2003.  All rights reserved.

package edu.mit.sketch.fig;

import edu.mit.sketch.geom.GeometricObject;
import edu.mit.sketch.toolkit.SimpleClassifier;
import edu.mit.sketch.toolkit.SimpleClassifier3;
import edu.mit.sketch.toolkit.StrokeData;
import edu.mit.sketch.toolkit.StrokeDataListener;
import edu.mit.sketch.ui.SketchPanel;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import javax.swing.JButton;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JPanel;

/**
 * 
 * <p>
 * Created:   Wed Apr 17 18:49:26 2002<br>
 * Copyright: Copyright (C) 2002 by MIT.  All rights reserved.<br>
 * 
 * @author <a href="mailto:moltmans@ai.mit.edu">Michael Oltmans</a>
 * @version $Id: SketchFig.java,v 1.5 2003/07/07 21:40:08 moltmans Exp $
 **/

public class SketchFig extends SketchPanel implements StrokeDataListener {
  private boolean m_debugging = true;
  private List m_shapes;
  
  public SketchFig() {
    m_shapes = new ArrayList();
    addStrokeDataListener( this );
    setOpaque( true );
    setBackground( Color.white );
    setForeground( Color.black );
  }

  public void handleStroke( StrokeData stroke ) 
  {
    SimpleClassifier c  = new SimpleClassifier3( stroke );
    if ( c.isLine() ) {
      debug("Got a line");
      addShape( c.getLineApproximation() );
    }
    else if( c.isPolygon() ) {
      debug("Got a polyline or polygon");
      addShape( c.getPolygonApproximation() );
    }
    else if ( c.isEllipse() ) {
      debug("Got an ellipse");
      addShape( c.getEllipseApproximation() );
    }
    else {
      debug("I'm sorry but I can't tell what you just drew");
    }
    repaint();
  }

  private void addShape( GeometricObject shape )
  {
    m_shapes.add( shape );
  }

  public void paintComponent( Graphics g )
  {
    super.paintComponent( g );
    
    Iterator it = m_shapes.iterator();
    while( it.hasNext() ) {
      GeometricObject shape = (GeometricObject)it.next();
      shape.paint( g );
    }
  }

  public void writeFigFile( File file ) 
  {
    debug( "Writing fig file" );
    FileWriter w = null;
    try {
      w = new FileWriter( file );
      FigDocument doc = new FigDocument( w );

      Iterator it = m_shapes.iterator();
      while( it.hasNext() ) {
	GeometricObject shape = (GeometricObject)it.next();
	doc.render( shape );
      }

      w.close();
    } catch( IOException e ) {
      debug( "trouble opening or writing to the file" );
    }
  }

  public void resetFig() 
  {
    m_shapes.clear();
    repaint();
  }
  
  /** Print some simple debug info if we want to **/
  private void debug( String txt )
  {
    if( m_debugging ) {
      System.out.println( txt );
    }
  }
  
  public static void main (String[] args) {
    // this is a workaround for a bug in java 1.4 where file locking
    // over NFS on fails for the user's java preferences file, causing
    // annoying messages and a file descriptor leak.
    // (see http://developer.java.sun.com/developer/bugParade/bugs/4673298.html)
    
    // this workaround was posted in the java forums at
    // http://forum.java.sun.com/thread.jsp?forum=27&thread=231280

    // it sets the sync interval to > 600 hours so that java doesn't
    // attempt to lock the file.

    System.setProperty("java.util.prefs.syncInterval","2000000");

    final JFrame frame = new JFrame("SketchFig");
    JPanel buttons = new JPanel();
    JButton exportButton = new JButton("Export");
    JButton resetButton = new JButton("Reset");

    buttons.add( exportButton );
    buttons.add( resetButton );

    final SketchFig sketchFig = new SketchFig();

    resetButton.addActionListener( new ActionListener() {
	public void actionPerformed( ActionEvent e ) {
	  sketchFig.resetFig();
	}
      });

    // write the fig file
    exportButton.addActionListener(new ActionListener() {
	public void actionPerformed(ActionEvent e) {
	  JFileChooser chooser = new JFileChooser();
	  if( chooser.showOpenDialog( frame ) == JFileChooser.APPROVE_OPTION ) {
	    File file = chooser.getSelectedFile();
	    sketchFig.writeFigFile( file );
	  }
	}
      });

    frame.getContentPane().add( sketchFig, BorderLayout.CENTER );
    frame.getContentPane().add( buttons, BorderLayout.SOUTH );
    frame.setSize( 400, 400 );
    frame.show();
    
    frame.addWindowListener(new WindowAdapter() {
	public void windowClosing(WindowEvent e) {
	  System.exit(0);
	}
      });
  }
  
}// SketchFig
