// (c) MIT 2003.  All rights reserved.

package edu.mit.sketch.system;

import edu.mit.util.Assert;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;



/**
 * 
 * <p>
 * Created:   Fri Feb 15 14:55:00 2002<br>
 * Copyright: Copyright (C) 2002 by MIT.  All rights reserved.<br>
 * 
 * @author <a href="mailto:moltmans@ai.mit.edu">Michael Oltmans</a>
 * @version $Id: BBItemType.java,v 1.6 2003/05/29 19:44:04 calvarad Exp $
 **/

public class BBItemType {
  private static HashMap s_allTypes = new HashMap();
  
  private String m_name;
  private List m_superTypes;

  public static BBItemType newType( String name, Collection superTypes ) 
  {
    if( typeExists( name ) ) {
      return getType( name );
    }
    
//      Assert.isTrue( s_allTypes.get( name ) == null,
//  		   "Attempt to recreate an existing BBItemType" );
    BBItemType type = new BBItemType( name, superTypes );
    s_allTypes.put( name, type );
    return type;
  }
 
  public static BBItemType newType( String name, BBItemType[] superTypes ) 
  {
    return newType( name, Arrays.asList( superTypes ) );
  }

  public static BBItemType newType( String name, BBItemType superType ) 
  {
    return newType( name, Collections.singleton( superType ) );
  }

  public static BBItemType newType( String name )
  {
    return newType( name, new ArrayList( 0 ) );
  }

  public static BBItemType getType( String name ) 
  {
    BBItemType type = (BBItemType)s_allTypes.get( name );
    Assert.notNull( type, "Attempt to access non existant BBItemType: " + name );
    return type;
  }

  public static boolean typeExists( String name )
  {
    return s_allTypes.containsKey( name );
  }

  /******************************************************************/

  protected BBItemType( String name, Collection superTypes )
  {
    m_name = name;
    m_superTypes = new LinkedList( superTypes );
  }

  /** Get the immediate parents of this type. **/
  public List getSuperTypes() 
  {
    return Collections.unmodifiableList( m_superTypes );
  }

  /** Get all of the ancesstors of this type. **/
  public Set getAllAncestors() 
  {
    HashSet supers = new HashSet();

    LinkedList queue = new LinkedList();
    queue.addAll( m_superTypes );
    
    while( !queue.isEmpty() ) {
      BBItemType superType = (BBItemType)queue.removeFirst();

      // When we add it check if it is new.  If it is a dupolicate we
      // don't need to add its supers again
      if ( supers.add( superType ) ) {
	queue.addAll( superType.getSuperTypes() );
      }
      
    }
    return supers;    
  }
  

  public boolean isa( BBItemType type ) 
  {
    return equals( type ) || getAllAncestors().contains( type );
  }

  public boolean equals( Object obj ) 
  {
    // They are equal if they have the same name.  It is not valid for
    // two types to have the same name but different super classes...
    return ( ( obj instanceof BBItemType ) &&
	     m_name.equals( ((BBItemType)obj).m_name ) );
  }

  public int hashCode() 
  {
    return m_name.hashCode();
  }

  public String toString() 
  {
    return m_name;
  }
  
  
    
    
  
}// BBItemType
