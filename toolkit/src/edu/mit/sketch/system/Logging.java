// (c) MIT 2003.  All rights reserved.

package edu.mit.sketch.system;

import java.net.URL;
import org.apache.log4j.BasicConfigurator;
import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;

/**
 * 
 * <p>
 * Created:   Mon Apr 15 20:15:12 2002<br>
 * Copyright: Copyright (C) 2002 by MIT.  All rights reserved.<br>
 * 
 * @author <a href="mailto:moltmans@ai.mit.edu">Michael Oltmans</a>
 * @version $Id: Logging.java,v 1.15 2004/01/16 21:26:03 calvarad Exp $
 **/

public class Logging {
  public static final Logger LOG = Logger.getLogger("drg.log");
  public static final Logger DISPLAY =
    Logger.getLogger( "drg.system.display" );
  public static final Logger BUG =
    Logger.getLogger( "drg.system.bug" );
  public static final Logger SCHEDULE =
    Logger.getLogger( "drg.system.scheduler" );
  public static final Logger BB = Logger.getLogger( "drg.system.bb" );
  public static final Logger EXPRESSIONS =
    getSubLogger( BB, "expressions" );
  public static final Logger KS = Logger.getLogger( "drg.system.ks" );
  public static final Logger SETUP = Logger.getLogger( "drg.system.setup" );
  public static final Logger TEMPLATE = Logger.getLogger( "drg.system.template" );
  public static final Logger TMS = Logger.getLogger( "drg.system.tms" );
  public static final Logger UI = Logger.getLogger( "drg.system.ui" );
  public static final Logger APPLICATION =
    Logger.getLogger( "drg.system.application" );
  public static final Logger BROWSER =
    Logger.getLogger( "drg.system.browser" );
  public static final Logger BN = Logger.getLogger( "drg.system.bayesnet" );
  public static final Logger DATA = Logger.getLogger( "drg.system.data" );
  public static final Logger TOOLKIT = Logger.getLogger( "drg.toolkit" );
  public static final Logger VIEWER = Logger.getLogger( "drg.viewer" );
  
  public static Logger getSubLogger( Logger logger, String subLogger ) 
  {
    return Logger.getLogger( logger.getName() + "." + subLogger );
  }
  
  public static void startLogging() 
  {
    // start the logging mechanism
    URL logConfig = Logging.class.getResource("my-logger.lcf");
    if( logConfig != null ) {
      PropertyConfigurator.configure( logConfig );
    }
    else {
      LOG.info( "Trouble loading your logger settings, using a default console based logger instead" );
      BasicConfigurator.configure();
    }
  }

  private Logging() {}
  
}// Logging
