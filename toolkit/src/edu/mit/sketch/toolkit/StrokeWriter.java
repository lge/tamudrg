// (c) MIT 2003.  All rights reserved.

package edu.mit.sketch.toolkit;

import edu.mit.sketch.geom.Vertex;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.Writer;
import java.util.Collection;
import java.util.Iterator;



/**
 * 
 * <p>
 * Created:   Thu May  2 20:18:18 2002<br>
 * Copyright: Copyright (C) 2002 by MIT.  All rights reserved.<br>
 * 
 * @author <a href="mailto:moltmans@ai.mit.edu">Michael Oltmans</a>
 * @version $Id: StrokeWriter.java,v 1.8 2004/01/14 21:26:09 moltmans Exp $
 **/

public class StrokeWriter {
  public static final String V1_0 = "v1.0";
  public static final String V2_0 = "v2.0";

  protected String m_version;

  protected PrintWriter m_writer;
  
  public StrokeWriter( Writer writer, double timeConversion, String name )
  {
    m_version = V2_0;
    m_writer = new PrintWriter( new BufferedWriter( writer ) );
    writeHeader( m_version, timeConversion, name );
  }

  public StrokeWriter( Writer writer, double timeConversion,
                       String name, String version)
  {
    m_version = version;
    m_writer = new PrintWriter( new BufferedWriter( writer ) );
    writeHeader( m_version, timeConversion, name );
  }

  protected StrokeWriter()
  {
  }

  public String getVersion()
  {
    return m_version;
  }
  
  public static void writeStrokesToFile(File file, double timeConversion,
                                        String name, Collection strokes)
    throws IOException
  {
    Writer writer = new FileWriter(file);       
    StrokeWriter strokeWriter = new StrokeWriter(writer, timeConversion,
                                                 name);
    strokeWriter.writeStrokes(strokes);
  }

  public void writeStrokes( Collection strokes )
  {
    Iterator it = strokes.iterator();
    while( it.hasNext() ) {
      StrokeData stroke = (StrokeData)it.next();
      writeStroke( stroke );
    }
    m_writer.flush();
  }

  /**
   * Write the file header:
   * <code>
   *   version=v1.0
   *   timeConversion=1.0
   *   name=Circles
   * </code>
   *
   * @param version the version of the file format
   * @param timeConversion a conversion from the given time units to
   *         milliseconds.  i.e. <code>milliseconds / unit</code>
   * @param name a name for this dataset
   **/
  protected void writeHeader( String version, double timeConversion,
			      String name )
  {
    m_writer.println( "version=" + version );
    m_writer.println( "timeConversion=" + timeConversion );
    m_writer.println( "name=" + name );
    m_writer.flush();
  }
  

  protected void writeStroke( StrokeData stroke ) 
  {
    m_writer.print( "{ " );
    Vertex[] points = stroke.getVertices();
    for( int i = 0; i < points.length; i++ ) {
      if( m_version.equals( V2_0 ) ) {
        m_writer.print( "(" );
      }
      
      m_writer.print( points[i].x + " " + points[i].y + " " +
                      points[i].getTimeStamp() );
      if( m_version.equals( V2_0 ) ) {
        m_writer.print( ")" );
      }
    }
    m_writer.println( " }" );
  }
  

  
}// StrokeWriter
