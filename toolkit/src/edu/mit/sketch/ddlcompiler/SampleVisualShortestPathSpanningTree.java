// (c) MIT 2003.  All rights reserved.

package edu.mit.sketch.ddlcompiler;

import salvo.jesus.graph.*;
import salvo.jesus.graph.algorithm.*;
import salvo.jesus.graph.visual.*;
import salvo.jesus.graph.visual.drawing.*;
import salvo.jesus.graph.visual.layout.*;
import java.awt.*;
import java.awt.event.*;
import javax.swing.*;

/**
 * SampleVisualShortestPathSpanningTree
 */

public class SampleVisualShortestPathSpanningTree extends JFrame {
    WeightedGraph    wgraph;
    WeightedGraph   subGraph;
    VisualGraph     vgraph;

    public SampleVisualShortestPathSpanningTree( WeightedGraph    wgraph,
                                                WeightedGraph   subGraph ) throws Exception {
        GraphLayoutManager  layoutManager;

        this.wgraph = wgraph;
        this.subGraph = subGraph;
        

        vgraph = new VisualGraph( subGraph );

        //layoutManager = new LayeredTreeLayout( vgraph );
        //layoutManager = new StraightLineLayout( vgraph );
        //layoutManager = new OrthogonalLineLayout( vgraph );
        layoutManager = new ForceDirectedLayout( vgraph );
         vgraph.setGraphLayoutManager( layoutManager );
        
        // Make it all visible
        this.getContentPane().setLayout( new GridLayout(1,2));
        this.getContentPane().add( new GraphScrollPane( vgraph) );

        Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
        Dimension frameSize = new Dimension( screenSize.width - 80, screenSize.height - 80 );

        //this.setSize( frameSize );
        //this.setLocation((int)(screenSize.getWidth() - frameSize.getWidth()) / 2, (int)(screenSize.getHeight() - frameSize.getHeight()) / 2);
        this.setSize( 50, 50 );
        this.setLocation( 1000, 500 );
        System.out.println( "Calling graph layout()" );
        layoutManager.layout();
        System.out.println( "Called graph layout()" );
    }
}
