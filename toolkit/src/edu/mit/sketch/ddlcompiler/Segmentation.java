// (c) MIT 2003.  All rights reserved.

////////////////////////////////////////////////////////////////////////////////
//                                                                            //
// AUTHOR:      Tevfik Metin Sezgin                                           //
//              Massachusetts Institute of Technology                         //
//              Department of Electrical Engineering and Computer Science     //
//              Artificial Intelligence Laboratory                            //
//                                                                            //
// E-MAIL:        mtsezgin@ai.mit.edu, mtsezgin@mit.edu                       //
//                                                                            //
// COPYRIGHT:   Tevfik Metin Sezgin                                           //
//              All rights reserved. This code can not be copied, modified,   //
//              or distributed in whole or partially without the written      //
//              permission of the author. Also see the COPYRIGHT file.        //
//                                                                            //
////////////////////////////////////////////////////////////////////////////////


package edu.mit.sketch.ddlcompiler;

import java.util.ArrayList;

import edu.mit.sketch.util.LoadStoreModule;
import edu.mit.sketch.util.Util;



/** 
  * 
  * See the end of the file for the log of changes.
  * 
  * $Author: mtsezgin $
  * $Date: 2003/06/16 17:11:56 $   
  * $Revision: 1.3 $
  * $Headers$
  * $Id: Segmentation.java,v 1.3 2003/06/16 17:11:56 mtsezgin Exp $     
  * $Name:  $   
  * $Locker:  $
  * $Source: /projects/drg/CVSROOT/drg/code/src/edu/mit/sketch/ddlcompiler/Segmentation.java,v $
  *  
  **/


/**
  *
  * This class is for dealing with segmentation results. You can 
  * read and write segmentation information with this class.
  *
  **/

public class
Segmentation
{
    
    /**
     * object ids (class)
     **/
    int object_ids[];
    
    /**
     * Ending indices for objects
     **/
    int ending_indices[];
    
    /**
     * segmentation for the observations
     **/
    int segmentation[];
    
    public
    Segmentation( String file_name )
    {
        ArrayList packed_segmentation =
            LoadStoreModule.loadSegmentationForClass( file_name + "_segmentation" );
    
        ArrayList unpacked_segmentation = new ArrayList();
    
        object_ids     = new int[packed_segmentation.size()];
        ending_indices = new int[packed_segmentation.size()];
        for ( int i=0; i<packed_segmentation.size(); i++ ) {
            int pair[] = (int[])(packed_segmentation.get(i));
            for ( int j=0; j<pair[1]; j++ ) {
                unpacked_segmentation.add( new Integer( pair[0] ) );
            }
            if ( i == 0 ) {
                ending_indices[i] = pair[1];
            } else {
                ending_indices[i] = ending_indices[i-1] + pair[1];
            }
            object_ids[i] = pair[0];
        }
        segmentation = Util.arrayListToIntArray( unpacked_segmentation );
    }
    
    
    /**
    *
    * save
    *
    **/
    public void
    save( String file_name ) 
    {
        LoadStoreModule.saveSegmentationForClass( file_name + "_segmentation", ending_indices, object_ids );
    }
    
    
    /**
    *
    * setEndingIndices() 
    *
    **/
    public void
    setEndingIndices( int ending_indices[] ) 
    {
        this.ending_indices = ending_indices;
    }
    
    
    /**
    *
    * setEndingIndices() 
    *
    **/
    public void
    setObjectIds( int object_ids[] ) 
    {
        this.object_ids = object_ids;
    }
    
    
    /**
    *
    * getEndingIndices() 
    *
    **/
    public int[]
    getEndingIndices() 
    {
        return ending_indices;
    }
    
    
    /**
    *
    * getEndingIndices() 
    *
    **/
    public int[]
    getObjectIds() 
    {
        return object_ids;
    }
    
    
    /**
    *
    * getEndingIndices() 
    *
    **/
    public int[]
    getSegmentationForObservations() 
    {
        return segmentation;
    }
}
