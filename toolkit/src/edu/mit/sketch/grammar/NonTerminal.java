// (c) MIT 2003.  All rights reserved.

////////////////////////////////////////////////////////////////////////////////
//                                                                            //
// AUTHOR:      Tevfik Metin Sezgin                                           //
//              Massachusetts Institute of Technology                         //
//              Department of Electrical Engineering and Computer Science     //
//              Artificial Intelligence Laboratory                            //
//                                                                            //
// E-MAIL:        mtsezgin@ai.mit.edu, mtsezgin@mit.edu                       //
//                                                                            //
// COPYRIGHT:   Tevfik Metin Sezgin                                           //
//              All rights reserved. This code can not be copied, modified,   //
//              or distributed in whole or partially without the written      //
//              permission of the author. Also see the COPYRIGHT file.        //
//                                                                            //
////////////////////////////////////////////////////////////////////////////////
package edu.mit.sketch.grammar;


/** 
  * 
  * See the end of the file for the log of changes.
  * 
  * $Author: moltmans $
  * $Date: 2003/10/13 19:46:36 $   
  * $Revision: 1.4 $
  * $Headers$
  * $Id: NonTerminal.java,v 1.4 2003/10/13 19:46:36 moltmans Exp $     
  * $Name:  $   
  * $Locker:  $
  * $Source: /projects/drg/CVSROOT/drg/code/src/edu/mit/sketch/grammar/NonTerminal.java,v $
  *  
  **/


import java.awt.Graphics; 
import java.util.Vector;

import edu.mit.sketch.ui.Paintable;
import edu.mit.sketch.geom.Translatable;
import edu.mit.sketch.geom.Rectangle;
import edu.mit.sketch.geom.Polygon;

/**
  *
  * All the objects who want to be NonTerminal must implement this
  * interface.
  *
  **/

public interface NonTerminal
extends          Paintable,
                 Translatable
{
    /**
    *
    * This method should return the Rectangular bounds of this 
    * non-terminal object.
    *
    **/
    public abstract Rectangle
    getRectangularBounds();
    
    
    /**
    *
    * This method should return the polygonal array bounds of this 
    * non-terminal object. This is a rough approximation of the
    * constituents of the object. 
    *
    **/
    public abstract Polygon[]
    getPolygonalBoundsArray();
    
    
    /**
    *
    * This method should return a vector containing the children of 
    * this object. Children are the Terminal or NonTerminal 
    * constituents of this object.
    *
    **/
    public abstract Vector
    getChildren();
}

/**
  *
  * $Log: NonTerminal.java,v $
  * Revision 1.4  2003/10/13 19:46:36  moltmans
  * Removed bad line endings.
  *
  * Revision 1.3  2003/03/06 01:08:49  moltmans
  * Added copyright to all the files.
  *
  * Revision 1.2  2001/11/26 18:28:59  mtsezgin
  * *** empty log message ***
  *
  * Revision 1.1  2001/11/23 03:23:49  mtsezgin
  * *** empty log message ***
  *
  * Revision 1.1.1.1  2001/03/29 16:25:00  moltmans
  * Initial directories for DRG
  *
  * Revision 1.10  2000/09/06 22:40:43  mtsezgin
  * Combinations of curves and polygons are successfully approximated
  * by straight lines and Bezier curves as appropriate. System works
  * quite reliably.
  *
  * Revision 1.4  2000/06/03 01:52:33  mtsezgin
  * *** empty log message ***
  *
  * Revision 1.3  2000/05/04 01:36:27  mtsezgin
  *
  * Fixed minor bugs.
  * The current version successfuly parses Motor, Cross, Nail and Ground.
  * In addition the ParseSupervisor is introduced here.
  *
  * Revision 1.2  2000/05/03 23:26:46  mtsezgin
  * *** empty log message ***
  *
  * Revision 1.1  2000/04/11 00:41:47  mtsezgin
  *
  * Now the whole package succesfully parses a motor.
  *
  *
  **/
