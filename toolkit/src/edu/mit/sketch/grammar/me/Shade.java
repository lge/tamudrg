// (c) MIT 2003.  All rights reserved.

////////////////////////////////////////////////////////////////////////////////
//                                                                            //
// AUTHOR:      Tevfik Metin Sezgin                                           //
//              Massachusetts Institute of Technology                         //
//              Department of Electrical Engineering and Computer Science     //
//              Artificial Intelligence Laboratory                            //
//                                                                            //
// E-MAIL:        mtsezgin@ai.mit.edu, mtsezgin@mit.edu                       //
//                                                                            //
// COPYRIGHT:   Tevfik Metin Sezgin                                           //
//              All rights reserved. This code can not be copied, modified,   //
//              or distributed in whole or partially without the written      //
//              permission of the author. Also see the COPYRIGHT file.        //
//                                                                            //
////////////////////////////////////////////////////////////////////////////////
package edu.mit.sketch.grammar.me;


/** 
  * 
  * See the end of the file for the log of changes.
  * 
  * $Author: moltmans $
  * $Date: 2003/10/13 19:46:37 $   
  * $Revision: 1.3 $
  * $Headers$
  * $Id: Shade.java,v 1.3 2003/10/13 19:46:37 moltmans Exp $     
  * $Name:  $   
  * $Locker:  $
  * $Source: /projects/drg/CVSROOT/drg/code/src/edu/mit/sketch/grammar/me/Shade.java,v $
  *  
  **/

import edu.mit.sketch.grammar.SequenceTerminal;
import edu.mit.sketch.geom.Rectangle;
import edu.mit.sketch.geom.Polygon;
import edu.mit.sketch.geom.Line;
import edu.mit.sketch.geom.Point;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;

import java.util.Vector;

import java.io.Serializable;

/**
  *
  * This class represents a line described by its end-points p, q.
  *
  **/
public 
class      Shade
extends    Rectangle
implements SequenceTerminal,
           Serializable
{
    /**
    *
    * Lines that formed this Shade.
    *
    **/
    public Vector lines;


    /**
    *
    * Number of shades to draw.
    *
    **/
    public int shades;


    /**
    *
    * The constructor.
    *
    **/    
  public Shade( Vector input_lines )
    {
        super( 0, 0, 0, 0 );
        this.lines = new Vector();
        
        for ( int i=0; i<input_lines.size(); i++ ) {
            lines.addElement( input_lines.elementAt( i ) );
        }
        shades     = lines.size();

        Polygon combined_lines = new Polygon();
        Line    line;
        for ( int i=0; i<lines.size(); i++ ) {
            line = (Line)lines.elementAt(i);
            combined_lines.addPoint( (int)line.x1, (int)line.y1 );
            combined_lines.addPoint( (int)line.x2, (int)line.y2 );
        }
        
        super.setRectangle( combined_lines.getRectangularBounds() );
    }
    

    /**
    *
    * Implement GeometricObject
    *
    **/    
    public String
    getType()
    {
        return "shade";
    }
    
    
    /**
    *
    * Override toString
    *
    **/    
    public String
    toString()
    {
        return "Shade";
    }


    /**
    *
    * Draw the object
    *
    **/
    public void
    paint()
    {
        if ( graphics == null ) // Draw only if g
            return;             // is valid
        graphics.setColor( Color.black );
        paint( graphics );
    }


    /**
    *
    * Draw the object
    *
    **/
    public void
    paint( Graphics g )
    {
        super.shade( g, 2*shades, 1, 1 );
    }
    
    
    /**
    *
    * This method is used to paint the original data points that
    * forms this GeometricObject
    *
    **/
    public void
    paintOriginal( Graphics g )
    {
        for ( int i=0; i<lines.size(); i++ ) {
            ((Line)lines.elementAt(i)).paintOriginal( g );
        }
    }
    
    
    /**
    *
    * Returns true if the point is within +-radius distance from
    * the curve defining the object. Returns false o/w.
    *
    **/
    public boolean
    pointIsOn( Point point, int radius )
    {
        return ( super.pointIsOn( point, radius ) );
    }


    /**
    *
    * Supplied for completeness.
    *
    **/
    public Rectangle
    getRectangularBounds()
    {
        return super.getRectangularBounds();
    }


    /**
    *
    * Returns the angle of the object WRT the x axis in radians.
    *
    **/
    public double
    getAngle()
    {
        return super.getAngle();
    }


    /**
    *
    * Sets the time stamp of the current Terminal
    *
    **/
    public void
    setTimeStamp( long time_stamp )
    {
        this.time_stamp = time_stamp;
    }
    
    /**
    *
    * Returns the time stamp of the current Terminal
    *
    **/
    public long
    getTimeStamp()
    {
        return time_stamp;
    }
    
    
    /**
    *
    * Add the arguments to the (x, y) position of the object.
    *
    **/
    public void
    translate( double x, double y )
    {
        super.translate( x, y );
        for ( int i=0; i<lines.size(); i++ ) {
            ((Line)lines.elementAt( i )).translate( x, y );
        }
    }
}

/** 
  * 
  * $Log: Shade.java,v $
  * Revision 1.3  2003/10/13 19:46:37  moltmans
  * Removed bad line endings.
  *
  * Revision 1.2  2003/03/06 01:08:49  moltmans
  * Added copyright to all the files.
  *
  * Revision 1.1  2001/11/23 03:23:56  mtsezgin
  * *** empty log message ***
  *
  * Revision 1.1.1.1  2001/03/29 16:25:00  moltmans
  * Initial directories for DRG
  *
  * Revision 1.13  2000/09/06 22:40:54  mtsezgin
  * Combinations of curves and polygons are successfully approximated
  * by straight lines and Bezier curves as appropriate. System works
  * quite reliably.
  *
  * Revision 1.7  2000/06/08 03:14:31  mtsezgin
  *
  * Made the class Serializable for supporting saving and loading
  * designs. Both the object attributes, and the original data points
  * are stored and restored.
  *
  * Revision 1.6  2000/06/03 01:52:35  mtsezgin
  * *** empty log message ***
  *
  * Revision 1.5  2000/05/03 23:26:47  mtsezgin
  * *** empty log message ***
  *
  * Revision 1.4  2000/04/28 04:45:05  mtsezgin
  *
  * Now each GeometricObject keeps the mouse input that was previously
  * discarded. User can switch between seeing the recognized mode and
  * the raw mode. setDataPoints( Polygon points ) and getDataPoints()
  * are added to GeometricObject, and all the implementors are modified
  * accordingly.
  *
  * Revision 1.3  2000/04/25 22:28:23  mtsezgin
  *
  * Modified to comply with the new Rectangle class.
  *
  * Revision 1.2  2000/04/20 03:59:54  mtsezgin
  * *** empty log message ***
  *
  * Revision 1.1  2000/04/13 06:17:47  mtsezgin
  *
  * This class extends Rectangle for now, bu it should be something
  * of its own. A Shade represents intuitive shading in diagrams.
  *
  *  
  **/
