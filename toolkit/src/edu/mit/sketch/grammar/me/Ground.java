// (c) MIT 2003.  All rights reserved.

////////////////////////////////////////////////////////////////////////////////
//                                                                            //
// AUTHOR:      Tevfik Metin Sezgin                                           //
//              Massachusetts Institute of Technology                         //
//              Department of Electrical Engineering and Computer Science     //
//              Artificial Intelligence Laboratory                            //
//                                                                            //
// E-MAIL:        mtsezgin@ai.mit.edu, mtsezgin@mit.edu                       //
//                                                                            //
// COPYRIGHT:   Tevfik Metin Sezgin                                           //
//              All rights reserved. This code can not be copied, modified,   //
//              or distributed in whole or partially without the written      //
//              permission of the author. Also see the COPYRIGHT file.        //
//                                                                            //
////////////////////////////////////////////////////////////////////////////////
package edu.mit.sketch.grammar.me;


/**
  *
  * See the end of the file for the log of changes.
  *
  * $Author: moltmans $
  * $Date: 2003/10/13 19:46:37 $
  * $Revision: 1.3 $
  * $Headers$
  * $Id: Ground.java,v 1.3 2003/10/13 19:46:37 moltmans Exp $
  * $Name:  $
  * $Locker:  $
  * $Source: /projects/drg/CVSROOT/drg/code/src/edu/mit/sketch/grammar/me/Ground.java,v $
  *
  **/


import edu.mit.sketch.grammar.NonTerminal;
import edu.mit.sketch.geom.*;

import java.awt.Color;
import java.awt.Graphics;

import java.util.Vector;

import java.io.Serializable;

/**
  *
  * A basic Ground object with shading and edges.
  *
  **/
public 
class      Ground
implements Rotatable,
           NonTerminal,
           Serializable
{
    /**
    *
    * Original body of the shade.
    *
    **/
    private Shade shade;
    
    /**
    *
    * Original edge of the ground object.
    *
    **/
    private Polygon edge;
    
    /**
    *
    * Modified edge.
    *
    **/
    private Polygon new_edge;
    
    
    /**
    *
    * children
    *
    **/
    private Vector children;

    /**
    *
    * Graphics context for this Geometric object.
    *
    **/
    public transient Graphics graphics;
    
    
    /**
    *
    * The constructor.
    *
    **/    
  public Ground( Shade shade, Polygon edge )
    {
        this.shade     = shade;
        this.edge      = edge;
        this.new_edge  = new Polygon();
        
        Polygon polygonal_bounds = shade.getPolygonalBounds();
        
        int min_index = 0;
        int npoints   = polygonal_bounds.npoints;
        for ( int i = 0; i<edge.npoints-1; i++ ) {
            double min_area = Double.MAX_VALUE; // Some large number
            for ( int j = 0; j<polygonal_bounds.npoints-1; j++ ) {
                Polygon tmp = new Polygon();
                tmp.addPoint( polygonal_bounds.xpoints[j], 
                              polygonal_bounds.ypoints[j] );
                tmp.addPoint( polygonal_bounds.xpoints[j+1], 
                              polygonal_bounds.ypoints[j+1] );
                tmp.addPoint( edge.xpoints[i],
                              edge.ypoints[i] );
                tmp.addPoint( edge.xpoints[i+1],
                              edge.ypoints[i+1] );
                if ( tmp.getRectangularBounds().area() < min_area ) {
                    min_index = j;
                    min_area = tmp.getRectangularBounds().area();
                }
            }
            new_edge.addPoint( polygonal_bounds.xpoints[min_index], 
                               polygonal_bounds.ypoints[min_index] );
            new_edge.addPoint( polygonal_bounds.xpoints[min_index+1], 
                               polygonal_bounds.ypoints[min_index+1] );
        }
        
        children = new Vector();
        
        children.addElement( shade );
        children.addElement( edge  );
    }

    /**
    *
    * Returns the angle of the Ground WRT the x axis in radians.
    * 
    **/
    public double
    getAngle()
    {
        return shade.getAngle();
    }


    /**
    *
    * Draw the Ground
    *
    **/
    public void
    paint()
    {
        if ( graphics == null ) // Draw only if g
            return;             // is valid
        graphics.setColor( Color.black );
        paint( graphics );
    }


    /**
    *
    * Draw the Ground
    *
    **/
    public void
    paint( Graphics g )
    {
        shade.paint( graphics );
        new_edge.paint( graphics );
    }
    
    
    /**
    *
    * This method is used to paint the original data points that
    * forms this GeometricObject
    *
    **/
    public void
    paintOriginal( Graphics g )
    {
        shade.paintOriginal( g );
        edge.paintOriginal( g );
    }


       /**
    *
    * Set graphics context for this Ground object.
    * Must be set at least once before doing any drawing.
    *
    **/
    public void
    setGraphicsContext( Graphics g )
    {
        graphics = g;
    }


    /**
    *
    * Implement Paintable
    *
    **/    
    public String
    getType()
    {
        return "ground";
    }
    
    
    /**
    *
    * This method should return the Rectangular bounds of this 
    * non-terminal object.
    *
    **/
    public Rectangle
    getRectangularBounds()
    {
        return shade.getRectangularBounds();
    }
    
    
    /**
    *
    * This method should return the polygonal array bounds of this 
    * non-terminal object. This is a rough approximation of the
    * constituents of the object. 
    *
    **/
    public Polygon[]
    getPolygonalBoundsArray()
    {
        Polygon[] polygonal_bounds_array = new Polygon[1];
        
        polygonal_bounds_array[0] = shade.getPolygonalBounds();
        
        return polygonal_bounds_array;
    }
    
    
    /**
    *
    * This method should return a vector containing the children of 
    * this object. Children are the Terminal or NonTerminal 
    * constituents of this object.
    *
    **/
    public Vector
    getChildren()
    {
        return children;
    }


    /**
    *
    * Add the arguments to the (x, y) position of the object.
    *
    **/
    public void
    translate( double x, double y )
    {
        new_edge.translate( x, y );
        
        for ( int i=0; i<children.size(); i++ ) {
            ((GeometricObject)children.elementAt( i )).translate( x, y );
        }
    }


    /**
    *
    * Returns true if the point is within +-radius distance from
    * the curve defining the object. Returns false o/w.
    *
    **/
    public boolean
    pointIsOn( Point p, int radius )
    {
        return ( shade.pointIsOn( p, radius ) || 
                 new_edge.pointIsOn( p, radius ) );
    }

    
    /**
    *
    * Returns true if the point is within +-radius distance from
    * the original curve defining the object. Returns false o/w.
    *
    **/
    public boolean
    pointIsOnOriginal( Point p, int radius )
    {
        return ( shade.pointIsOnOriginal( p, radius ) || 
                 edge.pointIsOnOriginal( p, radius ) );        
    }
}


/** 
  * 
  * $Log: Ground.java,v $
  * Revision 1.3  2003/10/13 19:46:37  moltmans
  * Removed bad line endings.
  *
  * Revision 1.2  2003/03/06 01:08:49  moltmans
  * Added copyright to all the files.
  *
  * Revision 1.1  2001/11/23 03:23:56  mtsezgin
  * *** empty log message ***
  *
  * Revision 1.1.1.1  2001/03/29 16:25:00  moltmans
  * Initial directories for DRG
  *
  * Revision 1.9  2000/09/06 22:40:38  mtsezgin
  * Combinations of curves and polygons are successfully approximated
  * by straight lines and Bezier curves as appropriate. System works
  * quite reliably.
  *
  * Revision 1.3  2000/06/08 03:14:30  mtsezgin
  *
  * Made the class Serializable for supporting saving and loading
  * designs. Both the object attributes, and the original data points
  * are stored and restored.
  *
  * Revision 1.2  2000/06/03 01:52:33  mtsezgin
  * *** empty log message ***
  *
  * Revision 1.1  2000/05/26 20:49:46  mtsezgin
  *
  * This class represents the ground object found in mechanical
  * diagrams. (Unfortunately added to the system late).
  *
  * Revision 1.2  2000/04/20 03:59:52  mtsezgin
  * *** empty log message ***
  *
  * Revision 1.1  2000/04/13 06:16:02  mtsezgin
  *
  * Just started, and for now it filters out Shade objects using the timing data.
  *
  *  
  **/
  
