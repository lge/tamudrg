// (c) MIT 2003.  All rights reserved.

////////////////////////////////////////////////////////////////////////////////
//                                                                            //
// AUTHOR:      Tevfik Metin Sezgin                                           //
//              Massachusetts Institute of Technology                         //
//              Department of Electrical Engineering and Computer Science     //
//              Artificial Intelligence Laboratory                            //
//                                                                            //
// E-MAIL:        mtsezgin@ai.mit.edu, mtsezgin@mit.edu                       //
//                                                                            //
// COPYRIGHT:   Tevfik Metin Sezgin                                           //
//              All rights reserved. This code can not be copied, modified,   //
//              or distributed in whole or partially without the written      //
//              permission of the author. Also see the COPYRIGHT file.        //
//                                                                            //
////////////////////////////////////////////////////////////////////////////////
package edu.mit.sketch.grammar.me;


/**
  *
  * See the end of the file for the log of changes.
  *
  * $Author: moltmans $
  * $Date: 2003/10/13 19:46:37 $
  * $Revision: 1.3 $
  * $Headers$
  * $Id: MechanicalDesignFilenameFilter.java,v 1.3 2003/10/13 19:46:37 moltmans Exp $
  * $Name:  $
  * $Locker:  $
  * $Source: /projects/drg/CVSROOT/drg/code/src/edu/mit/sketch/grammar/me/MechanicalDesignFilenameFilter.java,v $
  *
  **/

import java.io.File;
import java.io.FilenameFilter;

/**
  *
  * This class does the filtering of mechanical design files.
  *
  **/

public
class      MechanicalDesignFilenameFilter
implements FilenameFilter
{
    /**
    *
    * File signature for designs.
    *
    **/
    public static final String MECHANICAL_DESIGN_FILE_SIGNATURE = ".tms";
    
    
    /**
    *
    * File signature for designs.
    *
    **/
    public boolean
    accept( File dir, String name )
    {
        if ( name == null )  // JIC
            return false;
            
        return name.endsWith( MECHANICAL_DESIGN_FILE_SIGNATURE );
    }
}


/** 
  * 
  * $Log: MechanicalDesignFilenameFilter.java,v $
  * Revision 1.3  2003/10/13 19:46:37  moltmans
  * Removed bad line endings.
  *
  * Revision 1.2  2003/03/06 01:08:49  moltmans
  * Added copyright to all the files.
  *
  * Revision 1.1  2001/11/23 03:23:56  mtsezgin
  * *** empty log message ***
  *
  * Revision 1.1.1.1  2001/03/29 16:25:01  moltmans
  * Initial directories for DRG
  *
  * Revision 1.7  2000/09/06 22:40:42  mtsezgin
  * Combinations of curves and polygons are successfully approximated
  * by straight lines and Bezier curves as appropriate. System works
  * quite reliably.
  *
  * Revision 1.1  2000/06/09 21:25:42  mtsezgin
  *
  * Filter for filtering Mechanical design files.
  *
  *
  **/
