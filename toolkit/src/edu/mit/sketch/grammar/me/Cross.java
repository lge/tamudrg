// (c) MIT 2003.  All rights reserved.

////////////////////////////////////////////////////////////////////////////////
//                                                                            //
// AUTHOR:      Tevfik Metin Sezgin                                           //
//              Massachusetts Institute of Technology                         //
//              Department of Electrical Engineering and Computer Science     //
//              Artificial Intelligence Laboratory                            //
//                                                                            //
// E-MAIL:        mtsezgin@ai.mit.edu, mtsezgin@mit.edu                       //
//                                                                            //
// COPYRIGHT:   Tevfik Metin Sezgin                                           //
//              All rights reserved. This code can not be copied, modified,   //
//              or distributed in whole or partially without the written      //
//              permission of the author. Also see the COPYRIGHT file.        //
//                                                                            //
////////////////////////////////////////////////////////////////////////////////
package edu.mit.sketch.grammar.me;


/**
  *
  * See the end of the file for the log of changes.
  *
  * $Author: moltmans $
  * $Date: 2003/10/13 19:46:37 $
  * $Revision: 1.3 $
  * $Headers$
  * $Id: Cross.java,v 1.3 2003/10/13 19:46:37 moltmans Exp $
  * $Name:  $
  * $Locker:  $
  * $Source: /projects/drg/CVSROOT/drg/code/src/edu/mit/sketch/grammar/me/Cross.java,v $
  *
  **/


import edu.mit.sketch.grammar.NonTerminal;
import edu.mit.sketch.geom.*;

import java.awt.Color;
import java.awt.Graphics;

import java.util.Vector;

import java.io.Serializable;


/**
  *
  * A basic Cross with a two perpendicular lines.
  *
  **/
public 
class      Cross
implements Rotatable,
           NonTerminal,
           Serializable
{
    /**
    *
    * Original l2.
    *
    **/
    private Line original_l2;
    
    /**
    *
    * First line of the cross.
    *
    **/
    private Line l1;
    
    /**
    *
    * Modified l2 to be perpendicular to l1.
    *
    **/
    private Line l2;

    /**
    *
    * children
    *
    **/
    private Vector children;

    /**
    *
    * Graphics context for this Geometric object.
    *
    **/
    public transient Graphics graphics;
    
    
    /**
    *
    * The constructor.
    *
    **/    
  public Cross( Line l1, Line l2 )
    {
        this.l1          = l1;
        this.l2          = new Line();
        this.original_l2 = l2;

        double center_x = (l1.x1 + l1.x2)/2;
        double center_y = (l1.y1 + l1.y2)/2;
        double dx       = (l1.x1 - l1.x2)/2;
        double dy       = (l1.y1 - l1.y2)/2;
        
        this.l2.x1 = center_x + dy;
        this.l2.y1 = center_y - dx;
        this.l2.x2 = center_x - dy;
        this.l2.y2 = center_y + dx;

        children = new Vector();
        
        children.addElement( l1 );
        children.addElement( l2 );
    }

    /**
    *
    * Returns the angle of the motor WRT the x axis in radians.
    * This should be made more general by adding an angle field 
    * to the class.Take the angle on the positive x side. Angle 
    * is always between 0 and PI/2;
    *
    **/
    public double
    getAngle()
    {
        double dy = Math.abs((l1.y1 - l1.y2)/2);
        if ( dy == 0.0 )
            return 0.0;
            
        // Take the angle on the positive x side.
        // Angle is always between 0 and PI/2;
        return Math.atan2( dy, 1 );
    }


    /**
    *
    * Draw the Cross.
    *
    **/
    public void
    paint()
    {
        if ( graphics == null ) // Draw only if g
            return;             // is valid
        graphics.setColor( Color.black );
        paint( graphics );
    }


    /**
    *
    * Draw the Cross
    *
    **/
    public void
    paint( Graphics g )
    {
        l1.paint( g );
        l2.paint( g );
    }


       /**
    *
    * Set graphics context for this Cross object.
    * Must be set at least once before doing any drawing.
    *
    **/
    public void
    setGraphicsContext( Graphics g )
    {
        graphics = g;
    }


    /**
    *
    * Implement Paintable 
    *
    **/    
    public String
    getType()
    {
        return "cross";
    }
    
    
    /**
    *
    * This method is used to paint the original data points that
    * forms this GeometricObject
    *
    **/
    public void
    paintOriginal( Graphics g )
    {
        l1.paintOriginal( g );
        original_l2.paintOriginal( g );
    }
    
    
    /**
    *
    * This method should return the Rectangular bounds of this 
    * non-terminal object.
    *
    **/
    public Rectangle
    getRectangularBounds()
    {
        return (l1.getRectangularBounds()).union( l1.getRectangularBounds() );
    }
    
    
    /**
    *
    * This method should return the polygonal array bounds of this 
    * non-terminal object. This is a rough approximation of the
    * constituents of the object. 
    *
    **/
    public Polygon[]
    getPolygonalBoundsArray()
    {
        Polygon[] polygonal_bounds_array = new Polygon[2];
        
        polygonal_bounds_array[0] = l1.getPolygonalBounds();
        polygonal_bounds_array[1] = l2.getPolygonalBounds();
        
        return polygonal_bounds_array;
    }
    
    
    /**
    *
    * This method should return a vector containing the children of 
    * this object. Children are the Terminal or NonTerminal 
    * constituents of this object.
    *
    **/
    public Vector
    getChildren()
    {
        return children;
    }


    /**
    *
    * Add the arguments to the (x, y) position of the object.
    *
    **/
    public void
    translate( double x, double y )
    {
        l2.translate( x, y );
        
        for ( int i=0; i<children.size(); i++ ) {
            ((GeometricObject)children.elementAt( i )).translate( x, y );
        }
    }


    /**
    *
    * Returns true if the point is within +-radius distance from
    * the curve defining the object. Returns false o/w.
    *
    **/
    public boolean
    pointIsOn( Point p, int radius )
    {
        return ( l1.pointIsOn( p, radius ) || l2.pointIsOn( p, radius ) );
    }

    
    /**
    *
    * Returns true if the point is within +-radius distance from
    * the original curve defining the object. Returns false o/w.
    *
    **/
    public boolean
    pointIsOnOriginal( Point p, int radius )
    {
        return ( l1.pointIsOnOriginal( p, radius ) || 
                 original_l2.pointIsOnOriginal( p, radius ) );        
    }
}


/** 
  * 
  * $Log: Cross.java,v $
  * Revision 1.3  2003/10/13 19:46:37  moltmans
  * Removed bad line endings.
  *
  * Revision 1.2  2003/03/06 01:08:49  moltmans
  * Added copyright to all the files.
  *
  * Revision 1.1  2001/11/23 03:23:56  mtsezgin
  * *** empty log message ***
  *
  * Revision 1.1.1.1  2001/03/29 16:25:00  moltmans
  * Initial directories for DRG
  *
  * Revision 1.12  2000/09/06 22:40:33  mtsezgin
  * Combinations of curves and polygons are successfully approximated
  * by straight lines and Bezier curves as appropriate. System works
  * quite reliably.
  *
  *
  **/
