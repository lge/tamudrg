// (c) MIT 2003.  All rights reserved.

////////////////////////////////////////////////////////////////////////////////
//                                                                            //
// AUTHOR:      Tevfik Metin Sezgin                                           //
//              Massachusetts Institute of Technology                         //
//              Department of Electrical Engineering and Computer Science     //
//              Artificial Intelligence Laboratory                            //
//                                                                            //
// E-MAIL:        mtsezgin@ai.mit.edu, mtsezgin@mit.edu                       //
//                                                                            //
// COPYRIGHT:   Tevfik Metin Sezgin                                           //
//              All rights reserved. This code can not be copied, modified,   //
//              or distributed in whole or partially without the written      //
//              permission of the author. Also see the COPYRIGHT file.        //
//                                                                            //
////////////////////////////////////////////////////////////////////////////////
package edu.mit.sketch.grammar.me;


/**
  *
  * See the end of the file for the log of changes.
  *
  * $Author: moltmans $
  * $Date: 2003/10/13 19:46:37 $
  * $Revision: 1.3 $
  * $Headers$
  * $Id: Motor.java,v 1.3 2003/10/13 19:46:37 moltmans Exp $
  * $Name:  $
  * $Locker:  $
  * $Source: /projects/drg/CVSROOT/drg/code/src/edu/mit/sketch/grammar/me/Motor.java,v $
  *
  **/


import edu.mit.sketch.grammar.NonTerminal;
import edu.mit.sketch.geom.*;

import java.awt.Color;
import java.awt.Graphics;

import java.util.Vector;

import java.io.Serializable;


/**
  *
  * A basic motor with a body and a rotor
  *
  **/
public 
class      Motor
implements Rotatable,
           NonTerminal,
           Serializable
{
    /**
    *
    * Original body of the motor.
    *
    **/
    private Rectangle body;
    
    /**
    *
    * Original rotor of the motor.
    *
    **/
    private Line rotor;
    
    /**
    *
    * Modified rotor of the motor to be parallel to major axis.
    *
    **/
    private Line new_rotor;
    
    /**
    *
    * Rotor length of the rotor.
    *
    **/
    private int rotor_length;
    
    /**
    *
    * Angle that the line from the center of the body to the tip 
    * rotor makes with the x axis.
    *
    **/
    private double relative_rotor_angle;
    
    /**
    *
    * The position of the tip relative to the center of the body.
    *
    **/
    private Point tip_position;
    
    /**
    *
    * children
    *
    **/
    private Vector children;

    /**
    *
    * Graphics context for this Geometric object.
    *
    **/
    public transient Graphics graphics;
    
    
    /**
    *
    * The constructor.
    *
    **/    
  public Motor( Rectangle body, Line rotor )
    {
        this.body    = body;
        this.rotor   = rotor;
        rotor_length = (int)Point.distance( rotor.x1, rotor.y1, 
                                            rotor.x2, rotor.y2 );
        
        Point center = body.getCenter();
        
        if ( Point.distance( center.x, center.y, rotor.x1, rotor.y1 ) > 
             Point.distance( center.x, center.y, rotor.x2, rotor.y2 ) ) {
            tip_position = new 
                Point( ((int)rotor.x1), ((int)rotor.y1) );
        } else {
            tip_position = new         
                Point( ((int)rotor.x2), ((int)rotor.y2) );
        }
        
        relative_rotor_angle  = Math.atan2( tip_position.y, tip_position.y );
        
        int    major_axis_length = (int)body.getMajorAxisLength();
        double major_axis_angle  = body.getMajorAxisAngle();
        Point  new_rotor_tip1    = new 
            Point( (int)(center.x + Math.cos( major_axis_angle )*
                   (major_axis_length/2 + rotor_length   )), 
                   (int)(center.y + Math.sin( major_axis_angle )*
                   (major_axis_length/2 + rotor_length   )) );
        Point  new_rotor_tip2    = new 
            Point( (int)(center.x + Math.cos( major_axis_angle-Math.PI )*
                   (major_axis_length/2 + rotor_length   )), 
                   (int)(center.y + Math.sin( major_axis_angle-Math.PI )*
                   (major_axis_length/2 + rotor_length   )) );
        if ( tip_position.distance( new_rotor_tip1 ) <
            tip_position.distance( new_rotor_tip2 ) ) {
            tip_position = new_rotor_tip1; 
            new_rotor    = new 
                Line( (int)(center.x + Math.cos( major_axis_angle )*
                            major_axis_length/2),
                      (int)(center.y + Math.sin( major_axis_angle )*
                            major_axis_length/2),
                      tip_position.x,
                      tip_position.y );
        } else {
            tip_position = new_rotor_tip2; 
            new_rotor    = new 
                Line( (int)(center.x + Math.cos( major_axis_angle-Math.PI )*
                            major_axis_length/2),
                      (int)(center.y + Math.sin( major_axis_angle-Math.PI )*
                            major_axis_length/2),
                      tip_position.x,
                      tip_position.y );
        }
        
        children = new Vector();
        
        children.addElement( body  );
        children.addElement( rotor );
    }

    /**
    *
    * Returns the angle of the motor WRT the x axis in radians.
    * This should be made more general by adding an angle field 
    * to the class.
    * 
    **/
    public double
    getAngle()
    {
        return relative_rotor_angle;
    }


    /**
    *
    * Draw the Motor
    *
    **/
    public void
    paint()
    {
        if ( graphics == null ) // Draw only if g
            return;             // is valid
        graphics.setColor( Color.black );
        body.paint( graphics );
        new_rotor.paint( graphics );
    }


    /**
    *
    * Draw the Motor
    *
    **/
    public void
    paint( Graphics g )
    {
        body.paint( g );
        new_rotor.paint( g );
    }
    
    
    /**
    *
    * This method is used to paint the original data points that
    * forms this GeometricObject
    *
    **/
    public void
    paintOriginal( Graphics g )
    {
        body.paintOriginal( g );
        rotor.paintOriginal( g );
    }


       /**
    *
    * Set graphics context for this Motor object.
    * Must be set at least once before doing any drawing.
    *
    **/
    public void
    setGraphicsContext( Graphics g )
    {
        graphics = g;
    }


    /**
    *
    * Implement Paintable
    *
    **/    
    public String
    getType()
    {
        return "motor";
    }
    
    
    /**
    *
    * This method should return the Rectangular bounds of this 
    * non-terminal object.
    *
    **/
    public Rectangle
    getRectangularBounds()
    {
        return body.union( rotor.getRectangularBounds() );
    }
    
    
    /**
    *
    * This method should return the polygonal array bounds of this 
    * non-terminal object. This is a rough approximation of the
    * constituents of the object. 
    *
    **/
    public Polygon[]
    getPolygonalBoundsArray()
    {
        Polygon[] polygonal_bounds_array = new Polygon[2];
        
        polygonal_bounds_array[0] = body.getPolygonalBounds();
        polygonal_bounds_array[1] = rotor.getPolygonalBounds();
        
        return polygonal_bounds_array;
    }
    
    
    /**
    *
    * This method should return a vector containing the children of 
    * this object. Children are the Terminal or NonTerminal 
    * constituents of this object.
    *
    **/
    public Vector
    getChildren()
    {
        return children;
    }
    
    
    /**
    *
    * Add the arguments to the (x, y) position of the object.
    *
    **/
    public void
    translate( double x, double y )
    {
        new_rotor.translate( x, y );
        
        for ( int i=0; i<children.size(); i++ ) {
            ((GeometricObject)children.elementAt( i )).translate( x, y );
        }
    }


    /**
    *
    * Returns true if the point is within +-radius distance from
    * the curve defining the object. Returns false o/w.
    *
    **/
    public boolean
    pointIsOn( Point p, int radius )
    {
        return ( body.pointIsOn( p, radius ) || 
                 new_rotor.pointIsOn( p, radius ) );
    }

    
    /**
    *
    * Returns true if the point is within +-radius distance from
    * the original curve defining the object. Returns false o/w.
    *
    **/
    public boolean
    pointIsOnOriginal( Point p, int radius )
    {
        return ( body.pointIsOnOriginal( p, radius ) || 
                 rotor.pointIsOnOriginal( p, radius ) );        
    }
}


/** 
  * 
  * $Log: Motor.java,v $
  * Revision 1.3  2003/10/13 19:46:37  moltmans
  * Removed bad line endings.
  *
  * Revision 1.2  2003/03/06 01:08:49  moltmans
  * Added copyright to all the files.
  *
  * Revision 1.1  2001/11/23 03:23:56  mtsezgin
  * *** empty log message ***
  *
  * Revision 1.1.1.1  2001/03/29 16:25:00  moltmans
  * Initial directories for DRG
  *
  * Revision 1.16  2000/09/06 22:40:42  mtsezgin
  * Combinations of curves and polygons are successfully approximated
  * by straight lines and Bezier curves as appropriate. System works
  * quite reliably.
  *
  *
  **/
