// (c) MIT 2003.  All rights reserved.

////////////////////////////////////////////////////////////////////////////////
//                                                                            //
// AUTHOR:      Tevfik Metin Sezgin                                           //
//              Massachusetts Institute of Technology                         //
//              Department of Electrical Engineering and Computer Science     //
//              Artificial Intelligence Laboratory                            //
//                                                                            //
// E-MAIL:        mtsezgin@ai.mit.edu, mtsezgin@mit.edu                       //
//                                                                            //
// COPYRIGHT:   Tevfik Metin Sezgin                                           //
//              All rights reserved. This code can not be copied, modified,   //
//              or distributed in whole or partially without the written      //
//              permission of the author. Also see the COPYRIGHT file.        //
//                                                                            //
////////////////////////////////////////////////////////////////////////////////
package edu.mit.sketch.grammar.me;

/**
  *
  * See the end of the file for the log of changes.
  *
  * $Author: moltmans $
  * $Date: 2003/10/13 19:46:37 $
  * $Revision: 1.3 $
  * $Headers$
  * $Id: Pin.java,v 1.3 2003/10/13 19:46:37 moltmans Exp $
  * $Name:  $
  * $Locker:  $
  * $Source: /projects/drg/CVSROOT/drg/code/src/edu/mit/sketch/grammar/me/Pin.java,v $
  *
  **/


import edu.mit.sketch.grammar.NonTerminal;
import edu.mit.sketch.grammar.me.Cross;
import edu.mit.sketch.geom.*;

import java.awt.Color;
import java.awt.Graphics;

import java.util.Vector;

import java.io.Serializable;


/**
  *
  * A basic Pin with a round top and a cross.
  *
  **/
public 
class      Pin
implements NonTerminal,
           Serializable
{
    /**
    *
    * Original cross.
    *
    **/
    private Cross original_cross;
    
    /**
    *
    * Original circle
    *
    **/
    private Ellipse original_circle;
    
    /**
    *
    * Modified cross.
    *
    **/
    private Cross cross;
    
    /**
    *
    * Modified circle.
    *
    **/
    private Ellipse circle;

    /**
    *
    * children
    *
    **/
    private Vector children;

    /**
    *
    * Graphics context for this Geometric object.
    *
    **/
    public transient Graphics graphics;
    
    
    /**
    *
    * The constructor.
    *
    **/    
  public Pin( Ellipse circle, Cross cross )
    {
        int r = (int)(circle.width+circle.height)/2;
        this.original_circle = circle;
        this.original_cross  = cross;
        this.circle          = new Ellipse( circle.x, 
                                            circle.y,
                                            r,
                                            r );
        this.cross           = 
            new Cross(
                new Line( circle.x,
                          circle.y + r/2,
                          circle.x + r,
                          circle.y + r/2 ),
                new Line( circle.x + r/2, 
                          circle.y ,
                          circle.x + r/2,
                          circle.y + r ) );

        children = new Vector();
        
        children.addElement( original_circle );
        children.addElement( original_cross  );
    }


    /**
    *
    * Draw the Pin.
    *
    **/
    public void
    paint()
    {
        if ( graphics == null ) // Draw only if g
            return;             // is valid
        graphics.setColor( Color.black );
        paint( graphics );
    }


    /**
    *
    * Draw the Pin
    *
    **/
    public void
    paint( Graphics g )
    {
        cross.paint( g );
        circle.paint( g );
    }


       /**
    *
    * Set graphics context for this Pin object.
    * Must be set at least once before doing any drawing.
    *
    **/
    public void
    setGraphicsContext( Graphics g )
    {
        graphics = g;
    }


    /**
    *
    * Implement Paintable 
    *
    **/    
    public String
    getType()
    {
        return "pin";
    }
    
    
    /**
    *
    * This method is used to paint the original data points that
    * forms this GeometricObject
    *
    **/
    public void
    paintOriginal( Graphics g )
    {
        original_circle.paintOriginal( g );
        original_cross.paintOriginal( g );
    }
    
    
    /**
    *
    * This method should return the Rectangular bounds of this 
    * non-terminal object.
    *
    **/
    public Rectangle
    getRectangularBounds()
    {
        return (circle.getRectangularBounds()).union( 
                    cross.getRectangularBounds() );
    }
    
    
    /**
    *
    * This method should return the polygonal array bounds of this 
    * non-terminal object. This is a rough approximation of the
    * constituents of the object. 
    *
    **/
    public Polygon[]
    getPolygonalBoundsArray()
    {
        Polygon[] polygonal_bounds_array = new Polygon[3];
        
        polygonal_bounds_array[0] = circle.getPolygonalBounds();
        polygonal_bounds_array[1] = (cross.getPolygonalBoundsArray())[0];
        polygonal_bounds_array[2] = (cross.getPolygonalBoundsArray())[1];
        
        return polygonal_bounds_array;
    }
    
    
    /**
    *
    * This method should return a vector containing the children of 
    * this object. Children are the Terminal or NonTerminal 
    * constituents of this object.
    *
    **/
    public Vector
    getChildren()
    {
        return children;
    }
    
    
    /**
    *
    * Add the arguments to the (x, y) position of the object.
    *
    **/
    public void
    translate( double x, double y )
    {
        circle.translate( x, y );
        cross.translate( x, y );
        
        for ( int i=0; i<children.size(); i++ ) {
            ((GeometricObject)children.elementAt( i )).translate( x, y );
        }
    }


    /**
    *
    * Returns true if the point is within +-radius distance from
    * the curve defining the object. Returns false o/w.
    *
    **/
    public boolean
    pointIsOn( Point p, int radius )
    {
        return ( cross.pointIsOn( p, radius ) || 
                 circle.pointIsOn( p, radius ) );
    }

    
    /**
    *
    * Returns true if the point is within +-radius distance from
    * the original curve defining the object. Returns false o/w.
    *
    **/
    public boolean
    pointIsOnOriginal( Point p, int radius )
    {
        return ( original_cross.pointIsOnOriginal( p, radius ) || 
                 original_circle.pointIsOnOriginal( p, radius ) );        
    }
}


/** 
  * 
  * $Log: Pin.java,v $
  * Revision 1.3  2003/10/13 19:46:37  moltmans
  * Removed bad line endings.
  *
  * Revision 1.2  2003/03/06 01:08:49  moltmans
  * Added copyright to all the files.
  *
  * Revision 1.1  2001/11/23 03:23:56  mtsezgin
  * *** empty log message ***
  *
  * Revision 1.1.1.1  2001/03/29 16:25:00  moltmans
  * Initial directories for DRG
  *
  * Revision 1.9  2000/09/06 22:40:48  mtsezgin
  * Combinations of curves and polygons are successfully approximated
  * by straight lines and Bezier curves as appropriate. System works
  * quite reliably.
  *
  *
  **/
