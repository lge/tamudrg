// (c) MIT 2003.  All rights reserved.

////////////////////////////////////////////////////////////////////////////////
//                                                                            //
// AUTHOR:      Tevfik Metin Sezgin                                           //
//              Massachusetts Institute of Technology                         //
//              Department of Electrical Engineering and Computer Science     //
//              Artificial Intelligence Laboratory                            //
//                                                                            //
// E-MAIL:        mtsezgin@ai.mit.edu, mtsezgin@mit.edu                       //
//                                                                            //
// COPYRIGHT:   Tevfik Metin Sezgin                                           //
//              All rights reserved. This code can not be copied, modified,   //
//              or distributed in whole or partially without the written      //
//              permission of the author. Also see the COPYRIGHT file.        //
//                                                                            //
////////////////////////////////////////////////////////////////////////////////
package edu.mit.sketch.grammar;

/** 
  * 
  * See the end of the file for the log of changes.
  * 
  * $Author: moltmans $
  * $Date: 2003/03/06 01:08:49 $   
  * $Revision: 1.3 $
  * $Headers$
  * $Id: Terminal.java,v 1.3 2003/03/06 01:08:49 moltmans Exp $     
  * $Name:  $   
  * $Locker:  $
  * $Source: /projects/drg/CVSROOT/drg/code/src/edu/mit/sketch/grammar/Terminal.java,v $
  *  
  **/


import java.awt.Graphics; 
import edu.mit.sketch.ui.Paintable;

/**
  *
  * All the objects who want to be NonTerminal must implement this
  * interface.
  *
  **/

public interface Terminal
extends          Paintable
{
    /**
    *
    * Sets the time stamp of the current Terminal
    *
    **/
    public abstract void
    setTimeStamp( long time_stamp );
    
    /**
    *
    * Returns the time stamp of the current Terminal
    *
    **/
    public abstract long
    getTimeStamp();
}

/**
  *
  * $Log: Terminal.java,v $
  * Revision 1.3  2003/03/06 01:08:49  moltmans
  * Added copyright to all the files.
  *
  * Revision 1.2  2001/11/26 18:28:59  mtsezgin
  * *** empty log message ***
  *
  * Revision 1.1  2001/11/23 03:23:49  mtsezgin
  * *** empty log message ***
  *
  * Revision 1.1.1.1  2001/03/29 16:25:00  moltmans
  * Initial directories for DRG
  *
  * Revision 1.6  2000/09/06 22:41:07  mtsezgin
  * Combinations of curves and polygons are successfully approximated
  * by straight lines and Bezier curves as appropriate. System works
  * quite reliably.
  *
  * Revision 1.3  2000/06/03 01:52:36  mtsezgin
  * *** empty log message ***
  *
  * Revision 1.2  2000/04/13 06:18:30  mtsezgin
  *
  * Now Terminals have time_stamps and methods to support operations using
  * time_stamps.
  *
  * Revision 1.1  2000/04/11 00:41:49  mtsezgin
  *
  * Now the whole package succesfully parses a motor.
  *
  *
  **/
