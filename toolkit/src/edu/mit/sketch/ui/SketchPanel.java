// (c) MIT 2003.  All rights reserved.

package edu.mit.sketch.ui;

import edu.mit.sketch.geom.Point;
import edu.mit.sketch.toolkit.StrokeData;
import edu.mit.sketch.toolkit.StrokeDataListener;
import edu.mit.sketch.toolkit.StrokeDataReader;
import edu.mit.sketch.toolkit.Timer;
import java.awt.Graphics;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;
import java.util.Collections;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import javax.swing.JPanel;

/**
 * The panel that gathers data and displays the raw strokes as they
 * are being drawn.
 *
 * <p>
 * Created: Thu Mar 22 19:57:14 2001<br>
 * Copyright: Copyright (C) 2001 by MIT.  All rights reserved.<br>
 *
 * @author <a href="mailto:moltmans@ai.mit.edu">"Michael Oltmans</a>
 * @version $Id: SketchPanel.java,v 1.6 2003/06/16 17:14:37 mtsezgin Exp $
 **/
public class SketchPanel extends JPanel
{
  protected boolean m_platformIsWindows9x;
  protected List m_dataListeners;
  protected StrokeDataReader m_sdr;
  protected Point m_previousPoint;

  protected boolean m_isRecognizing = true;
  protected boolean m_paintAsWeGo = true;
  
  public SketchPanel () {
    super();
    m_platformIsWindows9x = 
      //      System.getProperty("os.name").equals("Windows NT") ||
      //      System.getProperty("os.name").equals("Windows 98") ||
      System.getProperty("os.name").equals("Windows 95");
      
//        ( System.getProperty("os.name").indexOf("95") >= 0 ||
//      System.getProperty("os.name").indexOf("98") >= 0 );
    
    m_sdr = new StrokeDataReader();
    m_dataListeners = new LinkedList();

    addMouseListeners();
  }

  protected void addMouseListeners()
  {
    addMouseMotionListener( new MouseMotionListener() {
    public void mouseMoved(MouseEvent e) {}
    public void mouseDragged(MouseEvent e) {
      if ( !m_isRecognizing ) {
        return;
      }
      Point point = new Point( e.getPoint() );
      if ( isPaintingAsWeGo() ) {
        Graphics g = getGraphics();
        g.setColor( getForeground() );
        g.drawLine( point.x,
            point.y,
            m_previousPoint.x,
            m_previousPoint.y );
      }
      m_previousPoint = point;
      addPoint( point, getTimeStamp( e ) );
    }
      } );

    addMouseListener( new MouseAdapter() {
    public void mousePressed(MouseEvent e) {
      reset();
      //        System.out.println( "\n\nStarting new session.." );
      if ( m_isRecognizing ) {
        m_previousPoint = new Point( e.getPoint() );
        addPoint( new Point( e.getPoint() ), getTimeStamp( e ) );
      }
    }
  
    public void mouseReleased(MouseEvent e) {
      if ( !m_isRecognizing || !m_sdr.hasSufficientData() ) {
        return;
      }

      StrokeData data = m_sdr.getStrokeData();
      fireStrokeDataEvent( data );
    }
      } );
  }
  

  /**
   *
   * Disable or enable recognition process by turning mouse event 
   * listening off or on. Set flag to be true if enabling is desired.
   * Set flag to be false if disabling is desired.
   *
   **/
  public void setIsRecognizing( boolean flag )
  {
    m_isRecognizing = flag;
  }

  public boolean isRecognizing()
  {
    return m_isRecognizing;
  }

  /**
   * This determines if we are responsible for laying down the ink
   * while it is being drawn.
   *
   * @return value of paintAsWeGo.
   */
  public boolean isPaintingAsWeGo() {
    return m_paintAsWeGo;
  }
  
  /**
   * This determines if we are responsible for laying down the ink
   * while it is being drawn.
   * 
   * @param v Value to assign to paintAsWeGo.
   */
  public void setIsPaintingAsWeGo(boolean v) {
    m_paintAsWeGo = v;
  }
  
  /**
   *
   * Reset the arrays
   *
   **/
  protected void reset() 
  {
    m_sdr.reset();
  }

  protected void fireStrokeDataEvent( StrokeData data ) 
  {
    Iterator it = m_dataListeners.iterator();
    while( it.hasNext() ) {
      StrokeDataListener listener = (StrokeDataListener)it.next();
      listener.handleStroke( data );
    }
  }

  public void addStrokeDataListener( StrokeDataListener listener ) 
  {
    m_dataListeners.add( listener );
  }

  public void removeStrokeDataListener( StrokeDataListener listener )
  {
    m_dataListeners.remove( listener );
  }

  public List getStrokeDataListeners() 
  {
    return Collections.unmodifiableList( m_dataListeners );
  }
  

  /**
   *
   * Add a new point to m_sdr with t as the time stamp.
   *
   **/
  protected void addPoint( Point p, long t ) 
  {
    Point point = new Point( p );
    point.setTimeStamp( t );
    m_sdr.addPoint( point );
  }

  /**
   *
   * This method is needed only for supporting the rather 
   * silly windows9X platform, because it is still using the
   * dos clock.
   *
   **/
  protected long getTimeStamp( MouseEvent e )    
  {
    if ( m_platformIsWindows9x ) {
      return Timer.tics();
    }
    else {
      return e.getWhen();
    }
  }
}// SketchPanel

