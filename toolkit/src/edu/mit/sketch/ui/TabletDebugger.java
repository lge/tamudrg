// (c) MIT 2003.  All rights reserved.

////////////////////////////////////////////////////////////////////////////////
//                                                                            //
// AUTHOR:      Tevfik Metin Sezgin                                           //
//              Massachusetts Institute of Technology                         //
//              Department of Electrical Engineering and Computer Science     //
//              Artificial Intelligence Laboratory                            //
//                                                                            //
// E-MAIL:        mtsezgin@ai.mit.edu, mtsezgin@mit.edu                       //
//                                                                            //
// COPYRIGHT:   Tevfik Metin Sezgin                                           //
//              All rights reserved. This code can not be copied, modified,   //
//              or distributed in whole or partially without the written      //
//              permission of the author. Also see the COPYRIGHT file.        //
//                                                                            //
////////////////////////////////////////////////////////////////////////////////
package edu.mit.sketch.ui;


  /**
  *
  * See the end of the file for the log of changes.
  *
  * $Author: moltmans $
  * $Date: 2003/11/05 01:42:02 $
  * $Revision: 1.3 $
  * $Headers$
  * $Id: TabletDebugger.java,v 1.3 2003/11/05 01:42:02 moltmans Exp $
  * $Name:  $
  * $Locker:  $
  * $Source: /projects/drg/CVSROOT/drg/code/src/edu/mit/sketch/ui/TabletDebugger.java,v $
  *
  **/

import java.awt.Color;
import java.awt.Frame;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;
import edu.mit.sketch.geom.Vertex;
import edu.mit.sketch.ui.Tablet;

/**
  * Acronyms: sdr -> StrokeDataReader, sd -> StrokeData
  *
  * This class listens to events happening in the Tablet and 
  * supplies debugging info as needed.
  *
  **/

public
class      TabletDebugger
implements KeyListener,
           MouseMotionListener,
           ActionListener
{
    /**
    *
    * The Tablet
    *
    **/
    Tablet tablet;
    

    /**
    *
    * The constructor.
    *
    **/
    public TabletDebugger( Tablet tablet )
    {
        this.tablet = tablet;
        
        tablet.addKeyListener( this );
        tablet.addMouseMotionListener( this );
    }

     
    /**
    *
    * Handle action events
    *
    **/
    public void
    actionPerformed( ActionEvent e ) 
    {
        String command = e.getActionCommand();
    }
            

    /**
    *
    * Handle mouse events
    *
    **/
    public void
    mouseDragged( MouseEvent e )
    {
    }


    /**
    *
    * Handle mouse events
    *
    **/
    public void
    mouseMoved( MouseEvent e )
    {
        
        if ( ( tablet.data                  == null  ) || 
             ( tablet.data.vertices         == null  ) ||
             ( tablet.vertex_identification == false ) )
            return;
        
        Vertex vertices[] = tablet.data.vertices;
        
        int    min_index    = 0;
        double distance     = 0.0;
        double min_distance = Double.MAX_VALUE;
        for ( int i=0; i<vertices.length; i++ ) {
            distance = vertices[i].distance( e.getPoint() );
            if ( distance < min_distance ) {
                min_index    = i;
                min_distance = distance;
            }
        }
        
        Vertex closest_vertex = vertices[min_index];
        
        if ( Math.random() < 0.5 ) {
            tablet.debug_graphics.setColor( Color.blue );
        } else {
            tablet.debug_graphics.setColor( Color.red );
        }
        tablet.debug_graphics.drawOval( closest_vertex.x - 1,
                                        closest_vertex.y - 1,
                                        2,
                                        2 );
        tablet.setInfoMessage( closest_vertex.index + " " + closest_vertex );
    }


    /**
    *
    * Handle key stroke events
    *
    **/
    public void
    keyPressed( KeyEvent k )
    {
    }
     

    /**
    *
    * Handle key stroke events
    *
    **/
    public void
    keyReleased( KeyEvent k )
    {    
    }


    /**
    *
    * Handle key stroke events
    *
    **/
    public void
    keyTyped( KeyEvent ke )
    {
        char key = ke.getKeyChar();
        
        if ( key == 'm' ) {
            tablet.addMouseMotionListener( this );
            return;
        }
        if ( key == 'n' ) {
            tablet.removeMouseMotionListener( this );
            return;
        }
    }
}

/**
  *
  * $Log: TabletDebugger.java,v $
  * Revision 1.3  2003/11/05 01:42:02  moltmans
  * Found more ^M's  They should all be gone now... Again...  For good?
  *
  * Revision 1.2  2003/03/06 01:08:52  moltmans
  * Added copyright to all the files.
  *
  * Revision 1.1  2001/11/23 03:24:13  mtsezgin
  * *** empty log message ***
  *
  * Revision 1.2  2001/10/12 22:25:53  mtsezgin
  * This is a commit of all files.
  * Shoapid
  * vi sux:q
  *
  * Revision 1.1.1.1  2001/03/29 16:25:01  moltmans
  * Initial directories for DRG
  *
  * Revision 1.25  2000/09/06 22:40:59  mtsezgin
  * Combinations of curves and polygons are successfully approximated
  * by straight lines and Bezier curves as appropriate. System works
  * quite reliably.
  *
  *
  *
  **/
