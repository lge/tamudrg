// (c) MIT 2003.  All rights reserved.

////////////////////////////////////////////////////////////////////////////////
//                                                                            //
// AUTHOR:      Tevfik Metin Sezgin                                           //
//              Massachusetts Institute of Technology                         //
//              Department of Electrical Engineering and Computer Science     //
//              Artificial Intelligence Laboratory                            //
//                                                                            //
// E-MAIL:        mtsezgin@ai.mit.edu, mtsezgin@mit.edu                       //
//                                                                            //
// COPYRIGHT:   Tevfik Metin Sezgin                                           //
//              All rights reserved. This code can not be copied, modified,   //
//              or distributed in whole or partially without the written      //
//              permission of the author. Also see the COPYRIGHT file.        //
//                                                                            //
////////////////////////////////////////////////////////////////////////////////
package edu.mit.sketch.ui;

/**
  *
  * See the end of the file for the log of changes.
  *
  * $Author: moltmans $
  * $Date: 2003/11/05 01:42:02 $
  * $Revision: 1.3 $
  * $Headers$
  * $Id: JointDisplay.java,v 1.3 2003/11/05 01:42:02 moltmans Exp $
  * $Name:  $
  * $Locker:  $
  * $Source: /projects/drg/CVSROOT/drg/code/src/edu/mit/sketch/ui/JointDisplay.java,v $
  *
  **/


import edu.mit.sketch.util.AWTUtil;
import edu.mit.sketch.util.GraphicsUtil;

import java.awt.Color;
import java.awt.Frame;
import java.awt.Graphics;
import java.awt.GridLayout;
import java.awt.Insets;
import java.awt.Panel;
import java.awt.Scrollbar;

import java.awt.geom.AffineTransform;

/**
  *
  * This class is used for viewing scale space data.
  *
  **/

public
class      JointDisplay
extends    Frame
{
    private Panel length_vs_dd_dt_panel;
    private Panel length_vs_speed_panel;
    
    /**
    *
    * The constructor.
    *
    **/
    public JointDisplay()
    {
        super( "Joint Display" );
        
        length_vs_dd_dt_panel = new Panel();
        length_vs_speed_panel = new Panel();
        
        setLayout( new GridLayout( 0, 1 ) );
        
        add( length_vs_dd_dt_panel );
        add( length_vs_speed_panel );

        length_vs_dd_dt_panel.setBackground( new Color( 0xffddff ) );
        length_vs_speed_panel.setBackground( new Color( 0xddffdd ) );
        
        validate();
    }


    /**
    *
    * updata the panel
    *
    **/
    public void
    paintSpeed( double speed[], double accumulated_length[] )
    {
        if ( !isVisible() ) {
            return;
        }
        
        Graphics g = length_vs_speed_panel.getGraphics();

        GraphicsUtil.clearComponent( length_vs_speed_panel );

        AffineTransform affine_transform = new 
            AffineTransform( Tablet.accumulated_length_scale,
                             0, 
                             0, 
                             -Tablet.v_scale,
                             40, 
                             9*length_vs_speed_panel.getSize().height/10 );
        AWTUtil.paintFvsG( accumulated_length,
                           speed, 
                           Color.blue,      
                           length_vs_speed_panel, 
                           affine_transform );
    }


    /**
    *
    * updata the panel
    *
    **/
    public void
    paintChangeInDirection( double dd_dt[], double accumulated_length[] )
    {
        if ( !isVisible() ) {
            return;
        }
        
        Graphics g = length_vs_dd_dt_panel.getGraphics();

        GraphicsUtil.clearComponent( length_vs_dd_dt_panel );

        AffineTransform affine_transform = new 
            AffineTransform( Tablet.accumulated_length_scale,
                             0, 
                             0, 
                             -Tablet.d_theta_scale,
                             40, 
                             9*length_vs_dd_dt_panel.getSize().height/10 );
        AWTUtil.paintFvsG( accumulated_length,
                           dd_dt, 
                           Color.blue,      
                           length_vs_dd_dt_panel, 
                           affine_transform );
    }
}

/**
  *
  * $Log: JointDisplay.java,v $
  * Revision 1.3  2003/11/05 01:42:02  moltmans
  * Found more ^M's  They should all be gone now... Again...  For good?
  *
  * Revision 1.2  2003/03/06 01:08:52  moltmans
  * Added copyright to all the files.
  *
  * Revision 1.1  2001/11/23 03:24:13  mtsezgin
  * *** empty log message ***
  *
  * Revision 1.2  2001/10/12 22:25:52  mtsezgin
  * This is a commit of all files.
  * Shoapid
  * vi sux:q
  *
  * Revision 1.1.1.1  2001/03/29 16:25:01  moltmans
  * Initial directories for DRG
  *
  * Revision 1.10  2000/09/06 22:40:33  mtsezgin
  * Combinations of curves and polygons are successfully approximated
  * by straight lines and Bezier curves as appropriate. System works
  * quite reliably.
  *
  * Revision 1.4  2000/06/08 03:11:46  mtsezgin
  * *** empty log message ***
  *
  * Revision 1.3  2000/06/03 01:52:31  mtsezgin
  * *** empty log message ***
  *
  * Revision 1.2  2000/06/02 21:11:14  mtsezgin
  * *** empty log message ***
  *
  * Revision 1.1  2000/05/26 20:39:25  mtsezgin
  *
  * This GUI lets the user play with the paramaters used by the
  * recognition algorithms.
  *
  * Revision 1.2  2000/05/24 01:53:22  mtsezgin
  *
  * The polygon angle normalization works reliably.
  *
  * Revision 1.1  2000/05/21 23:13:15  mtsezgin
  *
  * This module is used for plotting and investigating certain statistical
  * features.
  *
  *
  *
  **/
