// (c) MIT 2003.  All rights reserved.

////////////////////////////////////////////////////////////////////////////////
//                                                                            //
// AUTHOR:      Tevfik Metin Sezgin                                           //
//              Massachusetts Institute of Technology                         //
//              Department of Electrical Engineering and Computer Science     //
//              Artificial Intelligence Laboratory                            //
//                                                                            //
// E-MAIL:        mtsezgin@ai.mit.edu, mtsezgin@mit.edu                       //
//                                                                            //
// COPYRIGHT:   Tevfik Metin Sezgin                                           //
//              All rights reserved. This code can not be copied, modified,   //
//              or distributed in whole or partially without the written      //
//              permission of the author. Also see the COPYRIGHT file.        //
//                                                                            //
////////////////////////////////////////////////////////////////////////////////
package edu.mit.sketch.ui;

  /**
  *
  * See the end of the file for the log of changes.
  *
  * $Author: moltmans $
  * $Date: 2003/11/05 01:42:02 $
  * $Revision: 1.4 $
  * $Headers$
  * $Id: TabletGUI.java,v 1.4 2003/11/05 01:42:02 moltmans Exp $
  * $Name:  $
  * $Locker:  $
  * $Source: /projects/drg/CVSROOT/drg/code/src/edu/mit/sketch/ui/TabletGUI.java,v $
  *
  **/

import javax.swing.JFrame;
import javax.swing.JList;
import javax.swing.ScrollPaneLayout;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Frame;
import java.awt.GridLayout;
import java.awt.Image;
import java.awt.Label;
import java.awt.Menu;
import java.awt.MenuBar;
import java.awt.MenuItem;
import java.awt.Panel;
import java.awt.ScrollPane;
import java.awt.Toolkit;

import java.awt.event.AdjustmentEvent;
import java.awt.event.AdjustmentListener;

import java.util.Vector;

import java.awt.geom.AffineTransform;
import edu.mit.sketch.geom.Point;
import edu.mit.sketch.ui.JointDisplay;
import edu.mit.sketch.ui.Tablet;
import edu.mit.sketch.toolkit.StatisticsModule;
import edu.mit.sketch.toolkit.SpeedScaleSpaceViewer;
import edu.mit.sketch.toolkit.DirectionChangeScaleSpaceViewer;

/**
  *
  * The TabletGUI
  *
  **/

public
class      TabletGUI
extends    Frame
implements AdjustmentListener
{
    public Frame    v_window;
    public Frame    a_window;
    public Frame    d_window;             // Direction window
    public Frame    fits_window;          // Window for lsq errors of fits
    public Frame    dd_dt_window;         // Window for direction's derivative
    public JFrame   data_window;       // Window for data

    public JointDisplay joint_display; // Window for comparing d, dd_dt etc.

    public MenuBar  menubar;
    public Menu     file;
    public MenuItem open_menu;
    public MenuItem save_menu;
    public MenuItem save_as_menu;
    public MenuItem quit_menu;

    public Menu     edit;
    public MenuItem undo_menu;
    
    public Menu     windows;
    public MenuItem v_menu;
    public MenuItem a_menu;
    public Menu     pen_menu;
    public Menu     direction_menu;
    public MenuItem d_menu;
    public MenuItem fits_menu;
    public MenuItem data_menu;
    public MenuItem dd_dt_menu;
    public MenuItem gesture_gui_menu;
    public MenuItem controls_menu;
    public MenuItem statistics_menu;
    public MenuItem joint_display_menu;
    public MenuItem v_scale_space_menu;
    public MenuItem dd_dt_scale_space_menu;
    
    public Menu     options;
    public MenuItem vectors_menu;
    public MenuItem display_menu;

    public Menu     processing;
    public MenuItem save_features_menu;
    public MenuItem derive_snapshots_menu;
    public MenuItem derive_properties_menu;

    public static Label info_bar;
    public static Label status_bar;
    public JList x_list;
    public JList y_list;

    public static Graphics         debug_graphics;
    public static StatisticsModule bar_statistics_module;
    public static StatisticsModule dsw_statistics_module;
    public static StatisticsModule ddsw_statistics_module;
    public static StatisticsModule sliding_window_statistics_module;

    public Graphics v_window_graphics;
    public Graphics a_window_graphics;
    public Graphics d_window_graphics; 
    public Graphics fits_window_graphics; 
    public Graphics dd_dt_window_graphics;

    public Color    graph_color;
    
    public SpeedScaleSpaceViewer           v_scale_space_viewer;
    public DirectionChangeScaleSpaceViewer dd_dt_scale_space_viewer;
    
    // This determines how much of the window we clear.
    private int max_window_size = 1000;
        

    /**
    *
    * The constructor.
    *
    **/
    public TabletGUI( String  cursor_image_name )
    {
        super( "TabletGUI 1.0" );
        Toolkit toolkit = Toolkit.getDefaultToolkit();
        Image   image   = toolkit.getImage( cursor_image_name );
        Cursor  cursor  = toolkit.createCustomCursor( 
            image,
            new Point( 0, 0 ),
            "pencil" );
        setCursor( cursor );
        constructGUI();
        v_scale_space_viewer.scale.addAdjustmentListener( this );
        dd_dt_scale_space_viewer.scale.addAdjustmentListener( this );
    }

        
    /**
    *
    * construct the GUI part of the Tablet.
    *
    **/
    public void
    constructGUI()
    {
        graph_color = Color.blue;

        bar_statistics_module = new 
            StatisticsModule( "Bar" );
        dsw_statistics_module = new
            StatisticsModule( "DSW sliding window" );
        ddsw_statistics_module = new
            StatisticsModule( "DDSW histogram" );
        sliding_window_statistics_module = new
            StatisticsModule( "Sliding window" );
        
        menubar                = new MenuBar();
        file                   = new Menu( "File" );
        open_menu              = new MenuItem( "Open" );
        save_menu              = new MenuItem( "Save" );
        save_as_menu           = new MenuItem( "Save as" );
        quit_menu              = new MenuItem( "Quit" );
        edit                   = new Menu( "Edit" );
        undo_menu              = new MenuItem( "Undo" );
        windows                = new Menu( "Windows" );
        pen_menu               = new Menu( "MOTION" );
        v_menu                 = new MenuItem( "Speed" );
        a_menu                 = new MenuItem( "Acceleration" );
        direction_menu         = new Menu( "DIRECTION" );
        d_menu                 = new MenuItem( "  \u0398" );
        fits_menu              = new MenuItem( "Fits" );
        data_menu              = new MenuItem( "Data" );
        dd_dt_menu             = new MenuItem( "d\u0398/dt" );
        gesture_gui_menu      = new MenuItem( "GestureGUI" );
        controls_menu          = new MenuItem( "Controls" );
        statistics_menu        = new MenuItem( "Statistics" );
        joint_display_menu     = new MenuItem( "Joint Display" );
        v_scale_space_menu     = new MenuItem( "Speed Scale Space" );
        dd_dt_scale_space_menu = new MenuItem( "Direction Change Scale Space" );
        v_window               = new Frame( "Speed vs Time" );
        a_window               = new Frame( "Acceleration vs Time" );
        d_window               = new Frame( "Direction vs Time (In radians)" );
        fits_window            = new Frame( "LSQ Errors" );
        dd_dt_window           = new Frame( "dd/dt vs time" );
        data_window            = new JFrame( "data window" );
        joint_display          = new JointDisplay();
        options                = new Menu( "Options" );
        vectors_menu           = new MenuItem( "Display LSQ Vectors" );
        display_menu           = new MenuItem( "Show parsed objects" );
        processing             = new Menu( "Processing" );
        save_features_menu     = new MenuItem( "Save Feature Vectors" );
        derive_snapshots_menu  = new MenuItem( "Derive Snapshots" );
        derive_properties_menu = new MenuItem( "Derive Properties" );
        info_bar               = new Label( "" );
        status_bar             = new Label( "Ready..." );
        x_list                 = new JList();
        y_list                 = new JList();
        
        v_scale_space_viewer     = new SpeedScaleSpaceViewer();
        dd_dt_scale_space_viewer = new DirectionChangeScaleSpaceViewer();
    
        v_window.setLocation( 0, 300 );
        a_window.setLocation( 800, 50 );
        d_window.setLocation( 50,  300 );
        dd_dt_window.setLocation( 0, 600 );
        joint_display.setLocation( 550, 0 );
        fits_window.setLocation( 500, 500 );
        data_window.setLocation( 500, 500 );
        bar_statistics_module.setLocation( 500, 100 );
        dsw_statistics_module.setLocation( 500, 100 );
        ddsw_statistics_module.setLocation( 500, 500 );
        sliding_window_statistics_module.setLocation( 500, 500 );
        v_scale_space_viewer.setLocation( 0, 430 );
        dd_dt_scale_space_viewer.setLocation( 0, 730 );
        
        initializeGraphicsObjects();
        
        v_window.setSize( 1200, 300 );
        a_window.setSize( 400, 400 );
        d_window.setSize( 400, 400 );
        fits_window.setSize( 300, 300 );
        data_window.setSize( 400, 400 );
        dd_dt_window.setSize( 1200, 300 );
        joint_display.setSize( 740, 400 );
        bar_statistics_module.setSize( 400, 200 );
        dsw_statistics_module.setSize( 400, 200 );
        ddsw_statistics_module.setSize( 400, 200 );
        sliding_window_statistics_module.setSize( 400, 200 );
        v_scale_space_viewer.setSize( 1480, 300 );
        dd_dt_scale_space_viewer.setSize( 1480, 300 );
        
        ScrollPane scroll_pane = new ScrollPane();
        Panel      panel       = new Panel();
        panel.setLayout( new GridLayout( 1, 0 ) );
        data_window.getContentPane().setLayout( new GridLayout( 1, 1 ) );
        data_window.getContentPane().add( scroll_pane );
        panel.add( x_list );
        panel.add( y_list );
        scroll_pane.add( panel );

        setMenuBar( menubar );
        file.add( open_menu );
        file.add( save_menu );
        file.add( save_as_menu );
        file.addSeparator();
        file.add( quit_menu );
        
        edit.add( undo_menu );
        
        windows.add( pen_menu );
        windows.addSeparator();
        windows.add( direction_menu );
        windows.addSeparator();
        windows.add( joint_display_menu );
        windows.addSeparator();
        windows.add( v_scale_space_menu );
        windows.addSeparator();
        windows.add( dd_dt_scale_space_menu );
        windows.addSeparator();
        windows.add( statistics_menu );
        windows.addSeparator();
        windows.add( fits_menu );
        windows.addSeparator();
        windows.add( gesture_gui_menu );
        windows.addSeparator();
        windows.add( controls_menu );
        windows.addSeparator();
        windows.add( data_menu );
        
        options.add( vectors_menu );
        options.add( display_menu );

        processing.add( save_features_menu );
        processing.add( derive_snapshots_menu );
        processing.add( derive_properties_menu );
        
        pen_menu.add( v_menu );
        pen_menu.addSeparator();
        pen_menu.add( a_menu );
        
        direction_menu.add( d_menu );
        direction_menu.addSeparator();
        direction_menu.add( dd_dt_menu );
        
        menubar.add( file    );
        menubar.add( edit    );
        menubar.add( windows );
        menubar.add( options );
        menubar.add( processing );
    }


    /**
    *
    * Set the status bar label text to the input string.
    *
    **/
    public static void
    setInfoMessage( String message )
    {
        info_bar.setText( message );
    }


    /**
    *
    * Set the status bar label text to the input string.
    *
    **/
    public static void
    setStatusMessage( String message )
    {
        status_bar.setText( message );
    }


    /**
    *
    * Clear screen
    *
    **/
    public void
    clearScreen()
    {
        clearComponent( this );
    }


    /**
    *
    * initializeGraphicsObjects
    *
    **/
    public void
    initializeGraphicsObjects()
    {
        debug_graphics        = this.getGraphics();
        v_window_graphics      = v_window.getGraphics();
        a_window_graphics      = a_window.getGraphics();
        d_window_graphics      = d_window.getGraphics();
        fits_window_graphics  = fits_window.getGraphics();
        dd_dt_window_graphics = dd_dt_window.getGraphics();
    }


    /**
    *
    * Clear the component
    *
    **/
    public void
    clearComponent( Component component ) 
    {
        Graphics  g = component.getGraphics();
        g.setColor( Color.white );
        g.fillRect( 0, 
                    0, 
                    component.getWidth(),
                    component.getHeight() );
    }
    
    
    /**
    *
    * adjustmentValueChanged
    *
    **/
    public void
    adjustmentValueChanged( AdjustmentEvent e )
    {
        Object source = e.getSource();
        
        if ( source == dd_dt_scale_space_viewer.scale ) {
            dd_dt_scale_space_viewer.displayData( e.getValue()-1, 
                                                 (e.getValue()-1)/5,
                                                 (Tablet)this );
            dd_dt_scale_space_viewer.printData( e.getValue()-1 );
        }
        if ( source == v_scale_space_viewer.scale ) {
            v_scale_space_viewer.displayData( e.getValue()-1, 
                                             (e.getValue()-1)/5,
                                             (Tablet)this );
            v_scale_space_viewer.printData( e.getValue()-1 );
        }
    }
}

/**
  *
  * $Log: TabletGUI.java,v $
  * Revision 1.4  2003/11/05 01:42:02  moltmans
  * Found more ^M's  They should all be gone now... Again...  For good?
  *
  * Revision 1.3  2003/06/16 17:14:37  mtsezgin
  *
  * Changes to the GUI.
  *
  * Revision 1.2  2003/03/06 01:08:52  moltmans
  * Added copyright to all the files.
  *
  * Revision 1.1  2001/11/23 03:24:13  mtsezgin
  * *** empty log message ***
  *
  * Revision 1.2  2001/10/12 22:25:53  mtsezgin
  * This is a commit of all files.
  * Shoapid
  * vi sux:q
  *
  * Revision 1.1.1.1  2001/03/29 16:25:01  moltmans
  * Initial directories for DRG
  *
  * Revision 1.3  2000/09/06 22:41:04  mtsezgin
  * Combinations of curves and polygons are successfully approximated
  * by straight lines and Bezier curves as appropriate. System works
  * quite reliably.
  *
  * Revision 1.18  2000/06/08 03:20:07  mtsezgin
  *
  * Now the user can save the design, and read back the designs that
  * were saved earlied. This is accomplished through Serialization.
  *
  * Revision 1.16  2000/06/02 21:11:16  mtsezgin
  * *** empty log message ***
  *
  * Revision 1.15  2000/05/26 20:43:35  mtsezgin
  *
  * Modified to make use of the ControlsModule that lets the user play
  * with parameters used in the recognition process.
  *
  * Revision 1.13  2000/05/22 02:42:34  mtsezgin
  *
  * The current version enables polygons to be sketched in pieces.
  *
  * Revision 1.12  2000/05/07 17:27:58  mtsezgin
  * *** empty log message ***
  *
  * Revision 1.11  2000/04/28 04:45:05  mtsezgin
  *
  * Now each GeometricObject keeps the mouse input that was previously
  * discarded. User can switch between seeing the recognized mode and
  * the raw mode. setDataPoints( Polygon points ) and getDataPoints()
  * are added to GeometricObject, and all the implementors are modified
  * accordingly.
  *
  * Revision 1.10  2000/04/20 04:29:51  mtsezgin
  * *** empty log message ***
  *
  * Revision 1.8  2000/04/17 07:02:32  mtsezgin
  *
  * Finally made the Rectangle really rotatable.
  *
  * Revision 1.7  2000/04/13 06:24:09  mtsezgin
  *
  * The current version of the program recognized Crosses, and Shades.
  * Implementors of Terminal and their descendants were modified to
  * implement the changes in GeometricObject.
  *
  * Revision 1.6  2000/04/12 04:00:17  mtsezgin
  * *** empty log message ***
  *
  * Revision 1.5  2000/04/11 00:41:48  mtsezgin
  *
  * Now the whole package succesfully parses a motor.
  *
  * Revision 1.4  2000/04/06 19:16:24  mtsezgin
  *
  * Modified all the classes to use my Point class which extends java.awt.Point
  * instead of directly using java.awt.Point
  *
  * Revision 1.3  2000/04/01 20:34:04  mtsezgin
  *
  * Renamed Oval.java to Ellipse.java
  *
  * Revision 1.2  2000/04/01 04:11:32  mtsezgin
  * *** empty log message ***
  *
  * Revision 1.1.1.1  2000/04/01 03:07:07  mtsezgin
  * Imported sources
  *
  * Revision 1.3  2000/03/31 22:41:05  mtsezgin
  *
  * Started Log tracking.
  *
  * Revision 1.2  2000/03/31 22:30:31  mtsezgin
  *
  *
  * Starting the log management.
  *
  *
  **/
