// (c) MIT 2003.  All rights reserved.

////////////////////////////////////////////////////////////////////////////////
//                                                                            //
// AUTHOR:      Tevfik Metin Sezgin                                           //
//              Massachusetts Institute of Technology                         //
//              Department of Electrical Engineering and Computer Science     //
//              Artificial Intelligence Laboratory                            //
//                                                                            //
// E-MAIL:        mtsezgin@ai.mit.edu, mtsezgin@mit.edu                       //
//                                                                            //
// COPYRIGHT:   Tevfik Metin Sezgin                                           //
//              All rights reserved. This code can not be copied, modified,   //
//              or distributed in whole or partially without the written      //
//              permission of the author. Also see the COPYRIGHT file.        //
//                                                                            //
////////////////////////////////////////////////////////////////////////////////
package edu.mit.sketch.ui;


  /**
  *
  * See the end of the file for the log of changes.
  *
  * $Author: moltmans $
  * $Date: 2003/11/05 01:42:02 $
  * $Revision: 1.7 $
  * $Headers$
  * $Id: TabletDataProcessor.java,v 1.7 2003/11/05 01:42:02 moltmans Exp $
  * $Name:  $
  * $Locker:  $
  * $Source: /projects/drg/CVSROOT/drg/code/src/edu/mit/sketch/ui/TabletDataProcessor.java,v $
  *
  **/

import java.util.ArrayList;

import java.io.OutputStream;
import java.io.BufferedOutputStream;
import java.io.FileOutputStream;

import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;
import edu.mit.sketch.geom.Line;
import edu.mit.sketch.geom.Vertex;
import edu.mit.sketch.geom.PolarPoint;
import edu.mit.sketch.geom.Point;
import edu.mit.sketch.geom.GeneralPath;
import edu.mit.sketch.geom.Polygon;
import edu.mit.sketch.geom.GeometricObject;
import edu.mit.sketch.geom.GeometryUtil;
import edu.mit.sketch.ui.GestureGUI;
import edu.mit.sketch.ui.Tablet;
import edu.mit.sketch.toolkit.*;
import edu.mit.sketch.util.Util;
import edu.mit.sketch.util.LinearFit;
import edu.mit.sketch.util.GIFEncoder;

/**
  *
  * This class is for reading mouse input and processing it.
  * Various fits are generated and stored here.
  * Acronyms: sdr -> StrokeDataReader, sd -> StrokeData
  *
  **/

public
class      TabletDataProcessor
extends    TabletGUI
implements MouseMotionListener, 
           MouseListener
{
    /**
     * Stroke data reader used for reading and instantiating 
     * StrokeData
     **/
    public static StrokeDataReader sdr;
    
    /**
     * Stroke data 
     **/
    public static StrokeData data;
    
    public Vertex     final_fit[];
    public Vertex     speed_fit[];
    public Vertex     direction_fit[];
    
    public PolarPoint polar_points[];

    public int    direction_window_width      = 5;
    public double dd_dt_average_scale          = 1.8; //1.0
    public double speed_average_scale          = 0.5; //.6
    public double test_line_scale              = 1.05;
    public double combine_treshold            = 10;
    public double turn_angle_treshold          = Math.PI/32;  // Was/8 Was /6
    public double circle_vs_speed_bias        = 1.0; // Multiply by circle_LSQE
    public double circle_vs_general_path_bias = 0.7; // Multiply by circle_LSQE
    

    /**
     * Parameters and constants.
     **/
    public Point      line[];

    public boolean  in_recognition_mode  = true;
    public boolean  deriving_scale_space = false;
    
    public static boolean platform_is_windows9x = false;
    
    public static boolean debug;
    public static boolean debug2;

    public static int    fit_method   = LinearFit.ROTATION_METHOD;
    public static double window_width = Math.PI/36;

    public static long    sequence_delay = 1500;
    public static double  LSQE_treshold  = 36.0;  // A max deviation of 6 pixels

    public ObjectManager    object_manager;
    public ControlModule    control_module;
    public GestureGUI      gesture_gui;
    public SimpleClassifier classifier;

    /**
    *
    * The constructor.
    *
    **/
    public TabletDataProcessor( int     max_data_points, 
                                 String  cursor_image_name,
                                 boolean platform_is_windows9x )
    {
        super( cursor_image_name );

        this.platform_is_windows9x = platform_is_windows9x;
 
        object_manager = new ObjectManager( this );
        control_module = new ControlModule( this );
        gesture_gui   = new GestureGUI();
        
        control_module.setLocation( 500, 500 );
        gesture_gui.setLocation( 500, 400 );
        
        sdr = new StrokeDataReader();
        addEventListeners();
    }

        
    /**
    *
    * add event listeners
    *
    **/
    public void
    addEventListeners()
    {
        addKeyListener( object_manager );
        addMouseListener( object_manager );
        addMouseMotionListener( object_manager );
        info_bar.addKeyListener( object_manager );
        status_bar.addKeyListener( object_manager );
        
        addMouseListener( this );
        addMouseMotionListener( this );
    }

    /**
    *
    * Handle mouse events
    *
    **/
    public void
    classifyData( StrokeData data )
    {
        GeneralPath general_path_fit;
        Polygon     original_points;
        Vertex      black_board_result[];
        Vertex      points[];
        double      d[];
        int         point_count;
        
        boolean line_result;
        boolean circle_result;
        boolean polygon_result;
        
        double  speed_LSQ        = Double.MAX_VALUE;
        double  circle_LSQ       = Double.MAX_VALUE;
        double  final_fit_LSQ    = Double.MAX_VALUE;
        double  general_path_LSQ = Double.MAX_VALUE;

        this.data = data;
 
         classifier = new SimpleClassifier2( data );
        
        classifier.fit_method                   = fit_method;
        classifier.direction_window_width       = direction_window_width;
        classifier.dd_dt_average_scale         = dd_dt_average_scale;
        classifier.speed_average_scale         = speed_average_scale;
        classifier.test_line_scale             = test_line_scale;

        original_points = data.getDataPoints();

        GeometricObject recognized_object;
        point_count = data.vertices.length;
        
        switch ( classifier.classify() ) {
            case Classifier.LINE :
                recognized_object = classifier.getLineApproximation();
                break;
            case Classifier.ELLIPSE :
                recognized_object = classifier.getEllipseApproximation();
                break;
            case Classifier.POLYGON :
                recognized_object = classifier.getPolygonApproximation();
                //((Polygon)recognized_object).combineEndPoints(combine_treshold);
                break;
            case Classifier.COMPLEX :
                recognized_object = classifier.getComplexApproximation();
                break;
            default : 
            break;
        }
        
        if ( deriving_scale_space ) {
            data.deriveScaleSpaces();
            v_scale_space_viewer.setScaleSpaceData( data.speed_scale_space,
                                                    data.speed_gaussians,
                                                    data );
            dd_dt_scale_space_viewer.setScaleSpaceData( data.dd_dt_scale_space,
                                                        data.dd_dt_gaussians,
                                                        data );
            try {
                GIFEncoder encoder  =
                    new GIFEncoder( dd_dt_scale_space_viewer.getImage() );
                OutputStream output = 
                    new BufferedOutputStream( 
                        new FileOutputStream( "out_1.gif" ) );
            
                encoder.Write(output);
            } catch( Exception exception ) {
                System.out.println( exception );
            }
        }
                                                    
         control_module.setHybridFitList( Blackboard.hybrid_fits );
    }


    /**
    *
    * Handle mouse motion events. Strokes vector keeps strokes 
    * as point[]
    * 
    **/
    public void
    processStrokes( ArrayList strokes ) 
    {
        System.out.println( "strokes length " + strokes.size() );
        
        for ( int i=0; i<strokes.size(); i++ ) {
            Point input_vertices[] = ((Point[])strokes.get( i ));
             reset();
            for ( int j=0; j<input_vertices.length; j++ ) {
                // System.out.println( "Stroke "  + i + " point " + j + " = " + input_vertices[j] );
                addPoint( input_vertices[j], input_vertices[j].time_stamp );
            }
            GeneralPath general_path_fit;
            Polygon     original_points;
            Vertex      black_board_result[];
            Vertex      points[];
            double      d[];
            int         point_count;
 
            boolean line_result;
            boolean circle_result;
            boolean polygon_result;
 
            double  speed_LSQ        = Double.MAX_VALUE;
            double  circle_LSQ       = Double.MAX_VALUE;
            double  final_fit_LSQ    = Double.MAX_VALUE;
            double  general_path_LSQ = Double.MAX_VALUE;

 
            if ( !in_recognition_mode || !sdr.hasSufficientData() )
                return;
 
            data            = sdr.getStrokeData();
 
             classifier = new SimpleClassifier( data );
 
            classifier.fit_method                   = fit_method;
            classifier.direction_window_width       = direction_window_width;
            classifier.dd_dt_average_scale         = dd_dt_average_scale;
            classifier.speed_average_scale         = speed_average_scale;
            classifier.test_line_scale             = test_line_scale;

            original_points = data.getDataPoints();

            GeometricObject recognized_object;
            point_count = data.vertices.length;
 
            switch ( classifier.classify() ) {
                case Classifier.LINE :
                    recognized_object = new Line( data.vertices[0], data.vertices[data.vertices.length-1]  );//classifier.getLineApproximation();
                    break;
                case Classifier.ELLIPSE :
                    recognized_object = classifier.getEllipseApproximation();
                    break;
                case Classifier.POLYGON :
                    recognized_object = classifier.getPolygonApproximation();
                    //((Polygon)recognized_object).combineEndPoints(combine_treshold);
                    break;
                case Classifier.COMPLEX :
                    recognized_object = classifier.getComplexApproximation();
                    //((GeneralPath)recognized_object).combineEndPoints(combine_treshold);
                    break;
                default :
                return;
            }
 
            if ( !Tablet.very_quiet ) {
                System.out.println( "calling classifier.classifyAndRank()" );
                Util.printArray( classifier.classifyAndRank(), "approximations" );
            }
    
            if ( deriving_scale_space ) {
                data.deriveScaleSpaces();
                v_scale_space_viewer.setScaleSpaceData( data.speed_scale_space,
                                                        data.speed_gaussians,
                                                        data );
                dd_dt_scale_space_viewer.setScaleSpaceData( data.dd_dt_scale_space,
                                                            data.dd_dt_gaussians,
                                                            data );
                try {
                    GIFEncoder encoder  =
                        new GIFEncoder( dd_dt_scale_space_viewer.getImage() );
                    OutputStream output =
                        new BufferedOutputStream(
                            new FileOutputStream( "out_1.gif" ) );
 
                    encoder.Write(output);
                } catch( Exception exception ) {
                    System.out.println( exception );
                }
            }
 
            recognized_object.setTimeStamp( -1 );
            recognized_object.setDataPoints( original_points );
            object_manager.addObject( recognized_object );
            object_manager.addVertices( Vertex.cloneVertices( data.vertices ) );
            control_module.setHybridFitList( Blackboard.hybrid_fits );
            gesture_gui.current_gesture = new
                 GeneralPath( new
                    Polygon(
                        Blackboard.hybrid_fits[Blackboard.best_index].vertices ));
            gesture_gui.addExample( gesture_gui.current_gesture );
        }
    }


    /**
    *
    * Handle mouse events
    *
    **/
    public void
    mouseReleased( MouseEvent e )
    {
        GeneralPath general_path_fit;
        Polygon     original_points;
        Vertex      black_board_result[];
        Vertex      points[];
        double      d[];
        int         point_count;
        
        boolean line_result;
        boolean circle_result;
        boolean polygon_result;
        
        double  speed_LSQ        = Double.MAX_VALUE;
        double  circle_LSQ       = Double.MAX_VALUE;
        double  final_fit_LSQ    = Double.MAX_VALUE;
        double  general_path_LSQ = Double.MAX_VALUE;

        
        if ( !sdr.hasSufficientData() )
            return;
            
        data            = sdr.getStrokeData();
 
        //classifier = new SimpleClassifier2( data );
        classifier = new SimpleClassifier3( data );

        classifier.fit_method                   = fit_method;
        classifier.direction_window_width       = direction_window_width;
        classifier.dd_dt_average_scale         = dd_dt_average_scale;
        classifier.speed_average_scale         = speed_average_scale;
        classifier.test_line_scale             = test_line_scale;

        original_points = data.getDataPoints();

        GeometricObject recognized_object;
        point_count = data.vertices.length;
        
        switch ( classifier.classify() ) { 
            case Classifier.LINE :
                recognized_object = new Line( data.vertices[0], data.vertices[data.vertices.length-1]  );//classifier.getLineApproximation();
                break;
            case Classifier.ELLIPSE :
                recognized_object = classifier.getEllipseApproximation();
                break;
            case Classifier.POLYGON :
                recognized_object = classifier.getPolygonApproximation();
                //((Polygon)recognized_object).combineEndPoints(combine_treshold);
                break;
            case Classifier.COMPLEX :
                recognized_object = classifier.getComplexApproximation();
                //((GeneralPath)recognized_object).combineEndPoints(combine_treshold);
                break;
            case SimpleClassifier2.ARC :
                recognized_object = ((SimpleClassifier2)classifier).getArcApproximation();
                //((GeneralPath)recognized_object).combineEndPoints(combine_treshold);
                break;
            default : 
            return;
        }
        
        if ( !Tablet.very_quiet ) {
            System.out.println( "calling classifier.classifyAndRank()" );
            Util.printArray( classifier.classifyAndRank(), "approximations" );
        }
        
        if ( deriving_scale_space ) {
            data.deriveScaleSpaces();
            v_scale_space_viewer.setScaleSpaceData( data.speed_scale_space,
                                                    data.speed_gaussians,
                                                    data );
            dd_dt_scale_space_viewer.setScaleSpaceData( data.dd_dt_scale_space,
                                                        data.dd_dt_gaussians,
                                                        data );
            try {
                GIFEncoder encoder  = 
                    new GIFEncoder( dd_dt_scale_space_viewer.getImage() );
                OutputStream output = 
                    new BufferedOutputStream( 
                        new FileOutputStream( "out_1.gif" ) );
            
                encoder.Write(output);
            } catch( Exception exception ) {
                System.out.println( exception );
            }
        }
                                                    
        recognized_object.setTimeStamp( getTimeStamp( e ) );
        recognized_object.setDataPoints( original_points );
        System.out.println( "Adding object from TabletDataProcessor" );
        object_manager.addObject( recognized_object );
        object_manager.addVertices( Vertex.cloneVertices( data.vertices ) ); 
        control_module.setHybridFitList( Blackboard.hybrid_fits );
        gesture_gui.current_gesture = new
             GeneralPath( new 
                Polygon(
                    Blackboard.hybrid_fits[Blackboard.best_index].vertices ));
        gesture_gui.addExample( gesture_gui.current_gesture );
    }
    

    /**
    *
    * Set debugging mode
    *
    **/
    public void
    setDebugging( boolean d )
    {
        debug = d;
    }

    
    /**
    *
    * Handle mouse events
    *
    **/
    public void
    mouseClicked( MouseEvent e ) 
    {
    }
    

    /**
    *
    * Handle mouse events
    *
    **/
    public void
    mouseEntered( MouseEvent e ) 
    {
    }
    

    /**
    *
    * Handle mouse events
    *
    **/
    public void
    mouseExited( MouseEvent e ) 
    {
    }
    

    /**
    *
    * Handle mouse events
    *
    **/
    public void
    mousePressed( MouseEvent e ) 
    {
        reset();
        
        if ( !Tablet.very_quiet ) {
            System.out.println( "\n\nStarting new session.." );
        }
        
        addPoint( new Point( e.getPoint() ), getTimeStamp( e ) );
    }

    
    /**
    *
    * Reset the arrays
    *
    **/
    public void
    reset() 
    {
        sdr.reset();
        speed_fit     = null;
        final_fit     = null;
        direction_fit = null;
    }


    /**
    *
    * Add a new point to sdr with t as the time stamp.
    *
    **/
    public void
    addPoint( Point p, long t ) 
    {
      System.out.println( "Adding point " + p );
        Point point = new Point( p );
        point.setTimeStamp( t );
        sdr.addPoint( point );
    }


    /**
    *
    * Disable or enable recognition process by turning mouse event 
    * listening off or on. Set flag to be true if enabling is desired.
    * Set flag to be false if disabling is desired.
    *
    **/
    public void
    setRecognitionMode( boolean flag )
    {
        in_recognition_mode = flag;
    }


    /**
    *
    * Handle mouse motion events
    *
    **/
    public void
    mouseDragged( MouseEvent e ) 
    {
        Point point = new Point( e.getPoint() );
        addPoint( point, getTimeStamp( e ) );
    }


    /**
    *
    * Handle mouse motion events
    *
    **/
    public void
    mouseMoved( MouseEvent e ) 
    {
    }
    
    
    /**
    * 
    * Combines the ends of the polygon if they are sufficiently close.
    * 
    **/
    public Vertex[]
    combineEndPoints( Vertex polygon[] )
    {
        if ( ( polygon == null ) || ( polygon.length < 2 ) ) {
            System.err.println("Error: combineEndPoints no polygon...");
            return null;
        }
        
        int l = polygon.length-1;
        
        if ( polygon[l].distance( polygon[0] ) < combine_treshold ) {
            polygon[0].x  = polygon[l].x;
            polygon[0].y  = polygon[l].y;
            System.out.println( "COMBINED ENDS OF THE POLYGON" );
        }
        
        return polygon;
    }


    /**
    * 
    * Simplify the polygon to get rid of consecutive lines which are
    * sufficiently parallel.
    *
    **/
    public Vertex[]
    simplifyPolygon( Vertex input_vertices[] ) 
    {
        double theta     = 0.0;
        double old_theta = 0.0;
        
        if ( (input_vertices == null) || (input_vertices.length < 2) ) {
            System.err.println("Error: simplifyPolygon no polygon detected.");
            return null;
        }
        
        double  intermediate_angles[] = 
            GeometryUtil.getIntermediateAngles( input_vertices );
        boolean include[]             = 
            new boolean[intermediate_angles.length+1];
        
        if ( debug2 ) {
            System.out.println( "Starting simplification" );
            System.out.println( "# of points = " + input_vertices.length );
            System.out.println( "intermediate_angles.length = " + 
                                intermediate_angles.length );
        }

        include[0] = true;
        old_theta  = intermediate_angles[0];
        int deleted_points = 0;
        for ( int i=1; i<intermediate_angles.length; i++ ) {
            if ( Math.abs( old_theta - intermediate_angles[i] ) < 
                 turn_angle_treshold ) {
                if ( debug2 ) {
                    System.out.println( "Deleting point " + i );
                }
                deleted_points++;
                include[i] = false;
            } else {
                include[i] = true;
            }
            old_theta = intermediate_angles[i];
        }
        include[include.length-1] = true;
        System.out.println( "Simplification over" );
        System.out.println( "Deleted points = " +  deleted_points );

        Vertex new_polygon[] = new Vertex[include.length - deleted_points];
        System.out.println( "Created new polygon with " + 
                             new_polygon.length + " points");
        int index = 0;
        for ( int i=0; i<include.length; i++ ) {
            if ( include[i] ) {
                if ( debug2 ) {
                    System.out.println( "Included point " + i +
                                        " " + input_vertices[i] );
                }
                new_polygon[index] = new Vertex( input_vertices[i] );
                index++;
            } else {
                if ( debug2 ) {
                    System.out.println( "Excluded point " + i );
                }
            }
        }
        
        return new_polygon;
    }

    
    /**
    *
    * This method is needed only for supporting the rather 
    * silly windows9X platform, because it is still using the
    * dos clock.
    *
    **/
    public long
    getTimeStamp( MouseEvent e )    
    {
        if ( platform_is_windows9x ) {
            return Timer.tics();
        } else {
            return e.getWhen();
        }
    }
}

/**
  *
  * $Log: TabletDataProcessor.java,v $
  * Revision 1.7  2003/11/05 01:42:02  moltmans
  * Found more ^M's  They should all be gone now... Again...  For good?
  *
  * Revision 1.6  2003/06/26 19:57:15  calvarad
  * Lots of bug fixes
  *
  * Revision 1.5  2003/06/16 17:14:37  mtsezgin
  *
  * Changes to the GUI.
  *
  * Revision 1.4  2003/03/06 01:08:52  moltmans
  * Added copyright to all the files.
  *
  * Revision 1.3  2002/07/22 21:00:40  mtsezgin
  * Modified to make it work with the new classifier: SimpleClassifier2
  *
  * Revision 1.2  2001/12/06 02:48:19  mtsezgin
  * Modified these files to work with the changes made to SimpleClassifier.
  *
  * Revision 1.1  2001/11/23 03:24:13  mtsezgin
  * *** empty log message ***
  *
  * Revision 1.3  2001/10/12 23:32:29  mtsezgin
  * Turned off printing...
  *
  * Revision 1.2  2001/10/12 22:25:53  mtsezgin
  * This is a commit of all files.
  * Shoapid
  * vi sux:q
  *
  * Revision 1.1.1.1  2001/03/29 16:25:01  moltmans
  * Initial directories for DRG
  *
  * Revision 1.7  2001/03/23 00:00:17  mtsezgin
  * A major release with added files. Includes scale space method.
  *
  * Revision 1.4  2000/09/20 20:07:38  mtsezgin
  * This is a working version with curve recognition and curve
  * refinement. The GeneralPath approximation is refined if needed
  * to result in a better fit.
  *
  * Revision 1.3  2000/09/06 22:41:01  mtsezgin
  * Combinations of curves and polygons are successfully approximated
  * by straight lines and Bezier curves as appropriate. System works
  * quite reliably.
  *
  *
  *
  **/
