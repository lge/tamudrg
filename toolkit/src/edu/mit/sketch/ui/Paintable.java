// (c) MIT 2003.  All rights reserved.

////////////////////////////////////////////////////////////////////////////////
//                                                                            //
// AUTHOR:      Tevfik Metin Sezgin                                           //
//              Massachusetts Institute of Technology                         //
//              Department of Electrical Engineering and Computer Science     //
//              Artificial Intelligence Laboratory                            //
//                                                                            //
// E-MAIL:        mtsezgin@ai.mit.edu, mtsezgin@mit.edu                       //
//                                                                            //
// COPYRIGHT:   Tevfik Metin Sezgin                                           //
//              All rights reserved. This code can not be copied, modified,   //
//              or distributed in whole or partially without the written      //
//              permission of the author. Also see the COPYRIGHT file.        //
//                                                                            //
////////////////////////////////////////////////////////////////////////////////
package edu.mit.sketch.ui;

/** 
  * 
  * See the end of the file for the log of changes.
  * 
  * $Author: moltmans $
  * $Date: 2003/03/06 01:08:52 $   
  * $Revision: 1.2 $
  * $Headers$
  * $Id: Paintable.java,v 1.2 2003/03/06 01:08:52 moltmans Exp $     
  * $Name:  $   
  * $Locker:  $
  * $Source: /projects/drg/CVSROOT/drg/code/src/edu/mit/sketch/ui/Paintable.java,v $
  *  
  **/


import java.awt.Graphics; 

/**
  *
  * All the objects who want to be Paintable must implement this
  * interface.
  *
  **/

public interface Paintable
{
    /**
    *
    * Set graphics context for this Paintable object.
    * Must be set at least once before doing any drawing.
    *
    **/
    public abstract void
    setGraphicsContext( Graphics g );

    /**
    *
    * Draw the objects
    *
    **/
    public abstract void
    paint();

    /**
    *
    * Draw the objects using the given graphics context.
    *
    **/
    public abstract void
    paint( Graphics g );
    
    /**
    *
    * Returns the type of this NonTerminal
    *
    **/
    public abstract String
    getType();
    
    /**
    *
    * This method is used to paint the original data points that
    * forms this GeometricObject
    *
    **/
    public abstract void
    paintOriginal( Graphics g );
}

/**
  *
  * $Log: Paintable.java,v $
  * Revision 1.2  2003/03/06 01:08:52  moltmans
  * Added copyright to all the files.
  *
  * Revision 1.1  2001/11/26 18:29:07  mtsezgin
  * *** empty log message ***
  *
  * Revision 1.1.1.1  2001/03/29 16:25:01  moltmans
  * Initial directories for DRG
  *
  * Revision 1.7  2000/09/06 22:40:47  mtsezgin
  * Combinations of curves and polygons are successfully approximated
  * by straight lines and Bezier curves as appropriate. System works
  * quite reliably.
  *
  * Revision 1.4  2000/06/03 01:52:34  mtsezgin
  * *** empty log message ***
  *
  * Revision 1.3  2000/04/28 04:45:04  mtsezgin
  *
  * Now each GeometricObject keeps the mouse input that was previously
  * discarded. User can switch between seeing the recognized mode and
  * the raw mode. setDataPoints( Polygon points ) and getDataPoints()
  * are added to GeometricObject, and all the implementors are modified
  * accordingly.
  *
  * Revision 1.2  2000/04/11 01:41:45  mtsezgin
  * *** empty log message ***
  *
  * Revision 1.1  2000/04/11 00:41:47  mtsezgin
  *
  * Now the whole package succesfully parses a motor.
  *
  *
  **/
