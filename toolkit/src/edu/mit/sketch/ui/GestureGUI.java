// (c) MIT 2003.  All rights reserved.

////////////////////////////////////////////////////////////////////////////////
//                                                                            //
// AUTHOR:      Tevfik Metin Sezgin                                           //
//              Massachusetts Institute of Technology                         //
//              Department of Electrical Engineering and Computer Science     //
//              Artificial Intelligence Laboratory                            //
//                                                                            //
// E-MAIL:        mtsezgin@ai.mit.edu, mtsezgin@mit.edu                       //
//                                                                            //
// COPYRIGHT:   Tevfik Metin Sezgin                                           //
//              All rights reserved. This code can not be copied, modified,   //
//              or distributed in whole or partially without the written      //
//              permission of the author. Also see the COPYRIGHT file.        //
//                                                                            //
////////////////////////////////////////////////////////////////////////////////
package edu.mit.sketch.ui;

/**
  *
  * See the end of the file for the log of changes.
  *
  * $Author: moltmans $
  * $Date: 2003/11/05 01:42:02 $
  * $Revision: 1.3 $
  * $Headers$
  * $Id: GestureGUI.java,v 1.3 2003/11/05 01:42:02 moltmans Exp $
  * $Name:  $
  * $Locker:  $
  * $Source: /projects/drg/CVSROOT/drg/code/src/edu/mit/sketch/ui/GestureGUI.java,v $
  *
  **/


import java.awt.BorderLayout;
import java.awt.Button;
import java.awt.Choice;
import java.awt.Color;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Frame;
import java.awt.GridLayout;
import java.awt.Label;
import java.awt.List;
import java.awt.Menu;
import java.awt.MenuBar;
import java.awt.MenuItem;
import java.awt.Image;
import java.awt.Panel;
import java.awt.TextField;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.InputStreamReader;

import edu.mit.sketch.geom.GeometryUtil;
import edu.mit.sketch.geom.GeneralPath;
import edu.mit.sketch.geom.GeneralPathProcessor;
import edu.mit.sketch.util.LoadStoreModule;

/**
  *
  * This class is used to input training data for the gesture recognizer.
  *
  **/

public
class      GestureGUI
extends    Frame
implements ActionListener,
           ItemListener
{
    private Choice    classes;

    private TextField class_name;
    
    private Button    add_class;
    private Button    build_models;
    private Button    classify_gesture;
    private Button    prepare_training_data;
     
    private String    current_class;
    
    public GeneralPath current_gesture;
    
    /**
    *
    * The constructor.
    *
    **/
    public GestureGUI()
    {
        super( "GestureGUI" );
        
        classes                = new Choice();
        class_name             = new TextField( "" );
        add_class              = new Button( "Add Class" );
        build_models           = new Button( "Build Models" );
        classify_gesture       = new Button( "Classify Gesture" );
        prepare_training_data  = new Button( "Prepare Training Data" );
         
        setLayout( new BorderLayout() );
        

        Panel panel = new Panel();
        add( panel, BorderLayout.CENTER );
        
        panel.setLayout( new GridLayout( 0, 2 ) );
        panel.add( new Label( "Classes" ) );
        panel.add( classes );
        panel.add( new Label( "Class name" ) );
        panel.add( class_name );
        panel.add( add_class );
        panel.add( build_models );
        panel.add( classify_gesture );
        panel.add( prepare_training_data );

         
        classes.addItemListener( this ) ;
        add_class.addActionListener( this );
        build_models.addActionListener( this );
        classify_gesture.addActionListener( this );
        prepare_training_data.addActionListener( this );
        pack();
        validate();
    }
 
     
    /**
    *
    * itemStateChanged
    *
    **/
    public void
    itemStateChanged( ItemEvent e )
    {
        Object source = e.getSource();
        
        if ( source == classes ) {
            current_class = classes.getSelectedItem();
         }
    }

     
    /**
    *
    * Handle action events
    *
    **/
    public void
    actionPerformed( ActionEvent e ) 
    {
        String command = e.getActionCommand();
        Object source  = e.getSource();

        if ( source == classify_gesture ) {
            classifyGesture( current_gesture );
            return;
        }
        
        if ( source == build_models ) {
            buildModels();
            return;
        }           
        
        if ( source == prepare_training_data ) {
            prepareTrainingData();
            return;
        }           
        
        if ( source == add_class ) {
            current_class = class_name.getText();
            classes.addItem( current_class );
            return;
        }           
        
        if ( source == class_name ) {
            classes.addItem( class_name.getText() );
            return;
        }
    }
 
     
    /**
    *
    * classifyGesture
    *
    **/
    public void
    classifyGesture( GeneralPath general_path )
    {
        if ( current_class == null ) {
            return;
        }
        
        LoadStoreModule.storeStringInFile(
            "unknown_gesture.unk",
            "0 " + GeneralPathProcessor.featureString( general_path ) + "\n" );
            
        try {
            for ( int i=0; i<classes.getItemCount(); i++ ) {
                String current_class = classes.getItem( i );
                (Runtime.getRuntime().exec( "snf "  + 
                                            "unknown_gesture.unk " +
                                            current_class + ".mdl " +
                                            current_class + ".prd"  )).waitFor();
                                            
                FileInputStream fis = new FileInputStream( current_class + ".prd" );
                 BufferedReader br   = new BufferedReader( 
                                          new InputStreamReader( fis ) );

                  System.out.println( "Result for class " + current_class + " " + br.readLine() );
              }
        }  catch( Exception e ) {
                    System.out.println( e );
        }
    }
 
     
    /**
    *
    * addExample
    *
    **/
    public void
    addExample( GeneralPath general_path )
    {
        if ( current_class == null ) {
            return;
        }
        
        LoadStoreModule.appendStringToFile(
            current_class + ".raw",
            "1 " + GeneralPathProcessor.featureString( general_path ) + "\n" );
    }
 
     
    /**
    *
    * addExample
    *
    **/
    public void
    buildModels()
    {
        try {
            for ( int i=0; i<classes.getItemCount(); i++ ) {
                String current_class = classes.getItem( i );
                (Runtime.getRuntime().exec( "ogr "  + 
                                            current_class + ".trn " +
                                            current_class + ".mdl"  )).waitFor();
             }
        }  catch( Exception e ) {
                    System.out.println( e );
        }
    }
 
     
    /**
    *
    * addExample
    *
    **/
    public void
    prepareTrainingData()
    {
        for ( int i=0; i<classes.getItemCount(); i++ ) {
            String positive_class = classes.getItem( i );
            System.out.println( "Writing training data for " + positive_class );
            
            for ( int j=0; j<classes.getItemCount(); j++ ) {
                try {
                    String negative_class = classes.getItem( j );
                    FileInputStream fis = new FileInputStream( negative_class + ".raw" );
                     BufferedReader br   = new BufferedReader( 
                                              new InputStreamReader( fis ) );

                    String current_line; 
                     while ( ( current_line = br.readLine() ) != null ) {
                        if ( i ==j ) {
                            current_line = current_line + "\n"; 
                        } else {
                            current_line = "-" + current_line + "\n"; 
                        }
                        LoadStoreModule.appendStringToFile(
                            positive_class + ".trn",
                            current_line );
                    }
                 } catch( Exception e ) {
                    System.out.println( e );
                }
            }
        }
    }
}

/**
  *
  * $Log: GestureGUI.java,v $
  * Revision 1.3  2003/11/05 01:42:02  moltmans
  * Found more ^M's  They should all be gone now... Again...  For good?
  *
  * Revision 1.2  2003/03/06 01:08:52  moltmans
  * Added copyright to all the files.
  *
  * Revision 1.1  2001/11/23 03:24:13  mtsezgin
  * *** empty log message ***
  *
  * Revision 1.1  2001/10/12 22:25:52  mtsezgin
  * This is a commit of all files.
  * Shoapid
  * vi sux:q
  *
  * Revision 1.1.1.1  2001/03/29 16:25:00  moltmans
  * Initial directories for DRG
  *
  * Revision 1.10  2000/09/06 22:40:33  mtsezgin
  * Combinations of curves and polygons are successfully approximated
  * by straight lines and Bezier curves as appropriate. System works
  * quite reliably.
  *
  * Revision 1.4  2000/06/08 03:11:46  mtsezgin
  * *** empty log message ***
  *
  * Revision 1.3  2000/06/03 01:52:31  mtsezgin
  * *** empty log message ***
  *
  * Revision 1.2  2000/06/02 21:11:14  mtsezgin
  * *** empty log message ***
  *
  * Revision 1.1  2000/05/26 20:39:25  mtsezgin
  *
  * This GUI lets the user play with the paramaters used by the
  * recognition algorithms.
  *
  * Revision 1.2  2000/05/24 01:53:22  mtsezgin
  *
  * The polygon angle normalization works reliably.
  *
  * Revision 1.1  2000/05/21 23:13:15  mtsezgin
  *
  * This module is used for plotting and investigating certain statistical
  * features.
  *
  *
  *
  **/
