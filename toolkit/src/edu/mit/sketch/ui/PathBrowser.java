// (c) MIT 2003.  All rights reserved.

////////////////////////////////////////////////////////////////////////////////
//                                                                            //
// AUTHOR:      Tevfik Metin Sezgin                                           //
//              Massachusetts Institute of Technology                         //
//              Department of Electrical Engineering and Computer Science     //
//              Artificial Intelligence Laboratory                            //
//                                                                            //
// E-MAIL:        mtsezgin@ai.mit.edu, mtsezgin@mit.edu                       //
//                                                                            //
// COPYRIGHT:   Tevfik Metin Sezgin                                           //
//              All rights reserved. This code can not be copied, modified,   //
//              or distributed in whole or partially without the written      //
//              permission of the author. Also see the COPYRIGHT file.        //
//                                                                            //
////////////////////////////////////////////////////////////////////////////////
package edu.mit.sketch.ui;

  /**
  *
  * See the end of the file for the log of changes.
  *
  * $Author: moltmans $
  * $Date: 2003/10/13 19:46:38 $
  * $Revision: 1.4 $
  * $Headers$
  * $Id: PathBrowser.java,v 1.4 2003/10/13 19:46:38 moltmans Exp $
  * $Name:  $
  * $Locker:  $
  * $Source: /projects/drg/CVSROOT/drg/code/src/edu/mit/sketch/ui/PathBrowser.java,v $
  *
  **/

import javax.swing.JFrame;
import javax.swing.JList;
import javax.swing.ScrollPaneLayout;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Frame;
import java.awt.GridLayout;
import java.awt.Image;
import java.awt.Label;
import java.awt.List;
import java.awt.Menu;
import java.awt.MenuBar;
import java.awt.MenuItem;
import java.awt.Panel;
import java.awt.Scrollbar;
import java.awt.ScrollPane;
import java.awt.Toolkit;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.AdjustmentEvent;
import java.awt.event.AdjustmentListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

import java.util.ArrayList;

import java.awt.geom.AffineTransform;
import edu.mit.sketch.geom.Point;
import edu.mit.sketch.util.Util;
import edu.mit.sketch.ui.JointDisplay;
import edu.mit.sketch.ui.Tablet;
import edu.mit.sketch.toolkit.StatisticsModule;
import edu.mit.sketch.toolkit.SpeedScaleSpaceViewer;
import edu.mit.sketch.toolkit.DirectionChangeScaleSpaceViewer;

/**
  *
  * Browser
  *
  **/

public
class   PathBrowser
extends Frame
{
    private Tablet    tablet;
    private ArrayList segmentations;
    private List      current_segmentations;

    /**
    *
    * The constructor.
    *
    **/
    public PathBrowser( Tablet        tablet, 
                        ArrayList     segmentations, 
                        final boolean using_variable_length_hmms ) // Using this in the ItemListener constructor
    {
        super( "PathBrowser" );
        
        this.tablet        = tablet;
        this.segmentations = segmentations;
        current_segmentations = new List();
        current_segmentations.addItemListener( new ItemListener() {
            public void
            itemStateChanged( ItemEvent e ) {
                if ( using_variable_length_hmms ) {
                    paintVLHMMSegmentation();
                } else {
                    paintSegmentation();
                }
            }
        } );

        for ( int i=0; i<segmentations.size(); i++ ) {
            current_segmentations.add( Util.printArrayConciselyToString( ((int[])segmentations.get(i)), "s"+i ) );
        }

        setLayout( new BorderLayout() );
        
        add( current_segmentations, BorderLayout.CENTER );
        
        pack();
        setVisible( true );
        setLocation( 800, 500 );
        addWindowListener(new WindowAdapter() {
            public void 
            windowClosing(WindowEvent e) {
                dispose();
             }
        });

    }
    
            
    /**
    *
    * Show plausible segmentations starting at the intput index
    *
    **/
    public void
    paintSegmentation()
    {
        int selected_index = current_segmentations.getSelectedIndex();
        
        tablet.paintSegmentation( ((int[])(segmentations.get( selected_index ))) );
    }
    
            
    /**
    *
    * Show plausible segmentations starting at the intput index. 
    * This is called if the HMMs were designed for variable length 
    * input
    **/
    public void
    paintVLHMMSegmentation()
    {
        int selected_index = current_segmentations.getSelectedIndex();
        
        tablet.paintVLHMMSegmentation( ((int[])(segmentations.get( selected_index ))) );
    }
}

/**
  *
  * $Log: PathBrowser.java,v $
  * Revision 1.4  2003/10/13 19:46:38  moltmans
  * Removed bad line endings.
  *
  * Revision 1.3  2003/06/16 17:14:37  mtsezgin
  *
  * Changes to the GUI.
  *
  * Revision 1.2  2003/03/06 01:08:52  moltmans
  * Added copyright to all the files.
  *
  * Revision 1.1  2002/07/22 22:00:08  mtsezgin
  * *** empty log message ***
  *
  *
  **/
