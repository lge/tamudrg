// (c) MIT 2003.  All rights reserved.

////////////////////////////////////////////////////////////////////////////////
//                                                                            //
// AUTHOR:      Tevfik Metin Sezgin                                           //
//              Massachusetts Institute of Technology                         //
//              Department of Electrical Engineering and Computer Science     //
//              Artificial Intelligence Laboratory                            //
//                                                                            //
// E-MAIL:        mtsezgin@ai.mit.edu, mtsezgin@mit.edu                       //
//                                                                            //
// COPYRIGHT:   Tevfik Metin Sezgin                                           //
//              All rights reserved. This code can not be copied, modified,   //
//              or distributed in whole or partially without the written      //
//              permission of the author. Also see the COPYRIGHT file.        //
//                                                                            //
////////////////////////////////////////////////////////////////////////////////
package edu.mit.sketch.ui;

  /**
  *
  * See the end of the file for the log of changes.
  *
  * $Author: mtsezgin $
  * $Date: 2003/11/03 21:19:35 $
  * $Revision: 1.6 $
  * $Headers$
  * $Id: TabletGUIBehavior.java,v 1.6 2003/11/03 21:19:35 mtsezgin Exp $
  * $Name:  $
  * $Locker:  $
  * $Source: /projects/drg/CVSROOT/drg/code/src/edu/mit/sketch/ui/TabletGUIBehavior.java,v $
  *
  **/


import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JToolBar;

import java.awt.BasicStroke;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Frame;
import java.awt.GridLayout;
import java.awt.Image;
import java.awt.Label;
import java.awt.Menu;
import java.awt.MenuBar;
import java.awt.MenuItem;
import java.awt.Panel;
import java.awt.Toolkit;

import java.util.Vector;

import java.awt.geom.AffineTransform;

import java.awt.TextField;

import java.awt.image.BufferedImage;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;

import edu.mit.sketch.geom.Point;
import edu.mit.sketch.geom.Vertex;
import edu.mit.sketch.ui.TabletDataProcessor;
import edu.mit.sketch.util.Util;
import edu.mit.sketch.toolkit.StrokeData;
import edu.mit.sketch.util.GraphicsUtil;
import edu.mit.sketch.toolkit.Blackboard;

import edu.mit.util.ResourceFinder;


/**
  * Acronyms: sdr -> StrokeDataReader, data -> StrokeData
  *
  * This class handles the updates to various frames.
  *
  **/

public
class      TabletGUIBehavior
extends    TabletDataProcessor
implements ActionListener
{
    public static double a_scale                  = 1000*50000*.25*50000*.25;
    public static double v_scale                  = 25*50000*.2;
    public static double t_scale                  = 0.1*.0005;
    public static double theta_scale              = 10;
    public static double d_theta_scale            = 300*.25;
    public static double accumulated_length_scale = 0.5;
    public static float  brush_thickness          = 2.5f;
    
    private Point previous_point;
    
    public boolean display_lsq_vectors = false;
    public boolean show_parsed_objects = false;
    
    public boolean       buffer_ready  = false;
    public BufferedImage buffer;
    public Graphics      buffer_graphics;
    
    public Graphics v_window_graphics;
    public Graphics a_window_graphics;
    public Graphics d_window_graphics; 
    public Graphics fits_window_graphics; 
    public Graphics dd_dt_window_graphics;
    
    public TextField filename_field;

    /**
    *
    * The constructor.
    *
    **/
    public TabletGUIBehavior( int     max_data_points, 
                              String  cursor_image_name,
                              boolean platform_is_windows9x )
    {
        super( max_data_points,
               cursor_image_name,
               platform_is_windows9x );

        filename_field = new TextField();
        
        setRecognitionMode( true );                
        v_menu.addActionListener( this );
        a_menu.addActionListener( this );
        d_menu.addActionListener( this );
        data_menu.addActionListener( this );
        fits_menu.addActionListener( this );
        dd_dt_menu.addActionListener( this );
        gesture_gui_menu.addActionListener( this );
        controls_menu.addActionListener( this );
        statistics_menu.addActionListener( this );
        v_scale_space_menu.addActionListener( this );
        dd_dt_scale_space_menu.addActionListener( this );
        joint_display_menu.addActionListener( this );
        open_menu.addActionListener( this );
        save_menu.addActionListener( this );
        save_as_menu.addActionListener( this );
        quit_menu.addActionListener( this );
        undo_menu.addActionListener( this );
        vectors_menu.addActionListener( this );
        display_menu.addActionListener( this );
        save_features_menu.addActionListener( this );
        derive_snapshots_menu.addActionListener( this );
        derive_properties_menu.addActionListener( this );
        
        //addMouseListener( this );
        constructToolbar();
    }

        
    /**
    *
    * Overloaded for double buffering.
    *
    **/
    public void
    initializeBuffer()
    {
        buffer          = (BufferedImage)(createImage(1280, 1024));
        buffer_graphics = buffer.createGraphics();
        buffer_ready    = true;
    }


    /**
    *
    * Construct the toolbar
    *
    **/
    public void
    constructToolbar() 
    {
        Panel bottom_panel = new Panel();
        bottom_panel.setLayout( new GridLayout( 0, 1 ) );
        bottom_panel.addKeyListener( object_manager );

        Action open_action = 
            new AbstractAction( null,
				ResourceFinder.getIcon("icons/open.gif" )) {
            public void 
            actionPerformed(ActionEvent e) {
            object_manager.doOpen();
            }
        };
        
        Action save_action = 
            new AbstractAction( null,
                ResourceFinder.getIcon("icons/save.gif")) {
            public void 
            actionPerformed(ActionEvent e) {
            object_manager.doSave();
            }
        };
        
        Action quick_open_action = 
            new AbstractAction( null,
                ResourceFinder.getIcon("icons/quick_open.gif")) {
            public void 
            actionPerformed(ActionEvent e) {
                object_manager.openRawVertices( filename_field.getText() );
            }
        };

	Action quick_open_action_drs =
	  new AbstractAction( null,
			      ResourceFinder.getIcon( "icons/quick_open_drs.gif" ) ) {
	    public void actionPerformed( ActionEvent e ) {
	      object_manager.openRawVertices_drg( filename_field.getText() );
	    }
	  };
        
	
        Action quick_save_action = 
            new AbstractAction( null,
                ResourceFinder.getIcon("icons/quick_save.gif")) {
            public void 
            actionPerformed(ActionEvent e) {
                object_manager.saveRawVertices( filename_field.getText() );
            }
        };
        
        Action quick_open_append_action = 
            new AbstractAction( null,
                ResourceFinder.getIcon("icons/quick_open_append.gif")) {
            public void 
            actionPerformed(ActionEvent e) {
                object_manager.openAndAppendData( filename_field.getText() );
            }
        };
        
        Action quick_save_data_action = 
            new AbstractAction( null,
                ResourceFinder.getIcon("icons/quick_save_data.gif")) {
            public void 
            actionPerformed(ActionEvent e) {
                object_manager.saveData( filename_field.getText() );
            }
        };
        
        Action reset_action = 
            new AbstractAction( null,
                ResourceFinder.getIcon("icons/new.gif")) {
            public void 
            actionPerformed(ActionEvent e) {
                object_manager.reset();
                clearScreen();
            }
        };
        
        Action refresh_action = 
            new AbstractAction( null,
                ResourceFinder.getIcon("icons/refresh.gif")) {
            public void 
            actionPerformed(ActionEvent e) {
                repaint();
            }
        };
        
        Action toggle_action = 
            new AbstractAction( null,
                ResourceFinder.getIcon("icons/toggle.gif")) {
            public void 
            actionPerformed(ActionEvent e) {
                toggle();
            }
        };
        
        Action undo_action = 
            new AbstractAction( null,
                ResourceFinder.getIcon("icons/undo.gif")) {
            public void 
            actionPerformed(ActionEvent e) {
                object_manager.undo();
            }
        };
        
        Action print_v_action = 
            new AbstractAction( null,
                ResourceFinder.getIcon("icons/v.gif")) {
            public void 
            actionPerformed(ActionEvent e) {
                String values[];
                values = Util.printToStringArray( data.time, "time" );
                x_list.setListData( values );
                values = Util.printToStringArray( data.speed, "speed" );
                y_list.setListData( values );
                Util.printStrings( values );
            }
        };
        
        Action print_dv_action = 
            new AbstractAction( null,
                ResourceFinder.getIcon("icons/dv.gif")) {
            public void 
            actionPerformed(ActionEvent e) {
                String values[];
                values = Util.printToStringArray( data.time, "time" );
                x_list.setListData( values );
                values = Util.printToStringArray( data.acceleration,
                                                    "acceleration" );
                y_list.setListData( values );
                Util.printStrings( values );
            }
        };
        
        Action print_d_action = 
            new AbstractAction( null,
                ResourceFinder.getIcon("icons/d.gif")) {
            public void 
            actionPerformed(ActionEvent e) {
                String values[];
                values = Util.printToStringArray( data.time, "time" );
                x_list.setListData( values );
                values = Util.printToStringArray( data.d, "d" );
                y_list.setListData( values );
                Util.printStrings( values );
            }
        };
        
        Action print_dd_action = 
            new AbstractAction( null,
                ResourceFinder.getIcon("icons/dd.gif")) {
            public void 
            actionPerformed(ActionEvent e) {
                
                String values[];
                values = Util.printToStringArray( data.time, "time" );
                x_list.setListData( values );
                values = Util.printToStringArray( data.dd_dt, "dd_dt" );
                y_list.setListData( values );
                Util.printStrings( values );
            }
        };
        
        Action recompute_training_data_action = 
            new AbstractAction( null,
                ResourceFinder.getIcon("icons/recompute_training_data.gif")) {
            public void 
            actionPerformed(ActionEvent e) {
                object_manager.tablet.recomputeDataSet( "g:\\research\\hmm_models\\trained_sketch_hmm_model2" );
            }
        };
        
        Action segment_action = 
            new AbstractAction( null,
                ResourceFinder.getIcon("icons/segment.gif")) {
            public void 
            actionPerformed(ActionEvent e) {
                object_manager.tablet.computeAndShowSegmentationWithModel( "g:\\research\\hmm_models\\trained_sketch_hmm_model2" );
            }
        };
        
        Action segment_expanded_action = 
            new AbstractAction( null,
                ResourceFinder.getIcon("icons/segment_expanded.gif")) {
            public void 
            actionPerformed(ActionEvent e) {
                object_manager.tablet.computeAndShowSegmentationWithModel( "g:\\research\\hmm_models\\trained_sketch_hmm_model2_expanded" );
            }
        };
        
        JToolBar toolbar = new JToolBar();
        toolbar.add( open_action       );
        toolbar.add( save_action       );
        toolbar.add( undo_action       );
        toolbar.add( refresh_action    );
        toolbar.add( toggle_action     );
        toolbar.add( reset_action      );
        toolbar.add( print_v_action    );
        toolbar.add( print_dv_action   );
        toolbar.add( print_d_action    );
        toolbar.add( print_dd_action   );
        toolbar.add( filename_field    );
        
        JToolBar toolbar2 = new JToolBar();
        toolbar2.add( quick_open_append_action );
        toolbar2.add( quick_save_data_action );
        toolbar2.add( quick_open_action ); // changed from quick_open_action_drs
	                                   // (Prev. added by Christine)  -Metin
        toolbar2.add( quick_save_action );
        toolbar2.add( segment_action );
        toolbar2.add( segment_expanded_action );
        toolbar2.add( recompute_training_data_action    );

        setLayout( new BorderLayout() );
        Panel status_and_info_panel = new Panel();
        status_and_info_panel.setLayout( new GridLayout( 1, 2 ) );
        status_and_info_panel.add( status_bar  );
        status_and_info_panel.add( info_bar  );
        bottom_panel.add( status_and_info_panel );
        bottom_panel.add( toolbar  );
        bottom_panel.add( toolbar2  );
        
        add( bottom_panel, BorderLayout.SOUTH );
    }

        
    /**
    *
    * Overloaded for double buffering.
    *
    **/
    public void
    paint( Graphics g )
    {
        update( g );
    }

     
    /**
    *
    * Handle action events
    *
    **/
    public void
    actionPerformed( ActionEvent e ) 
    {
        String command = e.getActionCommand();
        
        if ( command == "Quit" ) {
            System.exit( 0 );
        }
        
        if ( command == "Open" ) {
            object_manager.doOpen();
        }
        
        if ( command == "Save as" ) {
            object_manager.doSaveAs();
        }
        
        if ( command == "Save" ) {
            object_manager.doSave();
        }
        
        if ( command == "Undo" ) {
            object_manager.undo();
        }
        
        if ( command == "Reset" ) {
            object_manager.reset();
        }
        
        if ( command == "Refresh" ) {
            repaint();
        }
        
        if ( command == "Speed" ) {
            v_window.setVisible( !v_window.isVisible() );
            v_window_graphics = v_window.getGraphics();
            System.out.println( "Speed window." );
        } 
            
        if ( command == "Acceleration" ) {
            a_window.setVisible( !a_window.isVisible() );
            a_window_graphics = a_window.getGraphics();
            System.out.println( "Acceleration window." );
        }

        if ( command == "  \u0398" ) {
            d_window.setVisible( !d_window.isVisible() );
            d_window_graphics = d_window.getGraphics();
            System.out.println( "Direction window." );
        }

        if ( command == "Fits" ) {
            fits_window.setVisible( !fits_window.isVisible() );
            fits_window_graphics = fits_window.getGraphics();
            System.out.println( "fits window." );
        }
        
        if ( command == "d\u0398/dt" ) {
            dd_dt_window.setVisible( !dd_dt_window.isVisible() );
            dd_dt_window_graphics = dd_dt_window.getGraphics();
            System.out.println( "dd/dt window." );
        }
        
        if ( command == "GestureGUI" ) {
            gesture_gui.setVisible( 
                !gesture_gui.isVisible() );
            System.out.println( "Learning GUI." );
        }
        
        if ( command == "Controls" ) {
            control_module.setVisible( 
                !control_module.isVisible() );
            System.out.println( "Controls module." );
        }
        
        if ( command == "Data" ) {
            data_window.setVisible( 
                !data_window.isVisible() );
            System.out.println( "Data_window." );
        }
        
        if ( command == "Speed Scale Space" ) {
            v_scale_space_viewer.setVisible( 
                !v_scale_space_viewer.isVisible() );
            System.out.println( "Speed scale space menu" );
        }
        
        if ( command == "Direction Change Scale Space" ) {
            dd_dt_scale_space_viewer.setVisible( 
                !dd_dt_scale_space_viewer.isVisible() );
            System.out.println( "Direction change scale space menu" );
        }
        
        if ( command == "Statistics" ) {
            bar_statistics_module.setVisible( 
                !bar_statistics_module.isVisible() );
                
            dsw_statistics_module.setVisible( 
                !dsw_statistics_module.isVisible() );
                
            ddsw_statistics_module.setVisible( 
                !ddsw_statistics_module.isVisible() );
                
            sliding_window_statistics_module.setVisible( 
                !sliding_window_statistics_module.isVisible() );
                
            System.out.println( "Statistics window." );
        }
        
        if ( command == "Display LSQ Vectors" ) {
            display_lsq_vectors = !display_lsq_vectors;
            System.out.println( "Display LSQ Vectors set " + 
                                 display_lsq_vectors );
        }
        
        if ( command == "Show parsed objects" ) {
            toggle();
        }

        if ( command == "Joint Display" ) {
            System.out.println( "Launching Joint Dislpay" );
            joint_display.setVisible( !joint_display.isVisible() );
        }
        
        if ( command == "Derive Properties" ) {
            System.out.println( "Deriving Properties" );
            object_manager.deriveProperties();
        }
        
        if ( command == "Derive Snapshots" ) {
            System.out.println( "Deriving Snapshots" );
            object_manager.deriveSnapshots();
        }
        
        if ( command == "Save Feature Vectors" ) {
            System.out.println( "Saving Feature Vectors" );
            object_manager.saveFeatureVectors( "features" );
        }
    }

    
    /**
    *
    * toggle the state related with showing the parsed objects.
    *
    **/
    public void
    toggle()
    {
        show_parsed_objects = !show_parsed_objects;
        object_manager.show_parsed_objects = show_parsed_objects;
        clearScreen();
        object_manager.paint( getGraphics() );
        System.out.println( "Show parsed objects set " + 
                             show_parsed_objects );
    }


    /**
    *
    * Handle mouse events
    *
    **/
    public void
    mouseReleased( MouseEvent e )
    {
        super.mouseReleased( e );
    }

    
    /**
    *
    * Handle mouse events
    *
    **/
    public void
    mouseClicked( MouseEvent e ) 
    {
        System.out.println( "mouseClicked()" );
        super.mouseClicked( e );
    }

    
    /**
    *
    * Handle mouse events
    *
    **/
    public void
    mouseEntered( MouseEvent e ) 
    {
        super.mouseEntered( e );
    }

    
    /**
    *
    * Handle mouse events
    *
    **/
    public void
    mouseExited( MouseEvent e ) 
    {
        super.mouseExited( e );
    }

    
    /**
    *
    * Handle mouse events
    *
    **/
    public void
    mousePressed( MouseEvent e ) 
    {
        System.out.println( "mouse pressed" );
        previous_point = new Point( e.getPoint() );
        super.mousePressed( e );
    }
    

    /**
    *
    * Handle mouse motion events
    *
    **/
    public void
    mouseDragged( MouseEvent e ) 
    {
        super.mouseDragged( e );
         Point point = new Point( e.getPoint() );
 
         Graphics2D g = (Graphics2D)getGraphics();
         g.setStroke( new BasicStroke( brush_thickness ) );
        
        if ( in_recognition_mode ) {
             g.setColor( Color.gray );
        } else {
             g.setColor( Color.blue );
          }
        
         g.drawLine( previous_point.x,
                     previous_point.y,
                     point.x,
                     point.y );
         previous_point = point;
    }


    /**
    *
    * Handle mouse motion events
    *
    **/
    public void
    mouseMoved( MouseEvent e )
    {
        super.mouseMoved( e );
    }
    
    
    /**
    *
    * Paint the vertices along with the path combining them
    *
    **/
    public void 
    displayVertices( Vertex vertices[] )
    {
        Graphics2D g = (Graphics2D)getGraphics();
        
        g.setColor( Color.green );
                
        for ( int i=0; i<vertices.length-1; i++ ) {
            g.drawLine( vertices[i].x,
                        vertices[i].y,
                        vertices[i+1].x,
                        vertices[i+1].y );
            System.out.println( vertices[i] );
        }
    }
    
    
    /**
    *
    * Paint the vertices along without the path combining them
    *
    **/
    public void 
    displayVerticesAsDots( Vertex vertices[] )
    {
        Graphics2D g = (Graphics2D)getGraphics();
        
        g.setColor( Color.black );
                
        for ( int i=0; i<vertices.length; i++ ) {
            g.drawLine( vertices[i].x,
                        vertices[i].y,
                        vertices[i].x,
                        vertices[i].y );
        }
    }
    
    
    /**
    *
    * displayFitByIndices
    *
    **/
    public void 
    displayFitByReversedIndices( int indices[] )
    {
        Graphics2D g = (Graphics2D)getGraphics();
        
        update( g );

        g.setColor( Color.green );
                
        for ( int i=indices.length-1; i>0; i-- ) {
            g.drawLine( data.vertices[indices[i]].x,
                        data.vertices[indices[i]].y,
                        data.vertices[indices[i-1]].x,
                        data.vertices[indices[i-1]].y );
        }
        
        g.setColor( Color.red );
                
        for ( int i=0; i<indices.length; i++ ) {
            g.drawOval( data.vertices[indices[i]].x-2,
                        data.vertices[indices[i]].y-2,
                        4,
                        4);
        }
    }
    
    
    /**
    *
    * displayFitByPositions
    *
    **/
    public void 
    displayFitByPositions( double x[], double y[], int indices[] )
    {
        Graphics2D g = (Graphics2D)getGraphics();
        
        g.setColor( Color.cyan );
                
        for ( int i=0; i<indices.length-1; i++ ) {
            g.drawLine( (int)x[indices[i]],   (int)y[indices[i]], 
                        (int)x[indices[i+1]], (int)y[indices[i+1]] );
         }
        
        g.setColor( Color.red );
                
        for ( int i=0; i<indices.length; i++ ) {
            g.drawOval( (int)x[indices[i]]-3,   (int)y[indices[i]]-3,
                        6,
                        6);
        }
        g.setColor( Color.orange );
        for ( int i=0; i<x.length-1; i++ ) {
            g.drawLine( (int)x[i], (int)y[i], (int)x[i+1], (int)y[i+1] );
        }
        
    }
    
    
    /**
    *
    * displayFitByIndices
    *
    **/
    public void 
    displayFitByIndices( int indices[] )
    {
        Graphics2D g = (Graphics2D)getGraphics();
        
        update( g );

        g.setColor( Color.green );
                
        for ( int i=0; i<indices.length-1; i++ ) {
            g.drawLine( data.vertices[indices[i]].x,
                        data.vertices[indices[i]].y,
                        data.vertices[indices[i+1]].x,
                        data.vertices[indices[i+1]].y );
        }
        
        g.setColor( Color.red );
                
        for ( int i=0; i<indices.length; i++ ) {
            g.drawOval( data.vertices[indices[i]].x-2,
                        data.vertices[indices[i]].y-2,
                        4,
                        4);
        }
    }
    
    
    /**
    *
    * displayFitByIndices
    *
    **/
    public void 
    displayFitByIndices( int indices[], Color edge_color, Color vertex_color )
    {
        Graphics2D g = (Graphics2D)getGraphics();
        
        g.setColor( edge_color );
                
        for ( int i=0; i<indices.length-1; i++ ) {
            g.drawLine( data.vertices[indices[i]].x,
                        data.vertices[indices[i]].y,
                        data.vertices[indices[i+1]].x,
                        data.vertices[indices[i+1]].y );
        }
        
        g.setColor( vertex_color );
                
        for ( int i=0; i<indices.length; i++ ) {
            g.drawOval( data.vertices[indices[i]].x-4,
                        data.vertices[indices[i]].y-4,
                        8,
                        8 );
        }
    }


    /**
    *
    * Update various graphs and statistics.
    *
    **/
    public void 
    update( Graphics g )
    {
        clearScreen();
        initializeGraphicsObjects();

        if ( data != null ) {
            paintVelocity( data, Color.blue );
            paintAcceleration( data, Color.blue );
            paint_d( data, Color.blue );
            paint_dd_dt( data, Color.blue );
        }
        
        update_fits();

        if ( dsw_statistics_module.isVisible() ) {
            dsw_statistics_module.plotSlidingWindowGraph( 
                data.dd_dt,
                -.01,
                .01, 
                .0001,
                .001 );

            ddsw_statistics_module.plotBarGraph(
                data.dd_dt, 
                -Math.PI,
                Math.PI, 
                Math.PI/180 );
        }
        object_manager.paint(g);
    }


    /**
    *
    * Update the velocity graph.
    *
    **/
    public void
    paintVelocity( StrokeData data, Color color )
    {
        if ( !v_window.isVisible() ) {
            return;
        }

        GraphicsUtil.clearComponent( v_window );
        
        AffineTransform affine_transform = new 
            AffineTransform( t_scale, 
                             0, 
                             0, 
                             -v_scale, 
                             40, 
                             v_window.getSize().height/2 );

        double speed[]        = data.speed;
        long   time[]         = data.time;
        double scaled_average = data.averageSpeed()*speed_average_scale;
        double arg0[]         = { time[0],        time[time.length-1] };
        double arg1[]         = { scaled_average, scaled_average      };
        paintFvsG( time, speed, color,      v_window, affine_transform );
        paintFvsG( arg0, arg1,  Color.red,  v_window, affine_transform );
    }


    /**
    *
    * Update the acceleration graph.
    *
    **/
    public void
    paintAcceleration( StrokeData data, Color color )
    {
        if ( !a_window.isVisible() ) {
            return;
        }
        
        GraphicsUtil.clearComponent( a_window     );
        
        AffineTransform affine_transform = new 
            AffineTransform( t_scale, 
                             0, 
                             0, 
                             -a_scale, 
                             40, 
                             a_window.getSize().height/2 );
        
        double acceleration[] = data.acceleration;
        long   time[]         = data.time;

        paintFvsG( time, acceleration, color, a_window, affine_transform );
    }

    
    /**
    *
    * Update the direction graph.
    *
    **/
    public void
    paint_d( StrokeData data, Color color )
    {
        if ( !d_window.isVisible() ) {
            return;
        }

        GraphicsUtil.clearComponent( d_window );
        
        AffineTransform affine_transform = new 
            AffineTransform( t_scale, 
                             0, 
                             0, 
                             -theta_scale, 
                             40, 
                             d_window.getSize().height/2 );
        
        double d[]     = data.d;
        long   time[]  = data.time;
        double average = data.averageFinG( d, time );
        double arg0[]  = { time[0], time[time.length-1] };
        double arg1[]  = { average, average                };
        paintFvsG( time, d,    color,      d_window, affine_transform );
        paintFvsG( arg0, arg1, Color.red,  d_window, affine_transform  );
    }


    /**
    *
    * Update change in direction
    *
    **/
    public void
    paint_dd_dt( StrokeData data, Color color )
    {
        if ( !dd_dt_window.isVisible() ) {
            return;
        }

        GraphicsUtil.clearComponent( dd_dt_window );

        AffineTransform affine_transform = new 
            AffineTransform( t_scale, 
                             0, 
                             0, 
                             -d_theta_scale, 
                             40, 
                             dd_dt_window.getSize().height/2 );
                             
        double dd_dt[]     = data.dd_dt;
        long   time[]      = data.time;
        double abs_dd_dt[] = new double[data.dd_dt.length];
        
        for ( int i=0; i<abs_dd_dt.length; i++ ) {
            abs_dd_dt[i] = Math.abs( dd_dt[i] );
        }
        
        double scaled_average = data.averageAbsoluteFinG( dd_dt, time )*
                                dd_dt_average_scale;
        double arg0[] = { time[0],         time[time.length-1] };
        double arg1[] = { scaled_average,  scaled_average      };
        paintFvsG( time, abs_dd_dt, color,     dd_dt_window, affine_transform );
        paintFvsG( arg0, arg1,  Color.red, dd_dt_window, affine_transform );
    }
    
    
    /**
    *
    * Paint f vs g in the frame using the input color and affine 
    * transform. f is on the horizontal axis.
    *
    **/
    public void
    paintFvsG( double          f[], 
               double          g[], 
               Color           color,
               Component       component,
               AffineTransform transform )
    {
        Graphics2D graphics = (Graphics2D)component.getGraphics();
        int        length   = ( f.length < g.length ) ? f.length : g.length;
        double     input[]  = new double[length*2];

        for ( int i=0; i<length; i++ ) {
            input[ i*2 ] = f[i];
            input[i*2+1] = g[i];
        }
        
        transform.transform( input, 0, input, 0, length );
        graphics.setColor( color );

        for ( int i=0; i<length-1; i++ ) {
            graphics.drawLine( (int)input[i*2],   (int)input[i*2+1],
                               (int)input[i*2+2], (int)input[i*2+3] );
        }
        
        // Axis
        int max_window_size = (int)( component.getSize().width/
                                     transform.getShearX() +
                                     component.getSize().height/
                                     transform.getShearY() );
        input               = new double[10];
        
        input[0] = -max_window_size;
        input[1] = 0;
        
        input[2] = max_window_size;
        input[3] = 0;
        
        input[4] = 0;
        input[5] = 0;
        
        input[6] = 0;
        input[7] = max_window_size;
        
        input[8] = 0;
        input[9] = -max_window_size;
        
        transform.transform( input, 0, input, 0, 5 );

        graphics.setColor( Color.black );
        
        for ( int i=0; i<4; i++ ) {
            graphics.drawLine( (int)input[i*2],   (int)input[i*2+1],
                               (int)input[i*2+2], (int)input[i*2+3] );
        }
    }
    
    
    /**
    *
    * Paint f vs g in the frame using the input color and affine 
    * transform. f is on the horizontal axis.
    *
    **/
    public void
    paintFvsG( long            f[], 
               double          g[], 
               Color           color,
               Component       component,
               AffineTransform transform )
    {
        double f_copy[] = new double[f.length];
        
        for ( int i=0; i<f.length; i++ ) {
            f_copy[i] = (double)f[i];
        }
        paintFvsG( f_copy, g, color, component, transform );        
    }


    /**
    *
    * Update the info about the various fits (LSQE etc.)
    *
    **/
    public void
    update_fits()
    {
        int window_width  = fits_window.getSize().width;
        int window_height = fits_window.getSize().height;

        if ( ( Blackboard.hybrid_fits == null ) ||
             ( fits_window_graphics   == null ) )
            return;
            
        fits_window_graphics.setColor( Color.white );
        GraphicsUtil.clearComponent( fits_window );
        
        int    length     = Blackboard.hybrid_fits.length;
        int    origin_x   = window_width/10;
        int    origin_y   = window_height*9/10;
        int    dx         = ((window_width-origin_x)/length)-1;
        int    max_height = window_height*5/10;
        double max_error  = Blackboard.hybrid_fits[length-1].getLSQError();

        fits_window_graphics.setColor( Color.black );
        fits_window_graphics.drawLine( 0,   origin_y, 
                                       500, origin_y );
        fits_window_graphics.setColor( Color.blue );
        Util.printArray(  Blackboard.hybrid_fits, "hybrid_fits" );
        for ( int i=0; i<length-1; i++ ) {
            fits_window_graphics.drawLine( 
                origin_x + dx*i,   
                (int)(origin_y - Blackboard.hybrid_fits[i].getLSQError() * 
                    max_height/max_error),
                origin_x + dx*(i+1), 
                (int)(origin_y - Blackboard.hybrid_fits[i+1].getLSQError() * 
                    max_height/max_error) );
        }
    }


    /**
    *
    * Change the fit method, rederive the direction and related
    * entities. Also update the graphs.
    *
    **/
    public void
    setFitMehod( int method )
    {
        fit_method = method;
        
        data.setFitMethod( method );
        
        if ( graph_color == Color.blue ) {
            graph_color = Color.cyan;
        } else { 
            graph_color = Color.blue;
        }
        
        paint_d( data, graph_color );
        paint_dd_dt( data, graph_color );
    }

    
    /**
    * 
    * Do convolution.
    *
    **/
    public void
    convolveDirection()
    {
        double filter[] = new double[5];

        filter[0] = 1.0/16.0;
        filter[1] = 4.0/16.0;
        filter[2] = 6.0/16.0;
        filter[3] = 4.0/16.0;
        filter[4] = 1.0/16.0;
        
        data.convolveDirection( filter );
        
        if ( graph_color == Color.blue ) {
            graph_color = Color.cyan;
        } else { 
            graph_color = Color.blue;
        }
        paint_d( data, graph_color );
        paint_dd_dt( data, graph_color );
    }

    
    /**
    * 
    * Do convolution.
    *
    **/
    public void
    convolveChangeInDirection()
    {
        double filter[] = new double[5];

        filter[0] = 1.0/16.0;
        filter[1] = 4.0/16.0;
        filter[2] = 6.0/16.0;
        filter[3] = 4.0/16.0;
        filter[4] = 1.0/16.0;
        
        data.convolveChangeInDirection( filter );
        
        if ( graph_color == Color.blue ) {
            graph_color = Color.cyan;
        } else { 
            graph_color = Color.blue;
        }
        paint_dd_dt( data, graph_color );
    }


    
    /**
    * 
    * Zoom in all window's y axis by k
    *
    **/
    public void
    zoomY( double k )
    {
        a_scale       *= k;
        v_scale       *= k;
        theta_scale   *= k;
        d_theta_scale *= k;
    }


    
    /**
    * 
    * Zoom in all window's X axis by k
    *
    **/
    public void
    zoomX( double k )
    {
        t_scale       *= k;
    }
}

/**
  *
  * $Log: TabletGUIBehavior.java,v $
  * Revision 1.6  2003/11/03 21:19:35  mtsezgin
  *
  * Undid some temporary testing stuff previously added by Christine. Also
  * modified the class to load the icon images from a jar file.
  *
  * Revision 1.5  2003/06/26 19:57:15  calvarad
  * Lots of bug fixes
  *
  * Revision 1.4  2003/06/16 17:14:37  mtsezgin
  *
  * Changes to the GUI.
  *
  * Revision 1.3  2003/03/06 01:08:52  moltmans
  * Added copyright to all the files.
  *
  * Revision 1.2  2002/07/22 21:00:41  mtsezgin
  * Modified to make it work with the new classifier: SimpleClassifier2
  *
  * Revision 1.1  2001/11/23 03:24:13  mtsezgin
  * *** empty log message ***
  *
  * Revision 1.3  2001/10/12 22:25:53  mtsezgin
  * This is a commit of all files.
  * Shoapid
  * vi sux:q
  *
  * Revision 1.2  2001/04/05 19:02:53  mtsezgin
  * The files needed to make the dll.
  *
  * Revision 1.1.1.1  2001/03/29 16:25:01  moltmans
  * Initial directories for DRG
  *
  * Revision 1.3  2000/09/06 22:41:06  mtsezgin
  * Combinations of curves and polygons are successfully approximated
  * by straight lines and Bezier curves as appropriate. System works
  * quite reliably.
  *
  *
  *
  **/
