// (c) MIT 2003.  All rights reserved.

// $Id: Debug.java,v 1.10 2003/03/06 01:08:54 moltmans Exp $
// Debug.java -- 
// Author: Michael Oltmans <moltmans@ai.mit.edu> 
// Created: <Fri Feb  4 13:26:20 2000> 

package edu.mit.util;


import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Frame;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.HashMap;
import javax.swing.Icon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JScrollBar;
import javax.swing.JScrollPane;
import javax.swing.JTabbedPane;
import javax.swing.JTextPane;
import javax.swing.SwingUtilities;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import javax.swing.text.AttributeSet;
import javax.swing.text.BadLocationException;
import javax.swing.text.DefaultStyledDocument;
import javax.swing.text.SimpleAttributeSet;
import javax.swing.text.StyleConstants;

/**
 * This class provides a mechanism for displaying debug output in
 * separate panels of a tabbed window.  The target page of the panel
 * is specified by a string.  <p>

 * For example:
 * <code>Debug.println( "speed info", speedinfoObj )</code><p>
 *
 * In addition buttons can be added to the debug window with the
 * {@link #addButton(String title, JButton button)} or
 * {@link #addButton(String title, String buttonName, ActionListener listener)}
 * methods.  This is useful for having simple on demand
 * output such as printing the value of a variable or performing
 * actions that are not performed outside of a debuging context.<p>
 *
 * The frame will be opened the first time that it is accessed.
 *
 * See the {@link #main main} function for a demonstration.
 **/
public class Debug extends JFrame
{
  private static Debug m_theDebug;
  private static boolean m_debug = true;
  private static HashMap m_panels = new HashMap();

  protected static final String NEWLINE =
    System.getProperty("line.separator");


  private JTabbedPane m_tabs;

  /**
   * Print obj to the debug panel named title.  If no such panel
   * exists, one will be created and added to the tab.
   **/
  public static void print( String title, Object obj ) 
  {
    if ( m_debug ) {
      getDebug( title ).print( obj );
      m_theDebug.markDirty( title );
    }
  }
  
  /**
   * Print obj to the debug panel named title followed by a newline.
   * If no such panel exists, one will be created and added to the
   * tab.
   **/
  public static void println( String title, Object obj )
  {
    print( title, obj + NEWLINE );
  }

  /**
   * Println to the note tab.
   **/
  public static void note( String msg ) 
  {
    println( "Notes", msg );
  }

  /**
   * println to the Error tab and bring the debug window to the front.
   **/
  public static void error( String msg ) 
  {
    println( "Errors", msg );
    makeActive( "Errors" );
  }
  
  public static void makeActive( final String title ) 
  {
    if ( m_debug ) {
      SwingUtilities.invokeLater( new Runnable() {
	  public void run() {
	    JPanel panel = getDebug( title );
	    m_theDebug.m_tabs.setSelectedComponent( panel );
	    makeActive();
	  }
	} );
    }
  }

  public static void makeActive()
  {
    if ( m_debug ) {
      SwingUtilities.invokeLater( new Runnable() {
	  public void run() {
	    JFrame frame = getDebugFrame();
	    frame.setState(Frame.NORMAL);
	    frame.setVisible( true );
	    frame.toFront();
	  }
	} );
    }
  }

  public static void toggleVisible() 
  {
    SwingUtilities.invokeLater( new Runnable() {
	public void run() {
	  JFrame frame = getDebugFrame();
	  if( frame != null ) {
	    if( frame.isShowing() ) {
	      frame.setVisible( false );
	    }
	    else {
	      frame.setState( Frame.NORMAL );
	      frame.setVisible( true );
	      frame.toFront();
	    }
	  }
	}
      } );
  }
  
  /**
   * Print an exception to the Error tab and bring the debug window to the front.
   *
   * @param exception a value of type {@link Throwable}
   */
  public static void error( Throwable exception ) 
  {
      StringWriter sw = new StringWriter();
      PrintWriter pw = new PrintWriter( sw );
      exception.printStackTrace( pw );
      
      error( exception.toString() + NEWLINE + sw.toString() );

  }

  public static void clear( String title )
  {
    if ( m_debug ) {
      getDebug( title ).clear();
    }
    
  }

  /**
   * Add a button to the bottom of the debug pane of the specified title.
   **/
  public static void addButton(String title, JButton button) 
  {
    if ( m_debug ) {
      getDebug( title ).addButton( button );
    }
  }

  /**
   * Provide a listener and a button name and the Debug window will
   * create a button (at the bottom of the specified panel) and attach
   * the listener to it.  This is usefull if you need to add one
   * button to several panels.  e.g.:<br>
   *
   * <pre>
   * ActionListener listener = new ActionListener() {
   *     public void actionPerformed( ActionEvent e ) 
   *     {
   *         Test.test();
   *     }
   * };
   *
   * Debug.addButton( "Random1", "run test", listener );
   * Debug.addButton( "Random2", "run test", listener );
   * Debug.addButton( "Random3", "run test", listener );
   * </pre>
   **/
  public static void addButton(String title, String buttonName,
                               ActionListener listener )
  {
    JButton b = new JButton( buttonName );
    b.addActionListener( listener );
    addButton( title, b );
  }
  

  /**
   * Get the frame that houses the debug pane.
   **/
  protected static JFrame getDebugFrame() 
  {
    if ( m_theDebug == null ) {
      m_theDebug = new Debug();
    }
    return m_theDebug;
  }

  public static void setDebugging( boolean value ) 
  {
    m_debug = value;
  }

  public static boolean isDebugging( boolean value ) 
  {
    return m_debug;
  }

  private static DebugPanel getDebug( String title ) 
  {
    if ( m_theDebug == null ) {
      m_theDebug = new Debug();
    }
    
    return m_theDebug.getDebugPanel( title );
  }
  
  
//////////////////////////////////////////////////////////////////////

  protected Debug() {
    this( "Debug Window" );
  }
  

  protected Debug( String title ) {
    super( title );

    m_tabs = new JTabbedPane();
    m_tabs.addMouseListener(new MouseAdapter() {
        public void mouseClicked(MouseEvent e) {
          Icon i = m_tabs.getIconAt(m_tabs.getSelectedIndex());
          ((DotIcon)i).setColor(Color.green);
          m_tabs.repaint();
        }
      });
      
    m_tabs.addChangeListener(new ChangeListener() {
        int state = 0;
        public void stateChanged(ChangeEvent e) {
          Icon i = m_tabs.getIconAt(m_tabs.getSelectedIndex());
          ((DotIcon)i).setColor(Color.green);
        }
      });

    addWindowListener( new WindowAdapter() {
	public void windowClosing( WindowEvent e ) {
	  hide();
	}
      } );
    
    getContentPane().add(m_tabs, BorderLayout.CENTER);
    setSize(400, 500);
    show();
  }

  protected DebugPanel getDebugPanel( final String title ) 
  {
    DebugPanel panel = (DebugPanel)m_panels.get( title );

    if ( panel != null ) {
      return panel;
    }

    final DebugPanel debugPanel = new DebugPanel( title );
    m_panels.put( title, debugPanel );
    
    SwingUtilities.invokeLater( new Runnable() {
	public void run() {
	  m_tabs.addTab(title, new DotIcon(Color.red), debugPanel);
	  m_tabs.setSelectedComponent(debugPanel);
	}
      } );
    
    return debugPanel;
  }

  void markDirty( final String title ) 
  {
    SwingUtilities.invokeLater( new Runnable() {
	public void run() {
	  int index = m_tabs.indexOfTab( title );
	  if ( m_tabs.getSelectedIndex() != index ) {
	    ((DotIcon)m_tabs.getIconAt( index )).setColor( Color.red );
	    m_tabs.repaint();
	  }
	}
      } );
  }

  /** Test out the debug windows **/
  public static void main( String[] args )
  {
    Debug.println( "out1", "  this is a comment" );
    Debug.println( "out1", "this is another comment" );

    for( int i = 1; i< 3; i++ ) {
      Debug.println( "out2", "other comments go here" );
      Debug.println( "out2", "it is like the second nature...:-)" );
    }

    JButton b = new JButton("out1");
    b.addActionListener(new ActionListener() {
        public void actionPerformed(ActionEvent e) {
          Debug.println("out1", "more output");
        }
      });

    JButton c = new JButton("out2");
    c.addActionListener(new ActionListener() {
        public void actionPerformed(ActionEvent e) {
          Debug.println("out2", "more output");
        }
      });

    JButton d = new JButton("clear");
    d.addActionListener(new ActionListener() {
        public void actionPerformed(ActionEvent e) {
          Debug.clear("out1");
        }
      });

    Debug.addButton("out2", b);
    Debug.addButton("out1", c);
    Debug.addButton("out1", d);
  }
  
} //Debug

class DebugPanel extends JPanel {

  private JTextPane m_pane;
  private JPanel m_buttonBar;
  private JScrollBar m_scrollBar;
  private DefaultStyledDocument m_doc;
  private String m_title;
  private AttributeSet m_attr;

  DebugPanel( String title )
  {
    m_title = title;
    m_doc = new DefaultStyledDocument();
    m_pane = new JTextPane(m_doc);
    m_buttonBar = new JPanel();

    JScrollPane scrollPane = new JScrollPane( m_pane );
    m_scrollBar = scrollPane.getVerticalScrollBar();

    setLayout(new BorderLayout());
    add( scrollPane, BorderLayout.CENTER );
    add( m_buttonBar, BorderLayout.SOUTH );

    m_pane.setEditable( false );
    SimpleAttributeSet m_attr = new SimpleAttributeSet();
    StyleConstants.setFontFamily(m_attr, "courier");
    StyleConstants.setFontSize(m_attr, 12);

    clear();
  }

  void print( Object obj ) 
  {
    String text = obj.toString().replace('\n', Debug.NEWLINE.charAt(0) );
    try {
      m_doc.insertString( m_doc.getLength(), obj.toString(), m_attr );
      SwingUtilities.invokeLater( new Runnable() {
	  public void run() {
	    m_scrollBar.setValue( m_scrollBar.getMaximum() );
	  }
	} );
      repaint();
    }
    catch ( BadLocationException ble ) {
    }
  }

  void clear()
  {
    try {
      m_doc.remove( 0, m_doc.getLength() );
    }
    catch ( BadLocationException ble ) {
    }
  }

  void addButton( JButton button )
  {
    m_buttonBar.add(button);
    validate();
  }

}// DebugPanel
