// (c) MIT 2003.  All rights reserved.

package edu.mit.util;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.net.MalformedURLException;
import java.net.URL;
import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JOptionPane;

/**
 * Get resources from the classpath.
 * <p>
 * Copyright: Copyright (C) 2001 by MIT.  All Rights Reserved
 * Created: Thu May 24 16:47:52 2001
 *
 * @author <a href="mailto:moltmans@ai.mit.edu">Michael Oltmans</a>
 * @version $Id: ResourceFinder.java,v 1.5 2003/09/18 21:01:43 moltmans Exp $
 **/
public class ResourceFinder 
{

  /**
   * Searches in the classpath for a named resource.  Paths should be
   * rooted in a directory on the classpath and should be separated
   * with '/' characters regardless of the target os.
   **/
  public static URL getResourceURL( String resource )
  {
    return ClassLoader.getSystemResource(resource);
  }

  /**
   * Get an icon with the given textual description and the given path
   * to its image.  There are a number of standard icons in the
   * lib/jlfgr-1_0.jar file.  To use them, simply include the path
   * inside the jar file of the icon and this method will extract it
   * from the jar (the jar file must be in the classpath though...).
   *
   * @see <a href="http://developer.java.sun.com/developer/techDocs/hi/repository/">The Java look and feel graphics repository</a>
   **/
  public static Icon getIcon( String text, String iconResource )
  {
    ImageIcon icon = null;
    URL iconURL = getResourceURL( iconResource );
    if (iconURL != null) {
      return new ImageIcon(iconURL, text );
    }
    return null;
  }
  
  /**
   * Get an icon with the given textual description and the given path
   * to its image.  There are a number of standard icons in the
   * lib/jlfgr-1_0.jar file.  To use them, simply include the path
   * inside the jar file of the icon and this method will extract it
   * from the jar (the jar file must be in the classpath though...).
   *
   * @see <a href="http://developer.java.sun.com/developer/techDocs/hi/repository/">The Java look and feel graphics repository</a>
   **/
  public static Icon getIcon( String iconResource )
  {
    return getIcon(null, iconResource);
  }

  public static void main( String[] args )
  {

    Debug.addButton( "Resources", "find", new ActionListener() {
	public void actionPerformed( ActionEvent e ) 
	{
	  String resource = null;
	  while ( !"q".equals( resource ) && ! "quit".equals( resource ) ) {
	    resource =
	      JOptionPane.showInputDialog("Please enter the resource");
	    
	    if ( resource == null ) {
	      System.exit(1);
	    }
      
	    Debug.note( resource );
	    Debug.note( "URL:  " + getResourceURL( resource) + "\n" );
	  }
	}
      } );

    Debug.makeActive( "Resources" );
  }
 
}
