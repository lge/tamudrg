// (c) MIT 2003.  All rights reserved.

// $Id: DotIcon.java,v 1.3 2003/03/06 01:08:55 moltmans Exp $
// DotIcon.java -- 
// Author: Michael Oltmans <moltmans@ai.mit.edu> 
// Created: <Sun Feb 04 17:58:10 2001> 
// Time-stamp: <2001-12-06 13:34:27 moltmans> 
package edu.mit.util;

import java.awt.*;
import javax.swing.Icon;

/**
 * DotIcon.java
 *
 *
 * Created: Sun Feb 04 17:58:16 2001
 *
 * @author <a href="mailto:moltmans@ai.mit.edu">Michael Oltmans</a>
 * @version $Id: DotIcon.java,v 1.3 2003/03/06 01:08:55 moltmans Exp $
 */

public class DotIcon implements Icon {
  private int m_width;
  private int m_height;
  private Color m_color;
    
  public DotIcon (Color color) {
    this(color, 10);
  }
  public DotIcon(Color color, int size)
  {
    m_color = color;
    m_width = m_height = size;
  }

  public int getIconHeight() 
  {
    return m_height;
  }

  public int getIconWidth()
  {
    return m_width;
  }

  public void setColor(Color color) 
  {
    m_color = color;
  }

  public void paintIcon(Component c, Graphics g, int x, int y)
  {
    g.setColor(m_color);
    g.fillOval(x, y, m_width, m_height);
  }
  
  
}// DotIcon
