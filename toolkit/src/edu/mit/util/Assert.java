// (c) MIT 2003.  All rights reserved.

// $Id: Assert.java,v 1.3 2003/03/06 01:08:54 moltmans Exp $
// Assert.java -- 
// Author: Michael Oltmans <moltmans@ai.mit.edu> 
// Created: <Thu May 24 12:17:25 2001> 
// Time-stamp: <2001-05-24 18:54:41 moltmans> 
package edu.mit.util;

import java.io.StringWriter;
import java.io.PrintWriter;
import javax.swing.JFrame;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

/**
 * Assert is a library of assertion routines.
 */
public final class Assert
{
  
  public static boolean m_window = true;
  
  /**
   * If the argument is true do nothing.  If the argument is false
   * then throw an execption.
   **/
  public static void isTrue( boolean b )
  {
    isTrue( b, "" );
  }

  /**
   * If the argument is true do nothing.  If the argument is false
   * then throw an execption with msg embeded in it.
   **/
  public static void isTrue( boolean b, String msg )
  {
    if ( b ) {
      return;
    }
  
    RuntimeException exception =
      new RuntimeException( "AssertedErrorException" );

    if( m_window ) {
      StringWriter sw = new StringWriter();
      PrintWriter pw = new PrintWriter( sw );
      exception.printStackTrace( pw );
      
      Debug.error( msg + "\n" + sw.toString() );
    }
    throw( exception );
  }

  /**
   * If the argument is not null do nothing.  If the argument is null
   * then throw an execption with msg embeded in it.
   **/
  public static void notNull( Object o, String msg )
  {
    isTrue( o != null, msg );
  }

  /**
   * If the argument is not null do nothing.  If the argument is null
   * then throw an execption.
   **/
  public static void notNull( Object o )
  {
    notNull( o, "" );
  }

  /**
   * Make a note if b is false.
   **/
  public static void note( boolean b, String msg )
  {
    if( !b ) {
      if( m_window ) {
	Debug.note(msg);
      }
    }
  }


  public static void main( String[] args ) 
  {
    Assert.isTrue( true, "No error here!" );
    Assert.isTrue( false, "Error here!" );
    Assert.note( true, "No notes" );
    Assert.note( false, "Yes!  Notes!" );
    
    JFrame f = new JFrame("Test");
    JButton b = new JButton( "test button" );
    f.getContentPane().add( b );
    b.addActionListener( new ActionListener() {
	public void actionPerformed( ActionEvent e ) 
	{
	  Assert.notNull( "Test", "But it's not null!" );
	  Assert.notNull( null, "Thank goodness it is null!" );
	}
      } );
    f.pack();
    f.show();
  }
  

}

