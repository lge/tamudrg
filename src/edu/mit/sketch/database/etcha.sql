\pset format unaligned
\pset tuples_only
\pset fieldsep '\t'

-- select study, author, sketch, st.id, path, times
--   from strokes as st,  sketches as sk, studies as t
--   where st.sketch = sk.id and sk.study = t.id
-- \g strokes.dat

-- select id, name from classes as c
--   join (select distinct(class) 
--           from hand_classified_strokes
--           where condition <> 'drg isa labeling') as h
--   on (h.class = c.id)
--   order by c.id
-- \g classes.dat


select sketches.id, author, studies.name  
  from sketches, studies  
  where studies.id = sketches.study  
  order by study desc  limit 10;

--  <SKETCH>
--  <CONTEXT><sketcher=hammond></CONTEXT>
-- 
--  <SHAPE type=Stroke id=234> <PART type=Path><Point x=10 y=20 t=230003233/><Point x=11 y=21 t=23003243></PART></SHAPE>
-- 
--  <SHAPE labeled=moltmans type=Line id=432> <PART type=Stroke>id</PART> <PART alias=p1 type=Point>id</PART>...</SHAPE>
-- 
--  <SHAPE labeled=mtsezgin type=Arc id=432> <PART type=Stroke>id</PART> </SHAPE>
--  <EDIT type=Deletion id=421><PART type=Shape>id</PART></EDIT>
--  </SKETCH>
-- 


 SELECT h1.stroke_id, studies.name AS study_name, 
        h1.condition, h1.class 
        FROM studies, strokes s, sketches sk,
             (hand_classified_strokes h1 JOIN hand_classified_strokes h2 
               USING (stroke_id, condition, class, value))
        WHERE ((((((s.id = h1.stroke_id)
                   AND (s.sketch = sk.id))
                  AND (sk.study = studies.id))
                 AND (h1.value = 't'::bool))
                AND (h1.labeler <> h2.labeler)) 
               AND (h1.condition <> 'drg isa labeling'::text))
        GROUP BY h1.stroke_id, studies.name, h1.condition, h1.class 
        HAVING (count(h1.stroke_id) >= 2) 
        ORDER BY h1.condition, h1.stroke_id;

 SELECT stroke_id 
        FROM hand_classified_strokes as h1,
             hand_classified_strokes as h2

select stroke_id, condition, c.name from agreed_labels a, classes c 
   where a.class = c.id limit 10;


