#! /bin/bash

if [ $# != 1 ]; then
    echo "Pass name of output file (e.g. etcha-sketches.sql.gz) as argument."
    exit 1
fi

ARCHIVE=full-db.sql.gz
FULL_LISTING=full-list
LISTING=to-load-list
DUMP_FILE=$1

TABLES="\(authors\|classes\|devices\|hand_classified_strokes\|pen_modifiers\|sketches\|stroke_types\|strokes\|studies\|labels\)"

pg_dump --format=c -U drg-admin --file $ARCHIVE sketchdb

# First get the listing from the archive and select only the stuff we need
pg_restore --list $ARCHIVE > $FULL_LISTING

grep "\(TABLE\( DATA\)\? $TABLES \)\|\(SEQUENCE\( SET\)\? $TABLES\)" $FULL_LISTING > $LISTING

# Now restore that into temporary database
pg_restore --no-owner --use-list=$LISTING --no-privileges $ARCHIVE | gzip > $DUMP_FILE

echo "Your database is available in: $DUMP_FILE"





