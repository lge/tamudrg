(define AndGate
	(components 
			( Line  	horizontal_line1 )
			( Line  	horizontal_line2 )
			( Line  	horizontal_line3 )
			( Line  	vertical_line ) 
			( GeneralPath   and_gate_arc )

	)
	(rename
			( double first_point_renaming_x horizontal_line1.x1 )
			( double first_point_renaming_y horizontal_line1.y1 ))	
	(derived_property
			( double derived_point_x horizontal_line2.x2 )
			( double derived_point_y horizontal_line2.y2 ))	
	(constraints
			( parallel 	horizontal_line1 horizontal_line2 )
			( parallel 	horizontal_line2 horizontal_line3 )
			( above 	horizontal_line1 horizontal_line2 )
			( left	 	horizontal_line1 horizontal_line3 )
			( left		horizontal_line2 horizontal_line3 )
			( touches 	and_gate_arc vertical_line )
			( touches 	and_gate_arc horizontal_line3 )
	)
)




