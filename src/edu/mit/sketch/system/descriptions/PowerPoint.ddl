(define Quadrilateral
  (components
    (Line line1)
    (Line line2)
    (Line line3)
    (Line line4)
  )
  (constraints
    (meets line1 line2 line1.p2 line2.p1)
    (meets line2 line3 line2.p2 line3.p1)
    (meets line3 line4 line3.p2 line4.p1)
    (meets line4 line1 line4.p2 line1.p1)
  )
)

;(define Arrow
;  (components
;    (Line shaft)
;    (Line head1)
;    (Line head2)
;  )
;  (constraints
;    (< (angle-between head1 shaft) 90)
;    (< (angle-between shaft head2) 90)
;    (meets shaft head1 shaft.p1 head1.p1)
;    (meets shaft head2 shaft.p1 head2.p1)
;    (meets head1 head2 head1.p1 head2.p1)    
;(= (angle-between head1 head2) (+ (angle-between head1 shaft) (angle-between head2 shaft) ))
;  )
;)

(define PPLine
  (components 
    (Line l)
  )
)

(define PPEllipse
  (components
    (Ellipse e)
  )
)

;(define Circle
;  (components
;    (Ellipse e)
;  )
;  (constraints
;    (= e.horizontalAxis.length e.verticalAxis.length)
;  )
;)

;(define Rectangle
;  (components
;    (Quadrilateral q)
;  )
;  (constraints
;    (= q.line1.length q.line3.length)
;    (= q.line2.length q.line4.length)
;    (= (angle-between  line1 line2) 90)
;  )
;)

;(define Square
;  (components
;    (Rectangle r)
;  )
;  (constriaints
;    (= r.q.line1.length r.q.line2.length)
;  )
;)


;
;(define PinJoint
;  (components
;    (Ellipse joint)
;  )
;  (constraints
;    (< joint.radius 20)
;  )
;)
;
;(define JointContext
;  (components
;    (PinJoint joint)
;    (Body-quad body)
;  )
;  (constraints
;    (overlaps joint body)
;  )
;)
;
;(define JointContext2
;  (components
;    (PinJoint joint)
;    (Body-ellipse body)
;  )
;  (constraints
;    (overlaps joint body)
;  )
;)
;
