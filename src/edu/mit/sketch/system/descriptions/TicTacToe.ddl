(define Board
  (components
    (Line left_line)
    (Line right_line)
    (Line top_line)
    (Line bottom_line)
 )
  (constraints
    (intersects left_line  top_line)
    (intersects left_line  bottom_line)
    (intersects right_line top_line)
    (intersects right_line bottom_line)
    (parallel   left_line  right_line)
    (parallel   top_line   bottom_line)
 )
)

(define X
  (components
    (Line line1)
    (Line line2)
 )
  (constraints
    (intersects line1 line2)
    (= line1.length line2.length)
;     (bisects line1 line2)
  )
)

(define O
  (components
    (Ellipse ellipse1)
 )
)

