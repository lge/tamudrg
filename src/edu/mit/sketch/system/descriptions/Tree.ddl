; This is the ddl for the family tree domain.  

; Here are the geometric primitives we will need

; Line
; Ellipse
; Polyline

(define Arrow
  (components
    (Line shaft)
    (Line head1)
    (Line head2)
  )
  (constraints
    (< (angle-between head1 shaft) 90)
    (< (angle-between shaft head2) 90)
    (coincident shaft.p1 head1.p1)
    (coincident shaft.p1 head2.p1)
    (coincident head1.p1 head2.p1)    
;(= (angle-between head1 head2) (+ (angle-between head1 shaft) (angle-between head2 shaft) ))
  )
)

(define Quadrilateral
  (components
    (Line line1)
    (Line line2)
    (Line line3)
    (Line line4)
  )
  (constraints
    (coincident line1.p2 line2.p1)
    (coincident line2.p2 line3.p1)
    (coincident line3.p2 line4.p1)
    (coincident line4.p2 line1.p1)
  )
)

; Now the domain components
(define Marriage-link
  (components
    (Line line1)))

(define Divorce-link
  (components
    (Polyline poly)))

(define Female
  (components
    (Ellipse e)))

(define Male
  (components
    (Quadrilateral q)))

(define Child-link
  (components
    (Arrow a)))


; Now here's the context for the various components

(define Divorce
  (components
    (Male husband)
    (Female wife)
    (Divorce-link link)
  )
  (constraints
    (touches link.poly husband link.poly.p1)
    (touches link.poly wife link.poly.p2)
  )
)


(define Marriage
  (components
    (Male husband)
    (Female wife)
    (Marriage-link link)
  )
  (constraints
    (touches link.line1 husband link.line1.p1)
    (touches link.line1 wife link.line1.p2)
  )
)


(define Partnership
  (components
    (Male partner1)
    (Male partner2)
    (Marriage-link link)
  )
  (constraints
    (touches link.line1 partner1 link.line1.p1)
    (touches link.line1 partner2 link.line1.p2)
  )
)

(define Father-daughter
  (components
    (Male parent)
    (Female child)
    (Child-link link)
  )
  (constraints
    (touches link.a.shaft parent link.a.shaft.p2)
    (touches link.a.shaft child link.a.shaft.p1)
  )
)

(define Father-son
  (components
    (Male parent)
    (Male child)
    (Child-link link)
  )
  (constraints
    (touches link.a.shaft parent link.a.shaft.p2)
    (touches link.a.shaft child link.a.shaft.p1)
  )
)

(define Mother-son
  (components
    (Female parent)
    (Male child)
    (Child-link link)
  )
  (constraints
    (touches link.a.shaft parent link.a.shaft.p2)
    (touches link.a.shaft child link.a.shaft.p1)
  )
)
(define Mother-daughter
  (components
    (Female parent)
    (Female child)
    (Child-link link)
  )
  (constraints
    (touches link.a.shaft parent link.a.shaft.p2)
    (touches link.a.shaft child link.a.shaft.p1)
  )
)
