(define Quadrilateral
  (components
    (Line line1)
    (Line line2)
    (Line line3)
    (Line line4)
  )
  (constraints
    (meets line1 line2 line1.p2 line2.p1)
    (meets line2 line3 line2.p2 line3.p1)
    (meets line3 line4 line3.p2 line4.p1)
    (meets line4 line1 line4.p2 line1.p1)
  )
)

(define Body-quad
  (components
    (Quadrilateral q1)
  )
)

(define Body-ellipse
  (components
    (Ellipse e1)
  )
)

(define Arrow
  (components
    (Line shaft)
    (Line head1)
    (Line head2)
  )
  (constraints
    (< (angle-between head1 shaft) 90)
    (< (angle-between shaft head2) 90)
    (meets shaft head1 shaft.p1 head1.p1)
    (meets shaft head2 shaft.p1 head2.p1)
    (meets head1 head2 head1.p1 head2.p1)    
;(= (angle-between head1 head2) (+ (angle-between head1 shaft) (angle-between head2 shaft) ))
  )
)

(define Force
  (components
    (Arrow a)
  )
)

; A complete and total hack to reuse code in a desperate situation
; right before a deadline
(define Body-Force
  (components
    (Body-ellipse body)
    (Force f)
  )
  (constraints
    (touches f.a.shaft body.e1 f.a.shaft.p1)
  )
)
	
(define Body-Force-quad
  (components
    (Body-quad body)
    (Force f)
  )
  (constraints
    (touches f.a.shaft body.q1 f.a.shaft.p1)
  )
)

;
;;(define Circle
;;  (components
;;    (Ellipse e)
;;  )
;;  (constraints
;;    (= e.horizontalAxis.length e.verticalAxis.length)
;;  )
;;)
;
;(define PinJoint
;  (components
;    (Ellipse joint)
;  )
;  (constraints
;    (< joint.radius 20)
;  )
;)
;
;(define JointContext
;  (components
;    (PinJoint joint)
;    (Body-quad body)
;  )
;  (constraints
;    (overlaps joint body)
;  )
;)
;
;(define JointContext2
;  (components
;    (PinJoint joint)
;    (Body-ellipse body)
;  )
;  (constraints
;    (overlaps joint body)
;  )
;)
;
