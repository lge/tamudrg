-- THIS IS OUT OF DATE AND NOT KEPT IN SYNC WITH CHANGES MADE TO THE
-- DB STRUCTURE

CREATE TABLE authors (
    id          serial PRIMARY KEY,
    name        text,
    handedness  char(1),
    experience  text,
    study       int4 REFERENCES studies(id)
);

CREATE TABLE devices (
    id          serial PRIMARY KEY,
    type        text,
    has_eraser  bool
);

-- CREATE TABLE contexts (
--     id          serial PRIMARY KEY,
--     context     varchar(20) NOT NULL,
--     sub_context varchar(20) DEFAULT 'none',
--     sub_sub_context varchar(20) DEFAULT 'none'
-- );

CREATE TABLE studies (
    id            serial PRIMARY KEY,
    contact_email text,
    description   text
);

CREATE TABLE drg_users (
    id          int4 PRIMARY KEY,
    name        text
);

CREATE TABLE stroke_types (
    id          int4 PRIMARY KEY,
    name        varchar(20)
);

CREATE TABLE sketches (
    id          serial PRIMARY KEY,
    device      int4 REFERENCES devices(id),
    author      int4 REFERENCES authors(id),
    author_date timestamp,
    study       int4 REFERENCES studies(id),
    task        text
);

CREATE TABLE pen_modifiers (
    id         int4 PRIMARY KEY,
    name       varchar(20)
);

CREATE TABLE strokes (
    id          serial PRIMARY KEY,
    stroke_type int4 REFERENCES stroke_types(id),
    path        path,
    sketch      int4 REFERENCES sketches(id),
    modifier    int2 REFERENCES pen_modifiers(id),
    times       int4[]
);

CREATE TABLE classes (
    id           int4 PRIMARY KEY,
    name         varchar(20)
);

CREATE TABLE classifiers (
    id           int2 PRIMARY KEY,
    name         varchar(20)
);

CREATE TABLE classifications (
    stroke_id    int4 REFERENCES strokes(id),
    class        int4 REFERENCES classes(id),
    classifier   int2 REFERENCES classifiers(id)
);



--CREATE TABLE hand_classified_strokes (
--    stroke_id    int4 REFERENCES strokes(id),
--    labeler      text,
--    polyline     int2 DEFAULT -1,
--    ellipse      int2 DEFAULT -1,
--    line         int2 DEFAULT -1,
--    arc          int2 DEFAULT -1,
--    spiral       int2 DEFAULT -1,
--    polygon      int2 DEFAULT -1
--);

-- NOTICE:  DROP TABLE implicitly drops referential integrity trigger from table "strokes"
CREATE TABLE hand_classified_strokes (
    stroke_id    int4 REFERENCES strokes(id),
    labeler      text,
    condition    text,
    class        int4 REFERENCES classes(id),
    value        bool
);


CREATE TABLE strokes_to_label (
    stroke_id    int4 REFERENCES strokes(id),
    condition    text NOT NULL
);

CREATE TABLE fit_errors (
    stroke_id   int4 REFERENCES strokes(id),
    class       int4 REFERENCES classes(id),
    error       float,
    error_to_length float,
    error_to_length_sq float
);

CREATE TABLE auto_classify (
    stroke_id   int4 REFERENCES strokes(id),
    class       int4 REFERENCES classes(id),
    source      text,
    error       double precision,
    error_to_length double precision
);


-- CREATE TABLE hand_labeled_strokes (
--     stroke_id    int4 REFERENCES strokes(id),
--     labeler      text,
--     class        int4 REFERENCES classes(id),
--     label        int2 DEFAULT -1
-- );



-- CREATE VIEW hand_labeled_strokes
--      AS SELECT stroke_id, labeler, 
--                0 as class, polyline as label from hand_classified_strokes
--         UNION SELECT stroke_id, labeler,
--                1 as class, ellipse as label from hand_classified_strokes
--         UNION SELECT stroke_id, labeler,
--                3 as class, line as label from hand_classified_strokes
--         UNION SELECT stroke_id, labeler,
--                4 as class, arc as label from hand_classified_strokes
--         UNION SELECT stroke_id, labeler,
--                5 as class, spiral as label from hand_classified_strokes
--         UNION SELECT stroke_id, labeler,
--                6 as class, polygon as label from hand_classified_strokes;
-- 
CREATE VIEW all_sketches
    AS SELECT
           authors.name AS author,
           studies.name AS study,
           studies.task,
           sketches.author_date,
           devices.type AS device_type,
           sketches.id
    FROM
    (((sketches LEFT OUTER JOIN authors
                ON (sketches.author = authors.id))
                LEFT OUTER JOIN devices
                ON (devices.id = sketches.device))
                LEFT OUTER JOIN studies
                ON (sketches.study = studies.id))
    ORDER BY study, authors.id, task;


CREATE VIEW stroke_label_counts
    AS SELECT stroke_id, condition, count(labeler) FROM 
        (SELECT stroke_id, condition, labeler 
             FROM hand_classified_strokes AS hcs
             GROUP BY stroke_id, hcs.condition, labeler) AS groups
        GROUP BY stroke_id, condition;

CREATE VIEW fully_labeled
    AS SELECT stroke_id, condition, count FROM stroke_label_counts
        WHERE count >= 2;

-- everything associated with a label in one table
CREATE VIEW label_info AS
  SELECT strokes.id AS stroke_id, sketches.id AS sketch, studies.name AS study,
        condition, labeler, classes.name as class, value
        FROM strokes, sketches, studies, classes, hand_classified_strokes as h
        WHERE h.stroke_id = strokes.id AND
              h.class = classes.id AND
              strokes.sketch = sketches.id AND
              sketches.study = studies.id
        ORDER BY studies.name, sketches.id, strokes.id, 
                 condition, labeler, class;


CREATE VIEW final_label_info AS
  SELECT strokes.id AS stroke_id, sketches.id AS sketch, studies.name AS study,
        condition, classes.name as class
        FROM strokes, sketches, studies, classes, agreed_labels as a
        WHERE a.stroke_id = strokes.id AND
              a.class = classes.id AND
              strokes.sketch = sketches.id AND
              sketches.study = studies.id
        ORDER BY condition, sketches.id, strokes.id, class
;


-- list stroke, class pairs in which at least 2 labelers agreed.
-- Strokes may be listed more than once per context if strokes have
-- multiple agreed on labels (e.g. IsA and CanBeA cases).  It includes
-- strokes that got no labels from any reviewers (IsA and CanBeA
-- cases) as 'other' = 10.

CREATE VIEW agreed_labels AS
  SELECT stroke_id, studies.name as study_name, condition, class FROM
       studies, strokes, sketches,
       ((SELECT stroke_id, condition, class
          FROM strokes as s, hand_classified_strokes AS h1
          JOIN hand_classified_strokes AS h2
             USING (stroke_id, condition, class, value)
          WHERE s.id = h1.stroke_id AND
                value = 't' AND 
                h1.labeler <> h2.labeler AND
                condition <> 'drg isa labeling'
          GROUP BY stroke_id, condition, class
          HAVING count(stroke_id) >= 2)
        UNION
        (SELECT stroke_id, condition, 10 FROM
           (SELECT stroke_id, condition, labeler, value, count(value)
            FROM hand_classified_strokes 
            WHERE condition IN ('CanBeA', 'IsA')
            GROUP BY stroke_id, condition, labeler, value
            HAVING (value = 'f' AND count(value) = 5)) AS nones
           GROUP BY stroke_id, condition
           HAVING count(*) > 1)) as ids
        WHERE stroke_id = strokes.id AND
              strokes.sketch = sketches.id AND
              sketches.study = studies.id
        ORDER BY condition, studies.name, stroke_id;
  
--     AS SELECT stroke_id, condition, class, count(class)
--         FROM hand_classified_strokes AS h1
--         JOIN hand_classified_strokes AS h2
--            USING (stroke_id, condition, class, value)
--         WHERE value = 't' AND 
--               h1.labeler <> h2.labeler AND
--               condition <> 'drg isa labeling'
--         GROUP BY stroke_id, condition, class
--         HAVING count(stroke_id) >= 2
--         ORDER BY condition, stroke_id;
    

COPY devices FROM stdin;
0	Acer TabletPC	true
1	Mimio	true
2	Mouse	false
\.

COPY drg_users FROM stdin;
1	moltmans
2	calvarad
3	mtsezgin
4	sjcates
5	hammond
6	cadlerun
7	jacobe
\.

COPY stroke_types FROM stdin;
0	ink
1	deleted ink
2	deletion
3	gesture
\.

COPY pen_modifiers FROM stdin;
0	none
1	button1
2	button2
3	button3
4	eraser
\.

COPY classifiers FROM stdin;
1	SimpleClassifier
2	SimpleClassifier2
3	SimpleClassifier3
4	HandLabeled
\.

COPY classes FROM stdin;
0	polyline
1	ellipse
2	complex
3	line
4	arc
5	spiral
6	polygon
7	closed_curve
8	open_curve
9	text
10	other
11	too_small
\.

