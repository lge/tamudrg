function [minW, minT] = minCut(V, W, n)

minV = min(V);
maxV = max(V);

minT = Inf;
minW = Inf;

% try a range of values (but make sure to try 0
for threshold = [0, minV:(maxV-minV)/n:max(V)],
  
  A = V >  threshold;
  B = V <= threshold;

  if sum(sum(A)) == 0 || sum(sum(B)) == 0
    weight = Inf;
  else
    cutAB = cut(W,A,B);
    assocA = assoc(W,A);
    assocB = assoc(W,B);
    weight = cutAB/assocA + cutAB/assocB;
  end
  if weight < minW
    minT = threshold;
    minW = weight;
  end
end
return;

%%%%%%%%%%% 
% Total conection between nodes in C and all other nodes 
% (excluding itself)
function [assoc] = assoc(W,C)
temp = W-diag(diag(W));
assocs = temp(find(C),:);
assoc = sum(sum(assocs));
return;


%%%%%%%%%%% 
% Total conection between nodes in A and nodes in B
function [cut] = cut(W,A,B)
cuts = W(find(A),find(B));
cut = sum(sum(cuts));
return;

