-- Get strokes with different classifications in different classifiers
SELECT c2.stroke_id, c2.class AS Simple2, c3.class AS Simple3 
        FROM classifications AS c2 
        INNER JOIN classifications AS c3 
        ON (c2.classifier = 2 AND c3.classifier = 3 and c2.class != c3.class AND c2.stroke_id = c3.stroke_id)
        ORDER BY stroke_id;


-- Get strokes with different classifications in different classifiers
SELECT c2.stroke_id, c1.class AS Classifier1, c2.class AS Classifier2
        FROM classifications AS c2 
        INNER JOIN classifications AS c1 

        ON (c1.classifier = 3 AND c2.classifier = 4

            AND c2.class != c1.class AND c2.stroke_id = c1.stroke_id)
        ORDER BY stroke_id;


SELECT s.id, c1.class AS simple1, c2.class AS simple2, 
        c3.class AS simple3 
        FROM (((strokes s LEFT JOIN classifications c1 
        ON ((s.id = c1.stroke_id))) 
        LEFT JOIN classifications c2 ON ((s.id = c2.stroke_id))) 
        LEFT JOIN classifications c3 ON ((s.id = c3.stroke_id))) 
        WHERE (((c1.classifier = 1) AND (c2.classifier = 2)) AND
                (c3.classifier = 3)) 
        ORDER BY s.id;


SELECT * FROM (SELECT classifications  AS l INNER JOIN classes AS c
        ON (l.class = c.id);


-- Count the number of times 2 people label the same stroke with a
-- given class in each way e.g. both yes, both no, or different.  Note
-- that there is double counting since a=Y b=N and b=N a=Y are counted
-- as are the a=Y b=Y and b=Y a=Y
SELECT a.class, a.label, b.label, count(*) 
        FROM hand_labeled_strokes AS a INNER JOIN
             hand_labeled_strokes AS b 
        ON (a.stroke_id = b.stroke_id AND a.class = b.class AND
           a.labeler != b.labeler) 
        GROUP BY a.class, a.label, b.label
        ORDER BY a.class, a.label, b.label, count(*);

SELECT *, count(*) FROM hand_classified_strokes AS h
        GROUP BY h.stroke_id, 
                 h.polyline, h.ellipse, h.line, h.arc, h.spiral, h.polygon;
        

-- Count the number of times that people disagreed on a complete
-- labeling and the number of times that they agreed
SELECT sub.total, count(sub.total)  FROM
(SELECT DISTINCT ON (h.stroke_id)  h.stroke_id, 
       h.polyline, h.ellipse, h.line, h.arc, h.spiral, h.polygon, 
       count(h.stroke_id) AS total
        FROM (SELECT stroke_id from hand_classified_strokes as hs
                GROUP BY hs.stroke_id 
                HAVING count(stroke_id) > 1) AS s,
             hand_classified_strokes AS h
        WHERE h.stroke_id = s.stroke_id
        GROUP BY h.stroke_id, 
                 h.polyline, h.ellipse, h.line, h.arc, 
                 h.spiral, h.polygon)
 AS sub
        GROUP BY sub.total;


SELECT id FROM strokes AS s
        WHERE s.context = 2 OR s.context = 3 OR s.context = 4





-- Grab strokes with intersecting bounding boxes (intersecting by >= a given amount)
SELECT box1.id, box2.id, 
       area(box_intersect(box1.box, box2.box))/(area(box1.box)+area(box2.box)+.0001) AS overlap
        FROM (select id, box(pclose(path)) from strokes where sketch=1) AS box1,
             (select id, box(pclose(path)) from strokes where sketch=1) AS box2
        WHERE  box1.id != box2.id AND
               box_intersect(box1.box, box2.box) IS NOT NULL AND 
                area(box_intersect(box1.box, box2.box))/
                (area(box1.box)+area(box2.box)+.01) > .1
        ORDER BY overlap;


-- Get the strokes that are part of the same object as defined by end
-- point proximity

SELECT stroke1.id, stroke2.id
        FROM strokes AS stroke1, 
             strokes AS stroke2
        WHERE stroke1.sketch = ? AND
              stroke1.sketch = stroke2.sketch AND
              stroke1.path ?# stroke2.path;

-- Compare the results from hand labeling with the results from each
-- of the classifiers

-- Need unique stroke id's from hand_labeled_strokes (use left outer
-- join)

SELECT s.stroke_id 
        FROM hand_labeled_strokes AS s,
             classifications AS c
        WHERE c.classifier = 1 AND
              s.stroke_id = c.stroke_id AND
              s.class = c.class;

-- get all the strokes from a context that have the given label (class)
SELECT s.stroke_id  
  FROM (SELECT stroke_id FROM hand_labeled_strokes  
          WHERE class=3 and label=1) as s, 
       sketches, strokes  
    WHERE s.stroke_id = strokes.id AND strokes.sketch = sketches.id AND
          sketches.context = 2;


-- get all strokes labeled as X from user Y
SELECT author, count(stroke_id)
  FROM (SELECT stroke_id FROM hand_labeled_strokes  
          WHERE class=3 and label=1) as s,
       strokes, sketches 
  WHERE s.stroke_id = strokes.id and strokes.sketch = sketches.id
  GROUP BY sketches.author, sketches.context;

SELECT authors.name AS author, sketches.id, 
       sketches.study, sketches.task,
       sketches.author_date, devices.type AS device_type
  FROM (sketches LEFT JOIN authors ON (sketches.author = authors.id))
       LEFT JOIN devices ON (devices.id = sketches.device)
  ORDER BY authors.id, sketches.author_date;


-- get strokes that have a bounding box whose longest edge is at least
-- 15 pixels

SELECT count(*) FROM strokes WHERE length(box(pclose(path))) >= 15;


(SELECT stroke_id
   FROM strokes_to_label AS stl
   LEFT OUTER JOIN hand_classified_strokes AS hcs USING(stroke_id)
   GROUP BY stroke_id, 
   HAVING count(labeler) < 2) -- remove grouped rows
 EXCEPT
(SELECT stroke_id FROM hand_classified_strokes AS hcs
   WHERE hcs.labeler = ?)


-- This makes a view that shows how many times each labeled stroke has
-- been labeled for each condition
SELECT stroke_id, condition, count(labeler) FROM 
    (SELECT stroke_id, condition, labeler 
         FROM hand_classified_strokes AS hcs
         GROUP BY stroke_id, hcs.condition, labeler) AS groups
    GROUP BY stroke_id, condition;

-- This gets the strokes that have been labeled by less than two
-- people and never by the given user in the given context
SELECT stroke_id FROM strokes_to_label
   WHERE stroke_id NOT IN 
       ((SELECT slc.stroke_id 
          FROM stroke_label_counts as slc
          WHERE slc.count >= 2 AND slc.condition = 'context')
        UNION
        (SELECT stroke_id FROM hand_classified_strokes
          WHERE labeler = 'labeler'))
  ORDER BY round(random()*10000) LIMIT 20;

-- get strokes from complete sketches for labeling
SELECT stroke_id FROM strokes_to_label, strokes
  WHERE full_sketch = 't' AND strokes.id = stroke_id
        AND stroke_id NOT IN  
         ((SELECT stroke_id  
            FROM stroke_label_counts as slc
            WHERE slc.count >= 2 AND slc.condition = 'condition') 
          UNION 
          (SELECT stroke_id FROM hand_classified_strokes  
             WHERE labeler = 'dummy'))
  ORDER BY strokes.sketch, stroke_id limit 20;





  