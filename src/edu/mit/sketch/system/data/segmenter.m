function [V,D] = segmenter(W)

D = diag(sum(W'));

A = D^(-.5)*(D-W)*D^(-.5);

[V,E] = eigs(A);

