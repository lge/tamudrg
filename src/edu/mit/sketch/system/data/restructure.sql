CREATE TABLE temp AS SELECT * FROM sketches;

DROP TABLE sketches;

CREATE TABLE sketches (
    id          serial PRIMARY KEY,
    device      int4 REFERENCES devices(id),
    author      int4 REFERENCES authors(id),
    author_date timestamp,
    study       int4 REFERENCES studies(id),
    task        text
);

INSERT into sketches SELECT s.id, s.device, s.author, s.author_date, 1 as study, c.sub_context as task 
  FROM temp as s, contexts as c
  WHERE s.context = c.id;
  --LIMIT 10;


--INSERT INTO sketches SELECT * FROM temp;

DROP TABLE temp;
     
-- by dropping sketches:
x-- NOTICE:  DROP TABLE implicitly drops referential integrity trigger from table "devices"
x-- NOTICE:  DROP TABLE implicitly drops referential integrity trigger from table "contexts"
-- NOTICE:  DROP TABLE implicitly drops referential integrity trigger from table "contexts"
x-- NOTICE:  DROP TABLE implicitly drops referential integrity trigger from table "authors"
x-- NOTICE:  DROP TABLE implicitly drops referential integrity trigger from table "authors"
x-- NOTICE:  DROP TABLE implicitly drops referential integrity trigger from table "drg_users"
x-- NOTICE:  DROP TABLE implicitly drops referential integrity trigger from table "drg_users"
x-- NOTICE:  DROP TABLE implicitly drops referential integrity trigger from table "strokes"

-- readded constraints with:
--alter table sketches add constraint device_fk foreign key (device) references devices(id) MATCH FULL;
--NOTICE:  ALTER TABLE will create implicit trigger(s) for FOREIGN KEY check(s)
--ALTER

--sketchdb=> alter table sketches add constraint author_fk foreign key (author) references authors(id) MATCH FULL;
--NOTICE:  ALTER TABLE will create implicit trigger(s) for FOREIGN KEY check(s)
--ALTER

--sketchdb=> alter table strokes add constraint sketch_fk foreign key (sketch) references sketches(id) MATCH FULL;
--NOTICE:  ALTER TABLE will create implicit trigger(s) for FOREIGN KEY check(s)
--ALTER


x-- NOTICE:  DROP TABLE implicitly drops referential integrity trigger from table "sketches"