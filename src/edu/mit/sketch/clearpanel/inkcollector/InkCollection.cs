//-------------------------------------------------------------------------
//
//  C# Clear Panel Overlay
//  A transparent overlay that processes ink from the input device and 
//  transmits the data to the corresponding java interface.
//
//--------------------------------------------------------------------------

using System;
using System.Diagnostics;
using System.Drawing;
using System.Windows.Forms;
using System.Collections;
using System.Threading;

// The Microsoft Tablet PC namespaces
using Microsoft.Ink;
using Microsoft.StylusInput;
using Microsoft.StylusInput.PluginData;

// Network
using System.Data;
using System.Net.Sockets ;
using System.IO;

namespace InkCollection
{

    /// <summary>
    /// The InkCollection form class
    /// </summary>
    public class InkCollection : Form, IStylusAsyncPlugin
    {

		TcpClient tc;
		NetworkStream ns;
		StreamWriter sw;
		StreamReader sr;
		Thread t;
		Graphics g;
		bool listen = true;

		StrokeTimer sTimer;
		int strokeLength;

		// myRecognizers is used to retrieve the number of installed recognizers
		Recognizers myRecognizers;

        // Declare constants for the pen widths used by this application.
        // Note that these constants are in himetric units (1 unit = .01mm)
        private const float ThinInkWidth = 10;
        private const float MediumInkWidth = 100;
        private const float ThickInkWidth = 200;

        // Declare the real time stylus
        private RealTimeStylus myRealTimeStylus;

        // Declare the renderers.  The dynamic renderer is used to render the ink
        // stroke that is currently being collected, whereas the static renderer 
        // is used to render strokes that have already been collected.
        private DynamicRenderer myDynamicRenderer;
        private Renderer myRenderer;

        // Declare the hashtable used to store packet data that is being collected by one
        // or more cursors.  StylusId's are used as the hashtable key to uniquely
        // identify the packet data collected for a given cursor.
        private Hashtable myPackets;

        // Declare the ink object used to store strokes collected by the real time stylus.
		private Ink myInk;
		private Ink recInk;

        #region Standard Template Code

        private System.Windows.Forms.MenuItem miMainFile;
        private System.Windows.Forms.MenuItem miExit;
        private System.Windows.Forms.MenuItem miMainInk;
        private System.Windows.Forms.MenuItem miEnabled;
        private System.Windows.Forms.MenuItem miColor;
        private System.Windows.Forms.MenuItem miRed;
        private System.Windows.Forms.MenuItem miGreen;
        private System.Windows.Forms.MenuItem miBlue;
        private System.Windows.Forms.MenuItem miBlack;
        private System.Windows.Forms.MenuItem miWidth;
        private System.Windows.Forms.MenuItem miThin;
        private System.Windows.Forms.MenuItem miMedium;
        private System.Windows.Forms.MenuItem miThick;
        private System.Windows.Forms.MainMenu miMain;
        private System.ComponentModel.Container components = null;
        #endregion

        public InkCollection()
        {
            #region Standard Template Code
            // Required for Windows Form Designer support
            InitializeComponent();
            #endregion
			g = CreateGraphics();

        }

        #region Standard Template Code
        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        protected override void Dispose( bool disposing )
        {
            if( disposing )
            {
                if (components != null) 
                {
                    components.Dispose();
                }
            }
            base.Dispose( disposing );          

        }
        #endregion

        #region Windows Form Designer generated code
        /// <summary>
        /// ----- Standard Template Code -----
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
			this.miMain = new System.Windows.Forms.MainMenu();
			this.miMainFile = new System.Windows.Forms.MenuItem();
			this.miExit = new System.Windows.Forms.MenuItem();
			this.miMainInk = new System.Windows.Forms.MenuItem();
			this.miEnabled = new System.Windows.Forms.MenuItem();
			this.miColor = new System.Windows.Forms.MenuItem();
			this.miRed = new System.Windows.Forms.MenuItem();
			this.miGreen = new System.Windows.Forms.MenuItem();
			this.miBlue = new System.Windows.Forms.MenuItem();
			this.miBlack = new System.Windows.Forms.MenuItem();
			this.miWidth = new System.Windows.Forms.MenuItem();
			this.miThin = new System.Windows.Forms.MenuItem();
			this.miMedium = new System.Windows.Forms.MenuItem();
			this.miThick = new System.Windows.Forms.MenuItem();
			// 
			// miMain
			// 
			this.miMain.MenuItems.AddRange(new System.Windows.Forms.MenuItem[] {
																				   this.miMainFile,
																				   this.miMainInk});
			// 
			// miMainFile
			// 
			this.miMainFile.Index = 0;
			this.miMainFile.MenuItems.AddRange(new System.Windows.Forms.MenuItem[] {
																					   this.miExit});
			this.miMainFile.Text = "File";
			// 
			// miExit
			// 
			this.miExit.Index = 0;
			this.miExit.Text = "Exit";
			this.miExit.Click += new System.EventHandler(this.miExit_Click);
			// 
			// miMainInk
			// 
			this.miMainInk.Index = 1;
			this.miMainInk.MenuItems.AddRange(new System.Windows.Forms.MenuItem[] {
																					  this.miEnabled,
																					  this.miColor,
																					  this.miWidth});
			this.miMainInk.Text = "Ink";
			// 
			// miEnabled
			// 
			this.miEnabled.Checked = true;
			this.miEnabled.Index = 0;
			this.miEnabled.Text = "Enabled";
			this.miEnabled.Click += new System.EventHandler(this.miEnabled_Click);
			// 
			// miColor
			// 
			this.miColor.Index = 1;
			this.miColor.MenuItems.AddRange(new System.Windows.Forms.MenuItem[] {
																					this.miRed,
																					this.miGreen,
																					this.miBlue,
																					this.miBlack});
			this.miColor.Text = "Color";
			// 
			// miRed
			// 
			this.miRed.Index = 0;
			this.miRed.Text = "Red";
			this.miRed.Click += new System.EventHandler(this.miRed_Click);
			// 
			// miGreen
			// 
			this.miGreen.Index = 1;
			this.miGreen.Text = "Green";
			this.miGreen.Click += new System.EventHandler(this.miGreen_Click);
			// 
			// miBlue
			// 
			this.miBlue.Index = 2;
			this.miBlue.Text = "Blue";
			this.miBlue.Click += new System.EventHandler(this.miBlue_Click);
			// 
			// miBlack
			// 
			this.miBlack.Index = 3;
			this.miBlack.Text = "Black";
			this.miBlack.Click += new System.EventHandler(this.miBlack_Click);
			// 
			// miWidth
			// 
			this.miWidth.Index = 2;
			this.miWidth.MenuItems.AddRange(new System.Windows.Forms.MenuItem[] {
																					this.miThin,
																					this.miMedium,
																					this.miThick});
			this.miWidth.Text = "Width";
			// 
			// miThin
			// 
			this.miThin.Index = 0;
			this.miThin.Text = "Thin";
			this.miThin.Click += new System.EventHandler(this.miThin_Click);
			// 
			// miMedium
			// 
			this.miMedium.Index = 1;
			this.miMedium.Text = "Medium";
			this.miMedium.Click += new System.EventHandler(this.miMedium_Click);
			// 
			// miThick
			// 
			this.miThick.Index = 2;
			this.miThick.Text = "Thick";
			this.miThick.Click += new System.EventHandler(this.miThick_Click);
			// 
			// InkCollection
			// 
			this.AutoScaleBaseSize = new System.Drawing.Size(5, 13);
			this.BackColor = System.Drawing.SystemColors.Window;
			this.ClientSize = new System.Drawing.Size(292, 245);
			this.ControlBox = false;
			this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
			this.Name = "InkCollection";
			this.Opacity = 0.4;
			this.ShowInTaskbar = false;
			this.SizeGripStyle = System.Windows.Forms.SizeGripStyle.Hide;
			this.Text = "Ink Collection";
			this.TopMost = true;
			this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.InkCollection_KeyDown);
			this.Click += new System.EventHandler(this.InkCollection_Click);
			this.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.InkCollection_KeyPress);
			this.Load += new System.EventHandler(this.InkCollection_Load);
			this.Paint += new System.Windows.Forms.PaintEventHandler(this.InkCollection_Paint);
			this.MouseLeave += new System.EventHandler(this.InkCollection_MouseLeave);

		}
        #endregion

        #region Standard Template Code
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main() 
        {
            Application.Run(new InkCollection());
        }
        #endregion      

        /// <summary>
        /// Event Handler from Form Load Event
        /// Set up the real time stylus for collection
        /// </summary>
        /// <param name="sender">The control that raised the event.</param>
        /// <param name="e">The event arguments.</param>
        private void InkCollection_Load(object sender, System.EventArgs e)
        {

            // Create the renderers.  The dynamic renderer is used to render the ink
            // stroke that is currently being collected, whereas the static renderer 
            // is used to render strokes that have already been collected.
            myDynamicRenderer = new DynamicRenderer(this);
            myRenderer = new Renderer();

            // 
            // If you do not modify the default drawing attributes, the default 
            // drawing attributes will use the following properties and values:
            // 
            //      AntiAliased     = true
            //      Color           = black
            //      FitToCurve      = false
            //      Height          = 1
            //      IgnorePressure  = false
            //      PenTip          = ball
            //      RasterOperation = copy pen
            //      Transparency    = 0
            //      Width           = 53 (2 pixels on a 96 dpi screen)
            // 
            // For an example of how to modify other drawing attributes, uncomment
            // the following lines of code:
            // myDynamicRenderer.DrawingAttributes.PenTip = PenTip.Rectangle;
            // myDynamicRenderer.DrawingAttributes.Height = (.5F)*MediumInkWidth;
            // myDynamicRenderer.DrawingAttributes.Transparency = 128;
            //

            // Create the real time stylus used to receive stylus notifications
            myRealTimeStylus = new RealTimeStylus(this, true);

			myRealTimeStylus.SetDesiredPacketDescription(new Guid[]{PacketProperty.X,
				PacketProperty.Y, PacketProperty.TimerTick, PacketProperty.NormalPressure});

            // Add the dynamic renderer to the synchronous plugin notification chain.
            // Synchronous notifications occur on the pen thread.
            myRealTimeStylus.SyncPluginCollection.Add(myDynamicRenderer);

            // Add the form to the asynchronous plugin notification chain.  This plugin
            // will be used to collect stylus data into an ink object.  Asynchronous
            // notifications occur on the UI thread.
            myRealTimeStylus.AsyncPluginCollection.Add(this);

            // Enable the real time stylus and the dynamic renderer
            myRealTimeStylus.Enabled = true;
            myDynamicRenderer.Enabled = true;  
      
            // Create the ink object used to store ink collected from the real time stylus
            myPackets = new Hashtable();
            myInk = new Ink();
			sTimer = new StrokeTimer();
			socketConnect();

			t = new Thread(new ThreadStart(processInput));
			t.Start();

			// Create the recognizers collection
			myRecognizers = new Recognizers();
        }

        /// <summary>
        /// Event Handler from the form Paint event.
        /// </summary>
        /// <param name="sender">The control that raised the event.</param>
        /// <param name="e">The event arguments.</param>
        private void InkCollection_Paint(object sender, System.Windows.Forms.PaintEventArgs e)
        {
            // Refresh the dynamic renderer, since it's possible that a stroke is being
            // collected at the time Paint occurs.  In this case, the portion of the stroke
            // that has already been collected will need to be redrawn.
            myDynamicRenderer.Refresh();

            // Use the static renderer to redraw strokes that have already been collected.
            myRenderer.Draw(e.Graphics, myInk.Strokes);
        }

        #region Menu Event Handlers
        /// <summary>
        /// Event Handler from Ink->Color->Red Menu Item
        /// </summary>
        /// <param name="sender">The control that raised the event.</param>
        /// <param name="e">The event arguments.</param>
        private void miRed_Click(object sender, System.EventArgs e)
        {

            myDynamicRenderer.DrawingAttributes.Color = Color.Red;
        }

        /// <summary>
        /// Event Handler from Ink->Color->Green Menu Item
        /// </summary>
        /// <param name="sender">The control that raised the event.</param>
        /// <param name="e">The event arguments.</param>
        private void miGreen_Click(object sender, System.EventArgs e)
        {
            myDynamicRenderer.DrawingAttributes.Color = Color.Green;
        }

        /// <summary>
        /// Event Handler from Ink->Color->Blue Menu Item
        /// </summary>
        /// <param name="sender">The control that raised the event.</param>
        /// <param name="e">The event arguments.</param>
        private void miBlue_Click(object sender, System.EventArgs e)
        {
            myDynamicRenderer.DrawingAttributes.Color = Color.Blue;
        }

        /// <summary>
        /// Event Handler from Ink->Color->Black Menu Item
        /// </summary>
        /// <param name="sender">The control that raised the event.</param>
        /// <param name="e">The event arguments.</param>
        private void miBlack_Click(object sender, System.EventArgs e)
        {
            myDynamicRenderer.DrawingAttributes.Color = Color.Black;
        }

        /// <summary>
        /// Event Handler from Ink->Width->Thin Menu Item
        /// </summary>
        /// <param name="sender">The control that raised the event.</param>
        /// <param name="e">The event arguments.</param>
        private void miThin_Click(object sender, System.EventArgs e)
        {
            myDynamicRenderer.DrawingAttributes.Width = ThinInkWidth;
        }

        /// <summary>
        /// Event Handler from Ink->Width->Medium Menu Item
        /// </summary>
        /// <param name="sender">The control that raised the event.</param>
        /// <param name="e">The event arguments.</param>
        private void miMedium_Click(object sender, System.EventArgs e)
        {
            myDynamicRenderer.DrawingAttributes.Width = MediumInkWidth;
        }

        /// <summary>
        /// Event Handler from Ink->Width->Thick Menu Item
        /// </summary>
        /// <param name="sender">The control that raised the event.</param>
        /// <param name="e">The event arguments.</param>
        private void miThick_Click(object sender, System.EventArgs e)
        {
            myDynamicRenderer.DrawingAttributes.Width = ThickInkWidth;
        }

        /// <summary>
        /// Event Handler from File->Exit Menu Item
        /// </summary>
        /// <param name="sender">The control that raised the event.</param>
        /// <param name="e">The event arguments.</param>
        private void miExit_Click(object sender, System.EventArgs e)
        {
            myRealTimeStylus.Enabled = false;
            Application.Exit();
        }

        /// <summary>
        /// Event Handler from Ink->Enabled Menu Item
        /// </summary>
        /// <param name="sender">The control that raised the event.</param>
        /// <param name="e">The event arguments.</param>
        private void miEnabled_Click(object sender, System.EventArgs e)
        {
            // Toggle the enabled state of the real time stylus
            myRealTimeStylus.Enabled = !myRealTimeStylus.Enabled;
            miEnabled.Checked = myRealTimeStylus.Enabled;
        }
        #endregion

        /// <summary>
        /// Occurs when the stylus touches the digitizer surface.
        /// Allocate a new array to store the packet data for this stylus.
        /// </summary>
        /// <param name="sender">The real time stylus associated with the notification</param>
        /// <param name="data">The notification data</param>
        public void StylusDown(RealTimeStylus sender, StylusDownData data)
		{
			
            // Allocate an empty array to store the packet data that will be
            // collected for this stylus.  
            ArrayList collectedPackets = new ArrayList();


            // Add the packet data from StylusDown to the array
            collectedPackets.AddRange(data.GetData());

            // Insert the array into a hashtable using the stylus id as a key.
            myPackets.Add(data.Stylus.Id, collectedPackets);
			

			// Send packet data to java server
			sTimer.startStroke();
			strokeLength = 0;
			SendData(data, "start", false);

        }

        /// <summary>
        /// Occurs when the stylus moves on the digitizer surface.
        /// Add new packet data into the packet array for this stylus.
        /// </summary>
        /// <param name="sender">The real time stylus associated with the notification</param>
        /// <param name="data">The notification data</param>
		public void Packets(RealTimeStylus sender, PacketsData data)
		{
			// Use the stylus id as a key to retrieve the packet array for the
			// stylus.  Insert the new packet data into this array.
			((ArrayList)(myPackets[data.Stylus.Id])).AddRange(data.GetData());
			
			// Send packet data to java server
			SendData(data, "stroke", false);
        }


        /// <summary>
        /// Occurs when the stylus leaves the digitizer surface.
        /// Retrieve the packet array for this stylus and use it to create
        /// a new stoke.
        /// </summary>
        /// <param name="sender">The real time stylus associated with the notification</param>
        /// <param name="data">The notification data</param>
        public void StylusUp(RealTimeStylus sender, StylusUpData data)
        {
			// Send packet data to java server and signify end of stroke
			SendData(data, "end", true);

			ArrayList collectedPackets = (ArrayList)myPackets[data.Stylus.Id];
			myPackets.Remove(data.Stylus.Id);

			// Add the packet data from StylusUp to the array
			collectedPackets.AddRange(data.GetData());

			// Create the stroke using the specified drawing attributes.
			int[] packets = (int[])(collectedPackets.ToArray(typeof(int)));

			for (int i = 0; i < packets.Length; i++)
			{
				//Console.Out.WriteLine(packets[i]);
			}

			TabletPropertyDescriptionCollection tabletProperties = myRealTimeStylus.GetTabletPropertyDescriptionCollection(data.Stylus.TabletContextId);

			Stroke stroke = myInk.CreateStroke(packets, tabletProperties);
			if (stroke != null) 
			{
				stroke.DrawingAttributes.Color = myDynamicRenderer.DrawingAttributes.Color;
				stroke.DrawingAttributes.Width = myDynamicRenderer.DrawingAttributes.Width;
			} 
        }


		public String RecognizeStrokes (Ink ink)
		{
			RecognizerContext myRecoContext = new RecognizerContext();
			string factoid = "((C|H|O|N|S|F|R)(2|3|4|5|6)?)+";

			myRecoContext.Factoid = factoid;
			myRecoContext.RecognitionFlags = RecognitionModes.Coerce;

			RecognitionStatus status;
			RecognitionResult recoResult;
			
			myRecoContext.Strokes = ink.Strokes;
			
			recoResult = myRecoContext.Recognize(out status);		

			RecognitionAlternates alternates = recoResult.GetAlternatesFromSelection();
			IEnumerator e = alternates.GetEnumerator();
			String result = "";
			int  c = 0;

			foreach (object alternate in alternates)
			{
				c ++;
				if (c < 4)
					result = result + " " + alternate.ToString();
			}

			return result;
		}

		/// <summary>
		/// Sends the position, time, and pressure of the samples in data
		/// to the java server
		/// </summary>
		/// <param name="data"></param>
		public void SendData (StylusDataBase data, String header, bool forced)
		{
			int[] content = data.GetData();
			int packetsize = data.PacketPropertyCount;

			for (int i = 0; i < content.Length; i+= packetsize)
			{
				socketSend(header + "," + ExtractData(content, i, packetsize));
				strokeLength ++;
			}
		}

		/// <summary>
		/// Extracts the position (in pixels) and other ink information from the
		/// contents of the data sample.  Offset/size speicfies the start/length of
		/// the current sample since the array can contain multiple samples 
		/// </summary>
		/// <param name="content"></param>
		/// <param name="offset"></param>
		/// <param name="size"></param>
		/// <returns></returns>
		public String ExtractData (int[] content, int offset, int size)
		{
			// First point
			Point pt = new Point(content[offset], content[offset+1]);
			myRenderer.InkSpaceToPixel(g, ref pt);

			long time = sTimer.registerSample(content[offset+2]);
			//Console.Out.WriteLine(pt.X + "," + pt.Y + "," + (content[offset+2]+sTimer.strokeStartTime) + "," + time);
            
			String result = pt.X + "," + pt.Y + "," + time;

			for (int i = offset+3; i < offset+size; i++)
				result = result + "," + content[offset + i];
			
			
			for (int i = size; i < 4; i++)
				result = result + ",0";

			return result;
		}

        /// <summary>
        /// Called when the current plugin or the ones previous in the list
        /// threw an exception.
        /// </summary>
        /// <param name="sender">The real time stylus</param>
        /// <param name="data">Error data</param>
        public void Error(RealTimeStylus sender, ErrorData data)
        {
            //Debug.Assert(false, null, "An error occurred.  DataId=" + data.DataId + ", " + "Exception=" + data.InnerException);
        }

        /// <summary>
        /// Defines the types of notifications the plugin is interested in.
        /// </summary>
        public DataInterestMask DataInterest
        {
            get
            {
                return DataInterestMask.StylusDown |
                       DataInterestMask.Packets |
                       DataInterestMask.StylusUp |
                       DataInterestMask.Error;
            }
        }

        // The remaining interface methods are not used in this sample application.
        public void RealTimeStylusDisabled(RealTimeStylus sender, RealTimeStylusDisabledData data) {}
        public void RealTimeStylusEnabled(RealTimeStylus sender, RealTimeStylusEnabledData data){}
        public void StylusOutOfRange(RealTimeStylus sender, StylusOutOfRangeData data) {}
        public void StylusInRange(RealTimeStylus sender, StylusInRangeData data) {}
        public void StylusButtonDown(RealTimeStylus sender, StylusButtonDownData data) {}
        public void StylusButtonUp(RealTimeStylus sender, StylusButtonUpData data) {}
        public void CustomStylusDataAdded(RealTimeStylus sender, CustomStylusData data){}
        public void SystemGesture(RealTimeStylus sender, SystemGestureData data){}
        public void InAirPackets(RealTimeStylus sender, InAirPacketsData data){}
        public void TabletAdded(RealTimeStylus sender, TabletAddedData data){}
        public void TabletRemoved(RealTimeStylus sender, TabletRemovedData data) {}

		/// <summary>
		/// Connects to the java listener running on localhost port 1600
		/// </summary>
		public void socketConnect()
		{
			try
			{
				tc = new TcpClient("127.0.0.1", 1600);
				ns = tc.GetStream();
			}
			catch(Exception e){
			  MessageBox.Show ( e.Message );
			}
		}
		
		/// <summary>
		/// Sends the ascii string "data" to the java listener
		/// </summary>
		/// <param name="data"></param>
		public void socketSend (String data)
		{
			byte[] byData = System.Text.Encoding.ASCII.GetBytes(data);
			sw = new StreamWriter(ns);
			sw.WriteLine(data);
			sw.Flush();
		}

		public void socketFlush ()
		{
			sr = new StreamReader(ns);
			Console.WriteLine(sr.ReadLine());
		}


		public TabletPropertyDescriptionCollection GenerateTPDC() 
		{
			// first set up the data description
			TabletPropertyDescriptionCollection tpdc = new TabletPropertyDescriptionCollection();

			// Define the X and Y extents for this TabletPropertyDescriptionCollection
			TabletPropertyMetrics tpmX = new TabletPropertyMetrics();
			tpmX.Maximum = 100000;
			TabletPropertyMetrics tpmY = new TabletPropertyMetrics();
			tpmY.Maximum = 100000;

			// Define the NormalPressure for the Tablet PropertyDescriptionCollection
			TabletPropertyMetrics tpmNP = new TabletPropertyMetrics();
			// This defines 1024 levels of pressure
			tpmNP.Maximum = 255;

			// store packet status -- which is the way the pen is oriented
			TabletPropertyMetrics tpmPS = new TabletPropertyMetrics();
			tpmPS.Maximum = 10;

			// try storing time stamps?? 
			TabletPropertyMetrics tpmTS = new TabletPropertyMetrics();
			tpmTS.Maximum = int.MaxValue;

			// Add each of the propertyMetrics to the TabletPropertyDescriptionCollection
			// This defines what the data stream will look like, in this case (X, Y, NP)
			tpdc.Add(new TabletPropertyDescription(Microsoft.Ink.PacketProperty.X, tpmX));
			tpdc.Add(new TabletPropertyDescription(Microsoft.Ink.PacketProperty.Y, tpmX));
			tpdc.Add(new TabletPropertyDescription(Microsoft.Ink.PacketProperty.NormalPressure,
				tpmNP));
			tpdc.Add(new TabletPropertyDescription(Microsoft.Ink.PacketProperty.TimerTick, tpmTS));

			return tpdc;
		}

		/// <summary>
		/// Listen for and execute commands (i.e. window overlay position) from the server.
		/// Should be run in a seperate thread.
		/// </summary>
		public void processInput ()
		{
			int x,y,w,h;

			try
			{
				sr = new StreamReader(ns);
				String input;

				while(listen)
				{
					input = sr.ReadLine();
					//Console.Out.WriteLine(input);

					string[] tokens = input.Split(',');
					string header = tokens[0];
					
					if (header.Equals("hidden"))
					{
						this.Hide();
					}
					else if (header.Equals("shown"))
					{
						this.Show();
					}
					else if (header.Equals("recognize"))
					{
						if (recInk != null)
						{
							string result = RecognizeStrokes(recInk);
							socketSend("recognized, " + result);
							recInk = null;
						}
					}
					else if (header.Equals("stroke"))
					{
						int[] packets = new int[tokens.Length-1];

						for (int i = 1; i < tokens.Length; i++) 
						{
							packets[i-1] = (int)double.Parse(tokens[i]);
						}

						if (recInk == null)
							recInk = new Ink();

						TabletPropertyDescriptionCollection tabletProperties  = GenerateTPDC();
						Stroke stroke = recInk.CreateStroke(packets, tabletProperties);
					}
					else
					{
						string[] values = input.Split(',');
			
						if (values.Length == 4) 
						{
							x = int.Parse(values[0]);
							y = int.Parse(values[1]);
							w = int.Parse(values[2]);
							h = int.Parse(values[3]);

							this.Location = new Point(x,y);
							this.Height = h;
							this.Width = w;
						}
					}
				}
			}
			catch(Exception e)
			{
				//MessageBox.Show ( e.Message );
				Application.Exit();
			}
		}

		private void InkCollection_KeyDown(object sender, System.Windows.Forms.KeyEventArgs e)
		{
			socketSend("key," + (int)e.KeyCode);
		}

		private void InkCollection_Click(object sender, EventArgs e)
		{
		}

		private void InkCollection_KeyPress(object sender, System.Windows.Forms.KeyPressEventArgs e)
		{		
		}

		private void InkCollection_MouseLeave(object sender, System.EventArgs e)
		{
			this.Hide();		
		}
    }

	public class StrokeTimer 
	{
		private double previousTime = 0;
		private double sampleFreq = 7.5187;
		public long strokeStartTime = 0;

		public long getCurrentTime()
		{
			return (DateTime.UtcNow.ToFileTimeUtc() - 116444736000000000)/10000;
		}
		
		public long registerSample(int sampleTime)
		{	
			previousTime = previousTime + sampleFreq;
			if (sampleTime - previousTime <= 10) 
			{				
			}
			else if (sampleTime - previousTime <= 30) 
			{
				previousTime = previousTime+(sampleTime - previousTime)/10;
			}
			else
			{
				previousTime = sampleTime;
			}
			return (long)previousTime + strokeStartTime;
		}		
		
		public long startStroke()
		{	
			strokeStartTime = getCurrentTime();
			return strokeStartTime;
		}
	}
}
