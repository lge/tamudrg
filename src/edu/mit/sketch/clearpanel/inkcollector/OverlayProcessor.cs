using System;
using System.IO;

namespace DRG.Ink
{
	/// <summary>
	/// Summary description for NetCommandProcessor.
	/// </summary>
	public class NetCommandProcessor
	{
		private StreamReader m_input;
		private NetworkCollector m_collector;

		public NetCommandProcessor(StreamReader input, NetworkCollector collector)
		{
            m_input = input;
			m_collector = collector;
		}

		// the thread
		public void Run() 
		{
			string input = "";

			try 
			{

				while //((m_input.Peek() != -1) && 
					((input = m_input.ReadLine()) != null)//) 
				{
					Console.WriteLine("received: "+input);
					if (input.StartsWith("getSpecs")) 
						m_collector.sendSpecs();
					else if (input.StartsWith("startRecording")) 
					{
						int first = input.IndexOf(',') + 1;
						int second = input.IndexOf(',',first+1);
						int third = input.LastIndexOf(',');
						string path = input.Substring(first,second-first);
						string localPath = input.Substring(second+1,third-second-1);
						string name = input.Substring(third+1);
						Console.WriteLine("vars>>>{0}<>{1}<>{2}",path,localPath,name);
						m_collector.startRecording(path,localPath,name);
					}
					else if (input.StartsWith("stopRecording"))
						m_collector.stopRecording();
					else if (input.StartsWith("disconnect"))
						m_collector.disconnect();
					else if (input.StartsWith("ping")) 
						m_collector.ping();
					else if (input.StartsWith("instructions")) 
						m_collector.instructions();
					else if (input.StartsWith("endInstructions")) 
						m_collector.endInstructions();
					else if (input.StartsWith("questionnaire")) 
						m_collector.questionnaire();
					else
					{
						// there is an error
						Console.WriteLine("Unknown message");
						m_collector.fail();
					}

				}
			}
			catch (IOException e) 
			{
				// send error message
				m_collector.sendErrorMsg(e.Message);
				Console.WriteLine("There was an IO error"+e.ToString());
				//Application.Exit();
				return;
			}
			
		}
	}
}
