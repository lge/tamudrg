#!/usr/bin/python

import random

#task = random.choice(['Best', 'Context', random.choice(['IsA', 'CanBeA'])])
#task = random.choice(['Best', random.choice(['IsA', 'CanBeA'])])
task = random.choice(['IsA', 'CanBeA'])

f1 = open(task+'.html')
instructions = f1.read()

f2 = open('Categories.html')
categories = f2.read()

f3 = open('java.html')
javaInstructions = f3.read()

print "Content-Type: text/html\n\n"
print '<TITLE>Stroke Labeling</TITLE>'
print instructions
print categories
print javaInstructions
print '<H2>Stroke Labeling</H2>'
print '<TABLE border="1"><TR><TD>'
print '<APPLET CODE="edu.mit.sketch.label.LabelApplet"'
print '        ARCHIVE="labeling.jar" WIDTH=1050 HEIGHT=550>'
print '<PARAM NAME=labelTask VALUE=' + task + '>'
print '</APPLET>'
print '</TD></TR></TABLE>'


