#! /bin/tcsh

if ( `hostname` != 'fracas' ) then
    echo "You must run this on fracas"
    exit
endif

unsetenv CLASSPATH

setenv BASE /opt/drg/dist/label

/opt/jdk/j2sdk1.4/bin/java \
  -Djava.security.policy=${BASE}/java.policy \
  -Djava.rmi.server.codebase=http://fracas.csail.mit.edu/stroke-labeling/labeling.jar \
  -classpath ${BASE}/labelserver.jar \
  edu.mit.sketch.label.StartLabelServer >>& /opt/drg/server-output
