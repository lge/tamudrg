#! /bin/sh

if [ `hostname` != 'fracas' ]; then
    echo "You must run this on fracas"
    exit
fi

CLASSPATH=
export CLASSPATH

BASE=/opt/drg/dist/label
JAVA_PROGRAM=/opt/jdk/j2sdk1.4/bin/java
PIDFILE=/opt/drg/labelserver.pid
RMI_PROGRAM=/opt/jdk/j2sdk1.4/bin/rmiregistry
RMI_PIDFILE=/opt/drg/rmiserver.pid

case "$1" in
start)	echo -n "Starting labeling server"
        /sbin/start-stop-daemon --start --background --make-pidfile --pidfile $RMI_PIDFILE --exec $RMI_PROGRAM
        /sbin/start-stop-daemon --start --background --make-pidfile --pidfile $PIDFILE \
            --exec $JAVA_PROGRAM -- -Djava.security.policy=${BASE}/java.policy \
            -Djava.rmi.server.codebase=http://fracas.csail.mit.edu/stroke-labeling/labeling.jar \
            -classpath ${BASE}/labelserver.jar \
            edu.mit.sketch.label.StartLabelServer > /opt/drg/server-output
        echo "." 
	;;

stop)	echo -n "Stopping labeling server"
        /sbin/start-stop-daemon --stop --pidfile $PIDFILE --exec $JAVA_PROGRAM
        /sbin/start-stop-daemon --stop --pidfile $RMI_PIDFILE --exec $RMI_PROGRAM
        echo "."
        ;;
esac
exit 0


