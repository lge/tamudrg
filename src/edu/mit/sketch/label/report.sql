-- Show how many strokes in each category have been labeled 2+ times
SELECT condition, count(*) AS "num strokes labeld 2+ times"
  FROM strokes_to_label_counts 
  WHERE count > 1
  GROUP BY condition;

-- count total number of strokes labeled 2+ times
SELECT count(distinct stroke_id) AS "num strokes labeld 2+ times"
  FROM strokes_to_label_counts 
  WHERE count > 1;


-- how many labelers are there for each condition
SELECT condition, count(distinct labeler) FROM hand_classified_strokes
  GROUP BY condition;

-- how many labelers are there total
SELECT count(distinct labeler)
  FROM hand_classified_strokes;

-- how many strokes are there per condition
select condition, count(distinct stroke_id) 
       from hand_classified_strokes
       group by condition;

-- count sketches
select st.name, count(*) from 
  sketches as sk 
  JOIN studies AS st ON (sk.study = st.id)
  GROUP BY name;

-- count the number of strokes
select name, count(distinct s.id)
    FROM sketches as sk 
    JOIN studies AS st ON (sk.study = st.id)
    JOIN strokes as s ON (s.sketch = sk.id)
    GROUP BY name;

-- count the number of authors
SELECT st.name, count(distinct author)
   FROM sketches as sk 
   JOIN studies AS st ON (sk.study = st.id)
   GROUP BY st.name;

-- number of agreed on strokes per condition (e.g. the dataset sizes)
SELECT condition, count(DISTINCT stroke_id) 
     FROM agreed_labels 
     GROUP BY condition;

-- number of agreed on strokes total
SELECT count(DISTINCT stroke_id) 
     FROM agreed_labels;

-- number of agreed upon strokes in each class and condition
SELECT condition, c.name, count(*) 
     FROM agreed_labels 
     JOIN classes as c on class = c.id
     GROUP BY condition, c.name
     ORDER BY condition, c.name;

-- number of agreed upon strokes in each class and condition and study
SELECT y.name, condition, c.name, count(*) 
     FROM sketches as sk, strokes as st, studies as y,
          agreed_labels 
     JOIN classes as c on class = c.id
     WHERE st.sketch = sk.id AND sk.study = y.id AND st.id = stroke_id
     GROUP BY y.name, condition, c.name
     ORDER BY y.name, condition, c.name;

-- list strokes in which at least 2 labelers agreed on more than one class
SELECT stroke_id, condition, count(class) FROM agreed_labels 
  GROUP BY condition, stroke_id 
  HAVING count(*) > 1
  ORDER BY condition, count(*), stroke_id;

-- count the above
SELECT condition, count(distinct stroke_id) FROM
 (SELECT stroke_id, condition, count(class) FROM agreed_labels 
   GROUP BY condition, stroke_id 
   HAVING count(*) > 1
   ORDER BY condition, count(*), stroke_id) as s
 GROUP BY condition;

-- We want to count all the strokes that were labeled by 2+
-- people that were given more than one label by some labeler.
-- Make sure to count strokes and not labelers since the number of
-- labelers will be different per stroke.  Also only include strokes
-- labeled 2+ times so that they have roughly the same chance of
-- having a multilabeling.
SELECT condition, multis, total, multis*1.0/total as percent_multis FROM 
 (SELECT condition, count(DISTINCT stroke_id) AS multis
      FROM num_labels_per_stroke
      JOIN fully_labeled 
      USING (stroke_id, condition) 
      WHERE num_labels > 1
      GROUP BY condition) AS a
 JOIN
 (SELECT condition, count(DISTINCT stroke_id) AS total
      FROM fully_labeled
      GROUP BY condition ) AS f
 USING (condition);

--------------------------------------------------

-- The class confusion matrix.  Show how many lines got labeled as
-- lines, arcs, etc...  This shows which classes get confused with
-- each other and not just the number of disagreements
SELECT condition, h1.class, h2.class, count(h1.class)
  FROM hand_classified_strokes as h1
  INNER JOIN hand_classified_strokes as h2
  USING (stroke_id, condition)
  WHERE h1.value = 't' AND h2.value = 't' AND
        h1.labeler <> h2.labeler
  GROUP BY condition, h1.class, h2.class
  HAVING (h1.class <= h2.class);


-- We want to count all the strokes that were assigned 2+ labels.
-- Make sure to count strokes and not labelers since the number of
-- labelers will be different per stroke.  Also only include strokes
-- labeled 2+ times so that they have roughly the same chance of
-- having a multilabeling.-- How many of the strokes getting labeled at all had multiple labelings
SELECT condition, multis, total, multis*1.0/total as percent_multis FROM 
 (SELECT condition, count(DISTINCT stroke_id) AS multis
      FROM num_labels_per_stroke
      WHERE num_labels > 1
      GROUP BY condition) AS a
 JOIN
 (SELECT condition, count(DISTINCT stroke_id) AS total
      FROM hand_classified_strokes
      GROUP BY condition ) AS f
 USING (condition);

-- list strokes that got 2+ labels from different labelers
SELECT condition, stroke_id, n1.num_labels, n2.num_labels
        FROM num_labels_per_stroke as n1
        JOIN num_labels_per_stroke as n2
        USING (condition, stroke_id)
        WHERE n1.num_labels > 1 AND 
              n2.num_labels > 1 AND  
              n1.labeler <> n2.labeler
        ORDER BY condition;

-- list all of the entries for strokes that are labeled multiple times.
-- This is the same as above but shows which classes where selected
-- not just how many
SELECT * FROM
 (SELECT stroke_id, labeler, condition, value FROM  
    (SELECT * FROM hand_classified_strokes        
      WHERE condition <> 'drg isa labeling') AS nodrg
    WHERE value = 't'
    GROUP BY stroke_id, labeler, condition, value
    HAVING (count(value) > 1)) AS multis
  INNER JOIN hand_classified_strokes as h 
    USING( stroke_id, labeler, condition, value )
  ORDER BY stroke_id, labeler, condition;

-- This is the querry for agreed labels
SELECT stroke_id, studies.name, condition, class, count(class)
  FROM studies, strokes as s, sketches as sk, hand_classified_strokes AS h1
  JOIN hand_classified_strokes AS h2
     USING (stroke_id, condition, class, value)
  WHERE s.id = h1.stroke_id AND
        s.sketch = sk.id AND
        sk.study = studies.id AND
        value = 't' AND 
        h1.labeler <> h2.labeler AND
        condition <> 'drg isa labeling'
  GROUP BY stroke_id, studies.name, condition, class
  HAVING count(stroke_id) >= 2
  ORDER BY condition, stroke_id;

-- Show the nubmer of votes for each class and the total number of
-- times that the stroke was labeled in the given condition.  Note
-- that for the IsA and CanBeA cases the some of all votes for a
-- stroke can be more than the number of labelings since more than one
-- class can be choosen per stroke.
SELECT stroke_id, condition, class, 
        count(h.class) as votes, c.count as num_labelings FROM 
   stroke_label_counts AS c join hand_classified_strokes AS h
   USING( stroke_id, condition )
   WHERE h.value = 't' and condition <> 'drg isa labeling'
   GROUP BY stroke_id, condition, c.count, h.class
   HAVING (c.count > 1)
   ORDER BY stroke_id, condition, h.class;

-- List all of the strokes and their possible labels as assigned in
-- some condition
SELECT stroke_id, class FROM hand_classified_strokes
  WHERE value = 't'
  GROUP BY stroke_id, class;


-- Show the average fit errors for strokes with agreed upon labels
-- across tasks
SELECT study_name, classes.name, count(*) as num_strokes, 
       avg(error_to_length) as avg_error, 
       avg(error_to_length_sq) as avg_error_sq, 
       stddev(error_to_length) as stddev_error,
       stddev(error_to_length_sq) as stddev_error_sq
  FROM classes, agreed_labels as a
  JOIN fit_errors as e
  USING (stroke_id, class)
  WHERE (condition = 'Best' OR condition = 'Context') AND class = classes.id
  GROUP BY study_name, classes.name
  ORDER BY classes.name, study_name;

-- Show the strokes with the worst errors
SELECT distinct on (error_to_length, stroke_id)
       stroke_id, class, error, error_to_length, error_to_length_sq
  FROM agreed_labels as a
  JOIN fit_errors as e
  USING (stroke_id, class)
  WHERE (condition = 'Best' OR condition = 'Context')
  ORDER BY error_to_length desc, stroke_id, class
  LIMIT 40;

-- Show the errors in a table that we can do Annova on
SELECT study_name, class, error_to_length
  FROM agreed_labels as a
  JOIN fit_errors as e
  USING (stroke_id, class)
  WHERE (condition = 'Best' OR condition = 'Context')
  ORDER BY class, study_name, stroke_id;


-- Find strokes labeled differently in different contexts.  E.g. 13886
-- and 13905 which are arcs out of context and lines in context.  This
-- is only for single label caases since multi labels lead to problems
-- since a class mismatch does not necessarily imply an error, it may
-- just be 2 valid labels
SELECT a1.condition, a2.condition,
       a1.class as best_class, 
       a2.class as context_class,
       count(*)
  FROM agreed_labels as a1 
  JOIN agreed_labels as a2
  USING (stroke_id)
  -- don't change to 'IsA' or 'CanBeA'
  WHERE a1.condition = 'Best' AND a2.condition = 'Context'
  GROUP BY a1.condition, a2.condition, best_class, context_class;

-- show how many times each stroke was labeled per user.  This is only
-- useful as a debug tool to make sure that the db inserts work
-- correctly.
SELECT labeler, condition, stroke_id, count(labeler) 
  FROM (select * from hand_classified_strokes 
          where condition <> 'drg isa labeling') as labels 
  GROUP BY labeler, condition, stroke_id
  ORDER BY count(labeler) desc limit 20 ;

-- List unique labelers and the number of sessions they did
select labeler, count(labeler) from study_referrals 
  group by labeler
  order by count;

-- Show how many strokes in each category are not fully labeled
select condition, count(*) as "labeled less than twice"
  from strokes_to_label_counts 
  where count < 2 group by condition;

-- Count the number of times 2 people label the same stroke with a
-- given class in each way e.g. both yes, both no, or different.  Note
-- that there is double counting since a=Y b=N and b=N a=Y are counted
-- as are the a=Y b=Y and b=Y a=Y
SELECT classes.name, a.condition, a.value, b.value, count(*) 
        FROM classes, hand_classified_strokes AS a INNER JOIN
             hand_classified_strokes AS b 
        USING (stroke_id, class, condition)
        WHERE (a.labeler != b.labeler AND
               a.condition <> 'drg isa labeling' AND
--               a.value = 't' AND b.value = 'f' AND
               (a.value = 't' OR b.value = 't') AND
               classes.id = a.class)
        GROUP BY classes.name, a.class, a.condition, a.value, b.value
        ORDER BY a.class, a.condition, a.value, b.value, count(*);

-- Get the auto_classify errors for strokes on a per-user per-task
-- basis so we can do 2-way anova.
SELECT author, task, a.class, a.stroke_id, a.error_to_length
        FROM auto_classify as a, sketches as sk, strokes as st
        WHERE st.sketch = sk.id AND st.id = a.stroke_id AND
              sk.study = 1 AND class = 3
        ORDER BY author, task, a.class, a.stroke_id;

SELECT condition, a.class, h.class
        FROM agreed_labels as h
        JOIN auto_classify as a
        USING (stroke_id)
        GROUP BY condition, a.class, h.class;


-- this is more subtle than I thought for the multi label case.  We
-- need to have a row for each time that a stroke was labled C1 by L1
-- and C1 by L2 and for each time it was labeled C1 by L1 and C2 by L2
-- and not C1 by L2 i.e. (l1.c1 = t and l2.c1 = t) OR (l1.c1 = t and
-- l2.c1 = f and l2.c2 = t) It probably isn't quite right for doing
-- Kappa because we are double counting the diagonal but not all of
-- the others.  for example if L1 = {c1,c2} and L2={c1} then we tally
-- c1,c1 = 2 and c2,c1 = 1 but there is no compliment for c1,c2...
SELECT  condition, stroke_id, h1.class as class1, h1.value, h2.class as class2, h2.value, h3.class, h3.value
        FROM hand_classified_strokes as h1
        INNER JOIN hand_classified_strokes as h2  
        USING (stroke_id, condition)  
        INNER JOIN hand_classified_strokes as h3
        USING (stroke_id, condition)
        WHERE h1.value = 't' AND h2.value = 't' AND 
              ((h1.class = h2.class AND h2.class = h3.class AND h3.value = 't') OR
               (h1.class = h3.class AND h3.value = 'f')) AND
              h1.labeler <> h2.labeler AND 
              h2.labeler = h3.labeler AND
              condition = 'IsA' -- <> 'drg isa labeling' 
        ORDER BY stroke_id, h1.class, h2.class;

-- This is the same as above but with output sutitable for the Kappa
-- function in Stats.java
SELECT  condition, h1.class as class1, h2.class as class2, 
        count(h1.class) as num
        FROM hand_classified_strokes as h1
        INNER JOIN hand_classified_strokes as h2  
        USING (stroke_id, condition)  
        INNER JOIN hand_classified_strokes as h3
        USING (stroke_id, condition)
        WHERE h1.value = 't' AND h2.value = 't' AND 
              ((h1.class = h2.class AND h2.class = h3.class AND h3.value = 't') OR
               (h1.class = h3.class AND h3.value = 'f')) AND
              h1.labeler <> h2.labeler AND 
              h2.labeler = h3.labeler AND
              condition in ('IsA', 'CanBeA')
        GROUP BY condition, h1.class, h2.class  
        ORDER BY condition, h1.class, h2.class;


-- Do the confusion matrix based on binary decisions instead of
-- assignments of labels.  This is so we can make a bunch of 2 by 2
-- matrixes for each class and then do kappa on them
SELECT  class as condition,
        h1.value as class1, h2.value as class2,
        count(*) as num
        FROM hand_classified_strokes as h1
        INNER JOIN hand_classified_strokes as h2  
        USING (stroke_id, condition, class)  
        WHERE h1.labeler <> h2.labeler AND
              condition = 'IsA'
        GROUP BY class, h1.value, h2.value
        ORDER BY class, h1.value, h2.value;

-- a subset of the hand_classified_strokes table for strokes labeled
-- 2+ times and a new column for the number of times the stroke was
-- classified per condition
SELECT stroke_id, labeler, condition, class, value, count as num_labelers
       FROM stroke_label_counts 
       JOIN hand_classified_strokes
       USING (stroke_id, condition)
       WHERE (stroke_label_counts.count >= 2);


-- select  *
--         FROM agreed_labels as a1
--         JOIN agreed_labels as a2
--         USING (stroke_id, class)
--         WHERE a1.condition = 'CanBeA' AND
--               a2.condition = 'IsA' AND
--               a1.class = a2.class;
select stroke_id, count(*) from 
   (select a1.stroke_id
        from agreed_labels as a1,
             agreed_labels as a2
        where a1.stroke_id = a2.stroke_id AND
              a1.condition = 'CanBeA' AND
              a2.condition = 'IsA' AND
              a1.class <> a2.class
        group by a1.stroke_id) as s
   join agreed_labels using (stroke_id)
   WHERE condition in ('CanBeA', 'IsA')
   group by stroke_id
-- this probably needs to group by condition as well...
   order by stroke_id;


-- Show strokes that got no lables in CanBeA and IsA cases:
SELECT stroke_id, condition, count(*) FROM
     (SELECT stroke_id, condition, labeler, value, count(value)
      FROM hand_classified_strokes 
      WHERE condition IN ('CanBeA', 'IsA')
      GROUP BY stroke_id, condition, labeler, value
      HAVING (value = 'f' AND count(value) = 5)) AS nones
     GROUP BY stroke_id, condition
     HAVING count(*) > 1
     ORDER BY stroke_id;
     

