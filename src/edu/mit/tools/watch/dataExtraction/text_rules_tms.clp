;;; ada: for tms version

;;; defining global variables

;;; The list of words to look for 
(bind $?*key-words* (create$ "then" "so" "we have" "there is" "there are" "next" "we've" "possibly" "ok" "it's" "it is" "with" "there's" "which" "plus" "i'll" "i will" "let's" "let us" "that's" "that is" "this is" "these are"))

;;; "and" words which right now is just "and"
(bind $?*and-words* (create$ "and"))

;;; "the" words which right now is just "the"
(bind $?*the-words* (create$ "the"))

;;; mumbled words 
(bind $?*mumbled-words* (create$ "<ahhh>" "<ummm>" "<uhhh>" "<ahhh>" "<hmmm>"))


;;; This rule adds statements for speech text containing and-words 
(defrule speech-and-words
  ?z <- (speech (startTime ?a) (endTime ?e) (text ?b))
  (test (> (num-contains-strings ?b $?*and-words*) 0))
  =>
  (assert-string ?*dep* (and-words-at ?a ?e num ?x) ?a ?e (num-contains-strings ?b $?*and-words*) 1 (call ?z getFactId))
)


;; This rule adds statements for speech text containing key-words
;; This rule is sort of bad since we run num-contains-strings twice... 
;; not sure how to fix this...
(defrule speech-key-words
  ?z <- (speech (startTime ?a) (endTime ?e) (text ?b))
  (test (> (num-contains-strings ?b $?*key-words* ) 0))
  =>
  (assert-string ?*dep* (key-words-at ?a ?e num ?x) ?a ?e (num-contains-strings ?b $?*key-words*) 1 (call ?z getFactId))
)

;;; This rule adds statements for speech text containing a the
;;; at the beginning of the speech
(defrule speech-the-words
  ?z <- (speech (startTime ?a) (endTime ?e) (text ?b))
  (test (> (start-of-string ?b $?*the-words*) 0))
  =>
  (assert-string ?*dep* (the-words-at ?a ?e) ?a ?e 1 (call ?z getFactId))
)


;;; This rule adds statements for speech text containg mumbled
;;; words
(defrule mumbled-words
  ?z <- (speech (startTime ?start) (endTime ?end) (text ?t))
  (test (> (end-of-string ?t $?*mumbled-words*) 0))
  =>
  (assert-string ?*dep* (mumbled-words-at ?start ?end) ?start ?end 1 (call ?z getFactId))
)



;;; If and is surrounded by gaps on both sides there is likely a break
(defrule and-with-gaps
  ?z1 <- (and-words-at ?gap1-end ?e num ?num)
  ?z2 <- (speech (startTime ?gap1-end) (endTime ?gap2-start))
  ?z3 <- (gap ?gap1-start ?gap1-end)
  ?z4 <- (gap ?gap2-start ?gap2-end)
  (test (< ?*and-gap* (- ?gap1-end ?gap1-start)))
  (test (< ?*and-gap* (- ?gap2-end ?gap2-start)))
  =>
  (assert-string ?*dep* (and-gaps ?gap1-end ?e) ?gap1-end ?e 4 (call ?z1 getFactId) (call ?z2 getFactId) (call ?z3 getFactId) (call ?z4 getFactId))
)


;;; Rule for linking mumbled words to the next speech utterance
(defrule link-mumbled-words 
  ?z1 <- (mumbled-words-at ?start-1 ?e)
  ?z2 <- (speech (startTime ?start-1) (endTime ?end-1) (idNum ?id))
  ?z3 <- (speech (startTime ?start-2) (endTime ?end-2) (idNum =(+ 1 ?id)))
  =>
  (assert-string ?*dep* (linked-by-mumbles ?start-1 ?end-2) ?start-1 ?end-2 3 (call ?z1 getFactId) (call ?z2 getFactId) (call ?z3 getFactId))
)


;;; Rule for indicating the number of words in one speech 
;;; utterance that also occur in the following speech 
;;; utterance
(defrule repeat-words
  ?z1 <- (speech (text ?t1) (startTime ?s) (idNum ?i))
  ?z2 <- (speech (text ?t2) (endTime ?e)   (idNum =(+ ?i 1)))

  ; test the number of overlap in words from the first speech to the 
  ; second and fire the rule if there is an overlap
  (test (> (num-words-overlap ?t1 ?t2) 0))
  =>
  (assert-string ?*dep* (repeat-words ?s ?e num ?x) ?s ?e (num-words-overlap ?t1 ?t2) 2 (call ?z1 getFactId) (call ?z2 getFactId))
)

