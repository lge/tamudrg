;;; defining global variables

;;; The list of words to look for 
(bind $?*key-words* (create$ "then" "so" "we have" "there is" "there are" "next" "we've" "possibly" "ok" "it's" "it is" "with" "there's" "which" "plus" "i'll" "i will" "let's" "let us" "that's" "that is" "this is" "these are"))

;;; "and" words which right now is just "and"
(bind $?*and-words* (create$ "and"))

;;; "the" words which right now is just "the"
(bind $?*the-words* (create$ "the"))

;;; mumbled words 
(bind $?*mumbled-words* (create$ "<ahhh>" "<ummm>" "<uhhh>" "<ahhh>" "<hmmm>"))


;;; This rule adds statements for speech text containing and-words 
(defrule speech-and-words
  (speech (startTime ?a) (endTime ?e) (text ?b))
  (test (> (num-contains-strings ?b $?*and-words*) 0))
  =>
  (assert (and-words-at ?a ?e num (num-contains-strings ?b $?*and-words*)))
)


;; This rule adds statements for speech text containing key-words
;; This rule is sort of bad since we run num-contains-strings twice... 
;; not sure how to fix this...
(defrule speech-key-words
  (speech (startTime ?a) (endTime ?e) (text ?b))
  (test (> (num-contains-strings ?b $?*key-words* ) 0))
  =>
  (assert (key-words-at ?a ?e num (num-contains-strings ?b $?*key-words*)))
)

;;; This rule adds statements for speech text containing a the
;;; at the beginning of the speech
(defrule speech-the-words
  (speech (startTime ?a) (endTime ?e) (text ?b))
  (test (> (start-of-string ?b $?*the-words*) 0))
  =>
  (assert (the-words-at ?a ?e))
)


;;; This rule adds statements for speech text containg mumbled
;;; words
(defrule mumbled-words
  (speech (startTime ?start) (endTime ?end) (text ?t))
  (test (> (end-of-string ?t $?*mumbled-words*) 0))
  =>
  (assert (mumbled-words-at ?start ?end))
)



;;; If and is surrounded by gaps on both sides there is likely a break
(defrule and-with-gaps
  (and-words-at ?gap1-end ?e num ?num)
  (speech (startTime ?gap1-end) (endTime ?gap2-start))
  (gap ?gap1-start ?gap1-end)
  (gap ?gap2-start ?gap2-end)
  (test (< ?*and-gap* (- ?gap1-end ?gap1-start)))
  (test (< ?*and-gap* (- ?gap2-end ?gap2-start)))
  =>
  (assert (and-gaps ?gap1-end ?e))
)


;;; Rule for linking mumbled words to the next speech utterance
(defrule link-mumbled-words 
  (mumbled-words-at ?start-1 ?e)
  (speech (startTime ?start-1) (endTime ?end-1) (idNum ?id))
  (speech (startTime ?start-2) (endTime ?end-2) (idNum =(+ 1 ?id)))
  =>
  (assert (linked-by-mumbles ?start-1 ?end-2))
)


;;; Rule for indicating the number of words in one speech 
;;; utterance that also occur in the following speech 
;;; utterance
(defrule repeat-words
  (speech (text ?t1) (startTime ?s) (idNum ?i))
  (speech (text ?t2) (endTime ?e)   (idNum =(+ ?i 1)))

  ; test the number of overlap in words from the first speech to the 
  ; second and fire the rule if there is an overlap
  (test (> (num-words-overlap ?t1 ?t2) 0))
  =>
  (assert (repeat-words ?s ?e num (num-words-overlap ?t1 ?t2) ))
)

