;;; defining templates
(deftemplate sketch 
  "This template is for items that were sketched"
  (slot text)       ; Text from Excel
  (slot objType)    ; i.e. Rectangle
  (slot objID)      ; an identifying string
  (slot startTime)  ; start time
  (slot endTime)    ; end time
  (slot actionType) ; drawing or erasing
  (slot idNum)      ; # indicating sketch order
)

(deftemplate speech 
  "This template is for items that were spoken"
  (slot text)       ; Text from Excel
  (slot startTime)  ; start time
  (slot endTime)    ; end time
  (slot idNum)      ; # indicating speech order
)

(deftemplate sketch-group
  "This template is for a group of sketched components that are part of the same object"
  (slot startTime)   ; start time
  (slot endTime)     ; end time
  (slot objType)     ; i.e. Rectangle
  (slot objID)       ; an identifying string 
  (slot groupID)     ; This is to easily identify a group
  (multislot idNums) ; idNum of component sketches
)

;(deftemplate speech-action
;  "Action described in speech"
;  (slot startTime)   ; start time
;  (slot endTime)     ; end time
;  (slot type)     ; i.e. Pendulum
;  (slot equal)    ; equal?
;  (slot identical)  ; identical?
;  (slot touch)       ; tocuhing?
;  (slot number)      ; number of objects
;)
