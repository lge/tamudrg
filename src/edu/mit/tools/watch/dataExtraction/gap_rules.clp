;;; Define what a long-gap is
(bind ?*long-gap* 25)

;;; Define what a and-gap is
(bind ?*and-gap* 7)

;;; defining rules

;;; this rule says you can't have a gap within a sketch event
(defrule sketch-time 
  (sketch (startTime ?x) (endTime ?y)) 
  => 
  (assert (no-gap ?x ?y))
)

;;; this rule says you can't have a gap within a speech event
(defrule speech-time 
  (speech (startTime ?x) (endTime ?y)) 
  => 
  (assert (no-gap ?x ?y))
)

;;; This rule says you can't have a gap where speech and sketching events overlap
(defrule sketch-speech-overlap 
  (sketch (startTime ?a) (endTime ?b)) 
  (speech (startTime ?c) (endTime ?d)) 
  (test (eq (overlap ?a ?b ?c ?d) TRUE)) 
  => 
  (assert (no-gap (min ?a ?c) (max ?b ?d)))
)

;;; This rule combines existing overlapping no-gap facts into one fact
(defrule combine-no-gap 
  ?g1 <- (no-gap ?a ?b) 
  ?g2 <- (no-gap ?c ?d) 
  (test (eq (anyOverlap ?a ?b ?c ?d) TRUE)) 
  (test (neq ?g1 ?g2)) 
  => 
;  (printout t "combining " ?a " " ?b " " ?c " " ?d crlf) 
  (retract ?g1) 
  (retract ?g2) 
  (assert (no-gap (min ?a ?c) (max ?b ?d)))
)

;;; This rule adds a no-gap between pieces of the same sketched item
(defrule same-object
  ?o1 <- (sketch (objType ?a) (objID ?b) (idNum ?c) (startTime ?s1) (endTime ?e1))
  ?o2 <- (sketch (objType ?a) (objID ?b) (idNum ?d) (startTime ?s2) (endTime ?e2))
  (test (neq ?o1 ?o2))
  (test (< ?c ?d)) ;;insures rule runs once, not twice
  =>
;  (printout t "same object type:" ?a " id:" ?b " nums: " ?c " and " ?d crlf) 
  (assert (no-gap (min ?s1 ?s2) (max ?e1 ?e2))))

;;; Rules for adding gaps

;;; need to indicate start of timeline so add a no-gap there
(assert (no-gap 0 0))

;;; this rule adds lots of gaps
(defrule gap
  (no-gap ?a ?b)
  (no-gap ?c&:(> ?c ?a) ?d)
  (not (no-gap ?e&:(and (> ?e ?a) (< ?e ?c)) ?f))
  =>
  ;(printout t "found a pair! " ?a " " ?b " and " ?c " " ?d crlf)
  (assert (gap ?b ?c))
)

;;; occasionally bad gaps maybe added because of no-gaps that are added. 
;;; This rule fixes this problem by taking out incorrect gaps
(defrule remove-gap
  ?r <- (gap ?a ?b)
  (no-gap ?c ?d)
  (test (eq (overlap ?a ?b ?c ?d) TRUE))
  =>
  ;(printout t "REMOVING gap " ?a " " ?b " -- no-gap " ?c " " ?d crlf)
  (retract ?r)
)



;; In general not sure that long pauses are needed! Can't we just
;; check the length of the gaps?

;;; This rule asserts that there is a long-pause when 
;;; a gap is larger than ?*long-gap*
(defrule long-pause
  (gap ?start ?end)
  (test (< ?*long-gap* (- ?end ?start)))
  =>
  (assert (long-pause ?start ?end))
)

;;; Sometimes a long-pause may get asserted but later a supporting gap 
;;; may be removed. Fix this by checking the long-pause to make sure 
;;; that if there is no gap there it is removed. 
(defrule remove-long-pause
  ?r <- (long-pause ?a ?b)
  (not (gap ?a ?b))
  =>
  (retract ?r)
)

;;; We shouldn't really look at the long pauses that occur prior to 
;;; erasures. So take those out.
(defrule remove-erase-pause
  ?r <- (long-pause ?start ?end)
  (sketch (startTime ?end) (actionType erase))
  =>
  (retract ?r)
)


