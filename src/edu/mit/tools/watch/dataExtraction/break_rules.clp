;;; Define what a sig-gap is
(bind ?*sig-gap* 5)

(bind $?*breaks* (create$))


(deffunction contains-break (?type ?time)
  ;; this function returns true if the type and time of the break are in the break list
  ;; and false if they are not

  (bind ?list-length (length$ $?*breaks*))
  (bind ?n 1) 
  (while (< ?n ?list-length) do 
    (bind ?num (nth$ ?n $?*breaks*))
    (bind ?t (nth$ (+ ?n 1) $?*breaks*))

    ; (printout t "%comparing%"?type"%"?t"%"?time"%"?num crlf)

    (if (numberp ?num)
     then
       (if (and (eq ?num ?time)
                (eq ?t ?type))
        then 
          ; (printout t "%EQUAL%"?type"%"?t"%"?time"%"?num crlf)
          (return TRUE)
        ))
    (bind ?n (+ 2 ?n))
  )

  (return FALSE)
)


(deffunction remove-break-from-list (?time ?type $?brks)
  (bind $?ret-brks (create$ $?brks))

  (bind ?list-length (length$ $?ret-brks))
  (bind ?n 1) 
  (while (< ?n ?list-length) do 
    (bind ?num (nth$ ?n $?ret-brks))
    (bind ?t (nth$ (+ ?n 1) $?ret-brks))

    (if (numberp ?num)
     then
       (if (and (eq ?num ?time)
                (eq ?t ?type))
        then 
          (bind $?ret-brks (delete$ $?ret-brks ?n (+ 1 ?n)))
          ;; now 2 fewer elements in the list
          (bind ?list-length (- ?list-length 2))
        else
          ;; only increment the counter if we haven't just deleted
          (bind ?n (+ 2 ?n))
       ))
  )
  (return $?ret-brks)
)




(deffunction insert-break (?type ?time)
  ;(printout t "list " $?*breaks* crlf)
  ; (printout t "adding break " ?time crlf)
  (if (eq FALSE (contains-break ?type ?time))
   then 
     ; (printout t "^^^ adding new to break list ^^^ " ?time ?type" ^^^" crlf)
     (bind $?*breaks* (insert$ $?*breaks* (+ 1 (length$ $?*breaks*)) ?time))
     (bind $?*breaks* (insert$ $?*breaks* (+ 1 (length$ $?*breaks*)) ?type))
     ;(printout t "list " $?*breaks* crlf)
  )
)



(deffunction delete-break (?type ?time)
  ;(printout t "list " $?*breaks* crlf)
  ;(printout t "deleting break " ?time crlf)

  ;(printout t "^^^ wouldnt delete from break list ^^^ " ?time " ^^^" crlf)

  (bind ?list-length (length$ $?*breaks*))
  (bind ?n 1) 
  (while (< ?n ?list-length) do 
    ; (printout t "n " ?n "list l " ?list-length crlf)

    (bind ?num (nth$ ?n $?*breaks*))
    (bind ?t (nth$ (+ ?n 1) $?*breaks*))
    (if (numberp ?num)
     then
       (if (and (eq ?num ?time)
                (eq ?t ?type))
        then 
          (bind $?*breaks* (delete$ $?*breaks* ?n (+ 1 ?n)))
          ;; now 2 fewer elements in the list
          (bind ?list-length (- ?list-length 2))
        else
          ;; only increment the counter if we haven't just deleted
          (bind ?n (+ 2 ?n))
       ))
  )


  ;(bind $?*breaks* (delete$ $?*breaks* (member$ ?time $?*breaks*) (member$ ?time $?*breaks*)))
  ;(printout t "list " $?*breaks* crlf)


  ; check to see if taking out a break means adding a new break for sketch-unit
  (bind ?next (next-sketch-unit ?time $?*sketch-units*))
  (if
    (eq FALSE (first-sketch-unit-since-break ?next
                                            (create$ (length$ $?*breaks*)
                                                     $?*breaks* $?*sketch-units*)))
  then
    (assert (break-if-gap ?next))
    ;(printout t ?next " adding Break-if-gap assertion" crlf)
  )


)


;;; potential problem with breaks: they have multiple sources and may need to be retracted:
;; two solutions, add a tag to the break to indicate why it is there
;; have 2 step processing, do everything minus breaks and then do breaks seperately.
;; going with tag for now.
(defrule long-pause-break
  (long-pause ?start ?end)
  =>
  (insert-break longPause ?end)
  (assert (break longPause ?end))
  ; (printout t ?end " adding long-pause" crlf)
  
)

(defrule retract-long-pause-break
  ?r <- (break longPause ?end)
  (not (long-pause ?start ?end))
  =>
  (delete-break longPause ?end)
  (retract ?r)
  ; (printout t ?end " retracting break because of no long pause" crlf)
)

(defrule sketch-unit-break
  (gap ?gs ?start)
  (sketch-unit ?start ?end)
  ;  (printout t "maybe adding *** " ?start crlf)
  (test (eq FALSE (first-sketch-unit-since-break ?start 
                                                 (create$ (length$ $?*breaks*)
                                                          $?*breaks* $?*sketch-units*)
        )))
  =>
  ;  (printout t "sketch unit rule firing " ?start crlf)
  (insert-break sketchUnit ?start)
  (assert (break sketchUnit ?start))
  ;  (printout t ?start " adding sketch-unit" crlf)
)

(defrule retract-sketch-unit-break-1
  ?b <- (break sketchUnit ?start)
  (not (sketch-unit ?start ?end))
  =>
  (delete-break sketchUnit ?start)
  (retract ?b)
  ;(printout t ?start " retracting break because of no sketch unit" crlf)
)

(defrule retract-sketch-unit-break-2
  ?b <- (break sketchUnit ?start)
  (not (gap ?gs ?start))
  =>
  (delete-break sketchUnit ?start)
  (retract ?b)
  ;(printout t ?start " retracting break because of no gap" crlf)
)

(defrule retract-sketch-unit-break-3
  ?b <- (break sketchUnit ?start)
  (gap ?gs ?start)
  ;  (printout t "*** running new rule ***" crlf)
  (test (eq TRUE (first-sketch-unit-since-break ?start 
                                                (create$ (length$ (remove-break-from-list ?start sketchUnit $?*breaks*))
                                                         (remove-break-from-list ?start sketchUnit $?*breaks*)
                                                         $?*sketch-units*)
        )))
  =>
  ;  (printout t "*** running new rule ***" crlf)
  (delete-break sketchUnit ?start)
  (retract ?b)
  ;  (printout t ?start " retracting break because first sketch unit since break" crlf)
)


(deffunction after (?i ?a)
  ;; if there is any break between ?i and ?a return false
  ;; otherwise return true
  (bind ?list-length (length$ $?*breaks*))
  (bind ?n 1) 
  (while (< ?n ?list-length) do 
    (bind ?num (nth$ ?n $?*breaks*))

    (if (numberp ?num)
     then
       (if (and (> ?num ?i) (< ?num ?a))
        then 
          (return FALSE)
        ))
    (bind ?n (+ 2 ?n))
  )

  ;; didn't find any inbetween
  (return TRUE)
)
  


;;; put back in????
(defrule retract-sketch-unit-break-4
  ?b <- (break sketchUnit ?before)
  ?a <- (break ?typeB ?after)
  (test (< ?before ?after))
  (test (eq TRUE (after ?before ?after)))
  (not (speech (startTime ?s) (endTime ?e&:(or (and (< ?e ?after) (> ?e ?before)) 
                                               (and (< ?s ?after) (> ?s ?before))
       ))))
  ; need to check to make sure that the breaks do NOT overlap with a strong-group-link
  (not (strong-group-link ?s-start ?s-end&:(or (and (> ?before ?s-start) (< ?before ?s-end))
                                               (and (> ?after ?s-start) (< ?after ?s-end)))))
  (gap ?before-s ?before)
  (gap ?after-s ?after)
  =>
  (if (< (- ?before ?before-s) (- ?after ?after-s))
   then
     (delete-break sketchUnit ?before)
     (retract ?b)
       ;(printout t ?before " retracting break because it doesn't contain any speech " ?before "/" ?before-s "-" ?after "/" ?after-s crlf)
   else
     (delete-break sketchUnit ?after)
     (retract ?a)
       ;(printout t ?after " retracting break because it doesn't contain any speech " ?before  "/" ?before-s "-" ?after "/" ?after-s crlf)
  )
)


;(defrule retract-sketch-unit-break-3
;  ?b <- (break sketchUnit ?start)
;  (gap ?gs ?start)
;  (not (test (> (- ?start ?gs) ?*sig-gap*)))
;  =>
;  (delete$ $?*breaks* (member$ ?start $?*breaks*) (member$ ?start $?*breaks*))
;  (retract ?b)
;  (printout t ?start " retracting break because of no sig gap" crlf)
;)


(defrule retract-sketch-unit-break-similar-shapes
  ?b <- (break sketchUnit ?brk)
  (time-and-shape-similar ?start ?end)
  (test (eq (overlap ?brk ?brk ?start ?end) TRUE))   
  =>
  (delete-break sketchUnit ?brk)
  (retract ?b)
  ;  (printout t ?brk " retracting break because of time-and-shape-similar overlap" crlf)
) 



;;; When there is a pause and a "the" there might be a break.
(defrule the-and-pause
  (long-pause ?start ?end)
  (the-words-at ?end)
  =>
  ; break should be at end
  (insert-break thePause ?end)
  (assert (break thePause ?end))
  ; (printout t ?end " adding the-pause" crlf)
)

;;; Sometimes a the-and-pause might be erroneously inserted because of
;;; a long-pause that shouldn't be there.
(defrule remove-the-and-pause
  ?b <- (break thePause ?end)
  (not (long-pause ?start ?end))
  =>
  (delete-break thePause ?end)
  (retract ?b)
  ; (printout t ?end " retracting break because of no the pause" crlf)
)



;;; Breaks shouldn't overlap mumbled words. 
(defrule remove-link-by-mumble-break
  ?b <- (break ?type ?end)
  (linked-by-mumbles ?m-start ?m-end)
  (test (< ?m-start ?end))
  (test (> ?m-end ?end))
  =>
  (delete-break ?type ?end)
  (retract ?b)
    (printout t ?end " retracting break because of link by mumbles" crlf)
)

;;; When there is a gap followed by a key word there might be a break
;;; the gap should be of a significat length
(defrule gap-and-keyWords
  (gap ?start ?end)
  (key-words-at ?end ?end2 num ?num)
  (test (> (- ?end ?start) ?*sig-gap*))
  =>
  ; break should be between them
  (insert-break gapKeyWords ?end)
  (assert (break gapKeyWords ?end))
  ; (printout t ?end " adding key-words" crlf)
)

;;; Sometimes a gap-an-keyWords might be erroneously inserted because of
;;; a gap that shouldn't be there. 
(defrule remove-gapKeyWords-1
  ?b <- (break gapKeyWords ?end)
  (not (gap ?start ?end))
  =>
  (delete-break gapKeyWords ?end)
  (retract ?b)
  ; (printout t ?end " retracting break because of no gap for key words" crlf)
)

;;; Sometimes a gap-an-keyWords might be erroneously inserted because of
;;; a gap that is too short
(defrule remove-gapKeyWords-2
  ?b <- (break gapKeyWords ?end)
  (gap ?start ?end)
  (not   (test (> (- ?end ?start) ?*sig-gap*)))
  =>
  (delete-break gapKeyWords ?end)
  (retract ?b)
  ; (printout t ?end " retracting break because of no significant gap for key words" crlf)
)

;;; Breaks shouldn't overlap strong-group-link 
(defrule remove-strong-group-link
  ?b <- (break ?type ?end)
  (strong-group-link ?s-start ?s-end)
  (test (< ?s-start ?end))
  (test (> ?s-end ?end))
  =>
  (delete-break ?type ?end)
  (retract ?b)
   ; (printout t ?end " retracting break because of strong group link" crlf)
)