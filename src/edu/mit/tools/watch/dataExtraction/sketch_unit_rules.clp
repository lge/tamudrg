;; This file has all the rules for sketch-units


(bind $?*sketch-units* (create$))


;; This time should be the START time of the sketch unit!
(deffunction insert-sketch-unit (?time)
  ;(printout t "S-list " $?*sketch-units* crlf)
  ;(printout t "S-adding " ?time crlf)
  (bind $?*sketch-units* (insert$ $?*sketch-units* (+ 1 (length$ $?*sketch-units*)) ?time))
  ;(printout t "S-list " $?*sketch-units* crlf)

  (bind ?next (next-sketch-unit ?time $?*sketch-units*))
  (if ;(and 
    ;(<> ?next 0)
    (eq FALSE (first-sketch-unit-since-break ?next
                                            (create$ (length$ $?*breaks*)
                                                     $?*breaks* $?*sketch-units*)))
    ;)
  then
    (assert (break-if-gap ?next))
    ;(printout t ?next " adding Break-if-gap assertion2" crlf)
  )
  ;(printout t ?time " adding sketch unit at" crlf) 
)


(defrule add-break-if-gap
  ?bg <- (break-if-gap ?time)
  (gap ?st ?time)
  =>
  (retract ?bg)
  (insert-break sketchUnit ?time)
  (assert (break sketchUnit ?time))
  ;(printout t ?time " adding break-if-gap" crlf)
)

;; get rid of the bad break-if-gap assertions
(defrule remove-break-if-gap
  ?bg <- (break-if-gap ?time)
  (no-gap ?s ?e&:(and (<= ?time ?e) (>= ?time ?s)))
  =>
  (retract ?bg)
  ;(printout t ?time " removing break-if-gap" crlf)
)


;; This time should be the START time of the sketch unit!
(deffunction delete-sketch-unit (?time)
  ;(printout t "S-list " $?*sketch-units* crlf)
  ;(printout t "S-deleting " ?time crlf)
  (bind $?*sketch-units* (delete$ $?*sketch-units* (member$ ?time $?*sketch-units*) (member$ ?time $?*sketch-units*)))
  ;(printout t "S-list " $?*sketch-units* crlf)
  ;(printout t ?time " deleting sketch unit at" crlf) 
)


;;; a sketch unit is an overlap between a sketch group and a no-gap
(defrule create-sketch-unit
  (sketch-group (groupID ?g1) (startTime ?s1) (endTime ?e1) (objType ?t) (idNums $?i1))
  (no-gap ?gs ?ge)
  (test (eq (overlap ?s1 ?e1 ?gs ?ge) TRUE)) 
  =>
  (insert-sketch-unit (min ?s1 ?gs))
  (assert (sketch-unit (min ?s1 ?gs) (max ?e1 ?ge)))
)

;; remove erroneous sketch units
(defrule remove-sketch-unit-1
  ?u <- (sketch-unit ?start ?end)
  (not (no-gap ?gs ?ge&:(contains ?gs ?ge ?start ?end)))
  =>
  (delete-sketch-unit ?start)
  (retract ?u)
)

(defrule remove-sketch-unit-2
  ?u <- (sketch-unit ?start ?end)
  (not (sketch-group (groupID ?g1) 
                     (startTime ?s1) 
                     (endTime ?e1&:(contains ?s1 ?e1 ?start ?end)) 
                     (objType ?t) 
                     (idNums $?i1)))
  =>
  (delete-sketch-unit ?start)
  (retract ?u)
)