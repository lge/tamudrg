;;; defining functions

;;; a function that computes if region 1 (r1start to r1end) is 
;;; contained in region 2 (r2start to r2end). This does not 
;;; test if region 2 is contained in region 1. It checks that 
;;; r1start <= r1end and r2start <= r2end
(deffunction contains (?r1start ?r1end ?r2start ?r2end) (
  if (and (<= ?r1start ?r1end)
          (<= ?r2start ?r2end)
          (<  ?r1start ?r2end)
          (>= ?r1start ?r2start)
          (>  ?r1end   ?r2start)
          (<= ?r1end   ?r2end))
  then TRUE
  else FALSE)
)

;;; a function that computes if region 1 (r1start to r1end) has
;;; and end that is in region 2 (r2start to r2end). This does not
;;; test if region 2 has an end in region 1. It checks that 
;;; r1start <= r1end and r2start <= r2end
(deffunction simple-overlap (?r1start ?r1end ?r2start ?r2end) (
  if (and (<= ?r1end   ?r2end)
          (>  ?r1end   ?r2start)
          (<  ?r1start ?r2start)
          (<= ?r1start ?r1end)
          (<= ?r2start ?r2end))
  then TRUE
  else FALSE)
)

;;; a function that computes if region 1 (r1start to r1end) touches
;;; region 2 (r2start to r2end) - meaning that the start of one region
;;; is the end of the other. It checks that r1start <= r1end and 
;;; r2start <= r2end
(deffunction touches (?r1start ?r1end ?r2start ?r2end) (
  if (and (<= ?r1start ?r1end)
          (<= ?r2start ?r2end)
          (or (= ?r1end ?r2start)
              (= ?r2end ?r1start))) 
  then TRUE
  else FALSE)
)

;;; a function that computes if two regions overlap. This function 
;;; does not consider 2 regions that just "touch" to be overlapping.
(deffunction overlap (?r1start ?r1end ?r2start ?r2end) (
  if (or (contains ?r1start ?r1end ?r2start ?r2end)
         (contains ?r2start ?r2end ?r1start ?r1end)
         (simple-overlap ?r1start ?r1end ?r2start ?r2end)
         (simple-overlap ?r2start ?r2end ?r1start ?r1end))
  then TRUE
  else FALSE)
)

;;; a function that computes if two regions overlap including just
;;; "touching"
(deffunction anyOverlap (?r1start ?r1end ?r2start ?r2end) (
  if (or (overlap ?r1start ?r1end ?r2start ?r2end)
         (touches ?r1start ?r1end ?r2start ?r2end))
  then TRUE
  else FALSE)
)

;;; this function returns the first mutifield from the input multifield
;;; the first item in the mutifield indicates the length of the first
;; mutifield.
(deffunction get-first-multifield ($?m)
  (bind ?length-first (nth$ 1 $?m))
  ;(bind $?first (subseq$ $?m 2 (+ 1 ?length-first)))
  (return (subseq$ $?m 2 (+ 1 ?length-first)))
  ;(return $?first)
)

;;; this function returns the second mutifield from the input multifield
;;; the first item in the mutifield indicates the length of the first
;; mutifield.
(deffunction get-second-multifield ($?m)
  (bind ?length-first (nth$ 1 $?m))
  (bind $?second (subseq$ $?m (+ 2 ?length-first) (length$ $?m)))
  (return $?second)
)



;;; a function that computes if 2 multifields contain consequtive ids
(deffunction consecutive-ids ($?i) 
  (bind $?i1 (get-first-multifield $?i))
  (bind $?i2 (get-second-multifield $?i))
  ;  (printout t crlf "NEW CALL   first set: " $?i1 " second set: " $?i2 crlf)
  (foreach ?x $?i1
    ; for each id in i1 see if it matches an id one larger or one 
    ; smaller in i2. Not that not eq FALSE is not the same as TRUE
    ; because member returns an integer or FALSE
    
    ; (printout t "plus " (+ ?x 1) " minus " (- ?x 1) " second set: " $?i2 crlf)
    
    (if (or (not (eq FALSE (member$ (+ ?x 1) $?i2)))
            (not (eq FALSE (member$ (- ?x 1) $?i2))))
     then (return TRUE)))
  (return FALSE)
)


;;; a function that determines if a sketch unit is the first one since the most recent 
;;; break. If it is returns true otherwise false
(deffunction first-sketch-unit-since-break (?start-of-sketch-unit $?i)
  (bind $?breaks (get-first-multifield $?i))
  (bind $?sketch-units (get-second-multifield $?i))
  
  (bind ?closest-break 0)

  ; for each break see if it is the latest break that is still before the start-of-sketch-unit
  (bind ?list-length (length$ $?breaks))
  (bind ?n 1) 
  (while (< ?n ?list-length) do 
    (bind ?num (nth$ ?n $?breaks))
    (bind ?t (nth$ (+ ?n 1) $?breaks))
    (if (and (<= ?num ?start-of-sketch-unit)
             (> ?num ?closest-break))
     then (bind ?closest-break ?num))
    (bind ?n (+ 2 ?n))
  )
  
  ; see if there is a sketch unit between closest-break and start-of-sketch-unit
  ; if there is return false
  (foreach ?y $?sketch-units 
    (if (and (< ?y ?start-of-sketch-unit)
             (>= ?y ?closest-break))
     then 
       ;  (printout t  ?start-of-sketch-unit " first-sketch-unit false " ?y crlf)
       (return FALSE)
     )
    )

  ;  (printout t  ?start-of-sketch-unit " first-sketch-unit true closest-break: " ?closest-break "breaks " $?breaks crlf)
  ; otherwise no sketch-units since the last break
  (return TRUE)
)


;; returns the next sketch-unit after current or 0 if there isn't one
(deffunction next-sketch-unit (?current $?sketch-units)
  (bind ?next 0)
  (foreach ?x $?sketch-units
    ; for each sketch unit see if is is after the current sketch-unit
    ; but before the current next sketch unit
    (if (> ?x ?current)
    then 
      (if (or (= ?next 0) (< ?x ?next))
      then (bind ?next ?x))
    )
  )

  (return ?next)
)
