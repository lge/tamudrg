;;; We need to have a counter that keeps track of group IDs to uniquely 
;;; identify the groups
(bind ?*group_id* 1) ;initialize the group id counter


;;; This rule creates a group for a sketched item that does not already
;;; have a group 
(defrule create-sketch-group
  (sketch (startTime ?s) (endTime ?e) (objType ?t) (objID ?o) (idNum ?i))
  ; this first line finds a sketch event
  (not (sketch-group (objType ?t) (objID ?o)))
  ; this line tests to make sure there is not an existing group that
  ; this event should belong in.
  =>
  ; the following line creates the new group
  (assert (sketch-group (groupID ?*group_id*) (startTime ?s) (endTime ?e) (objType ?t) (objID ?o) (idNums (create$ ?i))))
  ;the following line increments the groupID counter
  (bind ?*group_id* (+ ?*group_id* 1))
)


;;; This rule adds additional parts of a sketched item to its group
(defrule add-to-sketch-group
  ; the following line finds a sketch event
  (sketch (startTime ?s) (endTime ?e) (objType ?t) (objID ?o) (idNum ?i))
  ; this line finds a group that matches the sketch event
  ; note that a match has the same objType and objID
  ?g <- (sketch-group (startTime ?g-s) (endTime ?g-e) (objType ?t) (objID ?o) (idNums $?n))
  ; the next line tests to see if the sketch event is already a member
  ; of this group so that it can't be added twice
  (test (eq FALSE (member$ ?i $?n)))
  =>
  ; This line modifies the existing group to update the start and end
  ; times as well as add the new idNum of the new sketch event to the
  ; idNums multislot
  (modify ?g (startTime (min ?s ?g-s)) (endTime (max ?e ?g-e)) (idNums (union$ $?n (create$ ?i))))
)

;; rule for linking similar shapes drawn in sequence
(defrule time-and-shape-similar-shapes
  ; Find two sketch-group of the same type
  (sketch-group (groupID ?g1) (startTime ?s1) (endTime ?e1) (objType ?t) (idNums $?i1))
  (sketch-group (groupID ?g2) (startTime ?s2) (endTime ?e2) (objType ?t) (idNums $?i2)) 

  ; check to make sure that the two sketch groups are not the same group
  ; by using less than, we also ensure that this rule will only run once 
  ; not twice
  (test (< ?g1 ?g2))

  ; see if the two sketch-groups contain consecutive ids 
  ; if they do this means that the sketches occured near each other
  (test (eq TRUE (consecutive-ids (create$ (length$ $?i1) $?i1 $?i2)))) 

  =>
  ;(printout t "type " ?t " first set: " $?i1 " second set: " $?i2 crlf)
  (assert (time-and-shape-similar (min ?s1 ?s2) (max ?e1 ?e2)))
)

;; rule for taking out out-of-date time-and-shape-similar shapes
;; based on start times
(defrule remove-time-and-shape-similar-shapes-1
  ; find the time-and-shape assertion
  ?tss <- (time-and-shape-similar ?start ?end)
  ; if there is not a sketch-group that has the same start time
  (not (sketch-group (startTime ?start)))
  =>
  ;; then retract the assertion
  (retract ?tss)
)

;; rule for taking out out-of-date time-and-shape-similar shapes
;; based on end times
(defrule remove-time-and-shape-similar-shapes-2
  ; find the time-and-shape assertion
  ?tss <- (time-and-shape-similar ?start ?end)
  ; if there is not a sketch-group that has the same end time
  (not (sketch-group (endTime ?end)))
  =>
  ;; then retract the assertion
  (retract ?tss)
)


;;; we need to set up a tolerance on the overlap of 
;;; sketching groups and speech. This is a leeway
;;; such that they can overlap a little bit and still
;;; be considered a strong-group-link
(bind ?*strong-group-link-tolerance* 5) 

(defrule strong-group-link
  ; this rule adds an assertion if the time-and-shape-similar-shapes
  ; occur with no overlapping speech (within the 
  ;?*strong-group-tolerance* limit)
  (time-and-shape-similar ?start ?end)

  ; for a given time-and-shape-similar assertion there should not be
  ; a speech event that overlaps with it
  (not (speech (startTime ?s) 
               (endTime ?e&:(overlap ?start 
                                     ?end 
                                     (+ ?s ?*strong-group-link-tolerance*) 
                                     (- ?e ?*strong-group-link-tolerance*) ))
  ))
  =>
  (assert (strong-group-link ?start ?end))
)
