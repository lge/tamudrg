(printout t "----------------------------------" crlf)

;;; Test code for contains function
(printout t (contains 0 1 0 7) " should be TRUE" crlf) ;;; should be TRUE
(printout t (contains 0 7 0 7) " should be TRUE" crlf) ;;; should be TRUE
(printout t (contains 1 3 0 7) " should be TRUE" crlf) ;;; should be TRUE
(printout t (contains 5 7 0 7) " should be TRUE" crlf) ;;; should be TRUE
(printout t (contains 5 8 0 7) " should be FALSE" crlf) ;;; should be FALSE
(printout t (contains 0 3 2 7) " should be FALSE" crlf) ;;; should be FALSE
(printout t (contains 0 3 3 7) " should be FALSE" crlf) ;;; should be FALSE
(printout t (contains 5 6 3 5) " should be FALSE" crlf) ;;; should be FALSE
(printout t (contains 7 7 0 7) " should be FALSE" crlf) ;;; should be FALSE
(printout t (contains 0 7 0 1) " should be FALSE" crlf) ;;; should be FALSE
(printout t (contains 0 0 0 7) " should be FALSE" crlf) ;;; should be FALSE


(printout t "----------------------------------" crlf)

;;; Test code for overlap function

(printout t (overlap 1 5 1 7) " should be TRUE" crlf) ;;; should be TRUE
(printout t (overlap 1 5 1 5) " should be TRUE" crlf) ;;; should be TRUE
(printout t (overlap 1 5 2 3) " should be TRUE" crlf) ;;; should be TRUE
(printout t (overlap 2 3 1 5) " should be TRUE" crlf) ;;; should be TRUE
    
(printout t (overlap 2 3 3 5) " should be FALSE" crlf) ;;; should be FALSE
(printout t (overlap 3 5 1 3) " should be FALSE" crlf) ;;; should be FALSE
    
(printout t (overlap 1 10 5 15) " should be TRUE" crlf) ;;; should be TRUE
(printout t (overlap 5 15 1 10) " should be TRUE" crlf) ;;; should be TRUE
(printout t (overlap 1 3 5 7) " should be FALSE" crlf)  ;;; should be FALSE
(printout t (overlap 5 7 1 3) " should be FALSE" crlf)  ;;; should be FALSE

(printout t (overlap 0 0 0 3) " should be FALSE" crlf)  ;;; should be FALSE

(printout t "----------------------------------" crlf)


;;; Test code for anyOverlap function

(printout t (anyOverlap 1 5 1 7) " should be TRUE" crlf) ;;; should be TRUE
(printout t (anyOverlap 1 5 1 5) " should be TRUE" crlf) ;;; should be TRUE
(printout t (anyOverlap 1 5 2 3) " should be TRUE" crlf) ;;; should be TRUE
(printout t (anyOverlap 2 3 1 5) " should be TRUE" crlf) ;;; should be TRUE
    
(printout t (anyOverlap 2 3 3 5) " should be TRUE" crlf) ;;; should be TRUE
(printout t (anyOverlap 3 5 1 3) " should be TRUE" crlf) ;;; should be TRUE
    
(printout t (anyOverlap 1 10 5 15) " should be TRUE" crlf) ;;; should be TRUE
(printout t (anyOverlap 5 15 1 10) " should be TRUE" crlf) ;;; should be TRUE
(printout t (anyOverlap 1 3 5 7) " should be FALSE" crlf)  ;;; should be FALSE
(printout t (anyOverlap 5 7 1 3) " should be FALSE" crlf)  ;;; should be FALSE

(printout t "----------------------------------" crlf)