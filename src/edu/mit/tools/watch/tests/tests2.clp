;;; Test code for contains-string
;(printout t ">>----->>>>>> " (contains-string "And then my mom said" "And") crlf)
;(printout t ">>----->>>>>> " (contains-string "And then my mom said" "and") crlf)
;(printout t ">>----->>>>>> " (contains-string "And then my mom said" "dad") crlf)
  
;;; Test code for num-contains-strings
;(printout t ">>>----->>>>>> " (num-contains-strings "And then my mom said" "and" "then") crlf)
;(printout t ">>>----->>>>>> " (num-contains-strings "And then then and my mom said" "and" "then") crlf)
;(printout t ">>>----->>>>>> " (num-contains-strings "And then my mom said" "and" "then" "my") crlf)
;(printout t ">>>----->>>>>> " (num-contains-strings "And then my mom said" "while" "when") crlf)
;(printout t ">>>----->>>>>> " (num-contains-strings "And then my mom said") crlf)


;;; Test code for num-words-overlap
(printout t ">>>----->>>>>> " (num-words-overlap "And then then and my mom mom said" "then my mom mom foo Bar MOM said") crlf) ; should be 6
(printout t ">>>----->>>>>> " (num-words-overlap "foo bar" "baz bing bomb ding dong fo ba") crlf)