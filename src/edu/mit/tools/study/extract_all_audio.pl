#This perl script takes the root directory of our study tree,
#iterates through each relevant subdirectory, and extracts
#the audio using transcode
#expect it to take a long time to run, as each video takes a few minutes

@all_vids = <@ARGV[0]/*.mpeg>;
$num_vids = @all_vids;
$counter = 0;

print "Total vids: $num_vids\n";

#@cdirs = <@ARGV[0]/*>;
@vids = <@ARGV[0]/*.mpeg>;
#foreach $participant (@dirs){
#    @cdirs = <$participant/*>;
#    foreach $condition (@cdirs){
#	if (-d $condition){
#	    @vids = <$condition/*.mpeg>;
	    foreach $video (@vids){
		$counter++;
		$_ = $video;
		s/mpeg/wav/;
		s/video/audio/;
		$new_fname = $_;
		$transcode_str = "transcode".
		    " -x null".       #no video codec
		    " -a 0".          #only look at audio
		    " -i $video".     #specifies input file
		    " -y wav".        #output wav.  mp3 doesn't seem to work
		    " -J resample".   #not really sure what this is for
		    " -E 44100".      #sample rate
#		    " -q 0".          #be quiet
		    " -o $new_fname"; #output filename
		print $transcode_str."\n";
		$pid = system ("$transcode_str");# >> extract_audio.log 2>&1");
		wait;
		print "Done waiting -- next!\n";
	    }
#}
#    }
#}
