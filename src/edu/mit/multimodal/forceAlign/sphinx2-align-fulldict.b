#!/bin/sh
# example script to call sphinx2-batch to do forced alignment

# the binary to run
S2BATCH=sphinx2-batch

# the HMM directory 
HMM=/usr/local/share/sphinx2/model/hmm/6k

# the 'task'; the turtle demo by default.
#TASK=~/sphinx2-0.5/scripts/turtle
TASK=$1

# the control file, which lists the utterance names, one per line
CTLFILE=${TASK}/control.ctl

# the transcripts, one per line, to be used for the alignment
TACTLFN=${TASK}/transcript.cor

# the dictionary
DICT=~/TRIMODAL/corpus/speech/cmudict.0.6

# the phonemes
PHONE=~/TRIMODAL/corpus/speech/phoneset.0.6

$S2BATCH -adcin TRUE -adcext wav -ctlfn ${CTLFILE} -tactlfn ${TACTLFN} -taphone FALSE -datadir ${TASK} -agcmax TRUE -langwt 6.5 -fwdflatlw 8.5 -rescorelw 9.5 -ugwt .5 -fillpen 1e-8 -silpen 0.005 -inspen 0.65 -top 1 -topsenfrm 5 -topsenthresh -70000 -beam 2e-06 -npbeam 2e-06 -lpbeam 2e-05 -lponlybeam 0.0005 -nwbeam 0.0005 -fwdflat TRUE -fwdflatbeam 1e-08 -fwdflatnwbeam 0.0003 -bestpath TRUE -kbdumpdir ${TASK} -dictfn ${DICT} -ndictfn ${HMM}/noisedict -phnfn ${HMM}/phone -mapfn ${HMM}/map -hmmdir ${HMM} -hmmdirlist ${HMM} -8bsen TRUE -sendumpfn ${HMM}/sendump -cbdir ${HMM} -latsize 500000 -backtrace FALSE -logfn $2